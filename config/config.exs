# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns - Main Configuration File
# ===============================================================================
#
# This file is the primary configuration entry point for the FlyingDdns application.
# It defines base settings that apply across all environments (dev, test, prod).
# Environment-specific configurations will override these settings.
#
# This configuration file is loaded before any dependency and is restricted
# to this project.
#
# Key configurations defined here:
# - Database repository settings
# - Web endpoint configuration
# - Email/mailer settings
# - Asset compilation (esbuild, tailwind)
# - Logging format
# - JSON parsing library
# - External API adapters
# - Git commit tracking
# ===============================================================================

# General application configuration
import Config

# Configure the Ecto repositories for database access
config :flying_ddns,
  ecto_repos: [FlyingDdns.Repo]

# Configures the Phoenix web endpoint
# This defines how the web server will listen for connections and handle errors
config :flying_ddns, FlyingDdnsWeb.Endpoint,
  url: [ip: {0, 0, 0, 0, 0, 0, 0, 1}, port: 4000],
  render_errors: [
    formats: [html: FlyingDdnsWeb.ErrorHTML, json: FlyingDdnsWeb.ErrorJSON],
    layout: false
  ],
  pubsub_server: FlyingDdns.PubSub,
  live_view: [signing_salt: "i4JWdWVi"],
  server: true

# Configures the mailer for sending emails
#
# By default it uses the "Local" adapter which stores the emails
# locally. You can see the emails in your browser, at "/dev/mailbox".
#
# For production it's recommended to configure a different adapter
# at the `config/runtime.exs`.
config :flying_ddns, FlyingDdns.Mailer, adapter: Swoosh.Adapters.Local

# Swoosh API client configuration
# Required for email adapters other than SMTP
config :swoosh, :api_client, false

# Configure esbuild for JavaScript asset compilation
# The version is required to ensure consistent builds
config :esbuild,
  version: "0.14.41",
  default: [
    args:
      ~w(js/app.js --bundle --target=es2017 --outdir=../priv/static/assets --external:/fonts/* --external:/images/*),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

# Configure tailwind for CSS processing
# The version is required to ensure consistent styling
config :tailwind,
  version: "3.2.4",
  default: [
    args: ~w(
      --config=tailwind.config.js
      --input=css/app.css
      --output=../priv/static/assets/app.css
    ),
    cd: Path.expand("../assets", __DIR__)
  ]

# Configures Elixir's Logger for application logging
config :logger, :console,
  # Uncomment to include additional metadata in logs
  # , metadata: [:request_id, :user_id]
  format: "$date $time $metadata[$level] $message\n"

# Use Jason for JSON parsing in Phoenix
# This provides efficient JSON encoding/decoding
config :phoenix, :json_library, Jason

# Configure Tesla HTTP client for PowerDNS API communication
# Hackney is used as the underlying HTTP client
config :tesla, adapter: Tesla.Adapter.Hackney

# Git commit tracking for deployment identification
# These values are replaced during CI/CD pipeline execution
# Default values are used for local development
config :flying_ddns, commit_sha: "0000000000000000000000000000000000000000"
config :flying_ddns, commit_short_sha: "00000000"

# Configure Ecto to use second precision for timestamps
# This ensures consistent timestamp formatting across the application
config :flying_ddns, FlyingDdns.Repo, migration_timestamps: [type: :naive_datetime, precision: 0]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
