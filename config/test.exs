# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns - Test Environment Configuration
# ===============================================================================
#
# This file contains configuration settings specific to the test environment.
# It overrides the base settings from config.exs with values optimized for
# automated testing, including performance optimizations and isolation.
#
# Key configurations defined here:
# - Test database connection settings with sandboxing
# - Simplified password hashing for faster tests
# - Test-specific endpoint configuration
# - Email testing setup
# - Reduced logging during tests
# ===============================================================================

import Config

# Simplify password hashing in tests for better performance
# This reduces the computational complexity of bcrypt in the test environment
config :bcrypt_elixir, :log_rounds, 1

# Configure the test database
# The MIX_TEST_PARTITION environment variable enables parallel testing
# by providing database isolation between test processes
config :flying_ddns, FlyingDdns.Repo,
  username: "postgres",
  password: "postgres",
  hostname: "::1",
  database: "flying_ddns_test#{System.get_env("MIX_TEST_PARTITION")}",
  socket_options: [:inet6],
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

# Configure the test endpoint
# We don't run a server during automated tests by default
# The server can be enabled if needed for integration tests
config :flying_ddns, FlyingDdnsWeb.Endpoint,
  http: [ip: {0, 0, 0, 0, 0, 0, 0, 1}, port: 4002],
  secret_key_base: "oNXbxMphum6UH9a9G32HyEsgcvutgy/Xp3vGxODE6dJ83RvvSCQ0OTa5JYMiMLD3",
  server: false

# Configure email testing
# In test environment, emails are captured rather than sent
config :flying_ddns, FlyingDdns.Mailer, adapter: Swoosh.Adapters.Test

# Disable the Swoosh API client in test environment
# This avoids external API calls during testing
config :swoosh, :api_client, false

# Reduce logging noise during tests
# Only warnings and errors are logged to keep test output clean
config :logger, level: :warning

# Initialize plugs at runtime for faster test compilation
# This speeds up the test cycle by avoiding recompilation of plugs
config :phoenix, :plug_init_mode, :runtime

# Finally import the config/runtime.exs which loads secrets
# and configuration from environment variables.
import_config "runtime.exs"
