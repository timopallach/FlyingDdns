# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Packer Build Configuration
# ===============================================================================
#
# This file defines the build process for creating FlyingDdns virtual machine
# images across multiple platforms, architectures, and virtualization technologies.
# It specifies which source definitions (from sources.pkr.hcl) to use and what
# provisioning steps to execute on each build.
#
# The build configuration:
# 1. References source definitions for each OS/architecture/virtualization combination
# 2. Defines provisioning steps that prepare the environment and install FlyingDdns
# 3. Configures post-processors for image optimization and cleanup
#
# This file works in conjunction with:
# - variables.pkr.hcl: Defines variables used across the build process
# - sources.pkr.hcl: Defines the source machine configurations
# - plugins/*.pkr.hcl: Specifies required Packer plugins for each virtualization platform
#
# The build process creates consistent FlyingDdns environments across diverse
# platforms while accommodating platform-specific requirements.
# ===============================================================================

################################################################################
# The definition of the build
################################################################################
build {
  sources = [
    # Alpine
    "sources.qemu.alpine-amd64",
    "sources.virtualbox-iso.alpine-amd64",
    "sources.qemu.alpine-arm64",
    "sources.virtualbox-iso.alpine-arm64",
    "sources.vmware-iso.alpine-arm64",
    "sources.vsphere-iso.alpine-arm64",
    # Debian
    "sources.qemu.debian-amd64",
    "sources.virtualbox-iso.debian-amd64",
    #"sources.qemu.debian-arm64",
    "sources.vmware-iso.debian-arm64",
    "sources.vsphere-iso.debian-arm64",
    # Fedora
    "sources.qemu.fedora-amd64",
    "sources.virtualbox-iso.fedora-amd64",
    #"sources.qemu.fedora-arm64",
    "sources.vmware-iso.fedora-arm64",
    "sources.vsphere-iso.fedora-arm64",
    # FreeBSD
    "sources.qemu.freebsd-amd64",
    "sources.virtualbox-iso.freebsd-amd64",
    "sources.qemu.freebsd-arm64",
    "sources.virtualbox-iso.freebsd-arm64",
    "sources.vmware-iso.freebsd-arm64",
    "sources.vsphere-iso.freebsd-arm64",
    # macOS
    # The condition is required because the packer tart plugin does only exist on ARM64 macOS.
    var.os-name == "macos" ? "source.tart-cli.macos-arm64" : "source.null.null",
    # NetBSD
    "sources.qemu.netbsd-amd64",
    "sources.virtualbox-iso.netbsd-amd64",
    "sources.qemu.netbsd-arm64",
    "sources.virtualbox-iso.netbsd-arm64",
    "sources.vmware-iso.netbsd-arm64",
    "sources.vsphere-iso.netbsd-arm64",
    # NixOS
    "sources.qemu.nixos-amd64",
    "sources.virtualbox-iso.nixos-amd64",
    "sources.qemu.nixos-arm64",
    "sources.virtualbox-iso.nixos-arm64",
    "sources.vmware-iso.nixos-arm64",
    "sources.vsphere-iso.nixos-arm64",
    # OpenBSD
    "sources.hcloud.openbsd-arm64",
    "sources.qemu.openbsd-amd64",
    "sources.virtualbox-iso.openbsd-amd64",
    "sources.qemu.openbsd-arm64",
    "sources.virtualbox-iso.openbsd-arm64",
    "sources.vmware-iso.openbsd-arm64",
    #"sources.vsphere-iso.openbsd-arm64",
    # OpenIndiana
    "sources.virtualbox-iso.openindiana-amd64",
    # openSUSE
    "sources.qemu.opensuse-amd64",
    "sources.virtualbox-iso.opensuse-amd64",
    "sources.qemu.opensuse-arm64",
    "sources.vmware-iso.opensuse-arm64",
    "sources.vsphere-iso.opensuse-arm64",
    # Rocky Linux
    "sources.qemu.rocky-amd64",
    "sources.virtualbox-iso.rocky-amd64",
    #"sources.qemu.rocky-arm64",
    "sources.vmware-iso.rocky-arm64",
    "sources.vsphere-iso.rocky-arm64",
    # Ubuntu
    "sources.qemu.ubuntu-amd64",
    "sources.virtualbox-iso.ubuntu-amd64",
    #"sources.qemu.ubuntu-arm64",
    "sources.vmware-iso.ubuntu-arm64",
    "sources.vsphere-iso.ubuntu-arm64",
  ]

  # Install and configure vnstat and update the system
  provisioner "shell" {
    expect_disconnect = true
    environment_vars = concat(
      [
        "use_openbsd_snapshot=${var.openbsd-use-snapshot}",
      ],
    )
    scripts = [
      "bin/setup_vnstat.sh",
      "bin/update_system.sh",
    ]
  }

  # Run the complete system setup and finally the robot framework tests
  provisioner "shell" {
    pause_before      = "60s"
    expect_disconnect = true
    environment_vars = concat(
      "${var.robotframework-version}",
      "${var.robotframework-gherkin-parser-version}",
      "${var.flyingddns-env-vars}",
      [
        "use_openbsd_snapshot=${var.openbsd-use-snapshot}",
        "arch_name=${var.arch-name}",
        "os_name=${var.os-name}",
        "os_version=${var.os-version}",
        "virt_name=${var.virt-name}",
        "branch_name=${var.branch-name}",
        "project_url=${var.project-url}",
        "hex_version=${var.hex-version}",
        "asdf_version=${var.asdf-version}",
        "flyingddns_version=${var.flyingddns-version}",
        "flyingddns_commit_sha=${var.flyingddns-commit-sha}",
        "flyingddns_commit_short_sha=${var.flyingddns-commit-short-sha}",
        "flyingddns_robotframework_version=${var.flyingddns-robotframework-version}",
        "FDDNS_PROTOCOL=http",
        "FDDNS_HOSTNAME=[::1]",
        "FDDNS_PORT=4000",
        "FDDNS_IP_VERSION=ipv6",
        "FDDNS_VERSION=${var.flyingddns-robotframework-version}",
        "FDDNS_COMMIT_SHA=${var.flyingddns-commit-sha}",
        "FDDNS_COMMIT_SHORT_SHA=${var.flyingddns-commit-short-sha}",
      ],
    )
    scripts = [
      "bin/install_packages.sh",
      "bin/configure_postgresql.sh",
      "bin/configure_powerdns.sh",
      "bin/configure_and_build_flyingddns.sh",
      "bin/start_flyingddns.sh",
      "bin/run_pdnsutil_tests.sh",
      "bin/run_pdns_api_tests.sh",
      "bin/run_fddns_api_tests.sh",
    ]
  }

  # Grab the FlyingDdns release tar.gz file from within the vm
  provisioner "file" {
    direction   = "download"
    source      = "${local.build-user-home-dir}/flyingddns-${var.flyingddns-version}_${var.os-name}-${var.os-version}-${var.arch-name}.tar.gz"
    destination = "../"
  }

  # After finishing the setup we copy the packer and test logs from the vm to this host
  provisioner "file" {
    direction   = "download"
    source      = "${local.build-user-home-dir}/FlyingDdns/log/*"
    destination = "../log/"
  }

  # Show vnstat info
  provisioner "shell" {
    scripts = [
      "bin/get_vnstat_info.sh",
    ]
  }
}
