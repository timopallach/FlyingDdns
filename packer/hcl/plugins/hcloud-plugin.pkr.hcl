# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Packer Hetzner Cloud Plugin Configuration
# ===============================================================================
#
# This file configures the Hetzner Cloud (hcloud) builder plugin for Packer,
# which is used to create server images in the Hetzner Cloud infrastructure.
#
# Hetzner Cloud is a European cloud provider offering competitive pricing and
# high-performance infrastructure. In the FlyingDdns build process, Hetzner
# Cloud integration is particularly valuable for:
#
# 1. Creating cloud-native deployments in European data centers
# 2. Leveraging Hetzner's cost-effective ARM64 instances
# 3. Supporting GDPR-compliant hosting options
#
# The plugin is pinned to a specific version to ensure build reproducibility
# across different environments and over time.
#
# This plugin configuration is referenced by the main Packer build process
# defined in the parent directory.
# ===============================================================================

packer {
  required_plugins {
    hcloud = {
      version = "= 1.6.0"
      source  = "github.com/hetznercloud/hcloud"
    }
  }
}
