# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Packer VMware Plugin Configuration
# ===============================================================================
#
# This file configures the VMware builder plugin for Packer, which is used to
# create virtual machine images using VMware virtualization products.
#
# VMware provides enterprise-grade virtualization solutions that are widely used
# in production environments. In the FlyingDdns build process, VMware is
# particularly valuable for:
#
# 1. Creating images compatible with VMware ESXi and vSphere environments
# 2. Supporting enterprise deployment scenarios
# 3. Building images with advanced networking and storage capabilities
#
# The plugin is pinned to a specific version to ensure build reproducibility
# across different environments and over time.
#
# This plugin configuration is referenced by the main Packer build process
# defined in the parent directory.
# ===============================================================================

packer {
  required_plugins {
    vmware = {
      version = "= 1.1.0"
      source  = "github.com/hashicorp/vmware"
    }
  }
}
