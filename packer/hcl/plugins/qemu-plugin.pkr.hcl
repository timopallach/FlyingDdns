# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Packer QEMU Plugin Configuration
# ===============================================================================
#
# This file configures the QEMU builder plugin for Packer, which is used to create
# virtual machine images using the QEMU/KVM virtualization platform.
#
# QEMU is an open-source machine emulator and virtualizer that supports a wide
# range of hardware architectures, making it ideal for cross-platform development.
# In the FlyingDdns build process, QEMU is particularly valuable for:
#
# 1. Building images for both amd64 and arm64 architectures
# 2. Creating images compatible with KVM-based cloud environments
# 3. Supporting development on systems without hardware virtualization
#
# The plugin is pinned to a specific version to ensure build reproducibility
# across different environments and over time.
#
# This plugin configuration is referenced by the main Packer build process
# defined in the parent directory.
# ===============================================================================

packer {
  required_plugins {
    qemu = {
      version = "= 1.1.1"
      source  = "github.com/hashicorp/qemu"
    }
  }
}
