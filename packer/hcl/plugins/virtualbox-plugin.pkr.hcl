# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Packer VirtualBox Plugin Configuration
# ===============================================================================
#
# This file configures the VirtualBox builder plugin for Packer, which is used to
# create virtual machine images using Oracle VirtualBox virtualization platform.
#
# VirtualBox is a free, cross-platform virtualization application that supports
# a wide range of guest operating systems. In the FlyingDdns build process,
# VirtualBox is particularly valuable for:
#
# 1. Creating images for desktop development environments
# 2. Supporting both Windows and macOS host development systems
# 3. Providing a consistent testing environment across platforms
#
# The plugin is pinned to a specific version to ensure build reproducibility
# across different environments and over time.
#
# This plugin configuration is referenced by the main Packer build process
# defined in the parent directory.
# ===============================================================================

packer {
  required_plugins {
    virtualbox = {
      version = "= 1.1.1"
      source  = "github.com/hashicorp/virtualbox"
    }
  }
}
