# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Packer vSphere Plugin Configuration
# ===============================================================================
#
# This file configures the vSphere builder plugin for Packer, which is used to
# create virtual machine images directly in VMware vSphere environments.
#
# VMware vSphere is an enterprise-class virtualization platform that provides
# a complete infrastructure for virtual datacenters. In the FlyingDdns build
# process, vSphere integration is particularly valuable for:
#
# 1. Direct deployment to production vSphere environments
# 2. Leveraging enterprise-grade features like vMotion and DRS
# 3. Integration with existing VMware infrastructure and management tools
#
# The plugin is pinned to a specific version to ensure build reproducibility
# across different environments and over time.
#
# This plugin configuration is referenced by the main Packer build process
# defined in the parent directory.
# ===============================================================================

packer {
  required_plugins {
    vsphere = {
      version = "= 1.4.2"
      source  = "github.com/hashicorp/vsphere"
    }
  }
}
