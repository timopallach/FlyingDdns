# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Packer Tart Plugin Configuration
# ===============================================================================
#
# This file configures the Tart builder plugin for Packer, which is used to
# create macOS virtual machine images using the Tart virtualization tool.
#
# Tart is a virtualization toolset for macOS that allows running macOS virtual
# machines on Apple Silicon hardware. In the FlyingDdns build process, Tart
# integration is particularly valuable for:
#
# 1. Building native macOS ARM64 images for Apple Silicon Macs
# 2. Providing a lightweight alternative to other macOS virtualization options
# 3. Supporting development and testing on macOS platforms
#
# The plugin is pinned to a specific version to ensure build reproducibility
# across different environments and over time.
#
# This plugin configuration is referenced by the main Packer build process
# defined in the parent directory.
# ===============================================================================

packer {
  required_plugins {
    tart = {
      version = "= 1.14.0"
      source  = "github.com/cirruslabs/tart"
    }
  }
}
