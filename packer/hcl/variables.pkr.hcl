# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Packer Variables Configuration
# ===============================================================================
#
# This file defines all variables used throughout the Packer build process for
# creating FlyingDdns virtual machine images. It centralizes configuration
# parameters to maintain consistency across different build environments.
#
# The variables are organized into several categories:
# 1. Global variables: Apply to all builds (e.g., VM resources, credentials)
# 2. Build environment variables: Control the build process
# 3. OS-specific variables: Configure parameters for each operating system
# 4. Platform-specific variables: Settings for each virtualization technology
#
# The file also contains "locals" blocks that compute derived values based on
# the defined variables, such as ISO URLs and checksums for different platforms.
#
# This file works in conjunction with:
# - build.pkr.hcl: Uses these variables in the build definition
# - sources.pkr.hcl: References these variables to configure source machines
# - plugins/*.pkr.hcl: May use version variables for plugin requirements
#
# By centralizing variables here, the build process can be easily customized
# without modifying the core build logic in other files.
# ===============================================================================

################################################################################
# Packer global definitions
################################################################################
packer {
  required_version = ">= 1.10.0"
}

################################################################################
# Variables used by ALL packer build environments
################################################################################
variable "http-directory" {
  type    = string
  default = "config"
}
variable "asdf-version" {
  type    = string
  default = "0.16.5"
}
variable "robotframework-version" {
  type = list(string)
  default = [
    "robotframework_version=7.2.2",
  ]
}
variable "robotframework-gherkin-parser-version" {
  type = list(string)
  default = [
    "robotframework_gherkin_parser_version=0.3.2",
  ]
}
variable "hex-version" {
  type    = string
  default = "2.1.1"
}
variable "vm-host-name" {
  type    = string
  default = "flyingddns"
}
variable "cpu-count" {
  type    = string
  default = "2"
}
variable "memory-micro" {
  type    = string
  default = "512"
}
variable "memory-mini" {
  type    = string
  default = "1024"
}
variable "memory-medium" {
  type    = string
  default = "2048"
}
variable "memory-plenty" {
  type    = string
  default = "4096"
}
variable "memory-gb" {
  type    = string
  default = "4"
}
variable "disk-size" {
  type    = string
  default = "65535"
}
variable "disk-size-gb" {
  type    = string
  default = "64"
}
variable "root-password" {
  type    = string
  default = "root"
}
variable "flyingddns-env-vars" {
  type = list(string)
  default = [
    "MIX_ENV=prod",
    "DATABASE_URL=ecto://postgres@[::1]/flying_ddns_prod",
    "SECRET_KEY_BASE=12345678901234567890123456789012345678901234567890123456789012345",
    "PDNS_DB_NAME=pdns_db_name",
    "PDNS_DB_USER=pdns_db_user",
    "PDNS_DB_PASSWD=pdns_db_passwd",
    "PDNS_API_PASSWD=pdns_api_passwd",
    "DOMAIN_NAME=example.org",
    "ECTO_IPV6=true",
  ]
}
variable "arch-name" {
  type    = string
  default = ""
}
variable "os-name" {
  type    = string
  default = ""
}
variable "os-version" {
  type    = string
  default = ""
}
variable "virt-name" {
  type    = string
  default = ""
}
variable "branch-name" {
  type    = string
  default = ""
}
variable "project-url" {
  type    = string
  default = "https://gitlab.com/timopallach/FlyingDdns"
}
variable "flyingddns-version" {
  type    = string
  default = ""
}
variable "flyingddns-commit-sha" {
  type    = string
  default = ""
}
variable "flyingddns-commit-short-sha" {
  type    = string
  default = ""
}
variable "flyingddns-robotframework-version" {
  type    = string
  default = ""
}
variable "esxi-host" {
  type    = string
  default = "dummy"
}
variable "esxi-username" {
  type    = string
  default = "dummy"
}
variable "esxi-password" {
  type    = string
  default = "dummy"
}
variable "qemu-arm64-cpu" {
  type    = string
  default = "host"
}
variable "qemu-arm64-accelerator" {
  type    = string
  default = "hvf"
}
variable "qemu-arm64-firmware-path" {
  type    = string
  default = "/opt/homebrew/share/qemu/edk2-aarch64-code.fd"
}
variable "tart-username-password" {
  type    = string
  default = "admin"
}
variable "hcloud_token" {
  type      = string
  sensitive = true
  default   = "${env("HCLOUD_TOKEN")}"
}
locals {
  build-user-home-dir = var.os-name == "macos" ? "/Users/admin" : "/root"
}

################################################################################
# Variables used by Alpine packer build environments
################################################################################
variable "alpine-version" {
  type    = string
  default = "3.21.3"
}
variable "alpine-qemu-disk" {
  type    = string
  default = "vda"
}
variable "alpine-virtualbox-disk" {
  type    = string
  default = "sda"
}
variable "alpine-vmware-disk" {
  type    = string
  default = "nvme0n1"
}
variable "alpine-vsphere-disk" {
  type    = string
  default = "nvme0n1"
}
locals {
  alpine-amd64-iso-url      = format("https://dl-cdn.alpinelinux.org/alpine/v%s/releases/x86_64/alpine-standard-%s-x86_64.iso", regex_replace("${var.alpine-version}", ".\\d$", ""), "${var.alpine-version}")
  alpine-amd64-iso-checksum = format("file:https://dl-cdn.alpinelinux.org/alpine/v%s/releases/x86_64/alpine-standard-%s-x86_64.iso.sha256", regex_replace("${var.alpine-version}", ".\\d$", ""), "${var.alpine-version}")
  alpine-arm64-iso-url      = format("https://dl-cdn.alpinelinux.org/alpine/v%s/releases/aarch64/alpine-standard-%s-aarch64.iso", regex_replace("${var.alpine-version}", ".\\d$", ""), "${var.alpine-version}")
  alpine-arm64-iso-checksum = format("file:https://dl-cdn.alpinelinux.org/alpine/v%s/releases/aarch64/alpine-standard-%s-aarch64.iso.sha256", regex_replace("${var.alpine-version}", ".\\d$", ""), "${var.alpine-version}")
}

################################################################################
# Variables used by Debian packer build environments
################################################################################
variable "debian-name" {
  type    = string
  default = "bookworm"
}
variable "debian-version" {
  type    = string
  default = "12.9.0"
}
variable "debian-username-password" {
  type    = string
  default = "user"
}
locals {
  debian-amd64-iso-url      = format("https://cdimage.debian.org/cdimage/release/%s/amd64/iso-dvd/debian-%s-amd64-DVD-1.iso", "${var.debian-version}", "${var.debian-version}")
  debian-amd64-iso-checksum = format("file:https://cdimage.debian.org/cdimage/release/%s/amd64/iso-dvd/SHA256SUMS", "${var.debian-version}")
  debian-arm64-iso-url      = format("https://cdimage.debian.org/cdimage/release/%s/arm64/iso-dvd/debian-%s-arm64-DVD-1.iso", "${var.debian-version}", "${var.debian-version}")
  debian-arm64-iso-checksum = format("file:https://cdimage.debian.org/cdimage/release/%s/arm64/iso-dvd/SHA256SUMS", "${var.debian-version}")
}

################################################################################
# Variables used by Fedora packer build environments
################################################################################
variable "fedora-version" {
  type    = string
  default = "41"
}
variable "fedora-dot-version" {
  type    = string
  default = "1.4"
}
variable "fedora-qemu-disk" {
  type    = string
  default = "sda"
}
variable "fedora-virtualbox-disk" {
  type    = string
  default = "sda"
}
variable "fedora-vmware-disk" {
  type    = string
  default = "sda"
}
variable "fedora-vsphere-disk" {
  type    = string
  default = "nvme0n1"
}
locals {
  fedora-amd64-iso-url      = format("https://download.fedoraproject.org/pub/fedora/linux/releases/%s/Server/x86_64/iso/Fedora-Server-dvd-x86_64-%s-%s.iso", "${var.fedora-version}", "${var.fedora-version}", "${var.fedora-dot-version}")
  fedora-amd64-iso-checksum = format("file:https://download.fedoraproject.org/pub/fedora/linux/releases/%s/Server/x86_64/iso/Fedora-Server-%s-%s-x86_64-CHECKSUM", "${var.fedora-version}", "${var.fedora-version}", "${var.fedora-dot-version}")
  fedora-arm64-iso-url      = format("https://download.fedoraproject.org/pub/fedora/linux/releases/%s/Server/aarch64/iso/Fedora-Server-dvd-aarch64-%s-%s.iso", "${var.fedora-version}", "${var.fedora-version}", "${var.fedora-dot-version}")
  fedora-arm64-iso-checksum = format("file:https://download.fedoraproject.org/pub/fedora/linux/releases/%s/Server/aarch64/iso/Fedora-Server-%s-%s-aarch64-CHECKSUM", "${var.fedora-version}", "${var.fedora-version}", "${var.fedora-dot-version}")
}

################################################################################
# Variables used by FreeBSD packer build environments
################################################################################
variable "freebsd-version" {
  type    = string
  default = "14.2-RELEASE"
}
locals {
  freebsd-amd64-iso-url      = format("https://download.freebsd.org/ftp/releases/ISO-IMAGES/%s/FreeBSD-%s-amd64-disc1.iso", regex_replace("${var.freebsd-version}", "-.*", ""), "${var.freebsd-version}")
  freebsd-amd64-iso-checksum = format("file:https://download.freebsd.org/ftp/releases/ISO-IMAGES/%s/CHECKSUM.SHA256-FreeBSD-%s-amd64", regex_replace("${var.freebsd-version}", "-.*", ""), "${var.freebsd-version}")
  freebsd-arm64-iso-url      = format("https://download.freebsd.org/ftp/releases/ISO-IMAGES/%s/FreeBSD-%s-arm64-aarch64-disc1.iso", regex_replace("${var.freebsd-version}", "-.*", ""), "${var.freebsd-version}")
  freebsd-arm64-iso-checksum = format("file:https://download.freebsd.org/ftp/releases/ISO-IMAGES/%s/CHECKSUM.SHA256-FreeBSD-%s-arm64-aarch64", regex_replace("${var.freebsd-version}", "-.*", ""), "${var.freebsd-version}")
}

################################################################################
# Variables used by macOS packer build environments
################################################################################
variable "macos-name" {
  type    = string
  default = "sequoia"
}
variable "macos-version" {
  type    = string
  default = "15.2"
}
variable "macos-tart-flavor" {
  type    = string
  default = "base"
}

################################################################################
# Variables used by NetBSD packer build environments
################################################################################
variable "netbsd-version" {
  type    = string
  default = "10.1"
}
locals {
  netbsd-amd64-iso-url      = format("https://ftp.fau.de/netbsd/iso/%s/NetBSD-%s-amd64.iso", "${var.netbsd-version}", "${var.netbsd-version}")
  netbsd-amd64-iso-checksum = format("file:https://ftp.fau.de/netbsd/iso/%s/SHA512", "${var.netbsd-version}")
  netbsd-arm64-iso-url      = format("https://ftp.fau.de/netbsd/iso/%s/NetBSD-%s-evbarm-aarch64.iso", "${var.netbsd-version}", "${var.netbsd-version}")
  netbsd-arm64-iso-checksum = format("file:https://ftp.fau.de/netbsd/iso/%s/SHA512", "${var.netbsd-version}")
}

################################################################################
# Variables used by NixOS packer build environments
################################################################################
variable "nixos-version" {
  type    = string
  default = "24.11"
}
variable "nixos-hash" {
  type    = string
  default = "709933.c71ad5c34d51"
}
locals {
  nixos-amd64-iso-url      = format("https://releases.nixos.org/nixos/%s/nixos-%s.%s/nixos-minimal-%s.%s-x86_64-linux.iso", "${var.nixos-version}", "${var.nixos-version}", "${var.nixos-hash}", "${var.nixos-version}", "${var.nixos-hash}")
  nixos-amd64-iso-checksum = "sha256:e1b8cde9c391b9ea4920d662ea9328f37605d6c9f435868c69ea552cad00588c"
  #nixos-amd64-iso-checksum = format("file:https://releases.nixos.org/nixos/%s/nixos-%s.%s/nixos-minimal-%s.%s-x86_64-linux.iso.sha256", "${var.nixos-version}", "${var.nixos-version}", "${var.nixos-hash}", "${var.nixos-version}", "${var.nixos-hash}")
  nixos-arm64-iso-url      = format("https://releases.nixos.org/nixos/%s/nixos-%s.%s/nixos-minimal-%s.%s-aarch64-linux.iso", "${var.nixos-version}", "${var.nixos-version}", "${var.nixos-hash}", "${var.nixos-version}", "${var.nixos-hash}")
  nixos-arm64-iso-checksum = "sha256:27089b9670d6fc4eccb09cca47ef6b9df35484887a1e90a74d91226204a4f9a9"
  #nixos-arm64-iso-checksum = format("file:https://releases.nixos.org/nixos/%s/nixos-%s.%s/nixos-minimal-%s.%s-aarch64-linux.iso.sha256", "${var.nixos-version}", "${var.nixos-version}", "${var.nixos-hash}", "${var.nixos-version}", "${var.nixos-hash}")
}
variable "nixos-qemu-disk" {
  type    = string
  default = "vda"
}
variable "nixos-virtualbox-disk" {
  type    = string
  default = "sda"
}
variable "nixos-vmware-disk" {
  type    = string
  default = "nvme0n1"
}
variable "nixos-vsphere-disk" {
  type    = string
  default = "nvme0n1"
}

################################################################################
# Variables used by OpenBSD packer build environments
################################################################################
variable "openbsd-version" {
  type    = string
  default = "7.6"
}
variable "openbsd-use-snapshot" {
  type    = bool
  default = false
}
variable "openbsd-excluded-sets" {
  type    = string
  default = "-g* -m* -x*"
}
locals {
  openbsd-amd64-iso-url      = format("https://cdn.openbsd.org/pub/OpenBSD/%s/amd64/install%s.iso", "${var.openbsd-use-snapshot}" == true ? "snapshots" : "${var.openbsd-version}", replace("${var.openbsd-version}", ".", ""))
  openbsd-amd64-iso-checksum = format("file:https://cdn.openbsd.org/pub/OpenBSD/%s/amd64/SHA256", "${var.openbsd-use-snapshot}" == true ? "snapshots" : "${var.openbsd-version}")
  openbsd-arm64-iso-url      = format("https://cdn.openbsd.org/pub/OpenBSD/%s/arm64/install%s.iso", "${var.openbsd-use-snapshot}" == true ? "snapshots" : "${var.openbsd-version}", replace("${var.openbsd-version}", ".", ""))
  openbsd-arm64-iso-checksum = format("file:https://cdn.openbsd.org/pub/OpenBSD/%s/arm64/SHA256", "${var.openbsd-use-snapshot}" == true ? "snapshots" : "${var.openbsd-version}")
}
variable "openbsd-arm64-hcloud-id" {
  type    = string
  default = "193263410"
}

################################################################################
# Variables used by OpenIndiana packer build environments
################################################################################
variable "openindiana-version" {
  type    = string
  default = "20241026"
}
locals {
  openindiana-amd64-iso-url      = format("https://dlc.openindiana.org/isos/hipster/%s/OI-hipster-text-%s.iso", "${var.openindiana-version}", "${var.openindiana-version}")
  openindiana-amd64-iso-checksum = format("file:https://dlc.openindiana.org/isos/hipster/%s/OI-hipster-text-%s.iso.sha256sum", "${var.openindiana-version}", "${var.openindiana-version}")
}

################################################################################
# Variables used by openSUSE packer build environments
################################################################################
variable "opensuse-version" {
  type    = string
  default = "15.6"
}
locals {
  opensuse-amd64-iso-url      = format("https://download.opensuse.org/distribution/leap/%s/iso/openSUSE-Leap-%s-DVD-x86_64-Media.iso", "${var.opensuse-version}", "${var.opensuse-version}")
  opensuse-amd64-iso-checksum = format("file:https://download.opensuse.org/distribution/leap/%s/iso/openSUSE-Leap-%s-DVD-x86_64-Media.iso.sha256", "${var.opensuse-version}", "${var.opensuse-version}")
  opensuse-arm64-iso-url      = format("https://download.opensuse.org/distribution/leap/%s/iso/openSUSE-Leap-%s-DVD-aarch64-Media.iso", "${var.opensuse-version}", "${var.opensuse-version}")
  opensuse-arm64-iso-checksum = format("file:https://download.opensuse.org/distribution/leap/%s/iso/openSUSE-Leap-%s-DVD-aarch64-Media.iso.sha256", "${var.opensuse-version}", "${var.opensuse-version}")
}

################################################################################
# Variables used by Rocky packer build environments
################################################################################
variable "rocky-version" {
  type    = string
  default = "9.5"
}
variable "rocky-qemu-disk" {
  type    = string
  default = "sda"
}
variable "rocky-virtualbox-disk" {
  type    = string
  default = "sda"
}
variable "rocky-vmware-disk" {
  type    = string
  default = "sda"
}
variable "rocky-vsphere-disk" {
  type    = string
  default = "nvme0n1"
}
locals {
  rocky-amd64-iso-url      = format("https://download.rockylinux.org/pub/rocky/%s/isos/x86_64/Rocky-%s-x86_64-dvd.iso", regex_replace("${var.rocky-version}", ".\\d$", ""), "${var.rocky-version}")
  rocky-amd64-iso-checksum = format("file:https://download.rockylinux.org/pub/rocky/%s/isos/x86_64/CHECKSUM", regex_replace("${var.rocky-version}", ".\\d$", ""))
  rocky-arm64-iso-url      = format("https://download.rockylinux.org/pub/rocky/%s/isos/aarch64/Rocky-%s-aarch64-dvd.iso", regex_replace("${var.rocky-version}", ".\\d$", ""), "${var.rocky-version}")
  rocky-arm64-iso-checksum = format("file:https://download.rockylinux.org/pub/rocky/%s/isos/aarch64/CHECKSUM", regex_replace("${var.rocky-version}", ".\\d$", ""))
}

################################################################################
# Variables used by Ubuntu packer build environments
################################################################################
variable "ubuntu-version" {
  type    = string
  default = "24.10"
}
variable "ubuntu-username" {
  type    = string
  default = "user"
}
variable "ubuntu-password" {
  type    = string
  default = "user"
}
locals {
  ubuntu-amd64-iso-url      = format("https://releases.ubuntu.com/%s/ubuntu-%s-live-server-amd64.iso", "${var.ubuntu-version}", "${var.ubuntu-version}")
  ubuntu-amd64-iso-checksum = format("file:https://releases.ubuntu.com/%s/SHA256SUMS", "${var.ubuntu-version}")
  ubuntu-arm64-iso-url      = format("https://cdimage.ubuntu.com/releases/%s/release/ubuntu-%s-live-server-arm64.iso", "${var.ubuntu-version}", "${var.ubuntu-version}")
  ubuntu-arm64-iso-checksum = format("file:https://cdimage.ubuntu.com/releases/%s/release/SHA256SUMS", "${var.ubuntu-version}")
}
