# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Packer Source Definitions
# ===============================================================================
#
# This file defines all the source machine configurations used to build FlyingDdns
# virtual machine images. It specifies the base operating system images, hardware
# configurations, and initial setup parameters for each supported platform.
#
# The source definitions are organized by:
# 1. Operating System (Alpine, Debian, Fedora, FreeBSD, etc.)
# 2. Architecture (amd64, arm64)
# 3. Virtualization Platform (QEMU, VirtualBox, VMware, vSphere, etc.)
#
# Each source definition includes:
# - Base ISO image location and checksum
# - VM hardware specifications (CPU, memory, disk)
# - Boot and installation parameters
# - Network configuration
# - Authentication credentials
#
# This file works in conjunction with:
# - variables.pkr.hcl: References variables for consistent configuration
# - build.pkr.hcl: Uses these source definitions in the build process
# - plugins/*.pkr.hcl: Provides the required plugins for each virtualization platform
#
# The extensive range of source definitions enables FlyingDdns to be deployed
# consistently across diverse computing environments.
# ===============================================================================

################################################################################
# Alpine definition of the sources (platforms)
################################################################################
# Alpine: amd64 (Qemu @ FreeBSD @ Intel Xeon E5-2640 v4)
source "qemu" "alpine-amd64" {
  vm_name          = "{{build_type}}.{{build_name}}.{{uuid}}"
  iso_url          = "${local.alpine-amd64-iso-url}"
  iso_checksum     = "${local.alpine-amd64-iso-checksum}"
  http_directory   = "${var.http-directory}"
  communicator     = "ssh"
  ssh_username     = "root"
  ssh_password     = "${var.root-password}"
  shutdown_command = "/sbin/poweroff"
  headless         = true
  qemu_binary      = "qemu-system-x86_64"
  machine_type     = "pc"
  cpus             = "${var.cpu-count}"
  memory           = "${var.memory-medium}"
  disk_size        = "${var.disk-size}"
  format           = "qcow2"
  disk_interface   = "virtio"
  net_device       = "virtio-net"
  display          = "none"
  skip_compaction  = true
  boot_wait        = "120s"
  boot_command = [
    # Configure system for unattanded installation
    "root<return><wait2s>",
    "ifconfig eth0 up && udhcpc -i eth0<enter><wait10>",
    "wget http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<enter><wait5s>",
    # Run the unattanded installation
    "setup-alpine -f unattended.conf<return><wait8s>",
    "${var.root-password}<return><wait5s>",
    "${var.root-password}<return><wait100s>",
    "y<return><wait120s>",
    # Reboot into the installed OS
    "reboot<return><wait120s>",
    # Prepare installed OS to be accessible from packer
    "root<return><wait1s>",
    "${var.root-password}<return><wait1s>",
    "sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config<return><wait1s>",
    "service sshd restart<return><wait1s>",
    "exit<return>",
  ]
}
# Alpine: amd64 (VirtualBox @ MacOS @ Intel Core i7-4980HQ CPU @ 2.80GHz)
source "virtualbox-iso" "alpine-amd64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  guest_os_type        = "Debian_64" # rollback to Linux_64 if not working
  iso_url              = "${local.alpine-amd64-iso-url}"
  iso_checksum         = "${local.alpine-amd64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "/sbin/poweroff"
  skip_export          = true
  headless             = true
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-medium}"
  disk_size            = "${var.disk-size}"
  usb                  = true
  guest_additions_mode = "disable"
  vboxmanage = [
    ["setextradata", "{{.Name}}", "GUI/ScaleFactor", "2"],
    ["modifyvm", "{{.Name}}", "--vrdeproperty", "VNCPassword=a"],
    ["modifyvm", "{{.Name}}", "--ioapic=on"],
  ]
  boot_wait = "20s"
  boot_command = [
    # Configure system for unattanded installation
    "root<return><wait2s>",
    "ifconfig eth0 up && udhcpc -i eth0<enter><wait5>",
    "wget http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<enter><wait3s>",
    # Run the unattanded installation
    "setup-alpine -f unattended.conf<return><wait10s>",
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait30s>",
    "y<return><wait50s>",
    # Reboot into the installed OS
    #"mount /dev/${var.alpine-virtualbox-disk}3 /mnt<return><wait2s>",
    #"echo 'kernel.watchdog=0' >> /mnt/etc/sysctl.d/local.conf<return><wait2s>",
    ##"echo 'kernel.nmi_watchdog=0' >> /mnt/etc/sysctl.d/local.conf<return><wait2s>",
    #"umount /mnt<return><wait2s>",
    "sysctl kernel.watchdog=0<return><wait2s>",
    ##"sysctl kernel.nmi_watchdog=0<return><wait2s>",
    "reboot<return><wait50s>",
    # Prepare installed OS to be accessible from packer
    "root<return><wait1s>",
    "${var.root-password}<return><wait2s>",
    "sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config<return><wait2s>",
    "service sshd restart<return><wait2s>",
    "exit<return>",
  ]
}
# Alpine: arm64 (Qemu @ macOS @ >= Apple M1 Max)
source "qemu" "alpine-arm64" {
  vm_name          = "{{build_type}}.{{build_name}}.{{uuid}}"
  iso_url          = "${local.alpine-arm64-iso-url}"
  iso_checksum     = "${local.alpine-arm64-iso-checksum}"
  http_directory   = "${var.http-directory}"
  communicator     = "ssh"
  ssh_username     = "root"
  ssh_password     = "${var.root-password}"
  shutdown_command = "/sbin/poweroff"
  headless         = true
  qemu_binary      = "qemu-system-aarch64"
  machine_type     = "virt"
  cpus             = "${var.cpu-count}"
  memory           = "${var.memory-medium}"
  disk_size        = "${var.disk-size}"
  format           = "qcow2"
  disk_interface   = "virtio"
  net_device       = "virtio-net"
  display          = "none"
  firmware         = "${var.qemu-arm64-firmware-path}"
  cpu_model        = "${var.qemu-arm64-cpu}"
  accelerator      = "${var.qemu-arm64-accelerator}"
  qemuargs = [
    ["-boot", "c"],
    ["-device", "qemu-xhci"],
    ["-device", "usb-kbd"],
    ["-device", "virtio-gpu-pci"],
  ]
  skip_compaction = true
  boot_wait       = "120s"
  boot_command = [
    # Configure system for unattanded installation
    "root<return><wait2s>",
    "ifconfig eth0 up && udhcpc -i eth0<enter><wait10>",
    "wget http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<enter><wait5s>",
    # Run the unattanded installation
    "setup-alpine -f unattended.conf<return><wait8s>",
    "${var.root-password}<return><wait5s>",
    "${var.root-password}<return><wait100s>",
    "y<return><wait120s>",
    # Reboot into the installed OS
    "reboot<return><wait120s>",
    # Prepare installed OS to be accessible from packer
    "root<return><wait1s>",
    "${var.root-password}<return><wait1s>",
    "sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config<return><wait1s>",
    "service sshd restart<return><wait1s>",
    "exit<return>",
  ]
}
# TODO: Alpine: arm64 (Virtualbox @ macOS @ >= Apple M1 Max)
# After the bootcommand is executed the system still boots the ISO cd drive. 
source "virtualbox-iso" "alpine-arm64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  guest_os_type        = "Other_arm64"
  iso_url              = "${local.alpine-arm64-iso-url}"
  iso_checksum         = "${local.alpine-arm64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "/sbin/poweroff"
  skip_export          = true
  headless             = false
  firmware             = "efi"
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-medium}"
  iso_interface        = "virtio"
  hard_drive_interface = "virtio"
  disk_size            = "${var.disk-size}"
  usb                  = true
  gfx_controller       = "vmsvga"
  gfx_vram_size        = "16"
  guest_additions_mode = "disable"
  vboxmanage = [
    ["setextradata", "{{.Name}}", "GUI/ScaleFactor", "2"],
    ["modifyvm", "{{.Name}}", "--vrdeproperty", "VNCPassword=a"],
  ]
  boot_wait = "20s"
  boot_command = [
    # Configure system for unattanded installation
    "root<return><wait2s>",
    "ifconfig eth0 up && udhcpc -i eth0<enter><wait5>",
    "wget http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<enter><wait3s>",
    # Run the unattanded installation
    "setup-alpine -f unattended.conf<return><wait5s>",
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait20s>",
    "y<return><wait20s>",
    # Reboot into the installed OS
    "<wait99999s>",
    "cd /tmp<enter><wait1s>",
    "cp /sbin/reboot .<enter><wait1s>",
    "eject /dev/cdrom<enter><wait1s>",
    "./reboot -n -f<enter><wait35s>",
    # Prepare installed OS to be accessible from packer
    "root<return><wait1s>",
    "${var.root-password}<return><wait2s>",
    "sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config<return><wait2s>",
    "service sshd restart<return><wait2s>",
    "exit<return>",
  ]
  #boot_wait = "55s"
  #boot_command = [
  #  # Configure system for unattanded installation
  #  "<tab><return><wait2s>",
  #  "dhclient em0<enter><wait15s>",
  #  "fetch --output=/tmp/unattended.conf http://{{.HTTPIP}}:{{.HTTPPort}}/unattended.conf<enter><wait5s>",
  #  # Run the unattanded installation
  #  "bsdinstall script /tmp/unattended.conf<enter><wait40s>",
  #  "${var.root-password}<return><wait2s>",
  #  "${var.root-password}<return><wait2s>",
  #  # Reboot into the installed OS
  #  "cd /tmp<enter><wait1s>",
  #  "cp /sbin/reboot .<enter><wait1s>",
  #  "cdcontrol eject<enter><wait1s>",
  #  "./reboot -n -q<enter><wait35s>",
  #]
}
# Alpine: arm64 (VMware Fusion @ macOS @ >= Apple M1 Max)
source "vmware-iso" "alpine-arm64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  version              = "20"
  iso_url              = "${local.alpine-arm64-iso-url}"
  iso_checksum         = "${local.alpine-arm64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  vnc_disable_password = true
  shutdown_command     = "/sbin/poweroff"
  keep_registered      = false
  skip_export          = true
  headless             = true
  format               = "vmx"
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-medium}"
  disk_size            = "${var.disk-size}"
  disk_adapter_type    = "nvme"
  disk_type_id         = "0"
  network_adapter_type = "e1000e"
  usb                  = true
  guest_os_type        = "arm-other-64"
  vmx_data = {
    "firmware"         = "efi"
    "architecture"     = "arm-other-64"
    "usb_xhci.present" = "TRUE"
  }
  skip_compaction = true
  boot_wait       = "20s"
  boot_command = [
    # Configure system for unattanded installation
    "root<return><wait2s>",
    "ifconfig eth0 up && udhcpc -i eth0<enter><wait5>",
    "wget http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<enter><wait3s>",
    # Run the unattanded installation
    "setup-alpine -f unattended.conf<return><wait10s>",
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait70s>",
    "y<return><wait80s>",
    # Reboot into the installed OS
    "reboot<return><wait60s>",
    # Prepare installed OS to be accessible from packer
    "root<return><wait1s>",
    "${var.root-password}<return><wait2s>",
    "sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config<return><wait2s>",
    "service sshd restart<return><wait2s>",
    "exit<return>",
  ]
}
# Alpine: arm64 (VMware ESXi @ RPI5 8GB)
source "vsphere-iso" "alpine-arm64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  vcenter_server       = "${var.esxi-host}"
  host                 = "${var.esxi-host}"
  username             = "${var.esxi-username}"
  password             = "${var.esxi-password}"
  insecure_connection  = true
  iso_paths            = [format("[datastore1] alpine-standard-%s-aarch64.iso", "${var.alpine-version}")]
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "/sbin/poweroff"
  CPUs                 = "${var.cpu-count}"
  RAM                  = "${var.memory-medium}"
  RAM_reserve_all      = true
  disk_controller_type = ["nvme"]
  storage {
    disk_size = "${var.disk-size}"
  }
  network_adapters {
    network      = "VM Network"
    network_card = "e1000e"
  }
  guest_os_type  = "other4xLinux64Guest"
  cdrom_type     = "sata"
  destroy        = true
  usb_controller = ["xhci"]
  boot_wait      = "20s"
  boot_command = [
    # Configure system for unattanded installation
    "root<return><wait2s>",
    "ifconfig eth0 up && udhcpc -i eth0<enter><wait5>",
    "wget http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<enter><wait3s>",
    # Run the unattanded installation
    "setup-alpine -f unattended.conf<return><wait10s>",
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait70s>",
    "y<return><wait80s>",
    # Reboot into the installed OS
    "reboot<return><wait60s>",
    # Prepare installed OS to be accessible from packer
    "root<return><wait1s>",
    "${var.root-password}<return><wait2s>",
    "apk update && apk add open-vm-tools-guestinfo && rc-update add open-vm-tools boot && rc-service open-vm-tools start<return><wait30s>",
    "sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config<return><wait2s>",
    "service sshd restart<return><wait2s>",
    "exit<return>",
  ]
}

################################################################################
# Debian definition of the sources (platforms)
################################################################################
# Debian: amd64 (Qemu @ FreeBSD @ Intel Xeon E5-2640 v4)
source "qemu" "debian-amd64" {
  vm_name          = "{{build_type}}.{{build_name}}.{{uuid}}"
  iso_url          = "${local.debian-amd64-iso-url}"
  iso_checksum     = "${local.debian-amd64-iso-checksum}"
  http_directory   = "${var.http-directory}"
  communicator     = "ssh"
  ssh_username     = "root"
  ssh_password     = "${var.root-password}"
  shutdown_command = "/usr/sbin/poweroff"
  headless         = true
  qemu_binary      = "qemu-system-x86_64"
  machine_type     = "pc"
  cpus             = "${var.cpu-count}"
  memory           = "${var.memory-medium}"
  disk_size        = "${var.disk-size}"
  format           = "qcow2"
  disk_interface   = "virtio"
  net_device       = "virtio-net"
  display          = "none"
  skip_compaction  = true
  boot_wait        = "10s"
  boot_command = [
    # Configure system for unattanded installation
    "<down><down><return><wait2s>",
    "<down><down><down><down><down><down><return><wait240s>",
    # Run the unattanded installation
    "http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<tab><return><wait30s>",
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait1800s>",
  ]
}
# Debian: amd64 (VirtualBox @ MacOS @ Intel Core i7-4980HQ CPU @ 2.80GHz)
source "virtualbox-iso" "debian-amd64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  guest_os_type        = "Debian_64"
  iso_url              = "${local.debian-amd64-iso-url}"
  iso_checksum         = "${local.debian-amd64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "/usr/sbin/poweroff"
  skip_export          = true
  headless             = true
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-medium}"
  disk_size            = "${var.disk-size}"
  usb                  = true
  guest_additions_mode = "disable"
  vboxmanage = [
    ["setextradata", "{{.Name}}", "GUI/ScaleFactor", "2"],
    ["modifyvm", "{{.Name}}", "--vrdeproperty", "VNCPassword=a"],
  ]
  boot_wait = "15s"
  boot_command = [
    # Configure system for unattanded installation
    "<down><down><return><wait2s>",
    "<down><down><down><down><down><down><return><wait50s>",
    # Run the unattanded installation
    "http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<tab><return><wait10s>",
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait300s>",
  ]
}
# TODO: Debian: arm64 (Qemu @ macOS @ >= Apple M1 Max)
# Starts but keeps standing on black screen with blinking cursor
source "qemu" "debian-arm64" {
  vm_name          = "{{build_type}}.{{build_name}}.{{uuid}}"
  iso_url          = "${local.debian-arm64-iso-url}"
  iso_checksum     = "${local.debian-arm64-iso-checksum}"
  http_directory   = "${var.http-directory}"
  communicator     = "ssh"
  ssh_username     = "root"
  ssh_password     = "${var.root-password}"
  shutdown_command = "/usr/sbin/poweroff"
  headless         = true
  qemu_binary      = "qemu-system-aarch64"
  machine_type     = "virt"
  cpus             = "${var.cpu-count}"
  memory           = "${var.memory-medium}"
  disk_size        = "${var.disk-size}"
  format           = "qcow2"
  disk_interface   = "virtio"
  net_device       = "virtio-net"
  display          = "none"
  firmware         = "${var.qemu-arm64-firmware-path}"
  cpu_model        = "${var.qemu-arm64-cpu}"
  accelerator      = "${var.qemu-arm64-accelerator}"
  qemuargs = [
    ["-boot", "c"],
    ["-device", "qemu-xhci"],
    ["-device", "usb-kbd"],
    ["-device", "virtio-gpu-pci"],
  ]
  skip_compaction = true
  boot_wait       = "10s"
  boot_command = [
    # Configure system for unattanded installation
    "<down><down><return><wait2s>",
    "<down><down><down><down><down><return><wait240s>",
    # Run the unattanded installation
    "http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<tab><return><wait30s>",
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait1800s>",
  ]
}
# Debian: arm64 (VMware Fusion @ macOS @ >= Apple M1 Max)
source "vmware-iso" "debian-arm64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  iso_url              = "${local.debian-arm64-iso-url}"
  iso_checksum         = "${local.debian-arm64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  version              = "20"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  vnc_disable_password = true
  shutdown_command     = "/usr/sbin/poweroff"
  keep_registered      = false
  skip_export          = true
  headless             = true
  format               = "vmx"
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-plenty}"
  disk_size            = "${var.disk-size}"
  disk_adapter_type    = "nvme"
  disk_type_id         = "0"
  network_adapter_type = "e1000e"
  usb                  = true
  guest_os_type        = "arm-other-64"
  vmx_data = {
    "firmware"         = "efi"
    "architecture"     = "arm-other-64"
    "usb_xhci.present" = "TRUE"
  }
  skip_compaction = true
  boot_wait       = "20s"
  boot_command = [
    # Configure system for unattanded installation
    "<down><down><return><wait2s>",
    "<down><down><down><down><down><return><wait50s>",
    # Run the unattanded installation
    "http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<tab><return><wait10s>",
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait180s>",
  ]
}
# Debian: arm64 (VMware ESXi @ RPI5 8GB)
source "vsphere-iso" "debian-arm64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  vcenter_server       = "${var.esxi-host}"
  host                 = "${var.esxi-host}"
  username             = "${var.esxi-username}"
  password             = "${var.esxi-password}"
  insecure_connection  = true
  iso_paths            = [format("[datastore1] debian-%s-arm64-DVD-1.iso", "${var.debian-version}")]
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "/usr/sbin/poweroff"
  CPUs                 = "${var.cpu-count}"
  RAM                  = "${var.memory-medium}"
  RAM_reserve_all      = true
  disk_controller_type = ["nvme"]
  storage {
    disk_size = "${var.disk-size}"
  }
  network_adapters {
    network      = "VM Network"
    network_card = "e1000e"
  }
  guest_os_type  = "other4xLinux64Guest"
  cdrom_type     = "sata"
  destroy        = true
  usb_controller = ["xhci"]
  boot_wait      = "20s"
  boot_command = [
    # Configure system for unattanded installation
    "<down><down><return><wait2s>",
    "<down><down><down><down><down><return><wait50s>",
    # Run the unattanded installation
    "http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<tab><return><wait20s>",
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait400s>",
  ]
}

################################################################################
# Fedora definition of the sources (platforms)
################################################################################
# Fedora: amd64 (Qemu @ FreeBSD @ Intel Xeon E5-2640 v4)
source "qemu" "fedora-amd64" {
  vm_name          = "{{build_type}}.{{build_name}}.{{uuid}}"
  iso_url          = "${local.fedora-amd64-iso-url}"
  iso_checksum     = "${local.fedora-amd64-iso-checksum}"
  http_directory   = "${var.http-directory}"
  communicator     = "ssh"
  ssh_username     = "root"
  ssh_password     = "${var.root-password}"
  shutdown_command = "/usr/sbin/poweroff"
  headless         = true
  qemu_binary      = "qemu-system-x86_64"
  machine_type     = "pc"
  cpus             = "${var.cpu-count}"
  memory           = "${var.memory-medium}"
  disk_size        = "${var.disk-size}"
  format           = "qcow2"
  disk_interface   = "ide"
  net_device       = "virtio-net"
  display          = "none"
  skip_compaction  = true
  boot_wait        = "10s"
  boot_command = [
    # Run the unattanded installation
    "<up><wait1s>",
    "e<wait1s>",
    "<down><wait1s>",
    "<leftCtrlOn>kkkkk<leftCtrlOff><return>",
    "linux /images/pxeboot/vmlinuz ip=dhcp inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<return><wait1s>",
    "initrd /images/pxeboot/initrd.img<wait1s>",
    "<f10><wait2700s>",
  ]
}
# Fedora: amd64 (VirtualBox @ MacOS @ Intel Core i7-4980HQ CPU @ 2.80GHz)
source "virtualbox-iso" "fedora-amd64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  guest_os_type        = "Fedora_64"
  iso_url              = "${local.fedora-amd64-iso-url}"
  iso_checksum         = "${local.fedora-amd64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "/usr/sbin/poweroff"
  skip_export          = true
  headless             = true
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-medium}"
  disk_size            = "${var.disk-size}"
  usb                  = true
  guest_additions_mode = "disable"
  vboxmanage = [
    ["setextradata", "{{.Name}}", "GUI/ScaleFactor", "2"],
    ["modifyvm", "{{.Name}}", "--vrdeproperty", "VNCPassword=a"],
  ]
  boot_wait = "10s"
  boot_command = [
    # Run the unattanded installation
    "<up><wait1s>",
    "e<wait1s>",
    "<down><wait1s>",
    "<leftCtrlOn>kkkkk<leftCtrlOff><return>",
    "linux /images/pxeboot/vmlinuz ip=dhcp inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<return><wait1s>",
    "initrd /images/pxeboot/initrd.img<wait1s>",
    "<f10><wait400s>",
  ]
}
# TODO: Fedora: arm64 (Qemu @ macOS @ >= Apple M1 Max)
# After the boatloader (Grub) was configured "Display output is not active." is shown forever.
source "qemu" "fedora-arm64" {
  vm_name          = "{{build_type}}.{{build_name}}.{{uuid}}"
  iso_url          = "${local.fedora-arm64-iso-url}"
  iso_checksum     = "${local.fedora-arm64-iso-checksum}"
  http_directory   = "${var.http-directory}"
  communicator     = "ssh"
  ssh_username     = "root"
  ssh_password     = "${var.root-password}"
  shutdown_command = "/usr/sbin/poweroff"
  headless         = true
  qemu_binary      = "qemu-system-aarch64"
  machine_type     = "virt"
  cpus             = "${var.cpu-count}"
  memory           = "${var.memory-medium}"
  disk_size        = "${var.disk-size}"
  format           = "qcow2"
  disk_interface   = "virtio"
  net_device       = "virtio-net"
  display          = "none"
  firmware         = "${var.qemu-arm64-firmware-path}"
  cpu_model        = "${var.qemu-arm64-cpu}"
  accelerator      = "${var.qemu-arm64-accelerator}"
  qemuargs = [
    ["-boot", "c"],
    ["-device", "qemu-xhci"],
    ["-device", "usb-kbd"],
    ["-device", "virtio-gpu-pci"],
  ]
  skip_compaction = true
  boot_wait       = "10s"
  boot_command = [
    # Run the unattanded installation
    "<up><wait1s>",
    "e<wait1s>",
    "<down><wait1s>",
    "<leftCtrlOn>kkkkk<leftCtrlOff><return>",
    "linux /images/pxeboot/vmlinuz ip=dhcp inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<return><wait1s>",
    "initrd /images/pxeboot/initrd.img<wait1s>",
    "<f10><wait2700s>",
  ]
}
# Fedora: arm64 (VMware Fusion @ macOS @ >= Apple M1 Max)
source "vmware-iso" "fedora-arm64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  version              = "20"
  iso_url              = "${local.fedora-arm64-iso-url}"
  iso_checksum         = "${local.fedora-arm64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  vnc_disable_password = true
  shutdown_command     = "/usr/sbin/poweroff"
  keep_registered      = false
  skip_export          = true
  headless             = true
  format               = "vmx"
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-plenty}"
  disk_size            = "${var.disk-size}"
  disk_adapter_type    = "sata"
  disk_type_id         = "0"
  network_adapter_type = "e1000e"
  usb                  = true
  guest_os_type        = "arm-other-64"
  vmx_data = {
    "firmware"         = "efi"
    "architecture"     = "arm-other-64"
    "usb_xhci.present" = "TRUE"
  }
  skip_compaction = true
  boot_wait       = "10s"
  boot_command = [
    # Run the unattanded installation
    "<up><wait1s>",
    "e<wait1s>",
    "<down><wait1s>",
    "<leftCtrlOn>kkkkk<leftCtrlOff><return>",
    "linux /images/pxeboot/vmlinuz nomodeset ip=dhcp inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<return><wait1s>",
    "initrd /images/pxeboot/initrd.img<wait1s>",
    "<f10><wait240s>",
  ]
}
# Fedora: arm64 (VMware ESXi @ RPI5 8GB)
source "vsphere-iso" "fedora-arm64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  vcenter_server       = "${var.esxi-host}"
  host                 = "${var.esxi-host}"
  username             = "${var.esxi-username}"
  password             = "${var.esxi-password}"
  insecure_connection  = true
  iso_paths            = [format("[datastore1] Fedora-Server-dvd-aarch64-%s-%s.iso", "${var.fedora-version}", "${var.fedora-dot-version}")]
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "/usr/sbin/poweroff"
  CPUs                 = "${var.cpu-count}"
  RAM                  = "${var.memory-medium}"
  RAM_reserve_all      = true
  disk_controller_type = ["nvme"]
  storage {
    disk_size = "${var.disk-size}"
  }
  network_adapters {
    network      = "VM Network"
    network_card = "e1000e"
  }
  guest_os_type  = "other4xLinux64Guest"
  cdrom_type     = "sata"
  destroy        = true
  usb_controller = ["xhci"]
  boot_wait      = "20s"
  boot_command = [
    # Run the unattanded installation
    "<up><wait1s>",
    "e<wait1s>",
    "<down><wait1s>",
    "<leftCtrlOn>kkkkk<leftCtrlOff><return>",
    "linux /images/pxeboot/vmlinuz nomodeset ip=dhcp inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<return><wait1s>",
    "initrd /images/pxeboot/initrd.img<wait1s>",
    "<f10><wait600s>",
  ]
}

################################################################################
# FreeBSD definition of the sources (platforms)
################################################################################
# FreeBSD: amd64 (Qemu @ FreeBSD @ Intel Xeon E5-2640 v4)
source "qemu" "freebsd-amd64" {
  vm_name          = "{{build_type}}.{{build_name}}.{{uuid}}"
  iso_url          = "${local.freebsd-amd64-iso-url}"
  iso_checksum     = "${local.freebsd-amd64-iso-checksum}"
  http_directory   = "${var.http-directory}"
  communicator     = "ssh"
  ssh_username     = "root"
  ssh_password     = "${var.root-password}"
  shutdown_command = "/sbin/poweroff 2>&1"
  headless         = true
  qemu_binary      = "qemu-system-x86_64"
  machine_type     = "pc"
  cpus             = "${var.cpu-count}"
  memory           = "${var.memory-medium}"
  disk_size        = "${var.disk-size}"
  format           = "qcow2"
  disk_interface   = "virtio"
  net_device       = "virtio-net"
  display          = "none"
  skip_compaction  = true
  boot_wait        = "50s"
  boot_command = [
    # Configure system for unattanded installation
    "<tab><return><wait2s>",
    "dhclient vtnet0<enter><wait10s>",
    "fetch --output=/tmp/unattended.conf http://{{.HTTPIP}}:{{.HTTPPort}}/unattended.conf<enter><wait5s>",
    # Run the unattanded installation
    "bsdinstall script /tmp/unattended.conf<enter><wait240s>",
    "${var.root-password}<return><wait5s>",
    "${var.root-password}<return><wait5s>",
    # Reboot into the installed OS
    "reboot<enter><wait80s>",
  ]
}
# FreeBSD: amd64 (VirtualBox @ MacOS @ Intel Core i7-4980HQ CPU @ 2.80GHz)
source "virtualbox-iso" "freebsd-amd64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  guest_os_type        = "FreeBSD_64"
  iso_url              = "${local.freebsd-amd64-iso-url}"
  iso_checksum         = "${local.freebsd-amd64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "/sbin/poweroff 2>&1"
  skip_export          = true
  headless             = true
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-medium}"
  disk_size            = "${var.disk-size}"
  usb                  = true
  guest_additions_mode = "disable"
  vboxmanage = [
    ["setextradata", "{{.Name}}", "GUI/ScaleFactor", "2"],
    ["modifyvm", "{{.Name}}", "--vrdeproperty", "VNCPassword=a"],
  ]
  boot_wait = "35s"
  boot_command = [
    # Configure system for unattanded installation
    "<tab><return><wait2s>",
    "dhclient em0<enter><wait10s>",
    "fetch --output=/tmp/unattended.conf http://{{.HTTPIP}}:{{.HTTPPort}}/unattended.conf<enter><wait5s>",
    # Run the unattanded installation
    "bsdinstall script /tmp/unattended.conf<enter><wait70s>",
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    # Reboot into the installed OS
    "reboot<enter><wait35s>",
  ]
}
# FreeBSD: arm64 (Qemu @ macOS @ >= Apple M1 Max)
source "qemu" "freebsd-arm64" {
  vm_name          = "{{build_type}}.{{build_name}}.{{uuid}}"
  iso_url          = "${local.freebsd-arm64-iso-url}"
  iso_checksum     = "${local.freebsd-arm64-iso-checksum}"
  http_directory   = "${var.http-directory}"
  communicator     = "ssh"
  ssh_username     = "root"
  ssh_password     = "${var.root-password}"
  shutdown_command = "/sbin/poweroff 2>&1"
  headless         = true
  qemu_binary      = "qemu-system-aarch64"
  machine_type     = "virt"
  cpus             = "${var.cpu-count}"
  memory           = "${var.memory-medium}"
  disk_size        = "${var.disk-size}"
  format           = "qcow2"
  disk_interface   = "virtio"
  net_device       = "virtio-net"
  display          = "none"
  firmware         = "${var.qemu-arm64-firmware-path}"
  cpu_model        = "${var.qemu-arm64-cpu}"
  accelerator      = "${var.qemu-arm64-accelerator}"
  qemuargs = [
    ["-boot", "c"],
    ["-device", "qemu-xhci"],
    ["-device", "usb-kbd"],
    ["-device", "virtio-gpu-pci"],
  ]
  skip_compaction = true
  boot_wait       = "50s"
  boot_command = [
    # Configure system for unattanded installation
    "<tab><return><wait2s>",
    "dhclient vtnet0<enter><wait10s>",
    "fetch --output=/tmp/unattended.conf http://{{.HTTPIP}}:{{.HTTPPort}}/unattended.conf<enter><wait5s>",
    # Run the unattanded installation
    "bsdinstall script /tmp/unattended.conf<enter><wait240s>",
    "${var.root-password}<return><wait5s>",
    "${var.root-password}<return><wait5s>",
    # Reboot into the installed OS
    "reboot<enter><wait80s>",
  ]
}
# FreeBSD: arm64 (Virtualbox @ macOS @ >= Apple M1 Max)
source "virtualbox-iso" "freebsd-arm64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  guest_os_type        = "FreeBSD_arm64"
  iso_url              = "${local.freebsd-arm64-iso-url}"
  iso_checksum         = "${local.freebsd-arm64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "/sbin/poweroff 2>&1"
  skip_export          = true
  headless             = true
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-medium}"
  iso_interface        = "virtio"
  hard_drive_interface = "virtio"
  disk_size            = "${var.disk-size}"
  usb                  = true
  gfx_controller       = "vmsvga"
  gfx_vram_size        = "16"
  guest_additions_mode = "disable"
  vboxmanage = [
    ["setextradata", "{{.Name}}", "GUI/ScaleFactor", "2"],
    ["modifyvm", "{{.Name}}", "--vrdeproperty", "VNCPassword=a"],
  ]
  boot_wait = "55s"
  boot_command = [
    # Configure system for unattanded installation
    "<tab><return><wait2s>",
    "dhclient em0<enter><wait15s>",
    "fetch --output=/tmp/unattended.conf http://{{.HTTPIP}}:{{.HTTPPort}}/unattended.conf<enter><wait5s>",
    # Run the unattanded installation
    "bsdinstall script /tmp/unattended.conf<enter><wait40s>",
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    # Reboot into the installed OS
    "cd /tmp<enter><wait1s>",
    "cp /sbin/reboot .<enter><wait1s>",
    "cdcontrol eject<enter><wait1s>",
    "./reboot -n -q<enter><wait35s>",
  ]
}
# FreeBSD: arm64 (VMware Fusion @ macOS @ >= Apple M1 Max)
source "vmware-iso" "freebsd-arm64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  iso_url              = "${local.freebsd-arm64-iso-url}"
  iso_checksum         = "${local.freebsd-arm64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  version              = "20"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  vnc_disable_password = true
  shutdown_command     = "/sbin/poweroff 2>&1"
  keep_registered      = false
  skip_export          = true
  headless             = true
  format               = "vmx"
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-medium}"
  disk_size            = "${var.disk-size}"
  disk_adapter_type    = "nvme"
  disk_type_id         = "0"
  network_adapter_type = "e1000e"
  usb                  = true
  guest_os_type        = "arm-other-64"
  vmx_data = {
    "firmware"         = "efi"
    "architecture"     = "arm-other-64"
    "usb_xhci.present" = "TRUE"
  }
  skip_compaction = true
  boot_wait       = "30s"
  boot_command = [
    # Configure system for unattanded installation
    "<tab><return><wait2s>",
    "dhclient em0<enter><wait5s>",
    "fetch --output=/tmp/unattended.conf http://{{.HTTPIP}}:{{.HTTPPort}}/unattended.conf<enter><wait5s>",
    # Run the unattanded installation
    "bsdinstall script /tmp/unattended.conf<enter><wait25s>",
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    # Reboot into the installed OS
    "reboot<enter><wait35s>",
  ]
}
# FreeBSD: arm64 (VMware ESXi @ RPI5 8GB)
source "vsphere-iso" "freebsd-arm64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  vcenter_server       = "${var.esxi-host}"
  host                 = "${var.esxi-host}"
  username             = "${var.esxi-username}"
  password             = "${var.esxi-password}"
  insecure_connection  = true
  iso_paths            = [format("[datastore1] FreeBSD-%s-arm64-aarch64-disc1.iso", "${var.freebsd-version}")]
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "/sbin/poweroff 2>&1"
  CPUs                 = "${var.cpu-count}"
  RAM                  = "${var.memory-medium}"
  RAM_reserve_all      = true
  disk_controller_type = ["nvme"]
  storage {
    disk_size = "${var.disk-size}"
  }
  network_adapters {
    network      = "VM Network"
    network_card = "e1000e"
  }
  guest_os_type  = "other4xLinux64Guest"
  cdrom_type     = "sata"
  destroy        = true
  usb_controller = ["xhci"]
  boot_wait      = "20s"
  boot_command = [
    # Configure system for unattanded installation
    "<tab><return><wait2s>",
    "dhclient em0<enter><wait5s>",
    "fetch --output=/tmp/unattended.conf http://{{.HTTPIP}}:{{.HTTPPort}}/unattended.conf<enter><wait5s>",
    # Run the unattanded installation
    "bsdinstall script /tmp/unattended.conf<enter><wait160s>",
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    # Reboot into the installed OS
    "reboot<enter><wait35s>",
    # Prepare installed OS to be accessible from packer
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "pkg install --yes open-vm-tools-nox11<return><wait80s>",
    "service vmware-kmod enable && service vmware-kmod start<return><wait2s>",
    "service vmware-guestd enable && service vmware-guestd start<return><wait5s>",
    "exit<enter><wait2s>",
  ]
}

#################################################################################
## macOS definition of the sources (platforms)
#################################################################################
# macOS: arm64 (Tart Virtualization @ macOS @ >= Apple M1 Max)
source "tart-cli" "macos-arm64" {
  vm_name      = "{{build_type}}.{{build_name}}.{{uuid}}"
  vm_base_name = "ghcr.io/cirruslabs/macos-${var.macos-name}-${var.macos-tart-flavor}:latest"
  #vm_name      = "${var.vm-host-name}"
  cpu_count    = "${var.cpu-count}"
  memory_gb    = "${var.memory-gb}"
  disk_size_gb = "${var.disk-size-gb}"
  ssh_username = "${var.tart-username-password}"
  ssh_password = "${var.tart-username-password}"
  ssh_timeout  = "120s"
  headless     = true
}
# This source is used on host systems that do not support the tart plugin to not break the packer validation.
# TODO: Check if we can get rid of this source.
source "null" "null" {
  ssh_host     = "127.0.0.1"
  ssh_username = "a"
  ssh_password = "a"
}

################################################################################
# NetBSD definition of the sources (platforms)
################################################################################
# NetBSD: amd64 (Qemu @ FreeBSD @ Intel Xeon E5-2640 v4)
source "qemu" "netbsd-amd64" {
  vm_name          = "{{build_type}}.{{build_name}}.{{uuid}}"
  iso_url          = "${local.netbsd-amd64-iso-url}"
  iso_checksum     = "${local.netbsd-amd64-iso-checksum}"
  http_directory   = "${var.http-directory}"
  communicator     = "ssh"
  ssh_username     = "root"
  ssh_password     = "${var.root-password}"
  shutdown_command = "/sbin/poweroff"
  headless         = true
  qemu_binary      = "qemu-system-x86_64"
  machine_type     = "pc"
  cpus             = "${var.cpu-count}"
  memory           = "${var.memory-medium}"
  disk_size        = "${var.disk-size}"
  format           = "qcow2"
  disk_interface   = "virtio"
  net_device       = "virtio-net"
  display          = "none"
  skip_compaction  = true
  boot_wait        = "40s"
  boot_command = [
    "a<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "f<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "<return><wait2s>",
    "a<wait1s><return><wait15s>",
    "${var.vm-host-name}<return><wait2s>",
    "<return><wait2s>",
    "a<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "<return><wait2s>",
    "g<wait1s><return><wait2s>",
    "x<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "b<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "b<wait1s><return><wait2s>",
    "x<wait1s><return><wait2s>",
    "b<wait1s><return><wait6s>",
    "a<wait1s><return><wait2s>",
    "d<wait1s><return><wait2s>",
    "f<wait1s><return><wait2s>",
    "n<wait1s><return><wait2s>",
    "f<wait1s><return><wait2s>",
    "x<wait1s><return><wait2s>",
    "x<wait1s><return><wait2s>",
    "a<wait1s><return><wait300s>",
    "a<wait1s><return><wait2s>",
    "<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait5s>",
    "x<wait1s><return><wait5s>",
    "x<wait1s><return><wait5s>",
    "<return><wait5s>",
    "d<wait1s><return><wait80s>",
    "root<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "echo \"ip6addrctl=YES\" >> /etc/rc.conf<return><wait2s>",
    "echo \"ip6addrctl_policy=ipv4_prefer\" >> /etc/rc.conf<return><wait2s>",
    "reboot<wait1s><return><wait80s>",
    "root<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config<return><wait2s>",
    "echo \"sshd=YES\" >> /etc/rc.conf<return><wait2s>",
    "service sshd restart<return><wait30s>",
    "sed -i 's/#export PKG_PATH/export PKG_PATH/g' $HOME/.profile<return><wait2s>",
    "exit<return>",
  ]
}
# NetBSD: amd64 (VirtualBox @ MacOS @ Intel Core i7-4980HQ CPU @ 2.80GHz)
source "virtualbox-iso" "netbsd-amd64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  guest_os_type        = "NetBSD_64"
  iso_url              = "${local.netbsd-amd64-iso-url}"
  iso_checksum         = "${local.netbsd-amd64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "/sbin/poweroff"
  skip_export          = true
  headless             = true
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-medium}"
  disk_size            = "${var.disk-size}"
  usb                  = true
  guest_additions_mode = "disable"
  vboxmanage = [
    ["setextradata", "{{.Name}}", "GUI/ScaleFactor", "2"],
    ["modifyvm", "{{.Name}}", "--vrdeproperty", "VNCPassword=a"],
  ]
  boot_wait = "35s"
  boot_command = [
    "a<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "f<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "<return><wait2s>",
    "a<wait1s><return><wait20s>",
    "${var.vm-host-name}<return><wait2s>",
    "<return><wait2s>",
    "a<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "<return><wait2s>",
    "g<wait1s><return><wait2s>",
    "x<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "b<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "b<wait1s><return><wait2s>",
    "x<wait1s><return><wait2s>",
    "b<wait1s><return><wait6s>",
    "a<wait1s><return><wait2s>",
    "d<wait1s><return><wait2s>",
    "f<wait1s><return><wait2s>",
    "n<wait1s><return><wait2s>",
    "f<wait1s><return><wait2s>",
    "x<wait1s><return><wait2s>",
    "x<wait1s><return><wait2s>",
    "a<wait1s><return><wait120s>",
    "a<wait1s><return><wait2s>",
    "<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait5s>",
    "x<wait1s><return><wait5s>",
    "<return><wait5s>",
    #"<return><wait5s>",
    "d<wait1s><return><wait50s>",
    "root<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "echo \"ip6addrctl=YES\" >> /etc/rc.conf<return><wait2s>",
    "echo \"ip6addrctl_policy=ipv4_prefer\" >> /etc/rc.conf<return><wait2s>",
    "reboot<wait1s><return><wait50s>",
    "root<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config<return><wait2s>",
    "echo \"sshd=YES\" >> /etc/rc.conf<return><wait2s>",
    "service sshd restart<return><wait5s>",
    "sed -i 's/#export PKG_PATH/export PKG_PATH/g' $HOME/.profile<return><wait2s>",
    "exit<return>",
  ]
}
# TODO: NetBSD: arm64 (Qemu @ macOS @ >= Apple M1 Max)
# After the first question was answerd the screen shows "Display output is not active." is shown forever.
source "qemu" "netbsd-arm64" {
  vm_name          = "{{build_type}}.{{build_name}}.{{uuid}}"
  iso_url          = "${local.netbsd-arm64-iso-url}"
  iso_checksum     = "${local.netbsd-arm64-iso-checksum}"
  http_directory   = "${var.http-directory}"
  communicator     = "ssh"
  ssh_username     = "root"
  ssh_password     = "${var.root-password}"
  shutdown_command = "/sbin/poweroff"
  headless         = false
  qemu_binary      = "qemu-system-aarch64"
  machine_type     = "virt"
  cpus             = "${var.cpu-count}"
  memory           = "${var.memory-medium}"
  disk_size        = "${var.disk-size}"
  format           = "qcow2"
  disk_interface   = "virtio"
  net_device       = "virtio-net"
  display          = "cocoa"
  firmware         = "${var.qemu-arm64-firmware-path}"
  cpu_model        = "${var.qemu-arm64-cpu}"
  accelerator      = "${var.qemu-arm64-accelerator}"
  qemuargs = [
    ["-boot", "c"],
    ["-device", "qemu-xhci"],
    ["-device", "usb-kbd"],
    ["-device", "virtio-gpu-pci"],
  ]
  skip_compaction = true
  boot_wait       = "30s"
  boot_command = [
    "a<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "f<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "<return><wait2s>",
    "a<wait1s><return><wait15s>",
    "${var.vm-host-name}<return><wait2s>",
    "<return><wait2s>",
    "a<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "<return><wait2s>",
    "g<wait1s><return><wait2s>",
    "x<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "b<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "b<wait1s><return><wait2s>",
    "x<wait1s><return><wait2s>",
    "b<wait1s><return><wait6s>",
    "a<wait1s><return><wait2s>",
    "d<wait1s><return><wait2s>",
    "f<wait1s><return><wait2s>",
    "n<wait1s><return><wait2s>",
    "f<wait1s><return><wait2s>",
    "x<wait1s><return><wait2s>",
    "x<wait1s><return><wait2s>",
    "a<wait1s><return><wait300s>",
    "a<wait1s><return><wait2s>",
    "<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait5s>",
    "x<wait1s><return><wait5s>",
    "x<wait1s><return><wait5s>",
    "<return><wait5s>",
    "d<wait1s><return><wait80s>",
    "root<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "echo \"ip6addrctl=YES\" >> /etc/rc.conf<return><wait2s>",
    "echo \"ip6addrctl_policy=ipv4_prefer\" >> /etc/rc.conf<return><wait2s>",
    "reboot<wait1s><return><wait80s>",
    "root<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config<return><wait2s>",
    "echo \"sshd=YES\" >> /etc/rc.conf<return><wait2s>",
    "service sshd restart<return><wait30s>",
    "sed -i 's/#export PKG_PATH/export PKG_PATH/g' $HOME/.profile<return><wait2s>",
    "exit<return>",
  ]
}
# TODO: NetBSD: arm64 (Virtualbox @ macOS @ >= Apple M1 Max)
# VirtualBox suddenly closes at the end of the OpenBSD bootloader.
source "virtualbox-iso" "netbsd-arm64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  guest_os_type        = "FreeBSD_arm64"
  iso_url              = "${local.netbsd-arm64-iso-url}"
  iso_checksum         = "${local.netbsd-arm64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "/sbin/poweroff"
  skip_export          = true
  headless             = false
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-medium}"
  iso_interface        = "virtio"
  hard_drive_interface = "virtio"
  disk_size            = "${var.disk-size}"
  usb                  = true
  gfx_controller       = "vmsvga"
  gfx_vram_size        = "16"
  guest_additions_mode = "disable"
  vboxmanage = [
    ["setextradata", "{{.Name}}", "GUI/ScaleFactor", "2"],
    ["modifyvm", "{{.Name}}", "--vrdeproperty", "VNCPassword=a"],
  ]
  boot_wait = "60s"
  boot_command = [
    "a<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "f<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "<return><wait2s>",
    "a<wait1s><return><wait20s>",
    "${var.vm-host-name}<return><wait2s>",
    "<return><wait2s>",
    "a<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "<return><wait2s>",
    "g<wait1s><return><wait2s>",
    "x<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "b<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "b<wait1s><return><wait2s>",
    "x<wait1s><return><wait2s>",
    "b<wait1s><return><wait6s>",
    "a<wait1s><return><wait2s>",
    "d<wait1s><return><wait2s>",
    "f<wait1s><return><wait2s>",
    "n<wait1s><return><wait2s>",
    "f<wait1s><return><wait2s>",
    "x<wait1s><return><wait2s>",
    "x<wait1s><return><wait2s>",
    "a<wait1s><return><wait120s>",
    "a<wait1s><return><wait2s>",
    "<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait5s>",
    "x<wait1s><return><wait5s>",
    "<return><wait5s>",
    #"<return><wait5s>",
    "d<wait1s><return><wait50s>",
    "root<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "echo \"ip6addrctl=YES\" >> /etc/rc.conf<return><wait2s>",
    "echo \"ip6addrctl_policy=ipv4_prefer\" >> /etc/rc.conf<return><wait2s>",
    "reboot<wait1s><return><wait50s>",
    "root<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config<return><wait2s>",
    "echo \"sshd=YES\" >> /etc/rc.conf<return><wait2s>",
    "service sshd restart<return><wait5s>",
    "sed -i 's/#export PKG_PATH/export PKG_PATH/g' $HOME/.profile<return><wait2s>",
    "exit<return>",
  ]
}
# NetBSD: arm64 (VMware Fusion @ macOS @ >= Apple M1 Max)
source "vmware-iso" "netbsd-arm64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  version              = "20"
  iso_url              = "${local.netbsd-arm64-iso-url}"
  iso_checksum         = "${local.netbsd-arm64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  vnc_disable_password = true
  shutdown_command     = "/sbin/poweroff"
  keep_registered      = false
  skip_export          = true
  headless             = true
  format               = "vmx"
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-medium}"
  disk_size            = "${var.disk-size}"
  disk_adapter_type    = "nvme"
  disk_type_id         = "0"
  network_adapter_type = "e1000e"
  usb                  = true
  guest_os_type        = "arm-other-64"
  vmx_data = {
    "firmware"         = "efi"
    "architecture"     = "arm-other-64"
    "usb_xhci.present" = "TRUE"
  }
  skip_compaction = true
  boot_wait       = "15s"
  boot_command = [
    "1<wait5s>",
    "a<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "b<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "a<wait1s><return><wait2s>",
    "b<wait1s><return><wait2s>",
    "x<wait1s><return><wait2s>",
    "b<wait1s><return><wait6s>",
    "d<wait1s><return><wait2s>",
    "f<wait1s><return><wait2s>",
    "n<wait1s><return><wait2s>",
    "f<wait1s><return><wait2s>",
    "x<wait1s><return><wait2s>",
    "x<wait1s><return><wait2s>",
    "a<wait1s><return><wait40s>",
    "<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "x<wait1s><return><wait5s>",
    "x<wait1s><return><wait10s>",
    "<return><wait10s>",
    "d<wait1s><return><wait100s>",
    "root<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config<return><wait2s>",
    "service sshd restart<return><wait2s>",
    "sed -i 's/#export PKG_PATH/export PKG_PATH/g' $HOME/.profile<return><wait2s>",
    "exit<return>",
  ]
}
# NetBSD: arm64 (VMware ESXi @ RPI5 8GB)
source "vsphere-iso" "netbsd-arm64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  vcenter_server       = "${var.esxi-host}"
  host                 = "${var.esxi-host}"
  username             = "${var.esxi-username}"
  password             = "${var.esxi-password}"
  insecure_connection  = true
  iso_paths            = [format("[datastore1] NetBSD-%s-evbarm-aarch64.iso", "${var.netbsd-version}")]
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "/sbin/poweroff"
  CPUs                 = "${var.cpu-count}"
  RAM                  = "${var.memory-medium}"
  RAM_reserve_all      = true
  disk_controller_type = ["nvme"]
  storage {
    disk_size = "${var.disk-size}"
  }
  network_adapters {
    network      = "VM Network"
    network_card = "e1000e"
  }
  guest_os_type  = "other4xLinux64Guest"
  cdrom_type     = "sata"
  destroy        = true
  usb_controller = ["xhci"]
  boot_wait      = "20s"
  boot_command = [
    "1<wait10s>",
    "a<wait1s><return><wait5s>",
    "a<wait1s><return><wait5s>",
    "b<wait1s><return><wait5s>",
    "a<wait1s><return><wait5s>",
    "a<wait1s><return><wait5s>",
    "b<wait1s><return><wait5s>",
    "x<wait1s><return><wait5s>",
    "b<wait1s><return><wait30s>",
    "d<wait1s><return><wait5s>",
    "f<wait1s><return><wait2s>",
    "n<wait1s><return><wait5s>",
    "f<wait1s><return><wait5s>",
    "x<wait1s><return><wait5s>",
    "x<wait1s><return><wait5s>",
    "a<wait1s><return><wait120s>",
    "<return><wait5s>",
    "${var.root-password}<return><wait5s>",
    "${var.root-password}<return><wait5s>",
    "${var.root-password}<return><wait5s>",
    "x<wait1s><return><wait5s>",
    "x<wait1s><return><wait10s>",
    "<return><wait10s>",
    "d<wait1s><return><wait100s>",
    "root<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config<return><wait2s>",
    "service sshd restart<return><wait2s>",
    "sed -i 's/#export PKG_PATH/export PKG_PATH/g' $HOME/.profile<return><wait2s>",
    ". $HOME/.profile<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "pkg_add pkgin<return><wait20s>",
    "pkgin -y install open-vm-tools<return><wait200s>",
    "cp /usr/pkg/share/examples/rc.d/vmtools /etc/rc.d/vmtools<return><wait5s>",
    "echo 'vmtools=YES' >> /etc/rc.conf<return><wait2s>",
    "service vmtools start<return><wait2s>",
    "exit<return>",
  ]
}

################################################################################
# NixOS definition of the sources (platforms)
################################################################################
# TODO: NixOS: amd64 (Qemu @ FreeBSD @ Intel Xeon E5-2640 v4)
source "qemu" "nixos-amd64" {
  vm_name          = "{{build_type}}.{{build_name}}.{{uuid}}"
  iso_url          = "${local.nixos-amd64-iso-url}"
  iso_checksum     = "${local.nixos-amd64-iso-checksum}"
  http_directory   = "${var.http-directory}"
  communicator     = "ssh"
  ssh_username     = "root"
  ssh_password     = "${var.root-password}"
  shutdown_command = "poweroff"
  headless         = true
  qemu_binary      = "qemu-system-x86_64"
  machine_type     = "pc"
  cpus             = "${var.cpu-count}"
  memory           = "${var.memory-medium}"
  disk_size        = "${var.disk-size}"
  format           = "qcow2"
  disk_interface   = "virtio"
  net_device       = "virtio-net"
  display          = "none"
  skip_compaction  = true
  boot_wait        = "100s"
  boot_command = [
    # Become root
    "sudo su --login<return><wait1s>",
    "passwd root<return><wait1s>",
    "root<return><wait1s>",
    "root<return><wait1s>",
    # Preparing installation disk
    "parted /dev/${var.nixos-qemu-disk} -- mklabel msdos<return><wait2s>",
    "parted /dev/${var.nixos-qemu-disk} -- mkpart primary 1MB -8GB<return><wait2s>",
    "parted /dev/${var.nixos-qemu-disk} -- set 1 boot on<return><wait2s>",
    "parted /dev/${var.nixos-qemu-disk} -- mkpart primary linux-swap -8GB 100%<return><wait2s>",
    "mkfs.ext4 -L nixos /dev/${var.nixos-qemu-disk}1<return><wait2s>",
    "mkswap -L swap /dev/${var.nixos-qemu-disk}2<return><wait2s>",
    "mount /dev/disk/by-label/nixos /mnt<return><wait2s>",
    "swapon /dev/${var.nixos-qemu-disk}2<return><wait2s>",
    # Generate and configure installation config
    "nixos-generate-config --root /mnt<return><wait5s>",
    "sed -i 's/# services.openssh.enable = true;/services.openssh.enable = true;\\n  services.openssh.settings.PermitRootLogin = \"yes\";/g' /mnt/etc/nixos/configuration.nix<return><wait2s>",
    "sed -i 's/# boot.loader.grub.device/boot.loader.grub.device/g' /mnt/etc/nixos/configuration.nix<return><wait2s>",
    "sed -i 's/sda/${var.nixos-qemu-disk}/g' /mnt/etc/nixos/configuration.nix<return><wait2s>",
    # Install OS
    "nixos-install<return><wait2100s>",
    "${var.root-password}<return><wait1s>",
    "${var.root-password}<return><wait1s>",
    # Reboot into the installed OS
    "reboot<return><wait70s>",
  ]
}
# NixOS: amd64 (VirtualBox @ MacOS @ Intel Core i7-4980HQ CPU @ 2.80GHz)
source "virtualbox-iso" "nixos-amd64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  guest_os_type        = "Linux_64"
  iso_url              = "${local.nixos-amd64-iso-url}"
  iso_checksum         = "${local.nixos-amd64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "poweroff"
  skip_export          = true
  headless             = true
  firmware             = "efi"
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-medium}"
  disk_size            = "${var.disk-size}"
  usb                  = true
  guest_additions_mode = "disable"
  vboxmanage = [
    ["setextradata", "{{.Name}}", "GUI/ScaleFactor", "2"],
    ["modifyvm", "{{.Name}}", "--vrdeproperty", "VNCPassword=a"],
  ]
  boot_wait = "55s"
  boot_command = [
    # Become root
    "sudo su --login<return><wait1s>",
    "passwd root<return><wait1s>",
    "root<return><wait1s>",
    "root<return><wait1s>",
    # Preparing installation disk
    "parted /dev/${var.nixos-virtualbox-disk} -- mklabel gpt<return><wait2s>",
    "parted /dev/${var.nixos-virtualbox-disk} -- mkpart root ext4 512MB -8GB<return><wait2s>",
    "parted /dev/${var.nixos-virtualbox-disk} -- mkpart swap linux-swap -8GB 100%<return><wait2s>",
    "parted /dev/${var.nixos-virtualbox-disk} -- mkpart ESP fat32 1MB 512MB<return><wait2s>",
    "parted /dev/${var.nixos-virtualbox-disk} -- set 3 esp on<return><wait2s>",
    "mkfs.ext4 -L nixos /dev/${var.nixos-virtualbox-disk}1<return><wait4s>",
    "mkswap -L swap /dev/${var.nixos-virtualbox-disk}2<return><wait4s>",
    "mkfs.fat -F 32 -n boot /dev/${var.nixos-virtualbox-disk}3<return><wait4s>",
    "mount /dev/disk/by-label/nixos /mnt<return><wait2s>",
    "mkdir -p /mnt/boot<return><wait2s>",
    "mount -o umask=077 /dev/disk/by-label/boot /mnt/boot<return><wait2s>",
    "swapon /dev/${var.nixos-virtualbox-disk}2<return><wait2s>",
    # Generate and configure installation config
    "nixos-generate-config --root /mnt<return><wait2s>",
    "sed -i 's/# services.openssh.enable = true;/services.openssh.enable = true;\\n  services.openssh.settings.PermitRootLogin = \"yes\";/g' /mnt/etc/nixos/configuration.nix<return><wait2s>",
    # Install OS
    "nixos-install<return><wait240s>",
    "${var.root-password}<return><wait1s>",
    "${var.root-password}<return><wait1s>",
    # Reboot into the installed OS
    "reboot<return><wait60s>",
  ]
}
# NixOS: arm64 (Qemu @ macOS @ >= Apple M1 Max)
source "qemu" "nixos-arm64" {
  vm_name          = "{{build_type}}.{{build_name}}.{{uuid}}"
  iso_url          = "${local.nixos-arm64-iso-url}"
  iso_checksum     = "${local.nixos-arm64-iso-checksum}"
  http_directory   = "${var.http-directory}"
  communicator     = "ssh"
  ssh_username     = "root"
  ssh_password     = "${var.root-password}"
  shutdown_command = "poweroff"
  headless         = false
  qemu_binary      = "qemu-system-aarch64"
  machine_type     = "virt"
  cpus             = "${var.cpu-count}"
  memory           = "${var.memory-medium}"
  disk_size        = "${var.disk-size}"
  format           = "qcow2"
  disk_interface   = "virtio"
  net_device       = "virtio-net"
  display          = "cocoa"
  firmware         = "${var.qemu-arm64-firmware-path}"
  cpu_model        = "${var.qemu-arm64-cpu}"
  accelerator      = "${var.qemu-arm64-accelerator}"
  qemuargs = [
    ["-boot", "c"],
    ["-device", "qemu-xhci"],
    ["-device", "usb-kbd"],
    ["-device", "virtio-gpu-pci"],
  ]
  skip_compaction = true
  boot_wait       = "45s"
  boot_command = [
    # Become root
    "sudo su --login<return><wait1s>",
    "passwd root<return><wait1s>",
    "root<return><wait1s>",
    "root<return><wait1s>",
    # Preparing installation disk
    "parted /dev/${var.nixos-qemu-disk} -- mklabel gpt<return><wait2s>",
    "parted /dev/${var.nixos-qemu-disk} -- mkpart root ext4 512MB -8GB<return><wait2s>",
    "parted /dev/${var.nixos-qemu-disk} -- mkpart swap linux-swap -8GB 100%<return><wait2s>",
    "parted /dev/${var.nixos-qemu-disk} -- mkpart ESP fat32 1MB 512MB<return><wait2s>",
    "parted /dev/${var.nixos-qemu-disk} -- set 3 esp on<return><wait2s>",
    "mkfs.ext4 -L nixos /dev/${var.nixos-qemu-disk}1<return><wait4s>",
    "mkswap -L swap /dev/${var.nixos-qemu-disk}2<return><wait4s>",
    "mkfs.fat -F 32 -n boot /dev/${var.nixos-qemu-disk}3<return><wait4s>",
    "mount /dev/disk/by-label/nixos /mnt<return><wait2s>",
    "mkdir -p /mnt/boot<return><wait2s>",
    "mount -o umask=077 /dev/disk/by-label/boot /mnt/boot<return><wait2s>",
    "swapon /dev/${var.nixos-qemu-disk}2<return><wait2s>",
    # Generate and configure installation config
    "nixos-generate-config --root /mnt<return><wait2s>",
    "sed -i 's/# services.openssh.enable = true;/services.openssh.enable = true;\\n  services.openssh.settings.PermitRootLogin = \"yes\";/g' /mnt/etc/nixos/configuration.nix<return><wait2s>",
    # Install OS
    "nixos-install<return><wait55s>",
    "${var.root-password}<return><wait1s>",
    "${var.root-password}<return><wait1s>",
    # Reboot into the installed OS
    "reboot<return><wait35s>",
  ]
}
# TODO: NixOS: arm64 (Virtualbox @ macOS @ >= Apple M1 Max)
source "virtualbox-iso" "nixos-arm64" {
  vm_name          = "{{build_type}}.{{build_name}}.{{uuid}}"
  guest_os_type    = "Other_arm64"
  iso_url          = "${local.nixos-arm64-iso-url}"
  iso_checksum     = "${local.nixos-arm64-iso-checksum}"
  http_directory   = "${var.http-directory}"
  ssh_username     = "root"
  ssh_password     = "${var.root-password}"
  shutdown_command = "poweroff"
  skip_export      = true
  headless         = false
  #firmware             = "efi"
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-medium}"
  iso_interface        = "virtio"
  hard_drive_interface = "virtio"
  disk_size            = "${var.disk-size}"
  usb                  = true
  gfx_controller       = "vmsvga"
  gfx_vram_size        = "16"
  guest_additions_mode = "disable"
  vboxmanage = [
    ["setextradata", "{{.Name}}", "GUI/ScaleFactor", "2"],
    ["modifyvm", "{{.Name}}", "--vrdeproperty", "VNCPassword=a"],
    ["modifyvm", "{{.Name}}", "--ioapic=on"],
    ["storagectl", "{{.Name}}", "--name", "IDE Controller", "--remove"],
  ]
  boot_wait = "55s"
  boot_command = [
    # Become root
    "sudo su --login<return><wait1s>",
    "passwd root<return><wait1s>",
    "root<return><wait1s>",
    "root<return><wait1s>",
    # Preparing installation disk
    "parted /dev/${var.nixos-virtualbox-disk} -- mklabel msdos<return><wait2s>",
    "parted /dev/${var.nixos-virtualbox-disk} -- mkpart primary 1MB -8GB<return><wait2s>",
    "parted /dev/${var.nixos-virtualbox-disk} -- set 1 boot on<return><wait2s>",
    "parted /dev/${var.nixos-virtualbox-disk} -- mkpart primary linux-swap -8GB 100%<return><wait2s>",
    "mkfs.ext4 -L nixos /dev/${var.nixos-virtualbox-disk}1<return><wait2s>",
    "mkswap -L swap /dev/${var.nixos-virtualbox-disk}2<return><wait2s>",
    "mount /dev/disk/by-label/nixos /mnt<return><wait2s>",
    "swapon /dev/${var.nixos-virtualbox-disk}2<return><wait2s>",
    # Generate and configure installation config
    "nixos-generate-config --root /mnt<return><wait5s>",
    "sed -i 's/# services.openssh.enable = true;/services.openssh.enable = true;\\n  services.openssh.settings.PermitRootLogin = \"yes\";/g' /mnt/etc/nixos/configuration.nix<return><wait2s>",
    "sed -i 's/# boot.loader.grub.device/boot.loader.grub.device/g' /mnt/etc/nixos/configuration.nix<return><wait2s>",
    # Install OS
    "nixos-install<return><wait2100s>",
    "${var.root-password}<return><wait1s>",
    "${var.root-password}<return><wait1s>",
    # Reboot into the installed OS
    "reboot<return><wait60s>",
  ]
}
# NixOS: arm64 (VMware Fusion @ macOS @ >= Apple M1 Max)
source "vmware-iso" "nixos-arm64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  version              = "20"
  iso_url              = "${local.nixos-arm64-iso-url}"
  iso_checksum         = "${local.nixos-arm64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  vnc_disable_password = true
  shutdown_command     = "poweroff"
  keep_registered      = false
  skip_export          = true
  headless             = true
  format               = "vmx"
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-medium}"
  disk_size            = "${var.disk-size}"
  disk_adapter_type    = "nvme"
  disk_type_id         = "0"
  network_adapter_type = "e1000e"
  usb                  = true
  guest_os_type        = "arm-other-64"
  vmx_data = {
    "firmware"         = "efi"
    "architecture"     = "arm-other-64"
    "usb_xhci.present" = "TRUE"
  }
  skip_compaction = true
  boot_wait       = "35s"
  boot_command = [
    # Become root
    "sudo su --login<return><wait1s>",
    "passwd root<return><wait1s>",
    "root<return><wait1s>",
    "root<return><wait1s>",
    # Preparing installation disk
    "parted /dev/${var.nixos-vmware-disk} -- mklabel gpt<return><wait2s>",
    "parted /dev/${var.nixos-vmware-disk} -- mkpart root ext4 512MB -8GB<return><wait2s>",
    "parted /dev/${var.nixos-vmware-disk} -- mkpart swap linux-swap -8GB 100%<return><wait2s>",
    "parted /dev/${var.nixos-vmware-disk} -- mkpart ESP fat32 1MB 512MB<return><wait2s>",
    "parted /dev/${var.nixos-vmware-disk} -- set 3 esp on<return><wait2s>",
    "mkfs.ext4 -L nixos /dev/${var.nixos-vmware-disk}p1<return><wait4s>",
    "mkswap -L swap /dev/${var.nixos-vmware-disk}p2<return><wait4s>",
    "mkfs.fat -F 32 -n boot /dev/${var.nixos-vmware-disk}p3<return><wait4s>",
    "mount /dev/disk/by-label/nixos /mnt<return><wait2s>",
    "mkdir -p /mnt/boot<return><wait2s>",
    "mount -o umask=077 /dev/disk/by-label/boot /mnt/boot<return><wait2s>",
    "swapon /dev/${var.nixos-vmware-disk}p2<return><wait2s>",
    # Generate and configure installation config
    "nixos-generate-config --root /mnt<return><wait2s>",
    "sed -i 's/# services.openssh.enable = true;/services.openssh.enable = true;\\n  services.openssh.settings.PermitRootLogin = \"yes\";/g' /mnt/etc/nixos/configuration.nix<return><wait2s>",
    # Install OS
    "nixos-install<return><wait140s>",
    "${var.root-password}<return><wait1s>",
    "${var.root-password}<return><wait1s>",
    # Reboot into the installed OS
    "reboot<return><wait35s>",
  ]
}
# NixOS: arm64 (VMware ESXi @ RPI5 8GB)
source "vsphere-iso" "nixos-arm64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  vcenter_server       = "${var.esxi-host}"
  host                 = "${var.esxi-host}"
  username             = "${var.esxi-username}"
  password             = "${var.esxi-password}"
  insecure_connection  = true
  iso_paths            = [format("[datastore1] nixos-minimal-%s.%s-aarch64-linux.iso", "${var.nixos-version}", "${var.nixos-hash}")]
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "poweroff"
  CPUs                 = "${var.cpu-count}"
  RAM                  = "${var.memory-medium}"
  RAM_reserve_all      = true
  disk_controller_type = ["nvme"]
  storage {
    disk_size = "${var.disk-size}"
  }
  network_adapters {
    network      = "VM Network"
    network_card = "e1000e"
  }
  guest_os_type  = "other4xLinux64Guest"
  cdrom_type     = "sata"
  destroy        = true
  usb_controller = ["xhci"]
  boot_wait      = "40s"
  boot_command = [
    # Become root
    "sudo su --login<return><wait1s>",
    "passwd root<return><wait1s>",
    "root<return><wait1s>",
    "root<return><wait1s>",
    # Preparing installation disk
    "parted /dev/${var.nixos-vsphere-disk} -- mklabel gpt<return><wait2s>",
    "parted /dev/${var.nixos-vsphere-disk} -- mkpart root ext4 512MB -8GB<return><wait2s>",
    "parted /dev/${var.nixos-vsphere-disk} -- mkpart swap linux-swap -8GB 100%<return><wait2s>",
    "parted /dev/${var.nixos-vsphere-disk} -- mkpart ESP fat32 1MB 512MB<return><wait2s>",
    "parted /dev/${var.nixos-vsphere-disk} -- set 3 esp on<return><wait2s>",
    "mkfs.ext4 -L nixos /dev/${var.nixos-vsphere-disk}p1<return><wait4s>",
    "mkswap -L swap /dev/${var.nixos-vsphere-disk}p2<return><wait4s>",
    "mkfs.fat -F 32 -n boot /dev/${var.nixos-vsphere-disk}p3<return><wait4s>",
    "mount /dev/disk/by-label/nixos /mnt<return><wait2s>",
    "mkdir -p /mnt/boot<return><wait2s>",
    "mount -o umask=077 /dev/disk/by-label/boot /mnt/boot<return><wait2s>",
    "swapon /dev/${var.nixos-vsphere-disk}p2<return><wait2s>",
    # Generate and configure installation config
    "nixos-generate-config --root /mnt<return><wait2s>",
    "sed -i 's/# services.openssh.enable = true;/services.openssh.enable = true;\\n  services.openssh.settings.PermitRootLogin = \"yes\";\\n  virtualisation.vmware.guest.enable = true;\\n  virtualisation.vmware.guest.headless = true;/g' /mnt/etc/nixos/configuration.nix<return><wait2s>",
    # Install OS
    "nixos-install<return><wait420s>",
    "${var.root-password}<return><wait1s>",
    "${var.root-password}<return><wait1s>",
    # Reboot into the installed OS
    "reboot<return><wait50s>",
  ]
}

################################################################################
# OpenBSD definition of the sources (platforms)
################################################################################
# OpenBSD: arm64 (Hetzner Cloud CAX11)
source "hcloud" "openbsd-arm64" {
  token        = "${var.hcloud_token}"
  image        = "${var.openbsd-arm64-hcloud-id}"
  location     = "nbg1"
  server_type  = "cax11"
  ssh_username = "root"
}
# OpenBSD: amd64 (Qemu @ FreeBSD @ Intel Xeon E5-2640 v4)
source "qemu" "openbsd-amd64" {
  vm_name          = "{{build_type}}.{{build_name}}.{{uuid}}"
  iso_url          = "${local.openbsd-amd64-iso-url}"
  iso_checksum     = "${local.openbsd-amd64-iso-checksum}"
  http_directory   = "${var.http-directory}"
  communicator     = "ssh"
  ssh_username     = "root"
  ssh_password     = "${var.root-password}"
  shutdown_command = "/sbin/halt -p"
  headless         = true
  qemu_binary      = "qemu-system-x86_64"
  machine_type     = "pc"
  cpus             = "${var.cpu-count}"
  memory           = "${var.memory-medium}"
  disk_size        = "${var.disk-size}"
  format           = "qcow2"
  disk_interface   = "virtio"
  net_device       = "virtio-net"
  display          = "none"
  skip_compaction  = true
  boot_wait        = "40s"
  boot_command = [
    # Configure system for unattanded installation
    "autoinstall<return><wait2s>",
    "http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<enter><wait2s>",
    # Run the unattanded installation
    "install<return><wait480s>",
  ]
}
# OpenBSD: amd64 (VirtualBox @ MacOS @ Intel Core i7-4980HQ CPU @ 2.80GHz)
source "virtualbox-iso" "openbsd-amd64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  guest_os_type        = "OpenBSD_64"
  iso_url              = "${local.openbsd-amd64-iso-url}"
  iso_checksum         = "${local.openbsd-amd64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "/sbin/halt -p"
  skip_export          = true
  headless             = true
  iso_interface        = "sata"
  hard_drive_interface = "sata"
  cpus                 = "1"
  memory               = "${var.memory-medium}"
  disk_size            = "${var.disk-size}"
  usb                  = true
  guest_additions_mode = "disable"
  vboxmanage = [
    ["setextradata", "{{.Name}}", "GUI/ScaleFactor", "2"],
    ["modifyvm", "{{.Name}}", "--vrdeproperty", "VNCPassword=a"],
    ["modifyvm", "{{.Name}}", "--ioapic=off"],
  ]
  boot_wait = "65s"
  boot_command = [
    # Configure system for unattanded installation
    "autoinstall<return><wait5s>",
    "http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<enter><wait5s>",
    # Run the unattanded installation
    "install<return><wait300s>",
  ]
}
# TODO: OpenBSD: arm64 (Qemu @ macOS @ >= Apple M1 Max)
# The CD drive is not /dev/cd0!!!
source "qemu" "openbsd-arm64" {
  vm_name          = "{{build_type}}.{{build_name}}.{{uuid}}"
  iso_url          = "${local.openbsd-arm64-iso-url}"
  iso_checksum     = "${local.openbsd-arm64-iso-checksum}"
  http_directory   = "${var.http-directory}"
  communicator     = "ssh"
  ssh_username     = "root"
  ssh_password     = "${var.root-password}"
  shutdown_command = "/sbin/halt -p"
  headless         = false
  qemu_binary      = "qemu-system-aarch64"
  machine_type     = "virt"
  cpus             = "${var.cpu-count}"
  memory           = "${var.memory-medium}"
  disk_size        = "${var.disk-size}"
  format           = "qcow2"
  disk_interface   = "virtio"
  net_device       = "virtio-net"
  display          = "cocoa"
  firmware         = "${var.qemu-arm64-firmware-path}"
  cpu_model        = "${var.qemu-arm64-cpu}"
  accelerator      = "${var.qemu-arm64-accelerator}"
  qemuargs = [
    ["-boot", "c"],
    ["-device", "qemu-xhci"],
    ["-device", "usb-kbd"],
    ["-device", "virtio-gpu-pci"],
    #["-global", "virtio-pci.disable-modern=on"],
    #["-device", "virtio-scsi-pci"],
    #["-device", "scsi-cd,drive=cd"],
  ]
  skip_compaction = true
  boot_wait       = "40s"
  boot_command = [
    # Configure system for unattanded installation
    "autoinstall<return><wait2s>",
    "http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<enter><wait2s>",
    # Run the unattanded installation
    "install<return><wait480s>",
  ]
}
# TODO: OpenBSD: arm64 (Virtualbox @ macOS @ >= Apple M1 Max)
# VirtualBox suddenly closes at the end of the OpenBSD bootloader.
source "virtualbox-iso" "openbsd-arm64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  guest_os_type        = "OpenBSD_arm64"
  iso_url              = "${local.openbsd-arm64-iso-url}"
  iso_checksum         = "${local.openbsd-arm64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "/sbin/halt -p"
  skip_export          = true
  headless             = false
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-medium}"
  iso_interface        = "virtio"
  hard_drive_interface = "virtio"
  disk_size            = "${var.disk-size}"
  usb                  = true
  gfx_controller       = "vmsvga"
  gfx_vram_size        = "16"
  guest_additions_mode = "disable"
  vboxmanage = [
    ["setextradata", "{{.Name}}", "GUI/ScaleFactor", "2"],
    ["modifyvm", "{{.Name}}", "--vrdeproperty", "VNCPassword=a"],
  ]
  boot_wait = "30s"
  boot_command = [
    # Configure system for unattanded installation
    "autoinstall<return><wait2s>",
    "http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<enter><wait2s>",
    # Run the unattanded installation
    "install<return><wait120s>",
  ]
}
# OpenBSD: arm64 (VMware Fusion @ macOS @ >= Apple M1 Max)
source "vmware-iso" "openbsd-arm64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  version              = "20"
  iso_url              = "${local.openbsd-arm64-iso-url}"
  iso_checksum         = "${local.openbsd-arm64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  vnc_disable_password = true
  shutdown_command     = "/sbin/halt -p"
  keep_registered      = false
  skip_export          = true
  headless             = true
  format               = "vmx"
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-medium}"
  disk_size            = "${var.disk-size}"
  disk_adapter_type    = "nvme"
  disk_type_id         = "0"
  network_adapter_type = "e1000e"
  usb                  = true
  guest_os_type        = "arm-other-64"
  vmx_data = {
    "firmware"         = "efi"
    "architecture"     = "arm-other-64"
    "usb_xhci.present" = "TRUE"
  }
  skip_compaction = true
  boot_wait       = "30s"
  boot_command = [
    # Configure system for unattanded installation
    "autoinstall<return><wait2s>",
    "http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<enter><wait2s>",
    # Run the unattanded installation
    "install<return><wait120s>",
  ]
}
# TODO: OpenBSD: arm64 (VMware ESXi @ RPI5 8GB)
# We have to wait till the vmt driver is ported to arm64:
# https://man.openbsd.org/vmt.4
# https://github.com/openbsd/src/blob/master/sys/dev/pv/vmt.c
source "vsphere-iso" "openbsd-arm64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  vcenter_server       = "${var.esxi-host}"
  host                 = "${var.esxi-host}"
  username             = "${var.esxi-username}"
  password             = "${var.esxi-password}"
  insecure_connection  = true
  iso_paths            = [format("[datastore1] install%s.iso", replace("${var.openbsd-version}", ".", ""))]
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "/sbin/halt -p"
  CPUs                 = "${var.cpu-count}"
  RAM                  = "${var.memory-medium}"
  RAM_reserve_all      = true
  disk_controller_type = ["nvme"]
  storage {
    disk_size = "${var.disk-size}"
  }
  network_adapters {
    network      = "VM Network"
    network_card = "e1000e"
  }
  guest_os_type  = "other4xLinux64Guest"
  cdrom_type     = "sata"
  destroy        = true
  usb_controller = ["xhci"]
  boot_wait      = "30s"
  boot_command = [
    # Configure system for unattanded installation
    "autoinstall<return><wait2s>",
    "http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<enter><wait2s>",
    # Run the unattanded installation
    "install<return><wait100s>",
  ]
}

################################################################################
# OpenIndiana definition of the sources (platforms)
################################################################################
# OpenIndiana: amd64 (VirtualBox @ MacOS @ Intel Core i7-4980HQ CPU @ 2.80GHz)
source "virtualbox-iso" "openindiana-amd64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  guest_os_type        = "OpenSolaris_64"
  iso_url              = "${local.openindiana-amd64-iso-url}"
  iso_checksum         = "${local.openindiana-amd64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "/usr/sbin/poweroff"
  skip_export          = true
  headless             = true
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-medium}"
  disk_size            = "${var.disk-size}"
  usb                  = true
  guest_additions_mode = "disable"
  vboxmanage = [
    ["setextradata", "{{.Name}}", "GUI/ScaleFactor", "2"],
    ["modifyvm", "{{.Name}}", "--vrdeproperty", "VNCPassword=a"],
  ]
  boot_wait = "40s"
  boot_command = [
    "47<enter><wait4s>",
    "7<enter><wait15s>",
    "1<enter><wait15s>",
    "<f2><enter><wait2s>",
    "<f2><wait2s><tab><wait2s><enter><wait2s>",
    "<f2><enter><wait2s>",
    "<down><wait2s><f2><wait2s><enter><wait2s>",
    "<f2><enter><wait2s>",
    "<f2><enter><wait2s>",
    "${var.root-password}<wait2s><down><wait2s>",
    "${var.root-password}<wait2s><f2><wait2s>",
    "<f2><enter><wait720s>",
    "<f8><enter><wait110s>",
    "root<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "sed -i 's/PermitRootLogin no/PermitRootLogin yes/g' /etc/ssh/sshd_config<return><wait2s>",
    "svcadm restart ssh<return><wait2s>",
    "exit<return><wait2s>",
    "<wait99999s>",
  ]
}

################################################################################
# openSUSE definition of the sources (platforms)
################################################################################
# openSUSE: amd64 (Qemu @ FreeBSD @ Intel Xeon E5-2640 v4)
source "qemu" "opensuse-amd64" {
  vm_name          = "{{build_type}}.{{build_name}}.{{uuid}}"
  iso_url          = "${local.opensuse-amd64-iso-url}"
  iso_checksum     = "${local.opensuse-amd64-iso-checksum}"
  http_directory   = "${var.http-directory}"
  communicator     = "ssh"
  ssh_username     = "root"
  ssh_password     = "${var.root-password}"
  shutdown_command = "/usr/sbin/poweroff"
  headless         = true
  qemu_binary      = "qemu-system-x86_64"
  machine_type     = "pc"
  cpus             = "${var.cpu-count}"
  memory           = "${var.memory-medium}"
  disk_size        = "${var.disk-size}"
  format           = "qcow2"
  disk_interface   = "virtio"
  net_device       = "virtio-net"
  display          = "none"
  skip_compaction  = true
  boot_wait        = "10s"
  boot_command = [
    "<down><wait2s><return><wait300s>",
    "<leftAltOn>n<leftAltOff><wait20s>",
    "<leftAltOn>y<leftAltOff><wait10s>",
    "<leftAltOn>n<leftAltOff><wait180s>",
    "<leftAltOn>s<leftAltOff><wait5s><leftAltOn>n<leftAltOff><wait30s>",
    "<leftAltOn>n<leftAltOff><wait20s>",
    "<leftAltOn>n<leftAltOff><wait20s>",
    "<leftAltOn>s<leftAltOff><tab><tab><tab><tab><tab><tab><tab><return><wait10s>",
    "${var.root-password}<tab><wait2s>",
    "${var.root-password}<tab><tab><tab><tab><tab><tab><tab><tab><tab><tab><tab><return><wait10s>",
    "<return><wait60s>",
    "<leftAltOn>i<leftAltOff><wait5s><leftAltOn>i<leftAltOff><wait3000s>",
  ]
}
# openSUSE: amd64 (VirtualBox @ MacOS @ Intel Core i7-4980HQ CPU @ 2.80GHz)
source "virtualbox-iso" "opensuse-amd64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  guest_os_type        = "OpenSUSE_64"
  iso_url              = "${local.opensuse-amd64-iso-url}"
  iso_checksum         = "${local.opensuse-amd64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "/usr/sbin/poweroff"
  skip_export          = true
  headless             = true
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-medium}"
  disk_size            = "${var.disk-size}"
  usb                  = true
  guest_additions_mode = "disable"
  vboxmanage = [
    ["setextradata", "{{.Name}}", "GUI/ScaleFactor", "2"],
    ["modifyvm", "{{.Name}}", "--vrdeproperty", "VNCPassword=a"],
  ]
  boot_wait = "10s"
  boot_command = [
    "<down><wait2s><return><wait120s>",
    "<leftAltOn>n<leftAltOff><wait10s>",
    "<leftAltOn>y<leftAltOff><wait5s>",
    "<leftAltOn>n<leftAltOff><wait60s>",
    "<leftAltOn>s<leftAltOff><wait2s><leftAltOn>n<leftAltOff><wait5s>",
    "<leftAltOn>n<leftAltOff><wait20s>",
    "<leftAltOn>n<leftAltOff><wait5s>",
    "<leftAltOn>s<leftAltOff><tab><tab><tab><tab><tab><tab><tab><return><wait5s>",
    "${var.root-password}<tab><wait2s>",
    "${var.root-password}<tab><tab><tab><tab><tab><tab><tab><tab><tab><tab><tab><return><wait5s>",
    "<return><wait10s>",
    "<leftAltOn>i<leftAltOff><wait2s><leftAltOn>i<leftAltOff><wait780s>",
  ]
}
# TODO: openSUSE: arm64 (Qemu @ macOS @ >= Apple M1 Max)
# Starts but keeps standing on black screen with blinking cursor
source "qemu" "opensuse-arm64" {
  vm_name          = "{{build_type}}.{{build_name}}.{{uuid}}"
  iso_url          = "${local.opensuse-arm64-iso-url}"
  iso_checksum     = "${local.opensuse-arm64-iso-checksum}"
  http_directory   = "${var.http-directory}"
  communicator     = "ssh"
  ssh_username     = "root"
  ssh_password     = "${var.root-password}"
  shutdown_command = "/usr/sbin/poweroff"
  headless         = false
  qemu_binary      = "qemu-system-aarch64"
  machine_type     = "virt"
  cpus             = "${var.cpu-count}"
  memory           = "${var.memory-medium}"
  disk_size        = "${var.disk-size}"
  format           = "qcow2"
  disk_interface   = "virtio"
  net_device       = "virtio-net"
  display          = "cocoa"
  firmware         = "${var.qemu-arm64-firmware-path}"
  cpu_model        = "${var.qemu-arm64-cpu}"
  accelerator      = "${var.qemu-arm64-accelerator}"
  qemuargs = [
    ["-boot", "c"],
    ["-device", "qemu-xhci"],
    ["-device", "usb-kbd"],
    ["-device", "virtio-gpu-pci"],
  ]
  skip_compaction = true
  boot_wait       = "10s"
  boot_command = [
    "<return><wait130s>",
    "<leftAltOn>n<leftAltOff><wait5s>",
    "<leftAltOn>y<leftAltOff><wait5s>",
    "<leftAltOn>n<leftAltOff><wait70s>",
    "<leftAltOn>s<leftAltOff><wait5s><leftAltOn>n<leftAltOff><wait5s>",
    "<leftAltOn>n<leftAltOff><wait10s>",
    "<leftAltOn>n<leftAltOff><wait5s>",
    "<leftAltOn>s<leftAltOff><tab><tab><tab><tab><tab><tab><tab><return><wait5s>",
    "${var.root-password}<tab><wait2s>",
    "${var.root-password}<tab><tab><tab><tab><tab><tab><tab><tab><tab><tab><tab><return><wait2s>",
    "<return><wait10s>",
    "<leftAltOn>i<leftAltOff><wait5s><leftAltOn>i<leftAltOff><wait900s>",
  ]
}
# openSUSE: arm64 (VMware Fusion @ macOS @ >= Apple M1 Max)
source "vmware-iso" "opensuse-arm64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  version              = "20"
  iso_url              = "${local.opensuse-arm64-iso-url}"
  iso_checksum         = "${local.opensuse-arm64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  vnc_disable_password = true
  shutdown_command     = "/usr/sbin/poweroff"
  keep_registered      = false
  skip_export          = true
  headless             = true
  format               = "vmx"
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-plenty}"
  disk_size            = "${var.disk-size}"
  disk_adapter_type    = "sata"
  disk_type_id         = "0"
  network_adapter_type = "e1000e"
  usb                  = true
  guest_os_type        = "arm-other-64"
  vmx_data = {
    "firmware"         = "efi"
    "architecture"     = "arm-other-64"
    "usb_xhci.present" = "TRUE"
  }
  skip_compaction = true
  boot_wait       = "10s"
  boot_command = [
    "<return><wait90s>",
    "<leftAltOn>n<leftAltOff><wait5s>",
    "<leftAltOn>y<leftAltOff><wait5s>",
    "<leftAltOn>n<leftAltOff><wait60s>",
    "<leftAltOn>s<leftAltOff><wait5s><leftAltOn>n<leftAltOff><wait5s>",
    "<leftAltOn>n<leftAltOff><wait10s>",
    "<leftAltOn>n<leftAltOff><wait5s>",
    "<leftAltOn>s<leftAltOff><tab><tab><tab><tab><tab><tab><tab><return><wait5s>",
    "${var.root-password}<tab><wait2s>",
    "${var.root-password}<tab><tab><tab><tab><tab><tab><tab><tab><tab><tab><tab><return><wait2s>",
    "<return><wait10s>",
    "<leftAltOn>i<leftAltOff><wait5s><leftAltOn>i<leftAltOff><wait480s>",
  ]
}
# openSUSE: arm64 (VMware ESXi @ RPI5 8GB)
# Still to be tested
source "vsphere-iso" "opensuse-arm64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  vcenter_server       = "${var.esxi-host}"
  host                 = "${var.esxi-host}"
  username             = "${var.esxi-username}"
  password             = "${var.esxi-password}"
  insecure_connection  = true
  iso_paths            = [format("[datastore1] openSUSE-Leap-%s-DVD-aarch64-Media.iso", "${var.opensuse-version}")]
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "/usr/sbin/poweroff"
  CPUs                 = "${var.cpu-count}"
  RAM                  = "${var.memory-medium}"
  RAM_reserve_all      = true
  disk_controller_type = ["nvme"]
  storage {
    disk_size = "${var.disk-size}"
  }
  network_adapters {
    network      = "VM Network"
    network_card = "e1000e"
  }
  guest_os_type  = "other4xLinux64Guest"
  cdrom_type     = "sata"
  destroy        = true
  usb_controller = ["xhci"]
  boot_wait      = "10s"
  boot_command = [
    "<return><wait130s>",
    "<leftAltOn>n<leftAltOff><wait5s>",
    "<leftAltOn>y<leftAltOff><wait5s>",
    "<leftAltOn>n<leftAltOff><wait70s>",
    "<leftAltOn>s<leftAltOff><wait5s><leftAltOn>n<leftAltOff><wait5s>",
    "<leftAltOn>n<leftAltOff><wait10s>",
    "<leftAltOn>n<leftAltOff><wait5s>",
    "<leftAltOn>s<leftAltOff><tab><tab><tab><tab><tab><tab><tab><return><wait5s>",
    "${var.root-password}<tab><wait2s>",
    "${var.root-password}<tab><tab><tab><tab><tab><tab><tab><tab><tab><tab><tab><return><wait2s>",
    "<return><wait10s>",
    "<leftAltOn>i<leftAltOff><wait5s><leftAltOn>i<leftAltOff><wait900s>",
  ]
}

################################################################################
# Rocky Linux definition of the sources (platforms)
################################################################################
# Rocky Linux: amd64 (Qemu @ FreeBSD @ Intel Xeon E5-2640 v4)
source "qemu" "rocky-amd64" {
  vm_name          = "{{build_type}}.{{build_name}}.{{uuid}}"
  iso_url          = "${local.rocky-amd64-iso-url}"
  iso_checksum     = "${local.rocky-amd64-iso-checksum}"
  http_directory   = "${var.http-directory}"
  communicator     = "ssh"
  ssh_username     = "root"
  ssh_password     = "${var.root-password}"
  shutdown_command = "/usr/sbin/poweroff"
  headless         = true
  qemu_binary      = "qemu-system-x86_64"
  machine_type     = "pc"
  cpu_model        = "Icelake-Server-v7"
  cpus             = "${var.cpu-count}"
  memory           = "${var.memory-medium}"
  disk_size        = "${var.disk-size}"
  format           = "qcow2"
  disk_interface   = "ide"
  net_device       = "virtio-net"
  display          = "none"
  skip_compaction  = true
  boot_wait        = "10s"
  boot_command = [
    # Run the unattanded installation
    "<up><wait1s>",
    "<tab><wait1s>",
    "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
    "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
    "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
    "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
    "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
    "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
    "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
    "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
    "vmlinuz initrd=initrd.img ip=dhcp inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<return><wait1s>",
    "<wait3300s>",
  ]
}
# Rocky Linux: amd64 (VirtualBox @ MacOS @ Intel Core i7-4980HQ CPU @ 2.80GHz)
source "virtualbox-iso" "rocky-amd64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  guest_os_type        = "RedHat_64"
  iso_url              = "${local.rocky-amd64-iso-url}"
  iso_checksum         = "${local.rocky-amd64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "/usr/sbin/poweroff"
  skip_export          = true
  headless             = true
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-medium}"
  disk_size            = "${var.disk-size}"
  usb                  = true
  guest_additions_mode = "disable"
  vboxmanage = [
    ["setextradata", "{{.Name}}", "GUI/ScaleFactor", "2"],
    ["modifyvm", "{{.Name}}", "--vrdeproperty", "VNCPassword=a"],
  ]
  boot_wait = "10s"
  boot_command = [
    # Run the unattanded installation
    "<up><wait1s>",
    "<tab><wait1s>",
    "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
    "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
    "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
    "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
    "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
    "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
    "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
    "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
    "vmlinuz initrd=initrd.img ip=dhcp inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<return><wait1s>",
    "<wait420s>",
  ]
}
## TODO: Rocky Linux: arm64 (Qemu @ macOS @ >= Apple M1 Max)
## After the boatloader (Grub) was configured "Display output is not active." is shown forever.
#source "qemu" "rocky-arm64" {
#  vm_name          = "{{build_type}}.{{build_name}}.{{uuid}}"
#  iso_url          = "${local.fedora-arm64-iso-url}"
#  iso_checksum     = "${local.fedora-arm64-iso-checksum}"
#  http_directory   = "${var.http-directory}"
#  #vm_name          = "${var.vm-host-name}"
#  communicator     = "ssh"
#  ssh_username     = "root"
#  ssh_password     = "${var.root-password}"
#  shutdown_command = "/usr/sbin/poweroff"
#  headless         = true
#  qemu_binary      = "qemu-system-aarch64"
#  machine_type     = "virt"
#  cpus             = "${var.cpu-count}"
#  memory           = "${var.memory-medium}"
#  disk_size        = "${var.disk-size}"
#  format           = "qcow2"
#  disk_interface   = "virtio"
#  net_device       = "virtio-net"
#  display          = "none"
#  firmware         = "${var.qemu-arm64-firmware-path}"
#  cpu_model        = "${var.qemu-arm64-cpu}"
#  accelerator      = "${var.qemu-arm64-accelerator}"
#  qemuargs = [
#    ["-boot", "c"],
#    ["-device", "qemu-xhci"],
#    ["-device", "usb-kbd"],
#    ["-device", "virtio-gpu-pci"],
#  ]
#  skip_compaction  = true
#  boot_wait = "10s"
#  boot_command = [
#    # Run the unattanded installation
#    "<up><wait1s>",
#    "e<wait1s>",
#    "<down><wait1s>",
#    "<leftCtrlOn>kkkkk<leftCtrlOff><return>",
#    "linux /images/pxeboot/vmlinuz ip=dhcp inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<return><wait1s>",
#    "initrd /images/pxeboot/initrd.img<wait1s>",
#    "<f10><wait2700s>",
#  ]
#}
# Rocky Linux: arm64 (VMware Fusion @ macOS @ >= Apple M1 Max)
source "vmware-iso" "rocky-arm64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  version              = "20"
  iso_url              = "${local.rocky-arm64-iso-url}"
  iso_checksum         = "${local.rocky-arm64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  vnc_disable_password = true
  shutdown_command     = "/usr/sbin/poweroff"
  keep_registered      = false
  skip_export          = true
  headless             = true
  format               = "vmx"
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-plenty}"
  disk_size            = "${var.disk-size}"
  disk_adapter_type    = "sata"
  disk_type_id         = "0"
  network_adapter_type = "e1000e"
  usb                  = true
  guest_os_type        = "arm-other-64"
  vmx_data = {
    "firmware"         = "efi"
    "architecture"     = "arm-other-64"
    "usb_xhci.present" = "TRUE"
  }
  skip_compaction = true
  boot_wait       = "10s"
  boot_command = [
    # Run the unattanded installation
    "<up><wait1s>",
    "e<wait1s>",
    "<down><wait1s>",
    "<leftCtrlOn>kkkkk<leftCtrlOff><return>",
    "linux /images/pxeboot/vmlinuz nomodeset ip=dhcp inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<return><wait1s>",
    "initrd /images/pxeboot/initrd.img<wait1s>",
    "<f10><wait240s>",
  ]
}
# Rocky Linux: arm64 (VMware ESXi @ RPI5 8GB)
source "vsphere-iso" "rocky-arm64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  vcenter_server       = "${var.esxi-host}"
  host                 = "${var.esxi-host}"
  username             = "${var.esxi-username}"
  password             = "${var.esxi-password}"
  insecure_connection  = true
  iso_paths            = [format("[datastore1] Rocky-%s-aarch64-dvd.iso", "${var.rocky-version}")]
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "/usr/sbin/poweroff"
  CPUs                 = "${var.cpu-count}"
  RAM                  = "${var.memory-medium}"
  RAM_reserve_all      = true
  disk_controller_type = ["nvme"]
  storage {
    disk_size = "${var.disk-size}"
  }
  network_adapters {
    network      = "VM Network"
    network_card = "e1000e"
  }
  guest_os_type  = "other4xLinux64Guest"
  cdrom_type     = "sata"
  destroy        = true
  usb_controller = ["xhci"]
  boot_wait      = "20s"
  boot_command = [
    # Run the unattanded installation
    "<up><wait1s>",
    "e<wait1s>",
    "<down><wait1s>",
    "<leftCtrlOn>kkkkk<leftCtrlOff><return>",
    "linux /images/pxeboot/vmlinuz nomodeset ip=dhcp inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/unattended.conf<return><wait1s>",
    "initrd /images/pxeboot/initrd.img<wait1s>",
    "<f10><wait600s>",
    # Prepare installed OS to be accessible from packer
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "dnf install --setopt install_weak_deps=false --assumeyes open-vm-tools<return><wait30s>",
    "systemctl start vmtoolsd.service<return><wait5s>",
    "exit<enter><wait1s>",
  ]
}

################################################################################
# Ubuntu definition of the sources (platforms)
################################################################################
# Ubuntu: amd64 (Qemu @ FreeBSD @ Intel Xeon E5-2640 v4)
source "qemu" "ubuntu-amd64" {
  vm_name          = "{{build_type}}.{{build_name}}.{{uuid}}"
  iso_url          = "${local.ubuntu-amd64-iso-url}"
  iso_checksum     = "${local.ubuntu-amd64-iso-checksum}"
  http_directory   = "${var.http-directory}"
  communicator     = "ssh"
  ssh_username     = "root"
  ssh_password     = "${var.root-password}"
  shutdown_command = "/usr/sbin/poweroff"
  headless         = true
  qemu_binary      = "qemu-system-x86_64"
  machine_type     = "pc"
  cpus             = "${var.cpu-count}"
  memory           = "${var.memory-medium}"
  disk_size        = "${var.disk-size}"
  format           = "qcow2"
  disk_interface   = "virtio"
  net_device       = "virtio-net"
  display          = "none"
  skip_compaction  = true
  boot_wait        = "10s"
  boot_command = [
    # Configure system for unattanded installation
    "<esc><wait1s><esc><wait1s><esc><wait1s><esc><wait1s><esc><wait1s>",
    "e<wait1s>",
    "<down><down><down><wait1s>",
    "<end> autoinstall ds='nocloud-net;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}'<wait1s>",
    # Run the unattanded installation
    "<f10><wait2700s>",
    # Prepare installed OS to be accessible from packer
    "<return><wait5s>",
    "${var.ubuntu-username}<return><wait5s>",
    "user<return><wait5s>",
    "sudo su -l<return><wait5s>",
    "user<return><wait5s>",
    "passwd<return><wait5s>",
    "${var.root-password}<return><wait5s>",
    "${var.root-password}<return><wait5s>",
    "sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config<return><wait5s>",
    "service ssh restart<return><wait5s>",
    "exit<return><wait5s>",
    "exit<return><wait5s>",
  ]
}
# Ubuntu: amd64 (VirtualBox @ MacOS @ Intel Core i7-4980HQ CPU @ 2.80GHz)
source "virtualbox-iso" "ubuntu-amd64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  guest_os_type        = "Ubuntu_64"
  iso_url              = "${local.ubuntu-amd64-iso-url}"
  iso_checksum         = "${local.ubuntu-amd64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "/usr/sbin/poweroff"
  skip_export          = true
  headless             = true
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-medium}"
  disk_size            = "${var.disk-size}"
  usb                  = true
  guest_additions_mode = "disable"
  vboxmanage = [
    ["setextradata", "{{.Name}}", "GUI/ScaleFactor", "2"],
    ["modifyvm", "{{.Name}}", "--vrdeproperty", "VNCPassword=a"],
  ]
  boot_wait = "10s"
  boot_command = [
    # Configure system for unattanded installation
    "<esc><wait1s><esc><wait1s><esc><wait1s><esc><wait1s><esc><wait1s>",
    "e<wait1s>",
    "<down><down><down><wait1s>",
    "<end> autoinstall ds='nocloud-net;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}'<wait1s>",
    # Run the unattanded installation
    "<f10><wait540s>",
    # Prepare installed OS to be accessible from packer
    "<return><wait2s>",
    "${var.ubuntu-username}<return><wait2s>",
    "user<return><wait2s>",
    "sudo su -l<return><wait2s>",
    "user<return><wait2s>",
    "passwd<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "${var.root-password}<return><wait2s>",
    "sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config<return><wait2s>",
    "service ssh restart<return><wait2s>",
    "exit<return><wait2s>",
    "exit<return><wait2s>",
  ]
}
# TODO: Ubuntu: arm64 (Qemu @ macOS @ >= Apple M1 Max)
# After the boatloader (Grub) was configured "Display output is not active." is shown forever but only on non macOS.
source "qemu" "ubuntu-arm64" {
  vm_name          = "{{build_type}}.{{build_name}}.{{uuid}}"
  iso_url          = "${local.ubuntu-arm64-iso-url}"
  iso_checksum     = "${local.ubuntu-arm64-iso-checksum}"
  http_directory   = "${var.http-directory}"
  communicator     = "ssh"
  ssh_username     = "root"
  ssh_password     = "${var.root-password}"
  shutdown_command = "/usr/sbin/poweroff"
  headless         = true
  qemu_binary      = "qemu-system-aarch64"
  machine_type     = "virt"
  cpus             = "${var.cpu-count}"
  memory           = "${var.memory-medium}"
  disk_size        = "${var.disk-size}"
  format           = "qcow2"
  disk_interface   = "virtio"
  net_device       = "virtio-net"
  display          = "none"
  firmware         = "${var.qemu-arm64-firmware-path}"
  cpu_model        = "${var.qemu-arm64-cpu}"
  accelerator      = "${var.qemu-arm64-accelerator}"
  qemuargs = [
    ["-boot", "c"],
    ["-device", "qemu-xhci"],
    ["-device", "usb-kbd"],
    ["-device", "virtio-gpu-pci"],
  ]
  skip_compaction = true
  boot_wait       = "10s"
  boot_command = [
    # Configure system for unattanded installation
    "e<wait1s>",
    "<down><down><down><wait1s>",
    "<end> autoinstall ds='nocloud-net;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}'<wait1s>",
    # Run the unattanded installation
    "<f10><wait900s>",
    # Prepare installed OS to be accessible from packer
    "<return><wait5s>",
    "${var.ubuntu-username}<return><wait5s>",
    "user<return><wait5s>",
    "sudo su -l<return><wait5s>",
    "user<return><wait5s>",
    "passwd<return><wait5s>",
    "${var.root-password}<return><wait5s>",
    "${var.root-password}<return><wait5s>",
    "sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config<return><wait5s>",
    "service ssh restart<return><wait5s>",
    "exit<return><wait5s>",
    "exit<return><wait5s>",
  ]
}
# Ubuntu: arm64 (VMware Fusion @ macOS @ >= Apple M1 Max)
source "vmware-iso" "ubuntu-arm64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  iso_url              = "${local.ubuntu-arm64-iso-url}"
  iso_checksum         = "${local.ubuntu-arm64-iso-checksum}"
  http_directory       = "${var.http-directory}"
  version              = "20"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  vnc_disable_password = true
  shutdown_command     = "/usr/sbin/poweroff"
  keep_registered      = false
  skip_export          = true
  headless             = true
  format               = "vmx"
  cpus                 = "${var.cpu-count}"
  memory               = "${var.memory-plenty}"
  disk_size            = "${var.disk-size}"
  disk_adapter_type    = "nvme"
  disk_type_id         = "0"
  network_adapter_type = "e1000e"
  usb                  = true
  guest_os_type        = "arm-other-64"
  vmx_data = {
    "firmware"         = "efi"
    "architecture"     = "arm-other-64"
    "usb_xhci.present" = "TRUE"
  }
  skip_compaction = true
  boot_wait       = "10s"
  boot_command = [
    # Configure system for unattanded installation
    "e<wait1s>",
    "<down><down><down><wait1s>",
    "<end> autoinstall ds='nocloud-net;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}'<wait5s>",
    # Run the unattanded installation
    "<f10><wait300s>",
    # Prepare installed OS to be accessible from packer
    "<return><wait5s>",
    "${var.ubuntu-username}<return><wait5s>",
    "user<return><wait5s>",
    "sudo su -l<return><wait5s>",
    "user<return><wait5s>",
    "passwd<return><wait5s>",
    "${var.root-password}<return><wait5s>",
    "${var.root-password}<return><wait5s>",
    "sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config<return><wait5s>",
    "service ssh restart<return><wait5s>",
    "exit<return><wait5s>",
    "exit<return><wait5s>",
  ]
}
# Ubuntu: arm64 (VMware ESXi @ RPI5 8GB)
source "vsphere-iso" "ubuntu-arm64" {
  vm_name              = "{{build_type}}.{{build_name}}.{{uuid}}"
  vcenter_server       = "${var.esxi-host}"
  host                 = "${var.esxi-host}"
  username             = "${var.esxi-username}"
  password             = "${var.esxi-password}"
  insecure_connection  = true
  iso_paths            = [format("[datastore1] ubuntu-%s-live-server-arm64.iso", "${var.ubuntu-version}")]
  http_directory       = "${var.http-directory}"
  ssh_username         = "root"
  ssh_password         = "${var.root-password}"
  shutdown_command     = "/usr/sbin/poweroff"
  CPUs                 = "${var.cpu-count}"
  RAM                  = "${var.memory-medium}"
  RAM_reserve_all      = true
  disk_controller_type = ["nvme"]
  storage {
    disk_size = "${var.disk-size}"
  }
  network_adapters {
    network      = "VM Network"
    network_card = "e1000e"
  }
  guest_os_type  = "other4xLinux64Guest"
  cdrom_type     = "sata"
  destroy        = true
  usb_controller = ["xhci"]
  boot_wait      = "10s"
  boot_command = [
    # Configure system for unattanded installation
    "e<wait1s>",
    "<down><down><down><wait1s>",
    "<end> autoinstall ds='nocloud-net;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}'<wait5s>",
    # Run the unattanded installation
    "<f10><wait900s>",
    # Prepare installed OS to be accessible from packer
    "<return><wait5s>",
    "${var.ubuntu-username}<return><wait5s>",
    "user<return><wait5s>",
    "sudo su -l<return><wait5s>",
    "user<return><wait5s>",
    "passwd<return><wait5s>",
    "${var.root-password}<return><wait5s>",
    "${var.root-password}<return><wait5s>",
    "sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config<return><wait5s>",
    "service ssh restart<return><wait5s>",
    "exit<return><wait5s>",
    "exit<return><wait5s>",
  ]
}
