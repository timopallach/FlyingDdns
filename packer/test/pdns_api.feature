# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

Feature: PowerDNS API Checks
  The PowerDNS API is used to manage DNS zones and records programmatically.
  These tests verify that the PowerDNS API is properly configured, accessible,
  and capable of performing essential DNS management operations.
  
  Background:
    # All scenarios require a running PowerDNS server with API access configured

  Scenario: Check status
    # Verifies the API is accessible and returns the expected server type
    Given the PowerDNS daemon is started and running locally
     When the PDNS API check status endpoint is curled
     Then the PDNS API returns authoritative

  Scenario: List existing zones
    # Verifies the API can list DNS zones and returns the expected server type
    Given the PowerDNS daemon is started and running locally
     When the PDNS API list existing zones endpoint is curled
     Then the PDNS API returns authoritative

  Scenario: Create the example zone
    # Verifies the API can create a new DNS zone
    # Expected outcome: A new zone is created and the API returns a valid response
    Given the PowerDNS daemon is started and running locally
     When the PDNS API create zone endpoint is curled
     Then the PDNS API returns a valid json string

  Scenario: Add a MX record to the example zone
    # Verifies the API can add an MX record to the zone
    # Expected outcome: MX record is added and the API returns an empty response
    Given the PowerDNS daemon is started and running locally
     When the PDNS API add record endpoint is curled to add a MX record to the example zone
     Then the PDNS API returns an empty json string

  Scenario: Add domain A record to the example zone
    # Verifies the API can add an A record for the domain itself
    # Expected outcome: A record is added and the API returns an empty response
    Given the PowerDNS daemon is started and running locally
     When the PDNS API add record endpoint is curled to add a domain A record to the example zone
     Then the PDNS API returns an empty json string

  Scenario: Add domain AAAA record to the example zone
    # Verifies the API can add an AAAA record for the domain itself
    # Expected outcome: AAAA record is added and the API returns an empty response
    Given the PowerDNS daemon is started and running locally
     When the PDNS API add record endpoint is curled to add a domain AAAA record to the example zone
     Then the PDNS API returns an empty json string

  Scenario: Add NS1 A record to the example zone
    # Verifies the API can add an A record for the first nameserver
    # Expected outcome: NS1 A record is added and the API returns an empty response
    Given the PowerDNS daemon is started and running locally
     When the PDNS API add record endpoint is curled to add a NS1 A record to the example zone
     Then the PDNS API returns an empty json string

  Scenario: Add NS1 AAAA record to the example zone
    # Verifies the API can add an AAAA record for the first nameserver
    # Expected outcome: NS1 AAAA record is added and the API returns an empty response
    Given the PowerDNS daemon is started and running locally
     When the PDNS API add record endpoint is curled to add a NS1 AAAA record to the example zone
     Then the PDNS API returns an empty json string

  Scenario: Add NS2 A record to the example zone
    # Verifies the API can add an A record for the second nameserver
    # Expected outcome: NS2 A record is added and the API returns an empty response
    Given the PowerDNS daemon is started and running locally
     When the PDNS API add record endpoint is curled to add a NS2 A record to the example zone
     Then the PDNS API returns an empty json string

  Scenario: Add NS2 AAAA record to the example zone
    # Verifies the API can add an AAAA record for the second nameserver
    # Expected outcome: NS2 AAAA record is added and the API returns an empty response
    Given the PowerDNS daemon is started and running locally
     When the PDNS API add record endpoint is curled to add a NS2 AAAA record to the example zone
     Then the PDNS API returns an empty json string

  Scenario: Add MX A record to the example zone
    # Verifies the API can add an A record for the mail server
    # Expected outcome: MX A record is added and the API returns an empty response
    Given the PowerDNS daemon is started and running locally
     When the PDNS API add record endpoint is curled to add a MX A record to the example zone
     Then the PDNS API returns an empty json string

  Scenario: Add MX AAAA record to the example zone
    # Verifies the API can add an AAAA record for the mail server
    # Expected outcome: MX AAAA record is added and will be verified in a later step
    Given the PowerDNS daemon is started and running locally
     When the PDNS API add record endpoint is curled to add a MX AAAA record to the example zone
     Then this will be checked later

  Scenario: List the example zone
    # Verifies the API can retrieve details of a specific zone
    # Expected outcome: Zone details are returned as valid JSON
    Given the PowerDNS daemon is started and running locally
     When the PDNS API list zone endpoint is curled
     Then the PDNS API returns a valid json string

  Scenario: Rectify the example zone
    # Verifies the API can rectify the zone (ensure DNSSEC records are correct)
    # Expected outcome: Zone is rectified and the API returns a valid response
    Given the PowerDNS daemon is started and running locally
     When the PDNS API rectify zone endpoint is curled
     Then the PDNS API returns a valid json string

  Scenario: Delete the example zone
    # Verifies the API can delete a zone
    # Expected outcome: Zone is deleted and the API returns an empty response
    Given the PowerDNS daemon is started and running locally
     When the PDNS API delete zone endpoint is curled
     Then the PDNS API returns an empty json string
