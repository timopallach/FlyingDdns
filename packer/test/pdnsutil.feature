# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

Feature: PowerDNS pdnsutil command line checks
  The PowerDNS server provides a command line tool called pdnsutil
  that can configure the PowerDNS daemon.
  
  These tests verify that pdnsutil is properly installed and can perform
  essential DNS management operations through the command line interface.
  This ensures that both API and CLI management options are functional.
  
  Background:
    # All scenarios require a running PowerDNS server with pdnsutil available

  Scenario: Check if the example zone does not already exist in the PowerDNS server
    # Ensures a clean test environment by removing any existing test zone
    # Expected outcome: The example zone does not exist before testing begins
    Given the PowerDNS daemon is started and running locally
     When the example zone will be deleted if already configured in the PowerDNS server
     Then the example zone does not exist in the current running PowerDNS instance

  Scenario: Create the example zone
    # Verifies pdnsutil can create a new DNS zone
    # Expected outcome: A new zone is created and can be verified using pdnsutil
    Given the PowerDNS daemon is started and running locally
     When the pdnsutil create-zone command is executed
     Then the example zone has been created and checked using pdnsutil

  Scenario: Add a ns2 record to the example zone
    # Verifies pdnsutil can add an NS record for the second nameserver
    # Expected outcome: NS2 record is added to the zone
    Given the example zone has been created and checked using pdnsutil
     When the pdnsutil add-record command is executed to create the ns2 record
     Then the ns2 record has been added to the example zone

  Scenario: Add an A record to the example zone
    # Verifies pdnsutil can add an A record for the domain itself
    # Expected outcome: A record is added to the zone
    Given the example zone has been created and checked using pdnsutil
     When the pdnsutil add-record command is executed to create the A record
     Then the A record has been added to the example zone

  Scenario: Add an AAAA record to the example zone
    # Verifies pdnsutil can add an AAAA record for the domain itself
    # Expected outcome: AAAA record is added to the zone
    Given the example zone has been created and checked using pdnsutil
     When the pdnsutil add-record command is executed to create the AAAA record
     Then the AAAA record has been added to the example zone

  Scenario: Add a MX record to the example zone
    # Verifies pdnsutil can add an MX record for mail delivery
    # Expected outcome: MX record is added to the zone
    Given the example zone has been created and checked using pdnsutil
     When the pdnsutil add-record command is executed to create the MX record
     Then the MX record has been added to the example zone

  Scenario: Add a NS1 A record to the example zone
    # Verifies pdnsutil can add an A record for the first nameserver
    # Expected outcome: NS1 A record is added to the zone
    Given the example zone has been created and checked using pdnsutil
     When the pdnsutil add-record command is executed to create the NS1 A record
     Then the NS1 A record has been added to the example zone

  Scenario: Add a NS1 AAAA record to the example zone
    # Verifies pdnsutil can add an AAAA record for the first nameserver
    # Expected outcome: NS1 AAAA record is added to the zone
    Given the example zone has been created and checked using pdnsutil
     When the pdnsutil add-record command is executed to create the NS1 AAAA record
     Then the NS1 AAAA record has been added to the example zone

  Scenario: Add a NS2 A record to the example zone
    # Verifies pdnsutil can add an A record for the second nameserver
    # Expected outcome: NS2 A record is added to the zone
    Given the example zone has been created and checked using pdnsutil
     When the pdnsutil add-record command is executed to create the NS2 A record
     Then the NS2 A record has been added to the example zone

  Scenario: Add a NS2 AAAA record to the example zone
    # Verifies pdnsutil can add an AAAA record for the second nameserver
    # Expected outcome: NS2 AAAA record is added to the zone
    Given the example zone has been created and checked using pdnsutil
     When the pdnsutil add-record command is executed to create the NS2 AAAA record
     Then the NS2 AAAA record has been added to the example zone

  Scenario: Add a MX A record to the example zone
    # Verifies pdnsutil can add an A record for the mail server
    # Expected outcome: MX A record is added to the zone
    Given the example zone has been created and checked using pdnsutil
     When the pdnsutil add-record command is executed to create the MX A record
     Then the MX A record has been added to the example zone

  Scenario: Add a MX AAAA record to the example zone
    # Verifies pdnsutil can add an AAAA record for the mail server
    # Expected outcome: MX AAAA record is added to the zone
    Given the example zone has been created and checked using pdnsutil
     When the pdnsutil add-record command is executed to create the MX AAAA record
     Then the MX AAAA record has been added to the example zone

  Scenario: Increase the SOA-serial of the example zone
    # Verifies pdnsutil can update the SOA serial number
    # Expected outcome: SOA serial is increased, which is necessary after zone changes
    Given the example zone has been created and checked using pdnsutil
     When the pdnsutil increase-serial command is executed to increase the SOA-serial of the zone
     Then the SOA-serial of the example zone has been increased

  Scenario: Check the example zone
    # Verifies pdnsutil can validate the zone configuration
    # Expected outcome: Zone passes validation checks
    Given the PowerDNS daemon is started and running locally
     When the pdnsutil check-zone command is executed
     Then the example zone has been checked

  Scenario: Delete the example zone
    # Verifies pdnsutil can delete a zone
    # Expected outcome: Zone is completely removed from the server
    Given the PowerDNS daemon is started and running locally
     When the pdnsutil delete-zone command is executed
     Then the example zone does not exist in the current running PowerDNS instance
