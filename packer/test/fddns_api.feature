# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

Feature: FlyingDdns API checks
  The FlyingDdns API provides a dynamic DNS update service that allows clients
  to update their DNS records when their IP addresses change.
  
  These tests verify that the FlyingDdns API is properly configured, accessible,
  and capable of performing essential dynamic DNS operations including retrieving
  public IP addresses and updating DNS records.
  
  Background:
    # All scenarios require a running FlyingDdns server with API access configured

  Scenario: Check the FlyingDdns Welcome Page
    # Verifies the server is running and accessible
    # Expected outcome: The welcome page is displayed correctly
    Given the status of the FlyingDdns server is OK
     When the Welcome Page is curled
     Then the correct Welcome Page text is shown

  Scenario: Check the FlyingDdns Version
    # Verifies the version endpoint returns the correct version
    # Expected outcome: The correct version string is returned
    Given the status of the FlyingDdns server is OK
     When the FlyingDdns version is curled
     Then the correct FlyingDdns version is shown

  Scenario: Check the FlyingDdns Commit SHA
    # Verifies the commit SHA endpoint returns the correct value
    # Expected outcome: The correct full commit SHA is returned
    Given the status of the FlyingDdns server is OK
     When the FlyingDdns commit SHA is curled
     Then the correct FlyingDdns commit SHA is shown

  Scenario: Check the FlyingDdns Commit Short SHA
    # Verifies the short commit SHA endpoint returns the correct value
    # Expected outcome: The correct abbreviated commit SHA is returned
    Given the status of the FlyingDdns server is OK
     When the FlyingDdns commit short SHA is curled
     Then the correct FlyingDdns commit short SHA is shown

  Scenario: Retrieve the Public IP Address from the FlyingDdns server
    # Verifies the IP detection functionality works correctly
    # Expected outcome: A valid public IP address is returned
    Given the status of the FlyingDdns server is OK
     When the FlyingDdns server is curled to retrieve the public IP
     Then the retrieved public IP is correct

  Scenario: Create the example zone
    # Verifies the zone creation functionality works correctly
    # Expected outcome: A new zone is created and can be verified using host command
    Given the status of the FlyingDdns server is OK
     When the example zone is created using curl
     Then the example zone has been created and checked using host

  Scenario: A Domain Record is added to the example zone
    # Verifies the record addition functionality works correctly
    # Expected outcome: A new A record is added to the zone and returns "good" with the IP
    Given the status of the FlyingDdns server is OK
     When the domain record is added to the example zone using curl
     Then the domain record has been added to the example zone

  Scenario: The Domain Record is updated in the example zone
    # Verifies the record update functionality works correctly
    # Expected outcome: The A record is updated with a new IP and returns "good" with the IP
    Given the status of the FlyingDdns server is OK
     When the domain record is updated in the example zone using curl
     Then the domain record has been updated in the example zone
