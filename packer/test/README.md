# FlyingDdns Gherkin Tests

This directory contains Gherkin feature files and Robot Framework resources for testing the FlyingDdns application and its dependencies.

## Table of Contents

- [Overview](#overview)
- [Test Structure](#test-structure)
  - [Feature Files](#feature-files)
  - [Resource Files](#resource-files)
- [Prerequisites](#prerequisites)
- [Running the Tests](#running-the-tests)
  - [Manual Test Execution](#manual-test-execution)
  - [Test Configuration](#test-configuration)
- [Test Reports](#test-reports)
- [Adding New Tests](#adding-new-tests)
- [Test Data Documentation](#test-data-documentation)
  - [Domain Names](#domain-names)
  - [IP Addresses](#ip-addresses)
  - [DNS Records](#dns-records)
  - [Server Configuration](#server-configuration)
  - [Test Data Flow](#test-data-flow)
  - [Test Data Dependencies](#test-data-dependencies)
  - [Visual Representation](#visual-representation)
  - [Modifying Test Data](#modifying-test-data)
  - [Test Data Security Considerations](#test-data-security-considerations)
  - [Common Test Data Patterns](#common-test-data-patterns)
- [Troubleshooting](#troubleshooting)
- [Integration with CI/CD](#integration-with-cicd)
- [Test Data Versioning](#test-data-versioning)
  - [Test Data Change History](#test-data-change-history)
- [Quick Reference Guide](#quick-reference-guide)
  - [Common Test Commands](#common-test-commands)
  - [Common PowerDNS Commands](#common-powerdns-commands)
  - [Common FlyingDdns API Calls](#common-flyingddns-api-calls)

## Overview

The test suite is designed to verify the functionality of three main components:

1. **PowerDNS API** - Tests the REST API of the PowerDNS server
2. **PowerDNS Command Line (pdnsutil)** - Tests the command-line utility for PowerDNS
3. **FlyingDdns API** - Tests the dynamic DNS update service API

## Test Structure

### Feature Files

- **pdns_api.feature**: Tests for the PowerDNS REST API
- **pdnsutil.feature**: Tests for the PowerDNS command-line utility
- **fddns_api.feature**: Tests for the FlyingDdns API

### Resource Files

- **pdns_api.resource**: Step implementations for PowerDNS API tests
- **pdnsutil.resource**: Step implementations for pdnsutil tests
- **fddns_api.resource**: Step implementations for FlyingDdns API tests
- **keywords.resource**: Common keywords used across all tests
- **variables.resource**: Common variables used across all tests

## Prerequisites

Before running the tests, ensure you have:

1. Python 3.x installed
2. Robot Framework installed (`pip install robotframework`)
3. Robot Framework Gherkin Parser installed (`pip install robotframework-gherkin-parser`)
4. PowerDNS server running with API access configured
5. FlyingDdns server running

## Running the Tests

The tests are designed to be run as part of the build process using the scripts in the `packer/bin/` directory:

- `run_pdns_api_tests.sh`: Runs the PowerDNS API tests
- `run_pdnsutil_tests.sh`: Runs the pdnsutil command-line tests
- `run_fddns_api_tests.sh`: Runs the FlyingDdns API tests

### Manual Test Execution

To run the tests manually:

1. Ensure your environment variables are set correctly (see `variables.resource`)
2. Navigate to the test directory:
   ```
   cd packer/test
   ```
3. Run a specific feature file:
   ```
   robot --parser GherkinParser pdns_api.feature
   robot --parser GherkinParser pdnsutil.feature
   robot --parser GherkinParser fddns_api.feature
   ```

### Test Configuration

You can configure the tests by setting environment variables or passing them as command-line arguments:

```
robot --parser GherkinParser --variable DOMAIN_NAME:example.com --variable FDDNS_PORT:4000 fddns_api.feature
```

Common variables include:

- `DOMAIN_NAME`: The domain name to use for testing (default: example.org)
- `FDDNS_PROTOCOL`: The protocol to use (http/https)
- `FDDNS_HOSTNAME`: The hostname of the FlyingDdns server
- `FDDNS_PORT`: The port of the FlyingDdns server
- `PDNS_API_URL`: The URL of the PowerDNS API

## Test Reports

Robot Framework generates detailed reports after test execution:

- `report.html`: Overview of test results
- `log.html`: Detailed log of test execution
- `output.xml`: XML output for integration with CI/CD systems

## Adding New Tests

To add new tests:

1. Create or modify a feature file with Gherkin syntax
2. Implement the step definitions in the corresponding resource file
3. Add any necessary keywords to `keywords.resource`
4. Add any necessary variables to `variables.resource`

## Test Data Documentation

This section provides detailed information about the test data used in the FlyingDdns Gherkin tests. Understanding this data is essential for maintaining and extending the test suite.

### Domain Names

| Variable | Value | Description |
|----------|-------|-------------|
| `${DOMAIN_NAME}` | `example.org` | Primary test domain used across all tests |
| `${DOMAIN_NAME_IPV4}` | `example-ipv4.org` | Domain used specifically for IPv4 testing |
| `${DOMAIN_NAME_IPV6}` | `example-ipv6.org` | Domain used specifically for IPv6 testing |

#### Subdomains

The following subdomains are created and used during testing:

- `ns1.example.org` - First nameserver
- `ns2.example.org` - Second nameserver
- `mail.example.org` - Mail server
- `test.example.org` - Used for dynamic DNS update tests

### IP Addresses

#### IPv4 Addresses

| Variable | Value | Description |
|----------|-------|-------------|
| `${IPV4_1}` | `1.0.0.1` | Primary IPv4 address used for DNS records |
| `${IPV4_2}` | `2.0.0.2` | Secondary IPv4 address used for update tests |

#### IPv6 Addresses

| Variable | Value | Description |
|----------|-------|-------------|
| `${IPV6_1}` | `1::1` | Primary IPv6 address used for DNS records |
| `${IPV6_2}` | `2::2` | Secondary IPv6 address used for update tests |

### DNS Records

The tests create and verify the following DNS record types:

#### A Records (IPv4)

| Record | Value | Description |
|--------|-------|-------------|
| `example.org` | `1.0.0.1` | Main domain A record |
| `ns1.example.org` | `1.0.0.1` | First nameserver A record |
| `ns2.example.org` | `1.0.0.1` | Second nameserver A record |
| `mail.example.org` | `1.0.0.1` | Mail server A record |
| `test.example.org` | `1.0.0.1` → `2.0.0.2` | Dynamic DNS record (updated during tests) |

#### AAAA Records (IPv6)

| Record | Value | Description |
|--------|-------|-------------|
| `example.org` | `1::1` | Main domain AAAA record |
| `ns1.example.org` | `1::1` | First nameserver AAAA record |
| `ns2.example.org` | `1::1` | Second nameserver AAAA record |
| `mail.example.org` | `1::1` | Mail server AAAA record |

#### MX Records

| Record | Value | Priority | Description |
|--------|-------|----------|-------------|
| `example.org` | `mail.example.org` | 10 | Mail exchange record |

#### NS Records

| Record | Value | Description |
|--------|-------|-------------|
| `example.org` | `ns1.example.org` | First nameserver record |
| `example.org` | `ns2.example.org` | Second nameserver record |

### Server Configuration

#### PowerDNS API

| Variable | Value | Description |
|----------|-------|-------------|
| `${PDNS_API_PROTOCOL}` | `http` | Protocol used to connect to PowerDNS API |
| `${PDNS_API_HOSTNAME}` | `[::1]` | Hostname (IPv6 localhost) |
| `${PDNS_API_PORT}` | `8081` | Port number |
| `${PDNS_API_PASSWD}` | `pdns_api_passwd` | API key for authentication |
| `${PDNS_API_URL}` | `http://[::1]:8081` | Complete URL (derived) |

#### FlyingDdns API

| Variable | Value | Description |
|----------|-------|-------------|
| `${FDDNS_PROTOCOL}` | `http` | Protocol used to connect to FlyingDdns API |
| `${FDDNS_HOSTNAME}` | `[::1]` | Hostname (IPv6 localhost) |
| `${FDDNS_PORT}` | `4000` | Port number |
| `${FDDNS_URL}` | `http://[::1]:4000` | Complete URL (derived) |
| `${FDDNS_IP_VERSION}` | `ipv6` | Default IP version for curl commands |

### Test Data Flow

The tests follow this general flow:

1. **Setup**: Ensure no test zones exist from previous runs
2. **Creation**: Create the example zone with initial NS records
3. **Addition**: Add various record types (A, AAAA, MX, etc.)
4. **Verification**: Verify records were added correctly
5. **Updates**: For dynamic DNS tests, modify records with new values
6. **Cleanup**: Remove the test zone

### Test Data Dependencies

Some tests have dependencies on other tests:

- PowerDNS API tests assume the PowerDNS server is running and accessible
- pdnsutil tests assume the PowerDNS server is running and the pdnsutil command is available
- FlyingDdns API tests assume both the PowerDNS server and FlyingDdns server are running

### Visual Representation

#### DNS Record Structure

```
                                  example.org
                                       |
                 +--------------------+--------------------+
                 |                    |                    |
            ns1.example.org     ns2.example.org     mail.example.org
                 |                    |                    |
          +------+------+      +------+------+      +------+------+
          |             |      |             |      |             |
    A (1.0.0.1)  AAAA (1::1)  A (1.0.0.1)  AAAA (1::1)  A (1.0.0.1)  AAAA (1::1)
```

#### Test Flow Diagram

```
+-------------------+     +-------------------+     +-------------------+
| PowerDNS Server   |     | PowerDNS API      |     | FlyingDdns Server |
| (pdnsutil tests)  |<--->| (pdns_api tests)  |<--->| (fddns_api tests) |
+-------------------+     +-------------------+     +-------------------+
        |                         |                         |
        v                         v                         v
+-----------------------------------------------------------+
|                      Shared Test Data                      |
|                                                           |
| - Domain Names (example.org, example-ipv4.org, etc.)      |
| - IP Addresses (1.0.0.1, 1::1, etc.)                      |
| - DNS Records (A, AAAA, MX, NS, SOA)                      |
+-----------------------------------------------------------+
```

#### Component Interaction

```
+----------------+     +-----------------+     +----------------+
| Client         |     | FlyingDdns      |     | PowerDNS       |
| (curl/tests)   |---->| (Dynamic DNS)   |---->| (DNS Server)   |
+----------------+     +-----------------+     +----------------+
       |                      |                       |
       | Request              | Update                | Store
       | DNS Update           | DNS Records           | DNS Data
       v                      v                       v
  "Update my IP"       "Process request"       "Serve DNS queries"
```

### Modifying Test Data

When modifying test data, consider these guidelines:

1. **Consistency**: Ensure all related values are updated consistently
2. **Isolation**: Test data should not conflict with real-world domains
3. **Simplicity**: Keep test data simple and easy to understand
4. **Documentation**: Update this documentation when changing test data

To use different test data:

1. Edit the variables in `variables.resource`
2. Pass variables via command line when running tests:
   ```
   robot --parser GherkinParser --variable DOMAIN_NAME:custom.example.com pdns_api.feature
   ```
3. Set environment variables before running tests

**Important**: When changing domain names, ensure all related subdomains (ns1, ns2, mail) are consistently updated throughout all test files.

### Test Data Security Considerations

When working with test data, keep these security considerations in mind:

1. **Avoid Real Domains**: Never use real, registered domains for testing to prevent unintended consequences.
2. **Reserved Domains**: Prefer using domains reserved for testing like `example.org`, `example.com`, or `example.net`.
3. **API Keys**: The test API keys should be different from production keys and have limited permissions.
4. **IP Addresses**: Use designated test IP ranges:
   - For IPv4: Use `192.0.2.0/24`, `198.51.100.0/24`, or `203.0.113.0/24` (TEST-NET ranges)
   - For IPv6: Use addresses in the `2001:db8::/32` range (reserved for documentation)
5. **Credentials**: Never commit real credentials to the repository, even in test files.

### Common Test Data Patterns

These patterns can be useful when extending the test suite with new scenarios:

1. **Record Creation Pattern**:
   ```
   Given the server is running
   When I create a new [record type] record
   Then the record should be retrievable
   ```

2. **Record Update Pattern**:
   ```
   Given a [record type] record exists
   When I update the record
   Then the changes should be reflected in DNS queries
   ```

3. **Error Handling Pattern**:
   ```
   Given the server is running
   When I attempt an invalid operation
   Then I should receive an appropriate error message
   ```

4. **Zone Management Pattern**:
   ```
   Given no test zones exist
   When I create and configure a zone
   Then I should be able to perform DNS operations on that zone
   ```

## Troubleshooting

If tests fail, check:

1. **Server connectivity issues**:
   - Verify PowerDNS is running: `systemctl status pdns` or `ps aux | grep pdns`
   - Check FlyingDdns is running: `systemctl status flyingddns` or `ps aux | grep flyingddns`
   - Test API connectivity: `curl --ipv6 --silent ${PDNS_API_URL}/api/v1/servers/localhost`

2. **Configuration problems**:
   - Verify environment variables: `echo $DOMAIN_NAME` to check if variables are set correctly
   - Check configuration files: Ensure PowerDNS API is enabled and the key matches
   - Validate port availability: `netstat -tuln | grep 8081` and `netstat -tuln | grep 4000`

3. **DNS resolution issues**:
   - Test DNS resolution: `host -t A example.org localhost`
   - Check zone configuration: `pdnsutil list-zone example.org`
   - Verify record creation: `pdnsutil list-all-zones`

4. **Log analysis**:
   - Robot Framework logs: Check `log.html` for detailed test execution
   - PowerDNS logs: `/var/log/pdns/pdns.log` or `journalctl -u pdns`
   - FlyingDdns logs: Check application logs for API errors

5. **Common errors and solutions**:
   - "Zone not found": Ensure the zone creation step succeeded
   - "API unauthorized": Check the API key in `variables.resource`
   - "Connection refused": Verify the server is running and ports are correct
   - "Record already exists": Clean up test data before running tests

## Integration with CI/CD

These tests are integrated into the build process and run automatically when building with Packer. See the `packer/build.sh` script for details. 

## Test Data Versioning

As your test suite evolves, you may need to version your test data. Here are some best practices:

1. **Document Changes**: When modifying test data, document the changes in commit messages and update this README.
2. **Semantic Versioning**: Consider using semantic versioning for significant test data changes:
   - Major version: Breaking changes to test data structure
   - Minor version: Adding new test data elements
   - Patch version: Fixing errors in existing test data
3. **Migration Scripts**: For major changes, provide migration scripts to update test environments.
4. **Backwards Compatibility**: Try to maintain backwards compatibility when possible.
5. **Change History**: Maintain a brief history of significant test data changes:

### Test Data Change History

| Date       | Version | Description of Changes                                    |
|------------|---------|----------------------------------------------------------|
| YYYY-MM-DD | 1.0.0   | Initial test data documentation                           |
| YYYY-MM-DD | 1.1.0   | Added IPv6 test cases                                     |
| YYYY-MM-DD | 1.2.0   | Added dynamic DNS update tests                            |
| YYYY-MM-DD | 2.0.0   | Restructured test data to support multiple domain testing |

## Quick Reference Guide

### Common Test Commands

| Task                                | Command                                                                                |
|-------------------------------------|----------------------------------------------------------------------------------------|
| Run all tests                       | `cd packer/test && robot --parser GherkinParser *.feature`                             |
| Run PowerDNS API tests              | `cd packer/test && robot --parser GherkinParser pdns_api.feature`                      |
| Run pdnsutil tests                  | `cd packer/test && robot --parser GherkinParser pdnsutil.feature`                      |
| Run FlyingDdns API tests            | `cd packer/test && robot --parser GherkinParser fddns_api.feature`                     |
| Run with custom domain              | `robot --parser GherkinParser --variable DOMAIN_NAME:custom.example.com pdns_api.feature` |
| Generate only XML report            | `robot --parser GherkinParser --output output.xml --log NONE --report NONE *.feature`  |
| Run tests with debug output         | `robot --parser GherkinParser --loglevel DEBUG *.feature`                              |
| Run a specific test case            | `robot --parser GherkinParser --test "Check status" pdns_api.feature`                  |

### Common PowerDNS Commands

| Task                                | Command                                                                                |
|-------------------------------------|----------------------------------------------------------------------------------------|
| List all zones                      | `pdnsutil list-all-zones`                                                              |
| Check zone                          | `pdnsutil check-zone example.org`                                                      |
| List zone records                   | `pdnsutil list-zone example.org`                                                       |
| Create zone                         | `pdnsutil create-zone example.org ns1.example.org`                                     |
| Add record                          | `pdnsutil add-record example.org @ A 1.0.0.1`                                          |
| Delete zone                         | `pdnsutil delete-zone example.org`                                                     |

### Common FlyingDdns API Calls

| Task                                | Command                                                                                |
|-------------------------------------|----------------------------------------------------------------------------------------|
| Check status                        | `curl --ipv6 --silent http://[::1]:4000/status`                                        |
| Create zone                         | `curl --ipv6 --silent 'http://[::1]:4000/create_zone?zonename=example.org&nameserver1=ns1.example.org'` |
| Update record                       | `curl --ipv6 --silent 'http://[::1]:4000/update?hostname=test.example.org&myip=1.0.0.1'` |
| Get version                         | `curl --ipv6 --silent http://[::1]:4000/version`                                       | 