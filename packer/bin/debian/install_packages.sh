#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# Debian Package Installation Script
# ===============================================================================
#
# This script installs necessary packages on Debian during the Packer
# build process for FlyingDdns. It ensures all required dependencies are
# available for the application.
#
# The script performs the following tasks:
# 1. Updates the package repository information
# 2. Installs a predefined set of packages required for FlyingDdns
# 3. Verifies successful installation of each package
#
# Packages installed include:
# - Development tools (build-essential, git)
# - Database (postgresql, postgresql-client)
# - DNS server (pdns-server, pdns-backend-pgsql, pdns-recursor)
# - Monitoring tools (vnstat)
# - Runtime dependencies (erlang, elixir)
# - System utilities (curl, bash, etc.)
#
# Usage:
#   ./install_packages.sh
#
# Exit Codes:
#   0 - Success
#   1 - Failed to update package repository
#   2 - Failed to install one or more packages
#
# ===============================================================================

script_name="install_packages.sh"

printf "%b %b INFO:  Installing the required packages:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! DEBIAN_FRONTEND="noninteractive" apt-get install --yes --quiet postgresql pdns-backend-pgsql make gcc libncurses-dev libssl-dev unzip tmux python3-venv curl git jq dnsutils; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Prepare python virtual environment:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (mkdir -p "${HOME}"/venv &&
    python3 -m venv "${HOME}"/venv &&
    "${HOME}"/venv/bin/python -m pip install --upgrade pip); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Installing the robotframework and robotframework-gherkin-parser pip package:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
# shellcheck disable=SC2154 #var is referenced but not assigned.
if ! ("${HOME}"/venv/bin/python -m pip install robotframework=="${robotframework_version}" robotframework-gherkin-parser=="${robotframework_gherkin_parser_version}"); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Make sure we are in the user\'s home directory:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! cd "${HOME}"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Getting asdf:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
# shellcheck disable=SC2154 #var is referenced but not assigned.
if ! (git config --global advice.detachedHead false && git clone https://github.com/asdf-vm/asdf.git "${HOME}"/.asdf --branch v"${asdf_version}") 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 5
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Enabling asdf:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! printf "\n. %b/.asdf/asdf.sh\n" "${HOME}" >>"${HOME}"/.profile; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 6
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Activating asdf:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
# shellcheck disable=SC1091 #Not following: (error message here)
if ! ASDF_DIR="${HOME}"/.asdf . "${HOME}"/.profile; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 7
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Installing erlang:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (asdf plugin-add erlang && asdf install erlang latest && asdf global erlang latest) 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 8
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Installing elixir:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (asdf plugin-add elixir && asdf install elixir latest && asdf global elixir latest) 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 9
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
