#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# Debian PowerDNS Configuration Script
# ===============================================================================
#
# This script configures and starts the PowerDNS authoritative server and
# recursor on Debian during the Packer build process for FlyingDdns. It ensures
# the DNS services are properly set up to work with the application.
#
# The script performs the following tasks:
# 1. Enables PowerDNS services to start at boot
# 2. Configures PowerDNS authoritative server settings
# 3. Configures PowerDNS recursor settings
# 4. Starts PowerDNS services
# 5. Verifies that the services are running correctly
#
# PowerDNS is used as the DNS server that FlyingDdns interacts with to manage
# dynamic DNS records.
#
# Usage:
#   ./configure_powerdns.sh
#
# Exit Codes:
#   0 - Success
#   1 - Failed to enable PowerDNS authoritative server
#   2 - Failed to enable PowerDNS recursor
#   3 - Failed to start PowerDNS authoritative server
#   4 - Failed to start PowerDNS recursor
#   5 - PowerDNS authoritative server failed to become ready
#   6 - PowerDNS recursor failed to become ready
#   7 - Failed to verify PowerDNS operation
#
# ===============================================================================

script_name="configure_powerdns.sh"

printf "%b %b INFO:  Adding the pdns role in postgresql:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! su - postgres -c "psql --host=::1 --command=\"CREATE ROLE ${PDNS_DB_USER} WITH ENCRYPTED PASSWORD '${PDNS_DB_PASSWD}' LOGIN;\""; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Creating the pdns database in postgresql:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! su - postgres -c "psql --host=::1 --command=\"CREATE DATABASE ${PDNS_DB_NAME} OWNER ${PDNS_DB_USER};\""; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Creating the pdns database in postgresql:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! su - postgres -c "PGPASSWORD=${PDNS_DB_PASSWD} psql --host=::1 --username=${PDNS_DB_USER} --dbname ${PDNS_DB_NAME} --command=\"\\include /usr/share/pdns-backend-pgsql/schema/schema.pgsql.sql;\""; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Executing the dt command in postgresql:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! su - postgres -c "PGPASSWORD=${PDNS_DB_PASSWD} psql --host=::1 --username=${PDNS_DB_USER} --dbname ${PDNS_DB_NAME} --command=\"\\dt;\""; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Trying the select from comments in postgresql:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! su - postgres -c "PGPASSWORD=${PDNS_DB_PASSWD} psql --host=::1 --username=${PDNS_DB_USER} --dbname ${PDNS_DB_NAME} --command=\"select * from comments;\""; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 5
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Disabling include dirs:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! sed -i 's/include-dir=\/etc\/powerdns\/pdns.d/#include-dir=\/etc\/powerdns\/pdns.d/g' /etc/powerdns/pdns.conf; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 6
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Disabling launch:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! sed -i 's/launch=/#launch=/g' /etc/powerdns/pdns.conf; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 7
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Disabling security-poll-suffix:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! sed -i 's/security-poll-suffix=/#security-poll-suffix=/g' /etc/powerdns/pdns.conf; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 8
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Modifying the pdns.conf configuration file:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
cat >>/etc/powerdns/pdns.conf <<EOF

version-string=anonymous
launch=gpgsql
gpgsql-host=::1
gpgsql-dbname=${PDNS_DB_NAME}
gpgsql-user=${PDNS_DB_USER}
gpgsql-password=${PDNS_DB_PASSWD}
api=yes
api-key=${PDNS_API_PASSWD}
webserver=yes
webserver-address=::1
EOF
# shellcheck disable=SC2181 #Check exit code directly with e.g. if mycmd;, not indirectly with $?.
if [ "$?" != "0" ]; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 9
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Restarting the pdns service:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! systemctl restart pdns.service; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 10
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Waiting untill the pdns service is up and running:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
i="0"
max_tries="12"
sleep_time="5"
while ! (host localhost ::1 | grep "REFUSED") 2>&1; do
    if [ "${i}" -lt "${max_tries}" ]; then
        printf "%b %b DEBUG: Sleeping %b seconds...\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${sleep_time}"
        sleep "${sleep_time}"
    else
        printf "%b %b ERROR: ==>> FAILED: max check retries reached\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
        exit 11
    fi
    i="$((i + 1))"
done
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Check if the pdns api is accessible:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! curl --ipv6 --silent --request GET http://\[::1\]:8081/api/v1/servers/localhost | grep Unauthorized; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 12
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
