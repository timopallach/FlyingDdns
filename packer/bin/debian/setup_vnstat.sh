#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# Debian VnStat Setup Script
# ===============================================================================
#
# This script installs and configures VnStat (network traffic monitor) on
# Debian during the Packer build process for FlyingDdns.
#
# The script performs the following tasks:
# 1. Installs the VnStat package if not already installed
# 2. Configures VnStat to monitor the eth0 network interface
# 3. Enables the VnStat service to start at boot
# 4. Starts the VnStat service
# 5. Verifies that the service is running correctly
#
# VnStat is used to monitor network traffic during and after the build process,
# providing valuable metrics for optimization and troubleshooting.
#
# Usage:
#   ./setup_vnstat.sh
#
# Exit Codes:
#   0 - Success
#   1 - Failed to install VnStat
#   2 - Failed to enable VnStat service
#   3 - Failed to start VnStat service
#
# ===============================================================================

script_name="setup_vnstat.sh"

egress_interface=$(ip route | grep default | rev | cut -d " " -f 2 | rev)

printf "%b %b INFO:  Disable APT CDROM entry:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! sed -i 's/deb cdrom:/#deb cdrom:/g' /etc/apt/sources.list; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Updating the apt cache:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! apt-get --yes update; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Installing the vnstat package:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! DEBIAN_FRONTEND="noninteractive" apt-get install --yes --quiet vnstat; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Setting the parameters in the vnstat config:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! printf "\nInterface \"%b\"\nSaveInterval 1\n" "${egress_interface}" >>/etc/vnstat.conf; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Restarting the vnstat service:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! systemctl restart vnstat.service; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 5
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
