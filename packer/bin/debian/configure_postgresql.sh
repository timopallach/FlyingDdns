#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# Debian PostgreSQL Configuration Script
# ===============================================================================
#
# This script configures and starts the PostgreSQL database service on Debian
# during the Packer build process for FlyingDdns. It ensures the database is
# properly set up and ready for use by the application.
#
# The script performs the following tasks:
# 1. Enables the PostgreSQL service to start at boot
# 2. Starts the PostgreSQL service
# 3. Waits for the service to be fully operational
# 4. Verifies database access by listing databases as the postgres user
#
# PostgreSQL is used as the backend database for both FlyingDdns and PowerDNS.
#
# Usage:
#   ./configure_postgresql.sh
#
# Exit Codes:
#   0 - Success
#   1 - Failed to enable PostgreSQL service
#   2 - Failed to start PostgreSQL service
#   3 - PostgreSQL service failed to become ready
#   4 - Failed to access PostgreSQL database
#
# ===============================================================================

script_name="configure_postgresql.sh"

postgresql_version="$(psql --version | cut -d " " -f 3 | cut -d "." -f 1)"

printf "%b %b INFO:  Allowing all operating system users to access the postgres as db users via tcp v6:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! sed -i "s/host    all             all             ::1\/128                 scram-sha-256/host    all             all             ::1\/128                 trust/g" /etc/postgresql/"${postgresql_version}"/main/pg_hba.conf; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Restart the postgres service to reload the config change:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! systemctl restart postgresql.service; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Waiting untill the postgresql service is up and running:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
i="0"
max_tries="12"
sleep_time="5"
while ! (pg_isready | grep "accepting connections") 2>&1; do
    if [ "${i}" -lt "${max_tries}" ]; then
        printf "%b %b DEBUG: Sleeping %b seconds...\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${sleep_time}"
        sleep "${sleep_time}"
    else
        printf "%b %b ERROR: ==>> FAILED: max check retries reached\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
        exit 3
    fi
    i="$((i + 1))"
done
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Accessing the postresql database as user postgres:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! su - postgres -c "psql --host=::1 --command=\"\\list\" | grep \"(3 rows)\""; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
