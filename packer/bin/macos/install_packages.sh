#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns macOS Package Installation Script
# ===============================================================================
#
# This script installs all required packages for the FlyingDdns environment on
# macOS systems using Homebrew. It also sets up a Python virtual environment and
# installs the necessary Python packages for testing.
#
# The script performs the following operations:
# 1. Updates Homebrew to ensure the latest package definitions are available
# 2. Installs required system packages:
#    - PostgreSQL (latest version available)
#    - PowerDNS (pdns)
#    - Elixir (for FlyingDdns application)
#    - tmux, python3, curl, git, jq (utilities)
# 3. Creates a Python virtual environment in the user's home directory
# 4. Upgrades pip to the latest version
# 5. Installs Robot Framework and the Gherkin parser for automated testing
#
# The script uses environment variables for version control:
# - robotframework_version: Version of Robot Framework to install
# - robotframework_gherkin_parser_version: Version of the Gherkin parser to install
#
# Requirements:
# - Homebrew installed and configured
# - Python 3 available
# ===============================================================================

script_name="install_packages.sh"

PATH="/opt/homebrew/bin:/opt/homebrew/sbin:${PATH}"
postgresql_version=$(brew search postgresql | grep @ | sort -V | tail -n 1 | cut -d "@" -f 2)
export HOMEBREW_NO_ENV_HINTS="1"

printf "%b %b INFO:  Updating brew:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! brew update --quiet --auto-update 2>&1; then
    printf_link_to_sources "${script_name}" "$((LINENO - 1))"
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Installing the required packages:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! brew install --quiet postgresql@"${postgresql_version}" pdns elixir tmux python3 curl git jq; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Prepare python virtual environment:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (mkdir -p "${HOME}"/venv &&
    python3 -m venv "${HOME}"/venv &&
    "${HOME}"/venv/bin/python -m pip install --upgrade pip); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Installing the robotframework and robotframework-gherkin-parser pip package:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
# shellcheck disable=SC2154 #var is referenced but not assigned.
if ! ("${HOME}"/venv/bin/python -m pip install robotframework=="${robotframework_version}" robotframework-gherkin-parser=="${robotframework_gherkin_parser_version}"); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
