#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Network Monitoring Setup Script
# ===============================================================================
#
# This script sets up network monitoring using vnstat on macOS systems. It also
# configures DNS settings to use Quad9 DNS servers for improved security and
# reliability during the build and test process.
#
# The script performs the following operations:
# 1. Sets Quad9 (9.9.9.9) as the system-wide DNS server for the Ethernet interface
# 2. Updates Homebrew to ensure the latest package definitions are available
# 3. Installs the vnstat package for network traffic monitoring
# 4. Configures vnstat to monitor the default network interface
# 5. Sets the save interval to 1 second for more granular monitoring
# 6. Starts the vnstat service using Homebrew services
#
# The network statistics collected by vnstat are later used by the analyze_build.sh
# script to measure network usage during the build and test process.
#
# Requirements:
# - Homebrew installed and configured
# - Administrative privileges for network configuration
# - Default network interface must be active
# ===============================================================================

script_name="setup_vnstat.sh"

printf "%b %b INFO:  PATH=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${PATH}"

PATH="/opt/homebrew/bin:/opt/homebrew/sbin:${PATH}"
egress_interface=$(netstat -r -f inet | grep default | rev | cut -w -f 2 | rev)
homebrew_dir="$(brew --prefix)"
export HOMEBREW_NO_ENV_HINTS="1"

printf "%b %b INFO:  Setting QUAD9 as systemwide DNS server:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! networksetup -setdnsservers Ethernet 9.9.9.9; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Updating brew:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! brew update --quiet --auto-update 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Installing the vnstat package:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! brew install --quiet vnstat; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Setting the parameters in the vnstat config:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! printf "\nInterface \"%b\"\nSaveInterval 1\n" "${egress_interface}" >>"${homebrew_dir}/etc/vnstat.conf"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Starting the vnstat service:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! sudo brew services start vnstat 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 5
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
