#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns PostgreSQL Configuration Script
# ===============================================================================
#
# This script configures PostgreSQL for the FlyingDdns environment on macOS.
# It installs, starts, and configures the PostgreSQL database service using
# Homebrew, and creates the necessary user accounts for the application.
#
# The script performs the following operations:
# 1. Installs Homebrew services for managing PostgreSQL
# 2. Starts the PostgreSQL service using the version installed by Homebrew
# 3. Waits for the PostgreSQL service to be fully operational (accepting connections)
# 4. Creates a PostgreSQL superuser named 'postgres' for compatibility with
#    other operating systems and for PowerDNS configuration
# 5. Verifies database access using the newly created postgres user
#
# The PostgreSQL database is used by PowerDNS for DNS zone storage and by
# the FlyingDdns application for its own data storage needs.
#
# Requirements:
# - Homebrew with PostgreSQL installed (handled by install_packages.sh)
# - User must have permissions to create PostgreSQL users
# ===============================================================================

script_name="configure_postgresql.sh"

PATH="/opt/homebrew/bin:/opt/homebrew/sbin:${PATH}"
postgresql_version=$(brew list | grep postgresql | cut -d "@" -f 2)
PATH="${PATH}:$(brew --prefix)/opt/postgresql@${postgresql_version}/bin"

printf "%b %b INFO:  Install brew services:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! brew tap --quiet homebrew/services; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Starting the postgresql service:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! brew services start postgresql@"${postgresql_version}"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Waiting untill the postgresql service is up and running:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
i="0"
max_tries="12"
sleep_time="5"
while ! (pg_isready | grep "accepting connections") 2>&1; do
    if [ "${i}" -lt "${max_tries}" ]; then
        printf "%b %b DEBUG: Sleeping %b seconds...\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${sleep_time}"
        sleep "${sleep_time}"
    else
        printf "%b %b ERROR: ==>> FAILED: max check retries reached\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
        exit 3
    fi
    i="$((i + 1))"
done
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Creating the postgres superuser:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! createuser --superuser postgres; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Accessing the postresql database as user postgres:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (psql --host=::1 --username=postgres --command="\\list" | grep "(3 rows)"); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 5
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
