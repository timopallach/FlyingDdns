#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns PowerDNS Configuration Script
# ===============================================================================
#
# This script configures PowerDNS for the FlyingDdns environment on macOS.
# It sets up the PowerDNS server with SQLite backend, configures the API access,
# and ensures the service is running correctly.
#
# The script performs the following operations:
# 1. Creates the PowerDNS database directory
# 2. Initializes the SQLite database with the PowerDNS schema
# 3. Creates and configures the PowerDNS configuration file (pdns.conf)
#    - Sets up SQLite as the backend database
#    - Enables the API interface with authentication
#    - Configures the webserver for API access
# 4. Creates the PowerDNS run directory for socket files
# 5. Starts the PowerDNS service using Homebrew services
# 6. Waits for the PowerDNS service to be fully operational
# 7. Verifies that the PowerDNS API is accessible
#
# The PowerDNS server provides DNS services for the FlyingDdns application,
# allowing dynamic DNS updates through its API interface.
#
# Environment variables used:
# - PDNS_API_PASSWD: Password for PowerDNS API authentication
#
# Requirements:
# - Homebrew with PowerDNS installed (handled by install_packages.sh)
# - SQLite3 command-line tool
# - PostgreSQL configured and running (handled by configure_postgresql.sh)
# ===============================================================================

script_name="configure_powerdns.sh"

PATH="/opt/homebrew/bin:/opt/homebrew/sbin:${PATH}"
homebrew_dir="$(brew --prefix)"
pdns_version="$(brew info pdns | grep files | cut -d "/" -f 6 | cut -d " " -f 1)"

printf "%b %b INFO:  Creating the PowerDNS database directory:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! mkdir -p "${homebrew_dir}/var/db/pdns"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Creating the PowerDNS database:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (sqlite3 -init "${homebrew_dir}"/Cellar/pdns/"${pdns_version}"/share/doc/pdns/schema.sqlite3.sql "${homebrew_dir}/var/db/pdns/powerdns.sqlite"); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Copying the template pdns config file:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! cp "${homebrew_dir}/etc/powerdns/pdns.conf-dist" "${homebrew_dir}/etc/powerdns/pdns.conf"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Modifying the pdns.conf configuration file:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
cat >>"${homebrew_dir}/etc/powerdns/pdns.conf" <<EOF

version-string=anonymous
launch=gsqlite3
gsqlite3-database=${homebrew_dir}/var/db/pdns/powerdns.sqlite
socket-dir=${homebrew_dir}/var/run/pdns
api=yes
api-key=${PDNS_API_PASSWD}
webserver=yes
webserver-address=::1
EOF
# shellcheck disable=SC2181 #Check exit code directly with e.g. if mycmd;, not indirectly with $?.
if [ "$?" != "0" ]; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Creating the PowerDNS run directory:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! mkdir -p "${homebrew_dir}/var/run/pdns/"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 5
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Starting the PowerDNS service:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! brew services start pdns; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 6
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Waiting untill the pdns service is up and running:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
i="0"
max_tries="12"
sleep_time="5"
while ! (host localhost ::1 | grep "REFUSED") 2>&1; do
    if [ "${i}" -lt "${max_tries}" ]; then
        printf "%b %b DEBUG: Sleeping %b seconds...\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${sleep_time}"
        sleep "${sleep_time}"
    else
        printf "%b %b ERROR: ==>> FAILED: max check retries reached\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
        exit 7
    fi
    i="$((i + 1))"
done
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Check if the pdns api is accessible:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! curl --ipv6 --silent --request GET http://\[::1\]:8081/api/v1/servers/localhost | grep Unauthorized; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 8
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
