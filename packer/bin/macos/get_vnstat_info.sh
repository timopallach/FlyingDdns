#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Network Statistics Retrieval Script
# ===============================================================================
#
# This script retrieves network traffic statistics from vnstat on macOS systems.
# It stops the vnstat service temporarily, queries the current statistics, and
# outputs them in a standardized format that can be parsed by the analyze_build.sh
# script.
#
# The script performs the following operations:
# 1. Stops the vnstat service using brew services
# 2. Waits for 5 seconds to ensure service is fully stopped
# 3. Queries vnstat for current statistics
# 4. Outputs the statistics in a one-line format
#
# The output is used by the build analysis tools to track network usage during
# the build and test process.
#
# Requirements:
# - Homebrew with vnstat installed
# - Sudo privileges to stop the vnstat service
# ===============================================================================

script_name="get_vnstat_info.sh"

PATH="/opt/homebrew/bin:/opt/homebrew/sbin:${PATH}"

printf "%b %b INFO:  Getting the vnstat infos:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (sudo brew services stop vnstat && sleep 5 && vnstat -q && printf "vnstat info: " && vnstat --oneline); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
