#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns macOS System Update Script
# ===============================================================================
#
# This script is a placeholder for system updates on macOS. Unlike other operating
# systems, macOS does not provide a straightforward command-line method for
# performing system updates or security patches.
#
# The script simply logs that macOS updates cannot be performed from the shell
# and exits successfully. This maintains compatibility with the build system,
# which expects an update_system.sh script for each supported operating system.
#
# In a production environment, macOS updates should be managed through:
# - Apple's Software Update mechanism
# - Mobile Device Management (MDM) solutions
# - The softwareupdate command with appropriate privileges
#
# For the FlyingDdns build environment, we assume the base macOS system is
# already up to date.
# ===============================================================================

script_name="update_system.sh"

printf "%b %b INFO:  macOS does not have the posibility to do security updates from the shell.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
