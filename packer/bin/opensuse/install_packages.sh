#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# openSUSE Package Installation Script
# ===============================================================================
#
# This script installs necessary packages on openSUSE during the Packer
# build process for FlyingDdns. It ensures all required dependencies are
# available for the application.
#
# The script performs the following tasks:
# 1. Updates the zypper repositories
# 2. Determines the appropriate Python version
# 3. Installs a predefined set of packages required for FlyingDdns
# 4. Sets up a Python virtual environment
# 5. Installs Python packages needed for testing
# 6. Installs Erlang and Elixir via asdf version manager
#
# Packages installed include:
# - Database (postgresql-server, postgresql-contrib)
# - DNS server (pdns-backend-postgresql)
# - Development tools (make, gcc, ncurses-devel, openssl-devel)
# - System utilities (tmux, python, curl, git-core, jq)
#
# Usage:
#   ./install_packages.sh
#
# Exit Codes:
#   0 - Success
#   1 - Failed to update zypper repositories
#   2 - Failed to install system packages
#   3 - Failed to set up Python virtual environment
#   4 - Failed to install Python packages
#   5-9 - Various failures in Erlang/Elixir installation
#
# ===============================================================================

script_name="install_packages.sh"

printf "%b %b INFO:  Updating the zypper repos:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! zypper update --no-confirm; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

python_version_short="$(zypper search python3 | grep -- -pip | sort -V | tail -n 1 | cut -d "n" -f 2 | cut -d "-" -f 1)"
python_version="$(echo "${python_version_short}" | cut -c 1).$(echo "${python_version_short}" | cut -c 2-)"

printf "%b %b INFO:  Installing the required packages:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! zypper install --no-confirm --force postgresql-server postgresql-contrib pdns-backend-postgresql make gcc ncurses-devel openssl-devel tmux python"${python_version_short}" curl git-core jq; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Prepare python virtual environment:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (mkdir -p "${HOME}"/venv &&
    python"${python_version}" -m venv "${HOME}"/venv &&
    "${HOME}"/venv/bin/python -m pip install --upgrade pip); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Installing the robotframework and robotframework-gherkin-parser pip package:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
# shellcheck disable=SC2154 #var is referenced but not assigned.
if ! ("${HOME}"/venv/bin/python -m pip install robotframework=="${robotframework_version}" robotframework-gherkin-parser=="${robotframework_gherkin_parser_version}"); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Make sure we are in the user\'s home directory:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! cd "${HOME}"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 5
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Getting asdf:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
# shellcheck disable=SC2154 #var is referenced but not assigned.
if ! (git config --global advice.detachedHead false && git clone https://github.com/asdf-vm/asdf.git "${HOME}"/.asdf --branch v"${asdf_version}") 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 6
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Enabling asdf:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! printf "\n. %b/.asdf/asdf.sh\n" "${HOME}" >>"${HOME}"/.profile; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 7
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Activating asdf:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
# shellcheck disable=SC1091 #Not following: (error message here)
if ! . "${HOME}"/.profile; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 8
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Installing erlang:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (asdf plugin-add erlang && asdf install erlang latest && asdf global erlang latest) 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 9
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Installing elixir:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (asdf plugin-add elixir && asdf install elixir latest && asdf global elixir latest) 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 10
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
