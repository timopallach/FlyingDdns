#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# Rocky Linux System Update Script
# ===============================================================================
#
# This script updates the Rocky Linux system during the Packer build process
# for FlyingDdns. It ensures the base system is up-to-date before installing
# application-specific packages and configurations.
#
# The script performs the following tasks:
# 1. Upgrades all installed packages to their latest versions using dnf
# 2. Reboots the system to apply any kernel updates
#
# This script is typically run at the beginning of the Packer build process
# to ensure a clean, up-to-date base system.
#
# Usage:
#   ./update_system.sh
#
# Exit Codes:
#   0 - Success
#   1 - Failed to upgrade packages
#   2 - Failed to reboot the system
#
# ===============================================================================

script_name="update_system.sh"

printf "%b %b INFO:  Upgrading the installed packages:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! dnf upgrade --assumeyes 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Rebooting the system:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! reboot; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
