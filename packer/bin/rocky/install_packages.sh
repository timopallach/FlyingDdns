#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

script_name="install_packages.sh"

printf "%b %b INFO:  Updating the package repositories:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! dnf check-update 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Installing the required packages:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! dnf install --setopt install_weak_deps=false --assumeyes postgresql-server postgresql-contrib pdns-backend-postgresql automake autoconf make gcc openssl-devel ncurses-devel tmux python3 curl git-core jq bind-utils tar unzip 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Prepare python virtual environment:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (mkdir -p "${HOME}"/venv &&
    python3 -m venv "${HOME}"/venv &&
    "${HOME}"/venv/bin/python -m pip install --upgrade pip); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Installing the robotframework and robotframework-gherkin-parser pip package:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
# shellcheck disable=SC2154 #var is referenced but not assigned.
if ! ("${HOME}"/venv/bin/python -m pip install robotframework=="${robotframework_version}" robotframework-gherkin-parser=="${robotframework_gherkin_parser_version}"); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Make sure we are in the user\'s home directory:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! cd "${HOME}"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 5
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Getting asdf:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
# shellcheck disable=SC2154 #var is referenced but not assigned.
if ! (git config --global advice.detachedHead false && git clone https://github.com/asdf-vm/asdf.git "${HOME}"/.asdf --branch v"${asdf_version}") 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 6
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Enabling asdf:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! printf "\n. %b/.asdf/asdf.sh\n" "${HOME}" >>"${HOME}"/.profile; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 7
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Activating asdf:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
# shellcheck disable=SC1091 #Not following: (error message here)
if ! . "${HOME}"/.profile; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 8
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Installing erlang:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (asdf plugin-add erlang && asdf install erlang latest && asdf global erlang latest) 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 9
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Installing elixir:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (asdf plugin-add elixir && asdf install elixir latest && asdf global elixir latest) 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 10
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
