#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

script_name="setup_vnstat.sh"

egress_interface=$(ip route | grep default | cut -d " " -f 5)

printf "%b %b INFO:  Installing the EPEL package:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! dnf install --setopt install_weak_deps=false --assumeyes epel-release 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Installing the vnstat package:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! dnf install --setopt install_weak_deps=false --assumeyes vnstat 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Setting the parameters in the vnstat config:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! printf "\nInterface \"%b\"\nSaveInterval 1\n" "${egress_interface}" >/etc/vnstat.conf; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Enable the vnstat service:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! systemctl enable vnstat.service 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Restarting the vnstat service:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! systemctl restart vnstat.service; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 5
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
