#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

script_name="update_system.sh"

printf "%b %b INFO:  Stop systemds internal dns resolver:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! systemctl stop systemd-resolved; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Disable systemds internal dns resolver:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! systemctl disable systemd-resolved 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Add the hosts DNS server to be able to resolve dns requests:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (rm -rf /etc/resolv.conf && echo "nameserver 9.9.9.9" >>/etc/resolv.conf); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Updating the apt cache:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! apt-get --yes update; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Dist upgrading the system:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! DEBIAN_FRONTEND="noninteractive" apt-get --yes --quiet dist-upgrade 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 5
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Rebooting the system:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! reboot; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 6
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
