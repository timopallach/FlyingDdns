#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# NixOS Package Installation Script
# ===============================================================================
#
# This script installs necessary packages on NixOS during the Packer
# build process for FlyingDdns. It ensures all required dependencies are
# available for the application.
#
# The script performs the following tasks:
# 1. Modifies the NixOS configuration file to include required packages
# 2. Rebuilds the NixOS system to apply the configuration changes
# 3. Sets up a Python virtual environment for testing tools
# 4. Installs Python packages needed for testing
#
# Packages installed include:
# - Database (postgresql)
# - DNS server (powerdns)
# - Runtime dependencies (elixir)
# - Development tools (gnumake, gcc)
# - System utilities (tmux, python3, curl, git, jq)
#
# Usage:
#   ./install_packages.sh
#
# Exit Codes:
#   0 - Success
#   1 - Failed to modify NixOS configuration
#   2 - Failed to rebuild NixOS system
#   3 - Failed to set up Python virtual environment
#   4 - Failed to install Python packages
#
# ===============================================================================

script_name="install_packages.sh"

printf "%b %b INFO:  Added the required packages to the configuration file:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! sed -i 's/services.vnstat.enable = true;/services.vnstat.enable = true;\n  environment.systemPackages = with pkgs; [\n    postgresql\n    powerdns\n    elixir\n    gnumake\n    gcc\n    tmux\n    python3\n    curl\n    git\n    jq\n  ];/g' /etc/nixos/configuration.nix; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Deploying changes made in the configuration.nix file:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! nixos-rebuild switch 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Prepare python virtual environment:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (mkdir -p "${HOME}"/venv &&
    python -m venv "${HOME}"/venv &&
    "${HOME}"/venv/bin/python -m pip install --upgrade pip); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Installing the robotframework and robotframework-gherkin-parser pip package:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
# shellcheck disable=SC2154 #var is referenced but not assigned.
if ! ("${HOME}"/venv/bin/python -m pip install robotframework=="${robotframework_version}" robotframework-gherkin-parser=="${robotframework_gherkin_parser_version}"); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
