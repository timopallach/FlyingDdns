#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

script_name="configure_powerdns.sh"

printf "%b %b INFO:  Adding the pdns role in postgresql:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! psql --host=::1 --username=postgres --command="CREATE ROLE ${PDNS_DB_USER} WITH ENCRYPTED PASSWORD '${PDNS_DB_PASSWD}' LOGIN;"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Creating the pdns database in postgresql:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! psql --host=::1 --username=postgres --command="CREATE DATABASE ${PDNS_DB_NAME} OWNER ${PDNS_DB_USER};"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Curling the latest pdns psql schema:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! curl --silent --output /tmp/schema.pgsql.sql https://raw.githubusercontent.com/PowerDNS/pdns/master/modules/gpgsqlbackend/schema.pgsql.sql; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Importing the pdns database schema into postgresql:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! PGPASSWORD=${PDNS_DB_PASSWD} psql --host=::1 --username="${PDNS_DB_USER}" --dbname "${PDNS_DB_NAME}" --command="\\include /tmp/schema.pgsql.sql;"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Executing the dt command in postgresql:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! psql --host=::1 --username="${PDNS_DB_USER}" --dbname "${PDNS_DB_NAME}" --command="\\dt;"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 5
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Trying the select from comments in postgresql:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! psql --host=::1 --username="${PDNS_DB_USER}" --dbname "${PDNS_DB_NAME}" --command="select * from comments;"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 6
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Configure the PowerDNS service:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! sed -i "s/services.vnstat.enable = true;/services.vnstat.enable = true;\n  services.powerdns = {\n    enable = true;\n    extraConfig = ''\n      version-string=anonymous\n      launch=gpgsql\n      gpgsql-host=::1\n      gpgsql-dbname=${PDNS_DB_NAME}\n      gpgsql-user=${PDNS_DB_USER}\n      gpgsql-password=${PDNS_DB_PASSWD}\n      api=yes\n      api-key=${PDNS_API_PASSWD}\n      webserver=yes\n      webserver-address=::1\n    '';\n  };/g" /etc/nixos/configuration.nix; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 7
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Deploying changes made in the configuration.nix file:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! nixos-rebuild switch 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 8
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Waiting untill the pdns service is up and running:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
i="0"
max_tries="12"
sleep_time="5"
while ! (host localhost ::1 | grep "REFUSED") 2>&1; do
    if [ "${i}" -lt "${max_tries}" ]; then
        printf "%b %b DEBUG: Sleeping %b seconds...\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${sleep_time}"
        sleep "${sleep_time}"
    else
        printf "%b %b ERROR: ==>> FAILED: max check retries reached\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
        exit 9
    fi
    i="$((i + 1))"
done
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Check if the pdns api is accessible:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! curl --ipv6 --silent --request GET http://\[::1\]:8081/api/v1/servers/localhost | grep Unauthorized; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 10
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
