#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

script_name="setup_vnstat.sh"

# https://search.nixos.org/packages
# First enable #services.vnstat.enable = true;
# Execute "nixos-rebuild switch"

printf "%b %b INFO:  Configure the vnstat service:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! sed -i 's/services.openssh.settings.PermitRootLogin = \"yes\";/services.openssh.settings.PermitRootLogin = \"yes\";\n  services.vnstat.enable = true;/g' /etc/nixos/configuration.nix; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Deploying changes made in the configuration.nix file:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! nixos-rebuild switch 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
