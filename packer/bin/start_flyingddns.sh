#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Server Startup Script
# ===============================================================================
#
# This script starts the FlyingDdns server application in a tmux session for
# persistent operation. It handles the following tasks:
#
# 1. Terminates any existing FlyingDdns tmux sessions
# 2. Navigates to the appropriate directory
# 3. Sets up environment variables for the application
# 4. Creates a new tmux session for the server
# 5. Starts the FlyingDdns application in production mode
#
# The script ensures that the server runs in a detached tmux session, allowing
# it to continue running even after the initial terminal is closed.
#
# Usage:
#   ./start_flyingddns.sh
#
# Environment Variables:
#   os_name - The operating system name (set by Packer)
#
# ===============================================================================

script_name="start_flyingddns.sh"

flyingddns_tmux_name="FlyingDdns-Server"

# shellcheck disable=SC2154 #var is referenced but not assigned.
if [ "${os_name}" = "macos" ]; then
    PATH="/opt/homebrew/bin:/opt/homebrew/sbin:${PATH}"
fi

printf "\n# Starting FlyingDdns\n"
printf "################################################################################\n"
printf "%b %b INFO:  Killing possibly running tmux sessions:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if (tmux ls 2>&1 | grep "${flyingddns_tmux_name}"); then
    printf "%b %b DEBUG: Found a tmux session called \"%b\". I am closing it now.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${flyingddns_tmux_name}"
    if ! tmux kill-session -t "${flyingddns_tmux_name}"; then
        printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
        exit 1
    fi
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Step into the user\'s home directory:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! cd "${HOME}"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Extract the tar.gz package file:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
# shellcheck disable=SC2154 #var is referenced but not assigned.
if ! tar xfz "flyingddns-${flyingddns_version}_${os_name}-${os_version}-${arch_name}.tar.gz"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Starting the phx server within tmux:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! tmux new-session -d -s "${flyingddns_tmux_name}" "_build/${MIX_ENV}/rel/flying_ddns/bin/flying_ddns start"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Waiting untill the phx server is up and running:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
i="0"
max_curl_tries="12"
sleep_time="5"
curl_output="$(curl --ipv6 --silent http://\[::1\]:4000/status)"
printf "%b %b DEBUG: curl_output=<%b>, i=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${curl_output}" "${i}"
while [ "${curl_output}" != "OK" ]; do
    if [ "${i}" -lt "${max_curl_tries}" ]; then
        printf "%b %b DEBUG: Sleeping %b seconds...\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${sleep_time}"
        sleep "${sleep_time}"
    else
        printf "%b %b ERROR: ==>> FAILED: max check retries reached\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
        exit 5
    fi
    i="$((i + 1))"
    curl_output="$(curl --ipv6 --silent http://\[::1\]:4000/status)"
    printf "%b %b DEBUG: curl_output=<%b>, i=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${curl_output}" "${i}"
done
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
