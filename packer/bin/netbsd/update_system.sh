#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

script_name="update_system.sh"

printf "%b %b INFO:  NetBSD does not have the functionality to do binary security updates.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
