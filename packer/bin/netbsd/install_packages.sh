#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2024, Timo Pallach (timo@pallach.de).

script_name="install_packages.sh"

PKG_PATH="https://cdn.NetBSD.org/pub/pkgsrc/packages/NetBSD/$(uname -p)/$(uname -r | cut -d "_" -f 1)/All"
export PKG_PATH
postgresql_version="$(pkgin show-deps powerdns-pgsql | grep postgres | cut -d "-" -f 1 | cut -d "l" -f 2)"
python_version_short="$(ftp -V -o - "${PKG_PATH}" | grep -i python3 | grep -v unit | cut -d "\"" -f 2 | cut -d "-" -f 1 | cut -d "n" -f 2 | sort -n | tail -n 1)"
python_version="$(echo "${python_version_short}" | sed "s/^3/3./g")"

printf "%b %b INFO:  Installing the required packages:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! pkgin -y install postgresql"${postgresql_version}"-server postgresql"${postgresql_version}"-contrib powerdns-pgsql elixir gmake python"${python_version_short}" curl git jq; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Prepare python virtual environment:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (mkdir -p "${HOME}"/venv &&
    python"${python_version}" -m venv "${HOME}"/venv &&
    "${HOME}"/venv/bin/python -m pip install --upgrade pip); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Installing the robotframework and robotframework-gherkin-parser pip package:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
# shellcheck disable=SC2154 #var is referenced but not assigned.
if ! ("${HOME}"/venv/bin/python -m pip install robotframework=="${robotframework_version}" robotframework-gherkin-parser=="${robotframework_gherkin_parser_version}"); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
