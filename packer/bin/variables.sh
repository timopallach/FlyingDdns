#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Packer Build Variables
# ===============================================================================
#
# This script defines common variables used across the Packer build process
# for FlyingDdns. It provides a centralized location for configuration values
# that need to be consistent across multiple scripts.
#
# Usage:
#   . ./bin/variables.sh
#
# ===============================================================================

# Variables used by packer.
export PACKER_LOG="1"
export PACKER_LOG_PATH="../log/packer.log"

# PostgreSQL database name for FlyingDdns
export PGDATABASE="flying_ddns_dev"
