#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Configuration and Build Script
# ===============================================================================
#
# This script configures and builds the FlyingDdns application during the Packer
# build process. It handles platform-specific configurations, dependency setup,
# database initialization, and application compilation across different operating
# systems.
#
# The script performs the following main tasks:
# 1. Sets up environment variables and paths
# 2. Configures PostgreSQL for the application
# 3. Initializes the database schema
# 4. Compiles the Elixir application
# 5. Creates a production release
#
# This script is typically called by Packer during VM image creation.
#
# Usage:
#   ./configure_and_build_flyingddns.sh
#
# Environment Variables:
#   os_name - The operating system name (set by Packer)
#   ELIXIR_ERL_OPTIONS - Options for the Erlang VM
#
# ===============================================================================

script_name="configure_and_build_flyingddns.sh"

export ELIXIR_ERL_OPTIONS="+fnu"

export PATH="${PATH}:${HOME}/venv/bin"

# On Rocky Linux and openSUSE we have to load asdf for erlang/elixir to work.
# shellcheck disable=SC2154 #var is referenced but not assigned.
if [ "${os_name}" = "debian" ] ||
    [ "${os_name}" = "rocky" ] ||
    [ "${os_name}" = "opensuse" ] ||
    [ "${os_name}" = "ubuntu" ]; then
    # shellcheck disable=SC1091 #Not following: (error message here)
    ASDF_DIR="${HOME}"/.asdf . "${HOME}"/.profile
fi

printf "\n# Configuring and building FlyingDdns\n"
printf "################################################################################\n"
printf "\n"
# The OS version on macOS is not checked with the uname command.
# shellcheck disable=SC2154 #var is referenced but not assigned.
if [ "${os_name}" = "macos" ]; then
    printf "%b %b DEBUG: macOS System Version=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "$(system_profiler SPSoftwareDataType | grep "System Version:" | cut -d ":" -f 2 | awk '{$1=$1};1')"
    PATH="/opt/homebrew/bin:/opt/homebrew/sbin:${PATH}"
    postgresql_version=$(brew list | grep postgresql | cut -d "@" -f 2)
    PATH="${PATH}:$(brew --prefix)/opt/postgresql@${postgresql_version}/bin"
else
    printf "%b %b DEBUG: OS version=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "$(uname -a)"
fi
# shellcheck disable=SC2154 #var is referenced but not assigned.
printf "%b %b DEBUG: virtualization technology=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${virt_name}"
printf "%b %b DEBUG: psql version:       <%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "$(psql --version | cut -d " " -f 3)"
printf "%b %b DEBUG: pg_isready version: <%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "$(pg_isready --version | cut -d " " -f 3)"
printf "%b %b DEBUG: pdnsutil version:   <%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "$(pdnsutil --version | cut -d " " -f 2)"
printf "%b %b DEBUG: elixir version:     <%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "$(elixir --version | grep -i elixir | cut -d " " -f 2)"
printf "%b %b DEBUG: mix version:        <%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "$(mix --version 2>/dev/null | grep Mix | cut -d " " -f 2)"
# shellcheck disable=SC2154 #var is referenced but not assigned.
printf "%b %b DEBUG: asdf version:       <%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${asdf_version}"
# shellcheck disable=SC2154 #var is referenced but not assigned.
printf "%b %b DEBUG: hex version:        <%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${hex_version}"
printf "%b %b DEBUG: tmux version:       <%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "$(tmux -V | cut -d " " -f 2)"
printf "%b %b DEBUG: python3 version:    <%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "$("${HOME}"/venv/bin/python3 --version | cut -d " " -f 2)"
printf "%b %b DEBUG: pip3 version:       <%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "$("${HOME}"/venv/bin/pip3 --version | cut -d " " -f 2)"
printf "%b %b DEBUG: robot version:      <%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "$("${HOME}"/venv/bin/robot --version | cut -d " " -f 3)"
# shellcheck disable=SC2154 #var is referenced but not assigned.
printf "%b %b DEBUG: rf gherkin version: <%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${robotframework_gherkin_parser_version}"
printf "%b %b DEBUG: curl version:       <%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "$(curl --version | grep ^curl | cut -d " " -f 2)"
printf "%b %b DEBUG: git version:        <%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "$(git --version | cut -d " " -f 3)"
printf "%b %b DEBUG: jq version:         <%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "$(jq --version | cut -d "-" -f 2)"
printf "%b %b DEBUG: MIX_ENV=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${MIX_ENV}"
# shellcheck disable=SC2154 #var is referenced but not assigned.
printf "%b %b DEBUG: branch_name=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${branch_name}"
# shellcheck disable=SC2154 #var is referenced but not assigned.
printf "%b %b DEBUG: flyingddns version=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${flyingddns_version}"
# shellcheck disable=SC2154 #var is referenced but not assigned.
printf "%b %b DEBUG: flyingddns commit SHA=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${flyingddns_commit_sha}"
# shellcheck disable=SC2154 #var is referenced but not assigned.
printf "%b %b DEBUG: flyingddns commit short SHA=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${flyingddns_commit_short_sha}"
# shellcheck disable=SC2154 #var is referenced but not assigned.
printf "%b %b DEBUG: flyingddns robotframework version=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${flyingddns_robotframework_version}"
# shellcheck disable=SC2154 #var is referenced but not assigned.
printf "%b %b DEBUG: project_url=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${project_url}"
printf "\n"

printf "%b %b INFO:  Stepping into the user\'s home directory:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! cd "${HOME}"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Installing the hex package:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! mix local.hex "${hex_version}" --force; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Installing the rebar3 package:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! mix local.rebar --force; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Make sure the FlyingDdns directory does not exist:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if [ -d "${HOME}/FlyingDdns" ]; then
    rm -rf "${HOME}/FlyingDdns"
    printf "%b %b INFO:  \=> DIRECTORY REMOVED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Cloning the FlyingDdns repo:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! git clone --quiet https://gitlab.com/timopallach/FlyingDdns.git; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Changing the directory to FlyingDdns:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! cd FlyingDdns; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 5
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Checking out the correct branch:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! git switch --quiet "${branch_name}"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 6
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Set the correct version in mix.exs:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! perl -i -pe "s/0.0.0/${flyingddns_robotframework_version}/g" mix.exs; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 7
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Set the correct commit SHA hash in config/config.exs:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! perl -i -pe "s/0000000000000000000000000000000000000000/${flyingddns_commit_sha}/g" config/config.exs; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 8
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Set the correct commit short SHA hash in config/config.exs:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! perl -i -pe "s/00000000/${flyingddns_commit_short_sha}/g" config/config.exs; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 9
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Installing the mix dependencies:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! mix deps.get --only "${MIX_ENV}"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 10
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Compiling mix:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! mix compile; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 11
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Creating digest:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! mix phx.digest; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 12
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Setup the database:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! mix ecto.reset; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 13
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Creating the Elixir release:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! mix release --overwrite; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 14
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# macOS's uname is not showing the version used by Apple but the Darwin kernel version.
if [ "${os_name}" = "macos" ]; then
    os_version_complete="$(sw_vers --productVersion)"
    os_version_major="$(printf "%b" "${os_version_complete}" | cut -d "." -f 1)"
    os_version_minor="$(printf "%b" "${os_version_complete}" | cut -d "." -f 2)"
    os_version_patch="$(printf "%b" "${os_version_complete}" | cut -d "." -f 3)"
    os_version="${os_version_major}.${os_version_minor}"
    printf "%b DEBUG: os_version_complete=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${os_version_complete}"
    printf "%b DEBUG: os_version_major=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${os_version_major}"
    printf "%b DEBUG: os_version_minor=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${os_version_minor}"
    printf "%b DEBUG: os_version_patch=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${os_version_patch}"
    printf "%b DEBUG: os_version=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${os_version}"
fi
# shellcheck disable=SC2154 #var is referenced but not assigned.
printf "%b %b INFO:  Creating release file flyingddns-%b_%b-%b-%b.tar.gz:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${flyingddns_version}" "${os_name}" "${os_version}" "${arch_name}"
if ! tar cfz "flyingddns-${flyingddns_version}_${os_name}-${os_version}-${arch_name}.tar.gz" "_build/${MIX_ENV}/rel/flying_ddns"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 15
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Move the created tar.gz package to the current users home directory:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! mv "flyingddns-${flyingddns_version}_${os_name}-${os_version}-${arch_name}.tar.gz" "${HOME}"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 16
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Make sure the database created during the build is dropped:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! mix ecto.drop; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 17
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
