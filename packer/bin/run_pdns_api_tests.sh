#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# PowerDNS API Test Script
# ===============================================================================
#
# This script tests the PowerDNS API integration to ensure FlyingDdns can
# communicate with PowerDNS correctly. It verifies that the PowerDNS API is
# accessible and functioning as expected.
#
# The script performs the following tests:
# 1. Verifies the PowerDNS API server is running and accessible
# 2. Tests zone creation and management
# 3. Tests record creation and updates
# 4. Tests API authentication and authorization
#
# This script is typically run after both PowerDNS and FlyingDdns have been
# started to verify that they can communicate correctly.
#
# Usage:
#   ./run_pdns_api_tests.sh
#
# Environment Variables:
#   os_name - The operating system name
#
# ===============================================================================

script_name="run_pdns_api_tests.sh"

# shellcheck disable=SC2154 #var is referenced but not assigned.
if [ "${os_name}" = "macos" ]; then
    PATH="/opt/homebrew/bin:/opt/homebrew/sbin:${PATH}"
fi

export PATH="${PATH}:${HOME}/venv/bin"

printf "\n"
printf "# Running tests to check the PDNS API installation and configuration\n"
printf "################################################################################\n"
printf "%b %b DEBUG: PATH=<%b>, HOME=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${PATH}" "${HOME}"

printf "%b %b INFO:  Stepping into the test directory:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! cd "${HOME}"/FlyingDdns/packer/test; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Executing the robotframework PDNS API tests:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! robot \
    --exitonfailure \
    --exitonerror \
    --variable DEBUG:"${ROBOT_DEBUG}" \
    --variable PDNS_API_PASSWD:"${PDNS_API_PASSWD}" \
    --variable DOMAIN_NAME:"${DOMAIN_NAME}" \
    --variable DOMAIN_NAME_IPV4:"${DOMAIN_NAME_IPV4}" \
    --variable DOMAIN_NAME_IPV6:"${DOMAIN_NAME_IPV6}" \
    --outputdir "${HOME}/FlyingDdns/log/robot_framework_pdns_api" \
    --loglevel TRACE \
    --xunit xunit.xml \
    --name "PDNS API tests" \
    --parser GherkinParser \
    pdns_api.feature; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Stepping back into the previous directory:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! cd -; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
