#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2024, Timo Pallach (timo@pallach.de).

#script_name="install_packages.sh"

## shellcheck disable=SC2154 #var is referenced but not assigned.
#if [ "${use_openbsd_snapshot}" = "true" ]; then
#    url_path="snapshots"
#else
#    url_path="$(uname -r)"
#fi
#
#openbsd_arch="$(uname -p)"
#python_version="$(ftp -V -o - "$(cat /etc/installurl)"/"${url_path}"/packages/"${openbsd_arch}"/ | grep python-3 | grep -v debug | grep -v -- -python | sort -V | tail -n 1 | cut -d "\"" -f 2 | sed s/python-// | sed s/.tgz//)"
#python_version_major="$(echo "${python_version}" | cut -d "." -f 1)"
#python_version_minor="$(echo "${python_version}" | cut -d "." -f 2)"
#python_version_patch="$(echo "${python_version}" | cut -d "." -f 3)"
#python_version_short="${python_version_major}${python_version_minor}"
#
#printf "%b %b DEBUG: use_openbsd_snapshot=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${use_openbsd_snapshot}"
#printf "%b %b DEBUG: url_path=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${url_path}"
#printf "%b %b DEBUG: openbsd_arch=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${openbsd_arch}"
#printf "%b %b DEBUG: python_version=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${python_version}"
#printf "%b %b DEBUG: python_version_major=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${python_version_major}"
#printf "%b %b DEBUG: python_version_minor=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${python_version_minor}"
#printf "%b %b DEBUG: python_version_patch=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${python_version_patch}"
#printf "%b %b DEBUG: python_version_short=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${python_version_short}"
#printf "%b %b DEBUG: url=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "$(cat /etc/installurl)/${url_path}/packages/${openbsd_arch}/"
#
#printf "%b %b INFO:  Installing the required packages:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#if ! pkg_add postgresql-server postgresql-contrib powerdns-pgsql elixir gmake python-"${python_version}" curl git jq 2>&1; then
#    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#    exit 1
#fi
#printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#
#printf "%b %b INFO:  Prepare python virtual environment:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#if ! (mkdir -p "${HOME}"/venv &&
#    python3 -m venv "${HOME}"/venv &&
#    "${HOME}"/venv/bin/python -m pip install --upgrade pip); then
#    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#    exit 2
#fi
#printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#
#printf "%b %b INFO:  Installing the robotframework and robotframework-gherkin-parser pip package:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
## shellcheck disable=SC2154 #var is referenced but not assigned.
#if ! ("${HOME}"/venv/bin/python -m pip install robotframework=="${robotframework_version}" robotframework-gherkin-parser=="${robotframework_gherkin_parser_version}"); then
#    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#    exit 3
#fi
#printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
