#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns API Test Script
# ===============================================================================
#
# This script runs API tests against the FlyingDdns server to verify its
# functionality. It tests various API endpoints to ensure the server is working
# correctly and can handle DNS record operations.
#
# The script performs the following tests:
# 1. Verifies the server is running and accessible
# 2. Tests authentication mechanisms
# 3. Tests DNS record creation
# 4. Tests DNS record updates
# 5. Tests error handling and edge cases
#
# This script is typically run after the FlyingDdns server has been started
# to verify that it's functioning correctly.
#
# Usage:
#   ./run_fddns_api_tests.sh
#
# Environment Variables:
#   flyingddns_version - The version of FlyingDdns being tested
#   os_name - The operating system name
#   os_version - The operating system version
#   arch_name - The architecture name
#
# ===============================================================================

script_name="run_fddns_api_tests.sh"

# shellcheck disable=SC2154 #var is referenced but not assigned.
if [ "${os_name}" = "macos" ]; then
    PATH="/opt/homebrew/bin:/opt/homebrew/sbin:${PATH}"
fi

export PATH="${PATH}:${HOME}/venv/bin"

printf "\n"
printf "# Running tests against the FlyingDdns installation and configuration\n"
printf "################################################################################\n"
printf "%b %b DEBUG: PATH=<%b>, HOME=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${PATH}" "${HOME}"

printf "%b %b INFO:  Stepping into the test directory:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! cd "${HOME}"/FlyingDdns/packer/test; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# Inside the packer VMs we only test ipv6
fddns_ip_version="ipv6"
printf "%b %b INFO:  Executing the robotframework FlyingDdns tests (%b %b):\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${fddns_ip_version}" "${DOMAIN_NAME}"
if ! robot \
    --exitonfailure \
    --exitonerror \
    --variable DEBUG:"${ROBOT_DEBUG}" \
    --variable FDDNS_PROTOCOL:"${FDDNS_PROTOCOL}" \
    --variable FDDNS_HOSTNAME:"${FDDNS_HOSTNAME}" \
    --variable FDDNS_PORT:"${FDDNS_PORT}" \
    --variable FDDNS_IP_VERSION:"${fddns_ip_version}" \
    --variable FDDNS_VERSION:"${FDDNS_VERSION}" \
    --variable FDDNS_COMMIT_SHA:"${FDDNS_COMMIT_SHA}" \
    --variable FDDNS_COMMIT_SHORT_SHA:"${FDDNS_COMMIT_SHORT_SHA}" \
    --variable DOMAIN_NAME:"${DOMAIN_NAME}" \
    --variable DOMAIN_NAME_IPV4:"${DOMAIN_NAME_IPV4}" \
    --variable DOMAIN_NAME_IPV6:"${DOMAIN_NAME_IPV6}" \
    --outputdir "${HOME}/FlyingDdns/log/robot_framework_fddns_api_${fddns_ip_version}" \
    --loglevel TRACE \
    --xunit xunit.xml \
    --name "FlyingDdns tests (${FDDNS_PROTOCOL}://${FDDNS_HOSTNAME}:${FDDNS_PORT} ${fddns_ip_version} ${DOMAIN_NAME})" \
    --parser GherkinParser \
    fddns_api.feature; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Stepping back into the previous directory:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! cd -; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
