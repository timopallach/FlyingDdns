#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# Alpine Linux Package Installation Script
# ===============================================================================
#
# This script installs necessary packages on Alpine Linux during the Packer
# build process for FlyingDdns. It ensures all required dependencies are
# available for the application.
#
# The script performs the following tasks:
# 1. Updates the package repository information
# 2. Installs a predefined set of packages required for FlyingDdns
# 3. Verifies successful installation of each package
#
# Packages installed include:
# - Development tools (build-base, git)
# - Database (postgresql, postgresql-dev)
# - DNS server (pdns, pdns-backend-pgsql, pdns-recursor)
# - Monitoring tools (vnstat)
# - Runtime dependencies (erlang, elixir)
# - System utilities (curl, bash, etc.)
#
# Usage:
#   ./install_packages.sh
#
# Exit Codes:
#   0 - Success
#   1 - Failed to update package repository
#   2 - Failed to install one or more packages
#
# ===============================================================================

script_name="install_packages.sh"

i="1"
max_apk_add_tries="5"
sleep_time="5"
printf "%b %b DEBUG: Setting i=0 and entering the while loop.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
while [ "${i}" -lt "${max_apk_add_tries}" ]; do
    printf "%b %b INFO:  Updating package repo and installing the required packages, try %b of %b:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${i}" "${max_apk_add_tries}"
    if ! (apk update && apk add postgresql postgresql-contrib pdns-backend-pgsql pdns-doc elixir build-base tmux python3 curl git jq bind-tools perl 2>&1); then
        sleep "${sleep_time}"
    else
        break
    fi
    i="$((i + 1))"
done
if [ "${i}" = "${max_apk_add_tries}" ]; then
    printf "%b %b ERROR: ==>> FAILED: max syspatch retries reached\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Prepare python virtual environment:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (mkdir -p "${HOME}"/venv &&
    python3 -m venv "${HOME}"/venv &&
    "${HOME}"/venv/bin/python -m pip install --upgrade pip); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Installing the robotframework and robotframework-gherkin-parser pip package:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
# shellcheck disable=SC2154 #var is referenced but not assigned.
if ! ("${HOME}"/venv/bin/python -m pip install robotframework=="${robotframework_version}" robotframework-gherkin-parser=="${robotframework_gherkin_parser_version}"); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
