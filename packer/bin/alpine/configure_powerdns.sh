#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# Alpine Linux PowerDNS Configuration Script
# ===============================================================================
#
# This script configures and starts the PowerDNS authoritative server and recursor
# on Alpine Linux. It sets up the necessary configuration for FlyingDdns to
# interact with PowerDNS.
#
# The script performs the following tasks:
# 1. Enables PowerDNS services to start at boot time
# 2. Configures PowerDNS with appropriate settings
# 3. Starts the PowerDNS services
# 4. Verifies that the services are running correctly
#
# This script is called by the main Packer build process when setting up
# the Alpine Linux environment for FlyingDdns.
#
# Usage:
#   ./configure_powerdns.sh
#
# Exit Codes:
#   0 - Success
#   1-7 - Various configuration and startup failures
#
# ===============================================================================

script_name="configure_powerdns.sh"

# shellcheck disable=SC2154 #var is referenced but not assigned.
printf "%b %b DEBUG: PDNS_API_PASSWD=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${PDNS_API_PASSWD}"
# shellcheck disable=SC2154 #var is referenced but not assigned.
printf "%b %b DEBUG: DOMAIN_NAME=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${DOMAIN_NAME}"
# shellcheck disable=SC2154 #var is referenced but not assigned.
printf "%b %b DEBUG: DOMAIN_NAME_IPV4=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${DOMAIN_NAME_IPV4}"
# shellcheck disable=SC2154 #var is referenced but not assigned.
printf "%b %b DEBUG: DOMAIN_NAME_IPV6=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${DOMAIN_NAME_IPV6}"

printf "%b %b INFO:  Adding the pdns role in postgresql:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! psql --host=::1 --username=postgres --command="CREATE ROLE ${PDNS_DB_USER} WITH ENCRYPTED PASSWORD '${PDNS_DB_PASSWD}' LOGIN;"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Creating the pdns database in postgresql:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! psql --host=::1 --username=postgres --command="CREATE DATABASE ${PDNS_DB_NAME} OWNER ${PDNS_DB_USER};"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Importing the pdns database schema into postgresql:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! PGPASSWORD=${PDNS_DB_PASSWD} psql --host=::1 --username="${PDNS_DB_USER}" --dbname "${PDNS_DB_NAME}" --command="\\include /usr/share/doc/pdns/schema.pgsql.sql;"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Executing the dt command in postgresql:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! psql --host=::1 --username="${PDNS_DB_USER}" --dbname "${PDNS_DB_NAME}" --command="\\dt;"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Trying the select from comments in postgresql:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! psql --host=::1 --username="${PDNS_DB_USER}" --dbname "${PDNS_DB_NAME}" --command="select * from comments;"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 5
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Disabling chroot:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! sed -i 's/chroot=\/var\/empty/#chroot=\/var\/empty/g' /etc/pdns/pdns.conf; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 6
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Disabling logfile:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! sed -i 's/use-logfile=no/#use-logfile=no/g' /etc/pdns/pdns.conf; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 7
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Disabling wildcards:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! sed -i 's/wildcards=yes/#wildcards=yes/g' /etc/pdns/pdns.conf; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 8
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Modifying the pdns.conf configuration file:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
cat >>/etc/pdns/pdns.conf <<EOF

version-string=anonymous
launch=gpgsql
gpgsql-host=::1
gpgsql-dbname=${PDNS_DB_NAME}
gpgsql-user=${PDNS_DB_USER}
gpgsql-password=${PDNS_DB_PASSWD}
api=yes
api-key=${PDNS_API_PASSWD}
webserver=yes
webserver-address=::1
EOF
# shellcheck disable=SC2181 #Check exit code directly with e.g. if mycmd;, not indirectly with $?.
if [ "$?" != "0" ]; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 9
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Enabling the pdns service:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! rc-update add pdns boot; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 10
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Starting the pdns service:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! service pdns start; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 11
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Waiting untill the pdns service is up and running:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
i="0"
max_tries="12"
sleep_time="5"
while ! (host localhost ::1 | grep "REFUSED") 2>&1; do
    if [ "${i}" -lt "${max_tries}" ]; then
        printf "%b %b DEBUG: Sleeping %b seconds...\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${sleep_time}"
        sleep "${sleep_time}"
    else
        printf "%b %b ERROR: ==>> FAILED: max check retries reached\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
        exit 12
    fi
    i="$((i + 1))"
done
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Check if the pdns api is accessible:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! curl --ipv6 --silent --request GET http://\[::1\]:8081/api/v1/servers/localhost | grep Unauthorized; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 13
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
