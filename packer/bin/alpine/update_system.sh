#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# Alpine Linux System Update Script
# ===============================================================================
#
# This script updates the Alpine Linux system during the Packer build process
# for FlyingDdns. It ensures the base system is up-to-date before installing
# application-specific packages and configurations.
#
# The script performs the following tasks:
# 1. Updates the package repository information
# 2. Upgrades all installed packages to their latest versions
# 3. Verifies the system is properly updated
#
# This script is typically run at the beginning of the Packer build process
# to ensure a clean, up-to-date base system.
#
# Usage:
#   ./update_system.sh
#
# Exit Codes:
#   0 - Success
#   1 - Failed to update package repository
#   2 - Failed to upgrade packages
#
# ===============================================================================

script_name="update_system.sh"

printf "%b %b INFO:  Updating the apk repository indexes:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! apk update; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  apk upgrading the system:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! apk upgrade; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Rebooting the system:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! reboot; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
