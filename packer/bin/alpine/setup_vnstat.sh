#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# Alpine Linux VnStat Setup Script
# ===============================================================================
#
# This script installs and configures VnStat (network traffic monitor) on
# Alpine Linux during the Packer build process for FlyingDdns.
#
# The script performs the following tasks:
# 1. Installs the VnStat package if not already installed
# 2. Configures VnStat to monitor the eth0 network interface
# 3. Enables the VnStat service to start at boot
# 4. Starts the VnStat service
# 5. Verifies that the service is running correctly
#
# VnStat is used to monitor network traffic during and after the build process,
# providing valuable metrics for optimization and troubleshooting.
#
# Usage:
#   ./setup_vnstat.sh
#
# Exit Codes:
#   0 - Success
#   1 - Failed to install VnStat
#   2 - Failed to enable VnStat service
#   3 - Failed to start VnStat service
#
# ===============================================================================

script_name="setup_vnstat.sh"

egress_interface=$(netstat -r | grep default | rev | cut -d " " -f 1 | rev)

i="1"
max_apk_add_tries="5"
sleep_time="5"
printf "%b %b DEBUG: Setting i=0 and entering the while loop.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
while [ "${i}" -lt "${max_apk_add_tries}" ]; do
    printf "%b %b INFO:  Installing the vnstat package, try %b of %b:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${i}" "${max_apk_add_tries}"
    if ! (apk update && apk add vnstat vnstat-openrc); then
        printf "%b %b DEBUG: Sleeping %b seconds...\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${sleep_time}"
        sleep "${sleep_time}"
    else
        break
    fi
    i="$((i + 1))"
done
if [ "${i}" = "${max_apk_add_tries}" ]; then
    printf "%b %b ERROR: ==>> FAILED: max syspatch retries reached\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Setting the parameters in the vnstat config:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! printf "\nInterface \"%b\"\nSaveInterval 1\n" "${egress_interface}" >>/etc/vnstat.conf; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Enabling the vnstat service:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! rc-update add vnstatd boot; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Starting the vnstatd service:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! service vnstatd start; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
