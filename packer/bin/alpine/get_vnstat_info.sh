#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# Alpine Linux VnStat Information Script
# ===============================================================================
#
# This script retrieves and displays network traffic statistics using VnStat
# on Alpine Linux. It provides information about network usage during the
# Packer build process.
#
# The script performs the following tasks:
# 1. Checks if VnStat is installed and running
# 2. Retrieves network traffic statistics
# 3. Displays the information in a readable format
#
# This script is typically called at the end of the Packer build process
# to provide information about network usage during the build.
#
# Usage:
#   ./get_vnstat_info.sh
#
# Exit Codes:
#   0 - Success
#   1 - VnStat service is not running
#
# ===============================================================================

script_name="get_vnstat_info.sh"

printf "%b %b INFO:  Disabling the vnstat service:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! rc-update del vnstatd boot; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Getting the vnstat infos:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (service vnstatd stop && sleep 5 && vnstat -q && printf "vnstat info: " && vnstat --oneline); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
