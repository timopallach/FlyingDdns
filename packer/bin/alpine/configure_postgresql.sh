#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# Alpine Linux PostgreSQL Configuration Script
# ===============================================================================
#
# This script configures and starts the PostgreSQL database service on Alpine Linux.
# It is used during the Packer build process to prepare the database environment
# for FlyingDdns.
#
# The script performs the following tasks:
# 1. Enables the PostgreSQL service to start at boot time
# 2. Starts the PostgreSQL service
# 3. Waits for the service to be fully operational
# 4. Verifies database access using the postgres user
#
# This script is called by the main Packer build process when setting up
# the Alpine Linux environment for FlyingDdns.
#
# Usage:
#   ./configure_postgresql.sh
#
# Exit Codes:
#   0 - Success
#   1 - Failed to enable PostgreSQL service
#   2 - Failed to start PostgreSQL service
#   3 - PostgreSQL service did not become ready within timeout
#   4 - Failed to access PostgreSQL database
#
# ===============================================================================

script_name="configure_postgresql.sh"

printf "%b %b INFO:  Enabling the postgresql service:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! rc-update add postgresql boot; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Starting the postgresql service:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! service postgresql start 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Waiting untill the postgresql service is up and running:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
i="0"
max_tries="12"
sleep_time="5"
while ! (pg_isready | grep "accepting connections") 2>&1; do
    if [ "${i}" -lt "${max_tries}" ]; then
        printf "%b %b DEBUG: Sleeping %b seconds...\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${sleep_time}"
        sleep "${sleep_time}"
    else
        printf "%b %b ERROR: ==>> FAILED: max check retries reached\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
        exit 3
    fi
    i="$((i + 1))"
done
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Accessing the postresql database as user postgres:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (psql --host=::1 --username=postgres --command="\\list" | grep "(3 rows)"); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
