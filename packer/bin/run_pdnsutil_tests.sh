#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# PowerDNS Utility Test Script
# ===============================================================================
#
# This script verifies that the PowerDNS utility tools (pdnsutil) are functioning
# correctly and can be used by FlyingDdns. It tests various pdnsutil commands to
# ensure they work as expected.
#
# The script performs the following tests:
# 1. Verifies pdnsutil is installed and accessible
# 2. Tests zone creation and management via pdnsutil
# 3. Tests record manipulation commands
# 4. Tests cryptographic operations (DNSSEC)
#
# This script is typically run during the setup phase to verify that PowerDNS
# is correctly configured and its utilities are working properly.
#
# Usage:
#   ./run_pdnsutil_tests.sh
#
# Environment Variables:
#   os_name - The operating system name
#
# ===============================================================================

script_name="run_pdnsutil_tests.sh"

# shellcheck disable=SC2154 #var is referenced but not assigned.
if [ "${os_name}" = "macos" ]; then
    PATH="/opt/homebrew/bin:/opt/homebrew/sbin:${PATH}"
fi

export PATH="${PATH}:${HOME}/venv/bin"

printf "\n"
printf "# Running local tests to check the pdnsutil installation and configuration\n"
printf "################################################################################\n"
printf "%b %b DEBUG: PATH=<%b>, HOME=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${PATH}" "${HOME}"

printf "%b %b INFO:  Stepping into the test directory:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! cd "${HOME}"/FlyingDdns/packer/test; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Executing the robotframework pdnsutil tests:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! robot \
    --exitonfailure \
    --exitonerror \
    --variable DEBUG:"${ROBOT_DEBUG}" \
    --variable DOMAIN_NAME:"${DOMAIN_NAME}" \
    --outputdir "${HOME}/FlyingDdns/log/robot_framework_pdnsutil" \
    --loglevel TRACE \
    --xunit xunit.xml \
    --name "Local pdnsutil tests" \
    --parser GherkinParser \
    pdnsutil.feature; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Stepping back into the previous directory:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! cd -; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
