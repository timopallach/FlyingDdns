#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# Sleep Utility Script
# ===============================================================================
#
# This simple utility script introduces a delay in the build process when needed.
# It's used to pause execution for a specified amount of time, which can be
# useful when waiting for services to start or operations to complete.
#
# Usage:
#   ./sleep.sh [seconds]
#
# Arguments:
#   seconds - Optional. The number of seconds to sleep. Defaults to 5 if not specified.
#
# ===============================================================================

# Default sleep time in seconds
sleep_time="${1:-5}"

# Sleep for the specified time
sleep "${sleep_time}"

exit 0
