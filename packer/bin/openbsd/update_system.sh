#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

script_name="update_system.sh"

# shellcheck disable=SC2154 #var is referenced but not assigned.
printf "%b %b DEBUG: use_openbsd_snapshot=<%b>.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${use_openbsd_snapshot}"

# Do not syspatch if we are using the snapshot version.
if [ "${use_openbsd_snapshot}" = "true" ]; then
    printf "%b %b INFO:  Due to the system running on an OpenBSD snapshot updates are not available.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 0
fi

i="1"
max_syspatch_tries="25"
sleep_time="5"
while [ "${i}" -lt "${max_syspatch_tries}" ]; do
    printf "%b %b DEBUG: Trying syspatch, try %b of %b.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${i}" "${max_syspatch_tries}"
    syspatch 2>&1
    syspatch_exit_status="${?}"
    if [ "${syspatch_exit_status}" = "2" ]; then
        printf "%b %b DEBUG: No patches have been released yet or all patches have been installed already.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
        break
    elif [ "${syspatch_exit_status}" != "0" ]; then
        printf "%b %b DEBUG: Sleeping %b seconds...\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${sleep_time}"
        sleep "${sleep_time}"
    else
        printf "%b %b DEBUG: syspatch executed without problems after the <%b> try.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${i}"
        break
    fi
    i="$((i + 1))"
done
if [ "${i}" = "${max_syspatch_tries}" ]; then
    printf "%b %b ERROR: ==>> FAILED: max syspatch retries reached\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Rebooting the system:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! reboot; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
