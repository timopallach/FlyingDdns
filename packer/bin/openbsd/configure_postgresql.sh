#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# OpenBSD PostgreSQL Configuration Script
# ===============================================================================
#
# This script configures and starts the PostgreSQL database service on OpenBSD
# during the Packer build process for FlyingDdns. It ensures the database is
# properly set up and ready for use by the application.
#
# The script performs the following tasks:
# 1. Sets up the necessary locale environment variables
# 2. Initializes the PostgreSQL database with UTF-8 encoding
# 3. Configures PostgreSQL to listen on IPv6 interfaces
# 4. Enables the PostgreSQL service to start at boot
# 5. Starts the PostgreSQL service
# 6. Verifies database access by listing databases as the postgres user
#
# PostgreSQL is used as the backend database for both FlyingDdns and PowerDNS.
#
# Usage:
#   ./configure_postgresql.sh
#
# Exit Codes:
#   0 - Success
#   1 - Failed to change to PostgreSQL directory
#   2 - Failed to initialize PostgreSQL database
#   3 - Failed to configure PostgreSQL
#   4 - Failed to enable PostgreSQL service
#   5 - Failed to start PostgreSQL service
#   6 - PostgreSQL service failed to become ready
#   7 - Failed to access PostgreSQL database
#
# ===============================================================================

script_name="configure_postgresql.sh"

LANG="en_US.UTF-8"
LANGUAGE="en_US.UTF-8"
LC_CTYPE="en_US.UTF-8"
LC_COLLATE="en_US.UTF-8"
LC_TIME="en_US.UTF-8"
LC_NUMERIC="en_US.UTF-8"
LC_MONETARY="en_US.UTF-8"
LC_MESSAGES="en_US.UTF-8"
LC_ALL="en_US.UTF-8"

printf "%b %b INFO:  Changing into the directory /var/postgresql/:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! cd /var/postgresql/; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Initiating the postgresql installation:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! su _postgresql -c "initdb --encoding utf8 --no-locale --username=postgres --pgdata /var/postgresql/data/" 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Enabling the postgresql service:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! rcctl enable postgresql; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Starting the postgresql service:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! rcctl start postgresql; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Waiting untill the postgresql service is up and running:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
i="0"
max_tries="12"
sleep_time="5"
while ! (pg_isready | grep "accepting connections") 2>&1; do
    if [ "${i}" -lt "${max_tries}" ]; then
        printf "%b %b DEBUG: Sleeping %b seconds...\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${sleep_time}"
        sleep "${sleep_time}"
    else
        printf "%b %b ERROR: ==>> FAILED: max check retries reached\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
        exit 5
    fi
    i="$((i + 1))"
done
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Accessing the postresql database as user postgres:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (psql --host=::1 --username=postgres --command="\\list" | grep "(3 rows)"); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 6
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
