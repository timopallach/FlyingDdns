#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FreeBSD PowerDNS Configuration Script
# ===============================================================================
#
# This script configures and starts the PowerDNS authoritative server and
# recursor on FreeBSD during the Packer build process for FlyingDdns. It ensures
# the DNS services are properly set up to work with the application.
#
# The script performs the following tasks:
# 1. Creates a PostgreSQL role for PowerDNS
# 2. Creates a PostgreSQL database for PowerDNS
# 3. Imports the PowerDNS schema into the database
# 4. Configures PowerDNS with appropriate settings
# 5. Enables and starts the PowerDNS services
# 6. Verifies that the services are running correctly
#
# PowerDNS is used as the DNS server that FlyingDdns interacts with to manage
# dynamic DNS records.
#
# Usage:
#   ./configure_powerdns.sh
#
# Exit Codes:
#   0 - Success
#   1-12 - Various configuration and startup failures
#
# ===============================================================================

script_name="configure_powerdns.sh"

printf "%b %b INFO:  Adding the pdns role in postgresql:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! psql --username=postgres --command="CREATE ROLE ${PDNS_DB_USER} WITH ENCRYPTED PASSWORD '${PDNS_DB_PASSWD}' LOGIN;"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Creating the pdns database in postgresql:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! psql --username=postgres --command="CREATE DATABASE ${PDNS_DB_NAME} OWNER ${PDNS_DB_USER};"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Importing the pdns database schema into postgresql:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! PGPASSWORD="${PDNS_DB_PASSWD}" psql --host=::1 --username="${PDNS_DB_USER}" --dbname="${PDNS_DB_NAME}" --command="\\include /usr/local/share/doc/powerdns/schema.pgsql.sql;"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Executing the dt command in postgresql:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! psql --host=::1 --username="${PDNS_DB_USER}" --dbname="${PDNS_DB_NAME}" --command="\\dt;"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Trying the select from comments in postgresql:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! psql --host=::1 --username="${PDNS_DB_USER}" --dbname="${PDNS_DB_NAME}" --command="select * from comments;"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 5
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Modifying the pdns.conf configuration file:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
cat >>/usr/local/etc/pdns/pdns.conf <<EOF

version-string=anonymous
launch=gpgsql
gpgsql-host=::1
gpgsql-dbname=${PDNS_DB_NAME}
gpgsql-user=${PDNS_DB_USER}
gpgsql-password=${PDNS_DB_PASSWD}
api=yes
api-key=${PDNS_API_PASSWD}
webserver=yes
webserver-address=::1
EOF
# shellcheck disable=SC2181 #Check exit code directly with e.g. if mycmd;, not indirectly with $?.
if [ "$?" != "0" ]; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 6
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Enabling the pdns service:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! service pdns enable; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 7
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Starting the pdns service:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! service pdns start; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 8
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Waiting untill the pdns service is up and running:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
i="0"
max_tries="12"
sleep_time="5"
while ! (host localhost ::1 | grep "REFUSED") 2>&1; do
    if [ "${i}" -lt "${max_tries}" ]; then
        printf "%b %b DEBUG: Sleeping %b seconds...\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${sleep_time}"
        sleep "${sleep_time}"
    else
        printf "%b %b ERROR: ==>> FAILED: max check retries reached\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
        exit 12
    fi
    i="$((i + 1))"
done
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Check if the pdns api is accessible:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! curl --ipv6 --silent --request GET http://\[::1\]:8081/api/v1/servers/localhost | grep Unauthorized; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 13
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
