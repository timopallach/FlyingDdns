#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

script_name="get_vnstat_info.sh"

printf "%b %b INFO:  Getting the vnstat infos:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (service vnstat stop && sleep 5 && vnstat -q && printf "vnstat info: " && vnstat --oneline); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
