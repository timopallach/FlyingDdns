#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FreeBSD PostgreSQL Configuration Script
# ===============================================================================
#
# This script configures and starts the PostgreSQL database service on FreeBSD
# during the Packer build process for FlyingDdns. It ensures the database is
# properly set up and ready for use by the application.
#
# The script performs the following tasks:
# 1. Enables the PostgreSQL service to start at boot
# 2. Initializes the PostgreSQL database
# 3. Starts the PostgreSQL service using tmux to prevent hanging
# 4. Waits for the service to be fully operational
# 5. Verifies database access by listing databases as the postgres user
#
# PostgreSQL is used as the backend database for both FlyingDdns and PowerDNS.
#
# Usage:
#   ./configure_postgresql.sh
#
# Exit Codes:
#   0 - Success
#   1 - Failed to enable PostgreSQL service
#   2 - Failed to initialize PostgreSQL database
#   3 - Failed to start PostgreSQL service
#   4 - PostgreSQL service failed to become ready
#   5 - Failed to access PostgreSQL database
#
# ===============================================================================

script_name="configure_postgresql.sh"

printf "%b %b INFO:  Enabling the postgresql service:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! service postgresql enable; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Initiating the postgresql installation:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! service postgresql initdb 2>&1; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# We are encapsulating the "service postgresql start" command
# into tmux because without tmux it hangs forever.
printf "%b %b INFO:  Starting the postgresql service:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! tmux new-session -d "service postgresql start"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Waiting untill the postgresql service is up and running:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
i="0"
max_tries="12"
sleep_time="5"
while ! (pg_isready | grep "accepting connections") 2>&1; do
    if [ "${i}" -lt "${max_tries}" ]; then
        printf "%b %b DEBUG: Sleeping %b seconds...\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${sleep_time}"
        sleep "${sleep_time}"
    else
        printf "%b %b ERROR: ==>> FAILED: max check retries reached\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
        exit 4
    fi
    i="$((i + 1))"
done
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Accessing the postresql database as user postgres:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (psql --host=::1 --username=postgres --command="\\list" | grep "(3 rows)"); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 5
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
