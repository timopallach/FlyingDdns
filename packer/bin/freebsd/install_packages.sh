#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FreeBSD Package Installation Script
# ===============================================================================
#
# This script installs necessary packages on FreeBSD during the Packer
# build process for FlyingDdns. It ensures all required dependencies are
# available for the application.
#
# The script performs the following tasks:
# 1. Bootstraps the pkg package manager
# 2. Updates the package repository information
# 3. Installs a predefined set of packages required for FlyingDdns
# 4. Verifies successful installation of each package
#
# Packages installed include:
# - Database (postgresql-server, postgresql-contrib)
# - DNS server (powerdns)
# - Runtime dependencies (erlang, elixir)
# - Development tools (gmake)
# - System utilities (tmux, python3, curl, git-lite, jq, ca_root_nss)
#
# Usage:
#   ./install_packages.sh
#
# Exit Codes:
#   0 - Success
#   1 - Failed to bootstrap pkg
#   2 - Failed to install one or more packages
#   3 - Failed to set up Python environment
#
# ===============================================================================

script_name="install_packages.sh"

postgresql_version="$(pkg rall-depends powerdns | grep postgresql | cut -d "l" -f 2 | cut -d "-" -f 1)"
#python_version_short="$(pkg search py | grep -- -pip- | sort | head -n 1 | cut -d "-" -f 1 | cut -d "y" -f 2)"
#python_version="$(echo "${python_version_short}" | cut -c 1).$(echo "${python_version_short}" | cut -c 2-)"
#printf "%b %b DEBUG: postgresql_version=<%b>, python_version=<%b>, python_version_short=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${postgresql_version}" "${python_version}" "${python_version_short}"

printf "%b %b INFO:  Bootstapping pkg:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! pkg bootstrap --yes --force; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

i="1"
max_pkg_install_tries="5"
sleep_time="5"
printf "%b %b DEBUG: Setting i=0 and entering the while loop.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
while [ "${i}" -lt "${max_pkg_install_tries}" ]; do
    printf "%b %b INFO:  Updating package repo and installing the required packages, try %b of %b:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${i}" "${max_pkg_install_tries}"
    if ! (pkg update --force && pkg install --yes postgresql"${postgresql_version}"-server postgresql"${postgresql_version}"-contrib powerdns elixir gmake tmux python3 curl git-lite jq ca_root_nss); then
        printf "%b %b DEBUG: Sleeping %b seconds...\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${sleep_time}"
        sleep "${sleep_time}"
    else
        printf "%b %b DEBUG: syspatch executed without problems after the <%b> try.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${i}"
        break
    fi
    i="$((i + 1))"
done
if [ "${i}" = "${max_pkg_install_tries}" ]; then
    printf "%b %b ERROR: ==>> FAILED: max syspatch retries reached\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Prepare python virtual environment:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (mkdir -p "${HOME}"/venv &&
    python3 -m venv "${HOME}"/venv &&
    "${HOME}"/venv/bin/python -m pip install --upgrade pip); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Installing the robotframework and robotframework-gherkin-parser pip package:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
# shellcheck disable=SC2154 #var is referenced but not assigned.
if ! ("${HOME}"/venv/bin/python -m pip install robotframework=="${robotframework_version}" robotframework-gherkin-parser=="${robotframework_gherkin_parser_version}"); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
