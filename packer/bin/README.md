# FlyingDdns Packer Scripts

This directory contains shell scripts used during the Packer build process for FlyingDdns. These scripts automate the setup and configuration of various operating systems to run the FlyingDdns application.

## Directory Structure

The scripts are organized by operating system:

- `alpine/` - Scripts for Alpine Linux
- `debian/` - Scripts for Debian Linux
- `fedora/` - Scripts for Fedora Linux
- `freebsd/` - Scripts for FreeBSD
- `macos/` - Scripts for macOS
- `netbsd/` - Scripts for NetBSD
- `nixos/` - Scripts for NixOS
- `openbsd/` - Scripts for OpenBSD
- `openindiana/` - Scripts for OpenIndiana
- `opensuse/` - Scripts for openSUSE
- `rocky/` - Scripts for Rocky Linux
- `ubuntu/` - Scripts for Ubuntu Linux

Additionally, the root of this directory contains common scripts used across multiple operating systems.

## Common Scripts

- `functions.sh` - Contains common functions used by other scripts
- `variables.sh` - Defines environment variables used by the build process
- `configure_and_build_flyingddns.sh` - Configures and builds the FlyingDdns application
- `start_flyingddns.sh` - Starts the FlyingDdns application
- `sleep.sh` - A utility script for introducing delays in the build process
- `run_fddns_api_tests.sh` - Runs API tests for FlyingDdns
- `run_pdns_api_tests.sh` - Runs API tests for PowerDNS
- `run_pdnsutil_tests.sh` - Runs utility tests for PowerDNS

## OS-Specific Scripts

Each OS directory typically contains the following scripts:

- `update_system.sh` - Updates the system packages
- `install_packages.sh` - Installs required packages
- `configure_postgresql.sh` - Configures PostgreSQL
- `configure_powerdns.sh` - Configures PowerDNS
- `setup_vnstat.sh` - Sets up VnStat for network monitoring
- `get_vnstat_info.sh` - Retrieves network statistics

## Usage

These scripts are primarily used by Packer during the automated build process. However, they can also be run manually for testing or development purposes.

Each script includes detailed documentation in its header section, explaining:
- The script's purpose
- Tasks performed by the script
- Usage instructions
- Exit codes and their meanings

## License

All scripts are licensed under the BSD 2-Clause License.
Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de). 