#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Packer Build Helper Functions
# ===============================================================================
#
# This script provides common utility functions used by the Packer build process
# for FlyingDdns. It contains functions for file manipulation, directory cleanup,
# system configuration, and build process management across different operating
# systems.
#
# These functions are sourced by other scripts in the Packer build pipeline to
# ensure consistent behavior and reduce code duplication.
#
# Usage:
#   . ./bin/functions.sh
#
# ===============================================================================

# replace_variable_in_file: Replaces a variable placeholder in a file with its value from variables.pkr.hcl
# Arguments:
#   $1 - The variable placeholder to replace
#   $2 - The file in which to perform the replacement
# Returns:
#   0 on success, non-zero on failure
replace_variable_in_file() {
    perl -pi -e "s/${1}/$(grep -A 2 "${1}" hcl/variables.pkr.hcl | grep default | cut -d "\"" -f 2)/g" "${2}"
}

# replace_arch_in_file: Replaces the 'arch' placeholder with the actual architecture name
# Arguments:
#   $1 - The file in which to perform the replacement
# Returns:
#   0 on success, non-zero on failure
replace_arch_in_file() {
    perl -pi -e "s/arch/${ARCH_NAME}/g" "${1}"
}

# replace_virt_in_file: Replaces the 'virt' placeholder with the actual virtualization name
# Arguments:
#   $1 - The file in which to perform the replacement
# Returns:
#   0 on success, non-zero on failure
replace_virt_in_file() {
    perl -pi -e "s/virt/${VIRT_NAME}/g" "${1}"
}

# cleanup_directories: Removes temporary files and directories created during the build process
# Arguments:
#   None
# Returns:
#   0 on success, non-zero on failure
cleanup_directories() {
    printf "\n"
    printf "# Cleaning up the directories and files (default)\n"
    printf "################################################################################\n"
    if ! find hcl/ -type l -exec unlink {} \;; then
        printf "%b ERROR: Unlinking packer plugin links did not succeed.\n" "$(date "+%Y-%m-%d %H:%M:%S")"
        return 1
    fi
    if ! find bin/ -type l -exec unlink {} \;; then
        printf "%b ERROR: Unlinking provisioner script links did not succeed.\n" "$(date "+%Y-%m-%d %H:%M:%S")"
        return 1
    fi
    if ! rm -rf output-* \
        tmp \
        ../log/* \
        config/unattended.conf \
        config/user-data \
        config/meta-data 2>&1; then
        printf "%b ERROR: Cleanup of directories and files did not succeed.\n" "$(date "+%Y-%m-%d %H:%M:%S")"
        return 2
    fi
    printf "%b INFO:  The local directories and files have been cleaned up.\n" "$(date "+%Y-%m-%d %H:%M:%S")"

    return 0
}

creating_link_to_packer_plugin() {
    printf "\n"
    printf "# Checking the if the symbolic links to the OS specific packer plugins exist\n"
    printf "################################################################################\n"
    for plugin_name in hcloud-plugin.pkr.hcl qemu-plugin.pkr.hcl virtualbox-plugin.pkr.hcl vmware-plugin.pkr.hcl vsphere-plugin.pkr.hcl; do
        if [ ! -L hcl/${plugin_name} ]; then
            cd hcl && ln -s "plugins/${plugin_name}" . && cd ..
            printf "%b INFO:  The symbolic link to ${plugin_name} has been created.\n" "$(date "+%Y-%m-%d %H:%M:%S")"
        else
            printf "%b INFO:  The symbolic link to ${plugin_name} already exists.\n" "$(date "+%Y-%m-%d %H:%M:%S")"
        fi
    done
    if [ "${HOST_OS_NAME}" = "MACOS" ]; then
        if [ ! -L hcl/tart-plugin.pkr.hcl ]; then
            cd hcl && ln -s "plugins/tart-plugin.pkr.hcl" . && cd ..
            printf "%b INFO:  The symbolic link to tart-plugin.pkr.hcl has been created.\n" "$(date "+%Y-%m-%d %H:%M:%S")"
        else
            printf "%b INFO:  The symbolic link to tart-plugin.pkr.hcl already exists.\n" "$(date "+%Y-%m-%d %H:%M:%S")"
        fi
    fi

    return 0
}

creating_link_to_provisioner_scripts() {
    printf "\n"
    printf "# Checking the if the symbolic links to the OS specific scripts exist\n"
    printf "################################################################################\n"
    for script_name in setup_vnstat.sh update_system.sh install_packages.sh configure_postgresql.sh configure_powerdns.sh get_vnstat_info.sh; do
        if [ ! -L bin/${script_name} ]; then
            cd bin && ln -s "${OS_NAME}/${script_name}" . && cd ..
            printf "%b INFO:  The symbolic link to ${script_name} has been created.\n" "$(date "+%Y-%m-%d %H:%M:%S")"
        else
            printf "%b INFO:  The symbolic link to ${script_name} already exists.\n" "$(date "+%Y-%m-%d %H:%M:%S")"
        fi
    done

    return 0
}

creating_unattended_conf() {
    printf "\n"
    printf "# Creating config/unattended.conf by copy\n"
    printf "################################################################################\n"
    cp config/"${OS_NAME}-unattended.conf" config/unattended.conf
    if [ "${OS_NAME}" = "alpine" ]; then
        #replace_arch_in_file "config/unattended.conf"
        replace_virt_in_file "config/unattended.conf"
        replace_variable_in_file "alpine-${VIRT_NAME}-disk" "config/unattended.conf"
        replace_variable_in_file "vm-host-name" "config/unattended.conf"
        #replace_variable_in_file "root-password" "config/unattended.conf"
        replace_variable_in_file "openbsd-excluded-sets" "config/unattended.conf"
    elif [ "${OS_NAME}" = "debian" ]; then
        replace_variable_in_file "vm-host-name" "config/unattended.conf"
        replace_variable_in_file "root-password" "config/unattended.conf"
        replace_variable_in_file "debian-username-password" "config/unattended.conf"
    elif [ "${OS_NAME}" = "fedora" ]; then
        replace_virt_in_file "config/unattended.conf"
        replace_variable_in_file "fedora-${VIRT_NAME}-disk" "config/unattended.conf"
        replace_variable_in_file "vm-host-name" "config/unattended.conf"
        replace_variable_in_file "root-password" "config/unattended.conf"
    elif [ "${OS_NAME}" = "freebsd" ]; then
        replace_variable_in_file "vm-host-name" "config/unattended.conf"
        replace_variable_in_file "root-password" "config/unattended.conf"
    elif [ "${OS_NAME}" = "netbsd" ]; then
        :
    elif [ "${OS_NAME}" = "nixos" ]; then
        replace_variable_in_file "vm-host-name" "config/unattended.conf"
        replace_variable_in_file "nixos-version" "config/unattended.conf"
    elif [ "${OS_NAME}" = "openbsd" ]; then
        replace_arch_in_file "config/unattended.conf"
        replace_variable_in_file "vm-host-name" "config/unattended.conf"
        replace_variable_in_file "root-password" "config/unattended.conf"
        replace_variable_in_file "openbsd-excluded-sets" "config/unattended.conf"
    elif [ "${OS_NAME}" = "openindiana" ]; then
        :
    elif [ "${OS_NAME}" = "opensuse" ]; then
        :
    elif [ "${OS_NAME}" = "rocky" ]; then
        replace_virt_in_file "config/unattended.conf"
        replace_variable_in_file "rocky-${VIRT_NAME}-disk" "config/unattended.conf"
        replace_variable_in_file "vm-host-name" "config/unattended.conf"
        replace_variable_in_file "root-password" "config/unattended.conf"
    elif [ "${OS_NAME}" = "ubuntu" ]; then
        replace_variable_in_file "vm-host-name" "config/unattended.conf"
        replace_variable_in_file "ubuntu-username" "config/unattended.conf"
        cp config/"unattended.conf" config/user-data
        touch config/meta-data
    else
        printf "%b ERROR: The OS_NAME=<%b> is not valid.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${OS_NAME}"
        return 1
    fi
    printf "%b INFO:  An unattended.conf copy has been created and was adopted to the current settings.\n" "$(date "+%Y-%m-%d %H:%M:%S")"

    return 0
}

initializing_packer() {
    printf "\n"
    printf "# Initializing packer and get the required plugins\n"
    printf "################################################################################\n"
    if ! packer init hcl/; then
        printf "%b ERROR: Initializing packer did not succeed.\n" "$(date "+%Y-%m-%d %H:%M:%S")"
        return 1
    fi
    printf "%b INFO:  Packer was successfully initialized.\n" "$(date "+%Y-%m-%d %H:%M:%S")"

    return 0
}

validating_packer() {
    printf "\n"
    printf "# Validating the current packer configuration file\n"
    printf "################################################################################\n"
    if ! packer validate -only="${BUILDER_NAME}"."${OS_NAME}"-"${ARCH_NAME}" hcl/; then
        printf "%b ERROR: Validating the packer configuration %b.%b-%b did not succeed.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${BUILDER_NAME}" "${OS_NAME}" "${ARCH_NAME}"
        return 1
    fi
    printf "%b INFO:  The current packer configuration was successfully validated.\n" "$(date "+%Y-%m-%d %H:%M:%S")"

    return 0
}

building_packer() {
    printf "\n"
    printf "# Installing %b at %b on %b (this can take several minutes)\n" "${OS_NAME}" "${ARCH_NAME}" "${BUILDER_NAME}"
    printf "################################################################################\n"
    printf "DEBUG: BUILDER_NAME.OS_NAME-ARCH_NAME=<%b>\n" "${BUILDER_NAME}.${OS_NAME}-${ARCH_NAME}"
    if [ "${VIRT_NAME}" = "vshpere" ]; then
        # shellcheck disable=SC2154 #var is referenced but not assigned.
        printf "DEBUG: esxi_host=<%b>, esxi_username=<%b>\n" "${esxi_host}" "${esxi_username}"
    fi
    if [ "${DEBUG}" = "yes" ]; then
        on_error="abort"
    elif [ "${DEBUG}" = "no" ]; then
        on_error="cleanup"
    else
        "${BUILDER_NAME}"."${OS_NAME}"-"${ARCH_NAME}"
        printf "%b ERROR: DEBUG=<%b> is undefined, has to be no or yes\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${DEBUG}"
        return 1
    fi
    if [ "${CI_PROJECT_URL}" = "" ]; then
        CI_PROJECT_URL="$(grep -A 2 project-url hcl/variables.pkr.hcl | grep default | cut -d "\"" -f 2)"
    fi
    # shellcheck disable=SC2154 #var is referenced but not assigned.
    if ! packer build -timestamp-ui \
        -force \
        -on-error="${on_error}" \
        -only="${BUILDER_NAME}"."${OS_NAME}"-"${ARCH_NAME}" \
        -var "arch-name=${ARCH_NAME}" \
        -var "os-name=${OS_NAME}" \
        -var "os-version=$(grep -A 2 "${OS_NAME}-version" hcl/variables.pkr.hcl | grep default | cut -d "\"" -f 2)" \
        -var "virt-name=${VIRT_NAME}" \
        -var "esxi-host=${esxi_host}" \
        -var "esxi-username=${esxi_username}" \
        -var "esxi-password=${esxi_password}" \
        -var "branch-name=${BRANCH_NAME}" \
        -var "project-url=${CI_PROJECT_URL}" \
        -var "flyingddns-version=${flyingddns_version}" \
        -var "flyingddns-commit-sha=${flyingddns_commit_sha}" \
        -var "flyingddns-commit-short-sha=${flyingddns_commit_short_sha}" \
        -var "flyingddns-robotframework-version=${flyingddns_robotframework_version}" \
        -var "qemu-arm64-cpu=${qemu_arm64_cpu}" \
        -var "qemu-arm64-accelerator=${qemu_arm64_accelerator}" \
        -var "qemu-arm64-firmware-path=${qemu_arm64_firmware_path}" \
        hcl/; then
        printf "%b ERROR: Building %b at %b for %b did not succeed.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${OS_NAME}" "${ARCH_NAME}" "${BUILDER_NAME}"
        return 2
    fi
    printf "%b INFO:  Building %b at %b on %b succeed.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${OS_NAME}" "${ARCH_NAME}" "${BUILDER_NAME}"

    return 0
}

running_build() {
    if ! cleanup_directories; then
        return 1
    fi
    if ! creating_link_to_packer_plugin; then
        return 2
    fi
    if ! creating_link_to_provisioner_scripts; then
        return 3
    fi
    if [ "${OS_NAME}" != "macos" ]; then
        if ! creating_unattended_conf; then
            return 4
        fi
    fi
    if ! initializing_packer; then
        return 5
    fi
    if ! validating_packer; then
        return 6
    fi
    if ! building_packer; then
        return 7
    fi
    return 0
}
