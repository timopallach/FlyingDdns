#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Build Performance Visualization - Shell Script Wrapper
# ===============================================================================
#
# This script provides a user-friendly interface for generating performance
# visualization graphs for FlyingDdns builds across different platforms.
# It processes CSV data containing build metrics and uses gnuplot (via draw_graph.plt)
# to generate either terminal-based ASCII graphs or PNG image files.
#
# The script supports two types of visualizations:
# 1. Time Performance Graph - Shows build time breakdown by phase (OS setup,
#    package installation, application setup)
# 2. Network Traffic Graph - Shows network traffic during build (sent/received)
#
# The script handles command-line argument parsing, data preparation, sorting,
# and invocation of gnuplot with the appropriate parameters.
#
# Dependencies:
# - gnuplot (with terminal and PNG support)
#   Note on terminal compatibility:
#   - For PNG output, the script will try to use 'pngcairo' terminal first
#   - If 'pngcairo' is not available (e.g., on FreeBSD), it will fall back to 'png'
#   - If neither is available, the script will warn but attempt to continue
# - A CSV file with build performance data in the expected format
#
# CSV Format for Time Graph:
#   OS,Total Build Time,OS Setup Time,Package Install Time,App Setup Time
#
# CSV Format for Network Graph:
#   OS,Total Traffic,Received Traffic,Sent Traffic
#
# See the usage() function or run with --help for detailed usage information.
# ===============================================================================

# Default values
OUTPUT_TYPE="terminal"
CSV_FILE="analyze-builds-5e3bfc11.csv"
OUTPUT_DIR="."
OUTPUT_FILE=""
SCRIPT_DIR=$(dirname "$0")
SORT_BY="none"    # Default is no sorting (use order in CSV)
SORT_DIR="desc"   # Default sort direction is descending
GRAPH_TYPE="time" # Default is to generate time graph
QUIET=false       # Default is to show messages
COMMENT=""        # Default is no comment in the heading

# Function to display usage information
usage() {
    printf "Usage: %b [options]\n" "$0"
    printf "Options:\n"
    printf "  -h, --help                 Display this help message\n"
    printf "  -t, --type TYPE            Output type: 'terminal' or 'png' (default: terminal)\n"
    printf "  -i, --input-file FILE      CSV file to use (default: analyze-builds-5e3bfc11.csv)\n"
    printf "  -o, --output-dir DIR       Output directory for PNG files (default: current directory)\n"
    printf "  -f, --output-file FILE     Output filename for PNG (default: build_performance.png or network_traffic.png)\n"
    printf "  -c, --comment TEXT         Add a comment to the diagram heading (shown in parentheses)\n"
    printf "  -s, --sort-by METRIC       Sort data by metric (default: none)\n"
    printf "                             For time graph: 'total', 'os', 'packages', 'setup'\n"
    printf "                             For network graph: 'total', 'received', 'sent'\n"
    printf "  -d, --sort-dir DIR         Sort direction: 'asc' or 'desc' (default: desc)\n"
    printf "  -g, --graph-type TYPE      Graph type to generate: 'time' or 'network' (default: time)\n"
    printf "  -q, --quiet                Suppress all messages, only show graph output\n"
    printf "\n"
    printf "Examples:\n"
    printf "  %b                         Generate ASCII time graph in terminal\n" "$0"
    printf "  %b --type png              Generate PNG time graph in current directory\n" "$0"
    printf "  %b --type png --output-dir /tmp  Generate PNG time graph in /tmp directory\n" "$0"
    printf "  %b --type png --output-file custom_graph.png  Generate PNG with custom filename\n" "$0"
    printf "  %b --comment \"Version 1.2.3\"  Add a comment to the diagram heading\n" "$0"
    printf "  %b --input-file other-builds.csv Use a different CSV file\n" "$0"
    printf "  %b --sort-by total         Sort by Total Build Time (descending)\n" "$0"
    printf "  %b --sort-by total --sort-dir asc  Sort by Total Build Time (ascending)\n" "$0"
    printf "  %b --graph-type network --sort-by received  Sort network graph by received traffic\n" "$0"
    printf "  %b --type png --graph-type time     Generate the time performance graph\n" "$0"
    printf "  %b --type png --graph-type network  Generate the network traffic graph\n" "$0"
    printf "  %b --quiet                 Only show the graph, no additional messages\n" "$0"
}

# Function to check if a string starts with a dash
starts_with_dash() {
    case "$1" in
    -*)
        return 0
        ;;
    *)
        return 1
        ;;
    esac
}

# Parse command line arguments
while [ $# -gt 0 ]; do
    case "$1" in
    -h | --help)
        usage
        exit 0
        ;;
    -t | --type)
        if [ -z "$2" ] || starts_with_dash "$2"; then
            printf "Error: Missing value for parameter %b\n" "$1"
            exit 1
        fi
        OUTPUT_TYPE="$2"
        shift 2
        ;;
    -i | --input-file)
        if [ -z "$2" ] || starts_with_dash "$2"; then
            printf "Error: Missing value for parameter %b\n" "$1"
            exit 1
        fi
        CSV_FILE="$2"
        shift 2
        ;;
    -o | --output-dir)
        if [ -z "$2" ] || starts_with_dash "$2"; then
            printf "Error: Missing value for parameter %b\n" "$1"
            exit 1
        fi
        OUTPUT_DIR="$2"
        shift 2
        ;;
    -f | --output-file)
        if [ -z "$2" ] || starts_with_dash "$2"; then
            printf "Error: Missing value for parameter %b\n" "$1"
            exit 1
        fi
        OUTPUT_FILE="$2"
        shift 2
        ;;
    -c | --comment)
        if [ -z "$2" ] || starts_with_dash "$2"; then
            printf "Error: Missing value for parameter %b\n" "$1"
            exit 1
        fi
        COMMENT="$2"
        shift 2
        ;;
    -s | --sort-by)
        if [ -z "$2" ] || starts_with_dash "$2"; then
            printf "Error: Missing value for parameter %b\n" "$1"
            exit 1
        fi
        SORT_BY="$2"
        shift 2
        ;;
    -d | --sort-dir)
        if [ -z "$2" ] || starts_with_dash "$2"; then
            printf "Error: Missing value for parameter %b\n" "$1"
            exit 1
        fi
        SORT_DIR="$2"
        shift 2
        ;;
    -g | --graph-type)
        if [ -z "$2" ] || starts_with_dash "$2"; then
            printf "Error: Missing value for parameter %b\n" "$1"
            exit 1
        fi
        GRAPH_TYPE="$2"
        shift 2
        ;;
    -q | --quiet)
        QUIET=true
        shift
        ;;
    *)
        printf "Error: Unknown parameter %b\n" "$1"
        usage
        exit 1
        ;;
    esac
done

# Function to print messages only if not in quiet mode
print_msg() {
    if [ "$QUIET" = "false" ]; then
        printf "%b\n" "$1"
    fi
}

# Validate output type
if [ "$OUTPUT_TYPE" != "terminal" ] && [ "$OUTPUT_TYPE" != "png" ]; then
    printf "Error: Invalid output type '%b'. Must be 'terminal' or 'png'.\n" "$OUTPUT_TYPE"
    exit 1
fi

# Check if gnuplot is installed
if ! command -v gnuplot >/dev/null 2>&1; then
    printf "Error: gnuplot is not installed. Please install it first.\n"
    printf "  - On Debian/Ubuntu: apt-get install gnuplot\n"
    printf "  - On FreeBSD: pkg install gnuplot\n"
    printf "  - On macOS: brew install gnuplot\n"
    exit 1
fi

# Check for PNG terminal support if PNG output is requested
if [ "$OUTPUT_TYPE" = "png" ]; then
    # Try to detect if pngcairo or png terminal is available
    if ! gnuplot -e "set terminal pngcairo" 2>/dev/null; then
        if ! gnuplot -e "set terminal png" 2>/dev/null; then
            printf "Warning: Neither 'pngcairo' nor 'png' terminal type is available in your gnuplot installation.\n"
            printf "PNG output may not work correctly. Consider using --type terminal instead.\n"
            printf "To enable PNG support:\n"
            printf "  - On Debian/Ubuntu: apt-get install gnuplot-x11\n"
            printf "  - On FreeBSD: pkg install gnuplot\n"
            printf "  - On macOS: brew install gnuplot --with-cairo\n"
            printf "Continuing anyway, the script will attempt to use a fallback terminal...\n"
        else
            print_msg "Using standard 'png' terminal (pngcairo not available)..."
        fi
    else
        print_msg "Using enhanced 'pngcairo' terminal for better quality..."
    fi
fi

# Validate sort direction
if [ "$SORT_DIR" != "asc" ] && [ "$SORT_DIR" != "desc" ]; then
    printf "Error: Invalid sort direction '%b'. Must be 'asc' or 'desc'.\n" "$SORT_DIR"
    exit 1
fi

# Validate sort-by option based on graph type
if [ "$GRAPH_TYPE" = "time" ]; then
    if [ "$SORT_BY" != "none" ] && [ "$SORT_BY" != "total" ] && [ "$SORT_BY" != "os" ] && [ "$SORT_BY" != "packages" ] && [ "$SORT_BY" != "setup" ]; then
        printf "Error: Invalid sort-by option '%b' for time graph. Must be 'none', 'total', 'os', 'packages', or 'setup'.\n" "$SORT_BY"
        exit 1
    fi
else # network graph
    if [ "$SORT_BY" != "none" ] && [ "$SORT_BY" != "total" ] && [ "$SORT_BY" != "received" ] && [ "$SORT_BY" != "sent" ]; then
        printf "Error: Invalid sort-by option '%b' for network graph. Must be 'none', 'total', 'received', or 'sent'.\n" "$SORT_BY"
        exit 1
    fi
fi

# Validate graph-type option
if [ "$GRAPH_TYPE" != "time" ] && [ "$GRAPH_TYPE" != "network" ]; then
    printf "Error: Invalid graph-type option '%b'. Must be 'time' or 'network'.\n" "$GRAPH_TYPE"
    exit 1
fi

# Check if CSV file exists
if [ ! -f "$SCRIPT_DIR/$CSV_FILE" ]; then
    printf "Error: CSV file '%b' not found.\n" "$SCRIPT_DIR/$CSV_FILE"
    exit 1
fi

# Check if output directory exists and is writable (for PNG output)
if [ "$OUTPUT_TYPE" = "png" ]; then
    if [ ! -d "$OUTPUT_DIR" ]; then
        printf "Error: Output directory '%b' does not exist.\n" "$OUTPUT_DIR"
        exit 1
    fi
    if [ ! -w "$OUTPUT_DIR" ]; then
        printf "Error: Output directory '%b' is not writable.\n" "$OUTPUT_DIR"
        exit 1
    fi
fi

# Create a temporary sorted CSV file if sorting is requested
TEMP_CSV_FILE=""
GNUPLOT_SORT_BY="none" # Default value for Gnuplot

if [ "$SORT_BY" != "none" ]; then
    TEMP_CSV_FILE=$(mktemp)

    # Determine which column to sort by based on graph type
    if [ "$GRAPH_TYPE" = "time" ]; then
        case "$SORT_BY" in
        "total")
            SORT_COLUMN=12 # Total Build Time
            print_msg "Sorting by Total Build Time..."
            ;;
        "os")
            SORT_COLUMN=3 # OS Installation
            print_msg "Sorting by OS Installation time..."
            ;;
        "packages")
            SORT_COLUMN=4 # OS Packages
            print_msg "Sorting by OS Packages installation time..."
            ;;
        "setup")
            SORT_COLUMN=8 # FlyingDdns Setup
            print_msg "Sorting by FlyingDdns Setup time..."
            ;;
        esac
    else # network graph
        case "$SORT_BY" in
        "total")
            SORT_COLUMN=15 # Total Traffic
            GNUPLOT_SORT_BY="total"
            print_msg "Sorting by Total Network Traffic..."
            ;;
        "received")
            SORT_COLUMN=13 # Received Traffic
            GNUPLOT_SORT_BY="received"
            print_msg "Sorting by Received Network Traffic..."
            ;;
        "sent")
            SORT_COLUMN=14 # Sent Traffic
            GNUPLOT_SORT_BY="sent"
            print_msg "Sorting by Sent Network Traffic..."
            ;;
        esac
    fi

    # Sort the CSV file by the selected column
    # First line is header, keep it at the top
    head -1 "$SCRIPT_DIR/$CSV_FILE" >"$TEMP_CSV_FILE"

    # Sort based on direction
    if [ "$SORT_DIR" = "desc" ]; then
        print_msg "Sorting in descending order..."
        tail -n +2 "$SCRIPT_DIR/$CSV_FILE" | sort -t, -k"${SORT_COLUMN}","${SORT_COLUMN}"nr >>"$TEMP_CSV_FILE"
    else
        print_msg "Sorting in ascending order..."
        tail -n +2 "$SCRIPT_DIR/$CSV_FILE" | sort -t, -k"${SORT_COLUMN}","${SORT_COLUMN}"n >>"$TEMP_CSV_FILE"
    fi

    # Use the sorted file for plotting
    PLOT_CSV_FILE="$TEMP_CSV_FILE"
else
    # Use the original file
    PLOT_CSV_FILE="$SCRIPT_DIR/$CSV_FILE"
fi

# Set Gnuplot variables based on output type and graph type
if [ "$OUTPUT_TYPE" = "png" ]; then
    TERM_TYPE="png"

    # Set output file based on graph type and custom output file
    if [ -n "$OUTPUT_FILE" ]; then
        # Use custom output file if specified
        FINAL_OUTPUT_FILE="$OUTPUT_DIR/$OUTPUT_FILE"
        print_msg "Graph will be saved to: $FINAL_OUTPUT_FILE"
    else
        # Use default output file based on graph type
        if [ "$GRAPH_TYPE" = "time" ]; then
            FINAL_OUTPUT_FILE="$OUTPUT_DIR/build_performance.png"
            print_msg "Build performance graph will be saved to: $FINAL_OUTPUT_FILE"
        else # network
            FINAL_OUTPUT_FILE="$OUTPUT_DIR/network_traffic.png"
            print_msg "Network traffic graph will be saved to: $FINAL_OUTPUT_FILE"
        fi
    fi

    print_msg "Generating PNG graph..."
else
    TERM_TYPE="dumb"
    FINAL_OUTPUT_FILE="/dev/null" # Use /dev/null as a placeholder

    if [ "$GRAPH_TYPE" = "time" ]; then
        print_msg "Generating ASCII build performance graph in terminal..."
    else # network
        print_msg "Generating ASCII network traffic graph in terminal..."
    fi
fi

# Run Gnuplot with the appropriate variables
if [ "$GRAPH_TYPE" = "network" ] && [ "$SORT_BY" != "none" ]; then
    # For network graph with sorting, pass the SORT_BY and SORT_DIR variables
    gnuplot -e "TERM_TYPE='$TERM_TYPE'; OUTPUT_FILE='$FINAL_OUTPUT_FILE'; CSV_FILE='$PLOT_CSV_FILE'; GRAPH_TYPE='$GRAPH_TYPE'; SORT_BY='$GNUPLOT_SORT_BY'; SORT_DIR='$SORT_DIR'; COMMENT='$COMMENT'" "$SCRIPT_DIR/draw_graph.plt" 2>gnuplot_error.log
else
    # For time graph or network graph without sorting
    gnuplot -e "TERM_TYPE='$TERM_TYPE'; OUTPUT_FILE='$FINAL_OUTPUT_FILE'; CSV_FILE='$PLOT_CSV_FILE'; GRAPH_TYPE='$GRAPH_TYPE'; COMMENT='$COMMENT'" "$SCRIPT_DIR/draw_graph.plt" 2>gnuplot_error.log
fi

# Check if the command was successful
RESULT=$?

# Clean up temporary file if it was created
if [ -n "$TEMP_CSV_FILE" ] && [ -f "$TEMP_CSV_FILE" ]; then
    rm -f "$TEMP_CSV_FILE"
fi

if [ $RESULT -eq 0 ]; then
    if [ "$OUTPUT_TYPE" = "png" ]; then
        print_msg "PNG graph generated successfully."
    else
        print_msg "ASCII graph displayed successfully."
    fi
    # Clean up error log
    [ -f gnuplot_error.log ] && rm -f gnuplot_error.log
    exit 0
else
    printf "Error: Failed to generate graph.\n"
    if [ -f gnuplot_error.log ]; then
        printf "Gnuplot error details:\n"
        cat gnuplot_error.log
        rm -f gnuplot_error.log

        # Check for common terminal errors
        if grep -q "unknown or ambiguous terminal" gnuplot_error.log; then
            printf "\nTerminal compatibility issue detected:\n"
            printf "  - For FreeBSD: Try 'pkg install gnuplot'\n"
            printf "  - For Debian/Ubuntu: Try 'apt-get install gnuplot-x11'\n"
            printf "  - For macOS: Try 'brew install gnuplot --with-cairo'\n"
            printf "  - Alternatively, use '--type terminal' for ASCII output\n"
        fi
    fi
    exit 1
fi
