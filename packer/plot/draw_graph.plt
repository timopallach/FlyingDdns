#!/usr/bin/env gnuplot -persist

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Build Performance Visualization - Gnuplot Script
# ===============================================================================
#
# This Gnuplot script generates visualizations of FlyingDdns build performance
# metrics across different platforms. It is designed to be called by the
# draw_graph.sh wrapper script, which handles data preparation and parameter
# passing.
#
# The script can generate two types of visualizations:
# 1. Time Performance Graph - Shows build time breakdown by phase (OS setup,
#    package installation, application setup)
# 2. Network Traffic Graph - Shows network traffic during build (sent/received)
#
# The script supports both terminal-based ASCII output and PNG image generation,
# with appropriate formatting adjustments for each output type.
#
# Expected Variables (set by the wrapper script):
# - TERM_TYPE: Output terminal type ("terminal" or "png")
# - OUTPUT_FILE: Path to output file (for PNG mode)
# - GRAPH_TYPE: Type of graph to generate ("time" or "network")
# - SORT_BY: Metric to sort by ("total", "os", "packages", "setup", "received", "sent")
# - SORT_DIR: Sort direction ("asc" or "desc")
# - COMMENT: Optional comment to display in the title (in parentheses)
#
# The script automatically adjusts formatting, colors, and labels based on the
# graph type and output format to ensure optimal visualization.
# ===============================================================================

# Terminal and output settings will be set by variables
# TERM_TYPE, OUTPUT_FILE, and GRAPH_TYPE are expected to be set by the wrapper script
# SORT_BY can be set to "total", "received", or "sent" to sort network graph (defaults to "total")
# SORT_DIR can be set to "asc" or "desc" to control sort direction (defaults to "desc")
# COMMENT can be set to add a comment in parentheses to the title

# Set a nice color palette
set palette defined (0 "#2596be", 1 "#ff7f0e", 2 "#2ca02c", 3 "#d62728", 4 "#9467bd", 5 "#8c564b", 6 "#e377c2", 7 "#7f7f7f", 8 "#bcbd22", 9 "#17becf")

# General settings
if (TERM_TYPE eq "png") {
    # Check if pngcairo terminal is available
    has_pngcairo = system("gnuplot -e 'set terminal pngcairo' 2>&1 | grep -c 'unknown or ambiguous terminal'")
    
    if (has_pngcairo == "0") {
        # pngcairo is available, use it for better quality
        set terminal pngcairo enhanced font "Arial,12" size 1200, 800
    } else {
        # Fall back to standard png terminal which is more widely available
        set terminal png enhanced font "Arial,12" size 1200, 800
    }
    
    set output OUTPUT_FILE
    set key outside bottom center horizontal Left reverse noenhanced autotitle box spacing 2
} else {
    # Increase terminal size for better readability (width 250, height 60)
    set terminal dumb 250 60
    # Place key below the graph with better spacing
    set key outside bottom center horizontal noreverse noenhanced autotitle nobox spacing 10
    # Reduce left margin to minimize empty space before first label
    set lmargin 5
    # Increase bottom margin to make room for the key
    set bmargin 12
}

set datafile separator ","
set style data histogram
set style histogram clustered gap 1
set style fill solid 0.8 border -1
set boxwidth 0.9

# X-axis settings
if (TERM_TYPE eq "png") {
    set xtics border in scale 0,0 nomirror rotate by -45 autojustify
    set xtics norangelimit
    set xtics font ",10"
} else {
    # For terminal mode, use simpler labels and no rotation
    set xtics border in scale 0,0 nomirror
    set xtics norangelimit
    # Increase the gap between tics for terminal mode
    set xtics 5
    # Offset the xtics to start closer to the left margin
    set xtics offset 0,0
}

# Create a combined OS+arch label for x-axis
set datafile missing '-'

# Function to create simplified labels for terminal mode
# For PNG: Use OS/arch format
# For terminal: Use two-line format with OS name on first line, arch on second
# Add padding to ensure all OS names are properly spaced and align architecture names
simplified_label(os, arch) = (TERM_TYPE eq "png") ? \
    sprintf("%s/%s", os, arch) : \
    (os eq "alpine") ? sprintf("alpine   \n   %s", arch) : \
    (os eq "debian") ? sprintf("debian   \n   %s", arch) : \
    (os eq "fedora") ? sprintf("fedora   \n   %s", arch) : \
    (os eq "freebsd") ? sprintf("freebsd  \n   %s", arch) : \
    (os eq "macos") ? sprintf("macos    \n   %s", arch) : \
    (os eq "netbsd") ? sprintf("netbsd   \n   %s", arch) : \
    (os eq "nixos") ? sprintf("nixos    \n   %s", arch) : \
    (os eq "openbsd") ? sprintf("openbsd  \n   %s", arch) : \
    (os eq "opensuse") ? sprintf("opensuse \n   %s", arch) : \
    (os eq "rocky") ? sprintf("rocky    \n   %s", arch) : \
    (os eq "ubuntu") ? sprintf("ubuntu   \n   %s", arch) : \
    sprintf("%s\n   %s", os, arch)

# Function to add comment to title if provided
title_with_comment(base_title) = strlen(COMMENT) > 0 ? sprintf("%s (%s)", base_title, COMMENT) : base_title

# Y-axis settings and plot based on graph type
if (GRAPH_TYPE eq "time") {
    # Y-axis settings for time graph
    set ylabel "Time (seconds)"
    set yrange [0:*]
    set grid y
    
    # Title with optional comment
    set title title_with_comment("FlyingDdns Build Performance Across Platforms") font ",16"
    
    # Plot the most relevant metrics
    if (TERM_TYPE eq "png") {
        plot CSV_FILE using 12:xticlabels(sprintf("%s/%s", stringcolumn(1), stringcolumn(2))) title "Total Build Time" lc rgb "#e41a1c", \
             '' using 3:xticlabels(sprintf("%s/%s", stringcolumn(1), stringcolumn(2))) title "OS Installation" lc rgb "#377eb8", \
             '' using 4:xticlabels(sprintf("%s/%s", stringcolumn(1), stringcolumn(2))) title "OS Packages" lc rgb "#4daf4a", \
             '' using 8:xticlabels(sprintf("%s/%s", stringcolumn(1), stringcolumn(2))) title "FlyingDdns Setup" lc rgb "#984ea3"
    } else {
        # For terminal mode, use custom characters in the legend
        set key title "Legend: # = Total Build Time, @ = OS Installation, % = OS Packages, + = FlyingDdns Setup"
        
        # Plot with boxes but use different fill patterns
        plot CSV_FILE using 12:xticlabels(simplified_label(stringcolumn(1), stringcolumn(2))) title "Total Build Time" with boxes, \
             '' using 3:xticlabels(simplified_label(stringcolumn(1), stringcolumn(2))) title "OS Installation" with boxes, \
             '' using 4:xticlabels(simplified_label(stringcolumn(1), stringcolumn(2))) title "OS Packages" with boxes, \
             '' using 8:xticlabels(simplified_label(stringcolumn(1), stringcolumn(2))) title "FlyingDdns Setup" with boxes
    }
} else {
    # Y-axis settings for network graph
    set ylabel "Network Traffic (MiB)"
    set yrange [0:*]
    set grid y
    
    # Determine which column to sort by
    if (!exists("SORT_BY")) {
        SORT_BY = "total"
    }
    
    # Set the sort column based on SORT_BY
    if (SORT_BY eq "received") {
        sort_column = 13
        sort_name = "Received Traffic"
    } else if (SORT_BY eq "sent") {
        sort_column = 14
        sort_name = "Sent Traffic"
    } else {
        # Default to total traffic
        sort_column = 15
        sort_name = "Total Traffic"
    }
    
    # Determine sort direction
    if (!exists("SORT_DIR")) {
        SORT_DIR = "desc"
    }
    
    # Add sort direction to the title
    sort_direction = (SORT_DIR eq "asc") ? "Ascending" : "Descending"
    
    # Title with sort information and optional comment
    base_title = sprintf("FlyingDdns Network Traffic Across Platforms (Sorted by %s, %s)", sort_name, sort_direction)
    set title title_with_comment(base_title) font ",16"
    
    # Create a sorted version of the data using the system command
    system("head -1 ".CSV_FILE." > sorted_network.csv")
    
    # Sort based on direction
    if (SORT_DIR eq "desc") {
        system(sprintf("tail -n +2 %s | sort -t, -k%d,%dnr >> sorted_network.csv", CSV_FILE, sort_column, sort_column))
    } else {
        system(sprintf("tail -n +2 %s | sort -t, -k%d,%dn >> sorted_network.csv", CSV_FILE, sort_column, sort_column))
    }
    
    # Plot network traffic metrics using the sorted data
    if (TERM_TYPE eq "png") {
        plot "sorted_network.csv" using 15:xticlabels(sprintf("%s/%s", stringcolumn(1), stringcolumn(2))) title "Total Traffic" lc rgb "#e41a1c", \
             '' using 13:xticlabels(sprintf("%s/%s", stringcolumn(1), stringcolumn(2))) title "Received Traffic" lc rgb "#377eb8", \
             '' using 14:xticlabels(sprintf("%s/%s", stringcolumn(1), stringcolumn(2))) title "Sent Traffic" lc rgb "#4daf4a"
    } else {
        # For terminal mode, use custom characters in the legend
        set key title "Legend: # = Total Traffic, @ = Received Traffic, % = Sent Traffic"
        
        # Plot with boxes using the sorted data
        plot "sorted_network.csv" using 15:xticlabels(simplified_label(stringcolumn(1), stringcolumn(2))) title "Total Traffic" with boxes, \
             '' using 13:xticlabels(simplified_label(stringcolumn(1), stringcolumn(2))) title "Received Traffic" with boxes, \
             '' using 14:xticlabels(simplified_label(stringcolumn(1), stringcolumn(2))) title "Sent Traffic" with boxes
    }
}
