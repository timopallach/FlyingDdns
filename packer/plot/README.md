# FlyingDdns Build Performance Visualization

This directory contains tools for visualizing build performance metrics for the FlyingDdns project across different platforms.

## Components

- `draw_graph.plt` - Gnuplot script that generates the actual graphs
- `draw_graph.sh` - Shell script wrapper that provides a user-friendly interface
- CSV files (e.g., `analyze-builds-5e3bfc11.csv`) - Contains the build performance data

## Usage

The `draw_graph.sh` script provides a simple interface to generate graphs from the build performance data:

```bash
# Display ASCII time graph in terminal (default)
./draw_graph.sh

# Generate PNG time graph in current directory
./draw_graph.sh --type png

# Use a different CSV file
./draw_graph.sh --file other-builds.csv

# Sort by Total Build Time (descending)
./draw_graph.sh --sort-by total

# Sort by Total Build Time (ascending)
./draw_graph.sh --sort-by total --sort-dir asc

# Generate PNG and sort by OS Installation time
./draw_graph.sh --type png --sort-by os

# Generate the time performance graph (default)
./draw_graph.sh --type png --graph-type time

# Generate the network traffic graph
./draw_graph.sh --type png --graph-type network

# Display network traffic as ASCII in terminal
./draw_graph.sh --graph-type network

# Sort network graph by received traffic in ascending order
./draw_graph.sh --graph-type network --sort-by received --sort-dir asc

# Only show the graph, no additional messages
./draw_graph.sh --quiet
```

### Command Line Options

| Option | Description |
|--------|-------------|
| `--help` | Display help message |
| `--type TYPE` | Output type: 'terminal' or 'png' (default: terminal) |
| `--file FILE` | CSV file to use (default: analyze-builds-5e3bfc11.csv) |
| `--output-dir DIR` | Output directory for PNG files (default: current directory) |
| `--sort-by METRIC` | Sort data by metric (default: none)<br>For time graph: 'total', 'os', 'packages', 'setup'<br>For network graph: 'total', 'received', 'sent' |
| `--sort-dir DIR` | Sort direction: 'asc' or 'desc' (default: desc) |
| `--graph-type TYPE` | Graph type to generate: 'time' or 'network' (default: time) |
| `--quiet` | Suppress all messages, only show graph output |

## Graph Types

The script can generate two types of graphs (one at a time):

1. **Build Performance Graph** (`--graph-type time`) - Shows build times for different components across platforms
   - Total Build Time
   - OS Installation
   - OS Packages
   - FlyingDdns Setup

2. **Network Traffic Graph** (`--graph-type network`) - Shows network traffic metrics
   - Total Traffic
   - Received Traffic
   - Sent Traffic

The default is to generate the build performance graph (`--graph-type time`).

## CSV File Format

The CSV file should contain the following columns:
- Column 1: OS name
- Column 2: Architecture
- Column 3: OS Installation time
- Column 4: OS Packages installation time
- Column 8: FlyingDdns Setup time
- Column 12: Total Build Time
- Column 13: Received Network Traffic
- Column 14: Sent Network Traffic
- Column 15: Total Network Traffic

## Sorting Options

The script allows sorting the data by different metrics:

### Time Graph Sorting Options

| Option | Description | Column |
|--------|-------------|--------|
| `total` | Sort by Total Build Time | 12 |
| `os` | Sort by OS Installation time | 3 |
| `packages` | Sort by OS Packages installation time | 4 |
| `setup` | Sort by FlyingDdns Setup time | 8 |

### Network Graph Sorting Options

| Option | Description | Column |
|--------|-------------|--------|
| `total` | Sort by Total Network Traffic | 15 |
| `received` | Sort by Received Network Traffic | 13 |
| `sent` | Sort by Sent Network Traffic | 14 |

By default, sorting is done in descending order (highest values first). Use the `--sort-dir asc` option to sort in ascending order (lowest values first).

## Examples

### Terminal Output
When run with the default terminal output, the script generates an ASCII graph directly in the terminal:

```
                                 FlyingDdns Build Performance Across Platforms                                  
  2000 +-------------------------------------------------------------------------------------------------------+
       |       +       +       +       +       +       +       +       +       +       +       +       +       |
       |                                                                                                       |
       |                                                                                                       |
  1800 |-+                                                                                                   +-|
       |                                                                                                       |
       |                                                                                                       |
  1600 |-+                                                                                                   +-|
       |                                                                                                       |
       |                                                                                                       |
  1400 |-+                                                                                                   +-|
       |                                                                                                       |
       |                                                                                                       |
  1200 |-+                                                                                                   +-|
       |                                                                                                       |
       |                                                                                                       |
  1000 |-+                                                                                                   +-|
       |                                                                                                       |
       |                                                                                                       |
   800 |-+                                                                                                   +-|
       |                                                                                                       |
       |                                                                                                       |
   600 |-+                                                                                                   +-|
       |                                                                                                       |
       |                                                                                                       |
   400 |-+                                                                                                   +-|
       |                                                                                                       |
       |                                                                                                       |
   200 |-+                                                                                                   +-|
       |                                                                                                       |
       |       +       +       +       +       +       +       +       +       +       +       +       +       |
     0 +-------------------------------------------------------------------------------------------------------+
                                                                                                                
```

### PNG Output
When run with the `--type png` option, the script generates a PNG file based on the `--graph-type` option:
- `build_performance.png` - Shows build performance metrics (generated with `--graph-type time`)
- `network_traffic.png` - Shows network traffic metrics (generated with `--graph-type network`)

## How It Works

1. The `draw_graph.sh` script parses command-line arguments and sets up the environment
2. It validates inputs and prepares the necessary variables for Gnuplot
3. If sorting is requested, it creates a temporary sorted CSV file
4. It calls the `draw_graph.plt` Gnuplot script with the appropriate parameters
5. The Gnuplot script reads the CSV data and generates the graph based on the specified output type and graph type
6. Any temporary files are cleaned up

## Requirements

- Gnuplot (must be installed on the system)
- Shell environment (sh/bash) 