#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

#shellcheck disable=SC1091 #Not following: (error message here)
#shellcheck disable=SC2154 #var is referenced but not assigned.
#shellcheck disable=SC2317 #Command appears to be unreachable. Check usage (or ignore if invoked indirectly).

#  _____ _       _             ____      _
# |  ___| |_   _(_)_ __   __ _|  _ \  __| |_ __  ___
# | |_  | | | | | | '_ \ / _` | | | |/ _` | '_ \/ __|
# |  _| | | |_| | | | | | (_| | |_| | (_| | | | \__ \
# |_|   |_|\__, |_|_| |_|\__, |____/ \__,_|_| |_|___/
#          |___/         |___/

# ===============================================================================
# FlyingDdns Packer Build Script
# ===============================================================================
#
# This script automates the process of building virtual machine images for
# FlyingDdns using Packer. It supports multiple operating systems, architectures,
# and virtualization platforms.
#
# The script performs the following operations:
# 1. Parses command-line arguments to determine:
#    - Target operating system (e.g., debian, ubuntu, freebsd)
#    - Target architecture (e.g., amd64, arm64)
#    - Virtualization platform (e.g., qemu, virtualbox, vsphere)
#    - Build options (e.g., headless mode, debug mode)
# 2. Sets up the build environment with appropriate variables
# 3. Performs pre-build validation and preparation
# 4. Validates the requested OS/architecture/virtualization combination
# 5. Executes the Packer build process via the running_build function
#
# For vsphere builds, environment variables esxi_host, esxi_username, and
# esxi_password must be set prior to execution.
# ===============================================================================

# The version of this script.
export VERSION="0.1"

# Getting the command line options and parsing them.
. bin/getoptions.sh
set -eu
parser_definition() {
    setup REST plus:true help:usage abbr:true -- \
        "Usage: ${2##*/} [options...]" ''
    msg -- 'Script to build FlyingDdns virtual environments' ''
    msg -- 'Options:'

    param OS_NAME -o --os-name export:true pattern:"alpine | debian | fedora | freebsd | macos | netbsd | nixos | openbsd | openindiana | opensuse | rocky | ubuntu" -- "alpine, debian, fedora, freebsd, macos, netbsd, nixos, openbsd, openindiana, opensuse, rocky or ubuntu"
    param ARCH_NAME -a --arch-name export:true pattern:"amd64 | arm64" -- "amd64 or arm64"
    param VIRT_NAME -v --virt-name export:true pattern:"hcloud | qemu | tart | virtualbox | vmware | vsphere" -- "hcloud, qemu, tart, virtualbox, vmware or vsphere"
    param BRANCH_NAME -b --branch-name export:true -- "the branch to be used withing the packer build"
    param DEBUG -d --debug export:true pattern:"no | yes" -- "no or yes"

    disp :usage -h --help
    disp VERSION -V --version
}
eval "$(getoptions parser_definition - "$0") exit 1"
# Check the required command line options
[ -z "${OS_NAME}" ] && printf "%b ERROR: Missing required parameter: --os-name\n" "$(date "+%Y-%m-%d %H:%M:%S")" && exit 2
[ -z "${ARCH_NAME}" ] && printf "%b ERROR: Missing required parameter: --arch-name\n" "$(date "+%Y-%m-%d %H:%M:%S")" && exit 3
[ -z "${VIRT_NAME}" ] && printf "%b ERROR: Missing required parameter: --virt-name\n" "$(date "+%Y-%m-%d %H:%M:%S")" && exit 4
[ -z "${DEBUG}" ] && DEBUG="no"
set +eu

# Check if environment variables esxi_host, esxi_username and esxi_password have been set when --virt-name is vsphere.
if [ "${VIRT_NAME}" = "vsphere" ]; then
    if [ "${esxi_host}" = "" ] || [ "${esxi_username}" = "" ] || [ "${esxi_password}" = "" ]; then
        printf "%b ERROR: You selected vsphere as virtualization platform (--virt-name).\n" "$(date "+%Y-%m-%d %H:%M:%S")"
        printf "%b ERROR: Please set the environment variables esxi_host, esxi_username and esxi_password prior to call this scrip.\n" "$(date "+%Y-%m-%d %H:%M:%S")"
        exit 5
    fi
fi

if [ "${OS_NAME}" = "macos" ]; then
    macos_name="$(grep -A 2 "${OS_NAME}-name" hcl/variables.pkr.hcl | grep default | cut -d "\"" -f 2)"
    export macos_name
fi
os_version="$(grep -A 2 "${OS_NAME}-version" hcl/variables.pkr.hcl | grep default | cut -d "\"" -f 2)"
export os_version
if [ "${OS_NAME}" = "openbsd" ]; then
    use_openbsd_snapshot="$(grep -A 2 openbsd-use-snapshot hcl/variables.pkr.hcl | grep default | cut -d "\"" -f 2 | cut -w -f 4)"
    export use_openbsd_snapshot
fi

current_directory="$(pwd)"
export current_directory
git_commit_sha="$(git rev-parse HEAD)"
export git_commit_sha
git_commit_short_sha="$(git rev-parse --short=8 HEAD)"
export git_commit_short_sha
git_tag="$(git tag --points-at HEAD)"
export git_tag
git_current_branch="$(git branch --show-current)"
export git_current_branch

# Check the optional command line options
[ -z "${BRANCH_NAME}" ] && BRANCH_NAME="${git_current_branch}"

if [ "${CI}" != "true" ]; then
    export flyingddns_version="${git_commit_short_sha}"
    export flyingddns_commit_sha="${git_commit_sha}"
    export flyingddns_commit_short_sha="${git_commit_short_sha}"
    export flyingddns_robotframework_version="0.0.0"
else
    printf "%b DEBUG: Taking the variables from the CI system.\n" "$(date "+%Y-%m-%d %H:%M:%S")"
fi

# Set the HOST OS variables (e.g. required by QEMU)
if (uname -a | grep Alpine >/dev/null 2>&1); then
    export HOST_OS_NAME="ALPINE"
elif (uname -a | grep Debian >/dev/null 2>&1); then
    export HOST_OS_NAME="DEBIAN"
elif (grep Fedora /etc/os-release >/dev/null 2>&1); then
    export HOST_OS_NAME="FEDORA"
elif (uname -a | grep FreeBSD >/dev/null 2>&1); then
    export HOST_OS_NAME="FREEBSD"
    if [ "${VIRT_NAME}" = "qemu" ] && [ "${ARCH_NAME}" = "arm64" ]; then
        export qemu_arm64_cpu="cortex-a53"
        export qemu_arm64_accelerator="tcg"
        export qemu_arm64_firmware_path="/usr/local/share/qemu/edk2-aarch64-code.fd"
    fi
elif (uname -a | grep Darwin >/dev/null 2>&1); then
    export HOST_OS_NAME="MACOS"
    if [ "${VIRT_NAME}" = "qemu" ] && [ "${ARCH_NAME}" = "arm64" ]; then
        export qemu_arm64_cpu="host"
        export qemu_arm64_accelerator="hvf"
        export qemu_arm64_firmware_path="/opt/homebrew/share/qemu/edk2-aarch64-code.fd"
    fi
elif (uname -a | grep NetBSD >/dev/null 2>&1); then
    export HOST_OS_NAME="NETBSD"
elif (uname -a | grep OpenBSD >/dev/null 2>&1); then
    export HOST_OS_NAME="OPENBSD"
elif (uname -a | grep Ubuntu >/dev/null 2>&1); then
    export HOST_OS_NAME="UBUNTU"
else
    printf "%b %b ERROR: Unsupported operating system uname=<%b>.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "$(uname -a)"
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 6
fi

printf "################################################################################\n"
printf "# Building the FlyingDdns stack\n"
printf "################################################################################\n"
printf "%b DEBUG: HOST_OS_NAME=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${HOST_OS_NAME}"
printf "%b DEBUG: Some important CI variables if run in a pipeline:\n" "$(date "+%Y-%m-%d %H:%M:%S")"
printf "%b DEBUG: CI=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${CI}"
printf "%b DEBUG: CI_COMMIT_BRANCH=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${CI_COMMIT_BRANCH}"
printf "%b DEBUG: CI_COMMIT_REF_NAME=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${CI_COMMIT_REF_NAME}"
printf "%b DEBUG: CI_COMMIT_TAG=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${CI_COMMIT_TAG}"
printf "%b DEBUG: CI_COMMIT_SHA=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${CI_COMMIT_SHA}"
printf "%b DEBUG: CI_COMMIT_SHORT_SHA=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${CI_COMMIT_SHORT_SHA}"
printf "%b DEBUG: Using the following options:\n" "$(date "+%Y-%m-%d %H:%M:%S")"
printf "%b DEBUG: os-name=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${OS_NAME}"
if [ "${OS_NAME}" = "macos" ]; then
    printf "%b DEBUG: macos-name=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${macos_name}"
fi
printf "%b DEBUG: os-version=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${os_version}"
if [ "${OS_NAME}" = "openbsd" ]; then
    printf "%b DEBUG: openbsd-use-snapshot=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${use_openbsd_snapshot}"
fi
printf "%b DEBUG: arch-name=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${ARCH_NAME}"
printf "%b DEBUG: virt-name=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${VIRT_NAME}"
printf "%b DEBUG: git-branch-name=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${BRANCH_NAME}"
printf "%b DEBUG: git-commit-sha=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${git_commit_sha}"
printf "%b DEBUG: git-commit-short-sha=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${git_commit_short_sha}"
printf "%b DEBUG: git-tag=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${git_tag}"
printf "%b DEBUG: git-current-branch=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${git_current_branch}"
if [ "${VIRT_NAME}" = "vsphere" ]; then
    printf "%b DEBUG: esxi_host=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${esxi_host}"
    printf "%b DEBUG: esxi_username=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${esxi_username}"
fi
if [ "${VIRT_NAME}" = "qemu" ] && [ "${ARCH_NAME}" = "arm64" ]; then
    printf "%b DEBUG: qemu_arm64_firmware_path=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${qemu_arm64_firmware_path}"
    printf "%b DEBUG: qemu_arm64_cpu=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${qemu_arm64_cpu}"
    printf "%b DEBUG: qemu_arm64_accelerator=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${qemu_arm64_accelerator}"
fi
printf "%b DEBUG: current_directory=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${current_directory}"
printf "%b DEBUG: flyingddns_version=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${flyingddns_version}"
printf "%b DEBUG: flyingddns_commit_sha=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${flyingddns_commit_sha}"
printf "%b DEBUG: flyingddns_commit_short_sha=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${flyingddns_commit_short_sha}"
printf "%b DEBUG: flyingddns_robotframework_version=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${flyingddns_robotframework_version}"
printf "%b DEBUG: DEBUG=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${DEBUG}"

# Sourcing the sript containing the functions
. bin/functions.sh
# Sourcing the sript containing the variables
. bin/variables.sh

# Checking the OS and the ARCH and run the build
# AMD64: All architectures
if [ "${ARCH_NAME}" = "amd64" ]; then
    if [ "${VIRT_NAME}" = "qemu" ]; then
        if [ "${OS_NAME}" = "alpine" ] || [ "${OS_NAME}" = "debian" ] || [ "${OS_NAME}" = "fedora" ] || [ "${OS_NAME}" = "freebsd" ] || [ "${OS_NAME}" = "netbsd" ] || [ "${OS_NAME}" = "nixos" ] || [ "${OS_NAME}" = "openbsd" ] || [ "${OS_NAME}" = "opensuse" ] || [ "${OS_NAME}" = "rocky" ] || [ "${OS_NAME}" = "ubuntu" ]; then
            export BUILDER_NAME="${VIRT_NAME}"
        else
            printf "%b ERROR: The VIRT_NAME=<%b> is not supported on ARCH_NAME=<%b> and OS_NAME=<%b>.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${VIRT_NAME}" "${ARCH_NAME}" "${OS_NAME}"
            exit 8
        fi
    elif [ "${VIRT_NAME}" = "virtualbox" ]; then
        if [ "${OS_NAME}" = "alpine" ] || [ "${OS_NAME}" = "debian" ] || [ "${OS_NAME}" = "fedora" ] || [ "${OS_NAME}" = "freebsd" ] || [ "${OS_NAME}" = "netbsd" ] || [ "${OS_NAME}" = "nixos" ] || [ "${OS_NAME}" = "openindiana" ] || [ "${OS_NAME}" = "openbsd" ] || [ "${OS_NAME}" = "opensuse" ] || [ "${OS_NAME}" = "rocky" ] || [ "${OS_NAME}" = "ubuntu" ]; then
            export BUILDER_NAME="${VIRT_NAME}-iso"
        else
            printf "%b ERROR: The VIRT_NAME=<%b> is not supported on ARCH_NAME=<%b> and OS_NAME=<%b>.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${VIRT_NAME}" "${ARCH_NAME}" "${OS_NAME}"
            exit 9
        fi
    else
        printf "%b ERROR: The VIRT_NAME=<%b> is not supported on ARCH_NAME=<%b> and OS_NAME=<%b>.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${VIRT_NAME}" "${ARCH_NAME}" "${OS_NAME}"
        exit 10
    fi
# ARM64: All architectures
elif [ "${ARCH_NAME}" = "arm64" ]; then
    if [ "${VIRT_NAME}" = "hcloud" ]; then
        if [ "${OS_NAME}" = "openbsd" ]; then
            export BUILDER_NAME="${VIRT_NAME}"
        else
            printf "%b ERROR: The VIRT_NAME=<%b> is not supported on ARCH_NAME=<%b> and OS_NAME=<%b>.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${VIRT_NAME}" "${ARCH_NAME}" "${OS_NAME}"
            exit 11
        fi
    elif [ "${VIRT_NAME}" = "qemu" ]; then
        if [ "${OS_NAME}" = "alpine" ] || [ "${OS_NAME}" = "debian" ] || [ "${OS_NAME}" = "fedora" ] || [ "${OS_NAME}" = "freebsd" ] || [ "${OS_NAME}" = "netbsd" ] || [ "${OS_NAME}" = "nixos" ] || [ "${OS_NAME}" = "openbsd" ] || [ "${OS_NAME}" = "opensuse" ] || [ "${OS_NAME}" = "rocky" ] || [ "${OS_NAME}" = "ubuntu" ]; then
            export BUILDER_NAME="${VIRT_NAME}"
        else
            printf "%b ERROR: The VIRT_NAME=<%b> is not supported on ARCH_NAME=<%b> and OS_NAME=<%b>.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${VIRT_NAME}" "${ARCH_NAME}" "${OS_NAME}"
            exit 12
        fi
    elif [ "${VIRT_NAME}" = "virtualbox" ] || [ "${VIRT_NAME}" = "vmware" ] || [ "${VIRT_NAME}" = "vsphere" ]; then
        if [ "${OS_NAME}" = "alpine" ] || [ "${OS_NAME}" = "debian" ] || [ "${OS_NAME}" = "fedora" ] || [ "${OS_NAME}" = "freebsd" ] || [ "${OS_NAME}" = "netbsd" ] || [ "${OS_NAME}" = "nixos" ] || [ "${OS_NAME}" = "openbsd" ] || [ "${OS_NAME}" = "opensuse" ] || [ "${OS_NAME}" = "rocky" ] || [ "${OS_NAME}" = "ubuntu" ]; then
            export BUILDER_NAME="${VIRT_NAME}-iso"
        else
            printf "%b ERROR: The VIRT_NAME=<%b> is not supported on ARCH_NAME=<%b> and OS_NAME=<%b>.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${VIRT_NAME}" "${ARCH_NAME}" "${OS_NAME}"
            exit 13
        fi
    elif [ "${VIRT_NAME}" = "tart" ]; then
        if [ "${OS_NAME}" = "macos" ]; then
            export BUILDER_NAME="${VIRT_NAME}-cli"
        else
            printf "%b ERROR: The VIRT_NAME=<%b> is not supported on ARCH_NAME=<%b> and OS_NAME=<%b>.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${VIRT_NAME}" "${ARCH_NAME}" "${OS_NAME}"
            exit 14
        fi
    else
        printf "%b ERROR: The VIRT_NAME=<%b> is not supported on ARCH_NAME=<%b> and OS_NAME=<%b>.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${VIRT_NAME}" "${ARCH_NAME}" "${OS_NAME}"
        exit 15
    fi
else
    printf "%b ERROR: The VIRT_NAME=<%b> is not supported on ARCH_NAME=<%b> and OS_NAME=<%b>.\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${VIRT_NAME}" "${ARCH_NAME}" "${OS_NAME}"
    exit 16
fi

if ! running_build; then
    exit 17
fi

exit 0
