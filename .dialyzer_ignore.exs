# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Dialyzer Ignore Configuration
# ===============================================================================
#
# This file configures which Dialyzer warnings should be ignored in the FlyingDdns
# project. Dialyzer is a static analysis tool that identifies software discrepancies
# such as type errors, unreachable code, and other potential bugs.
#
# The file contains an empty list, meaning no warnings are currently being ignored.
# This is intentional as the project aims to address all Dialyzer warnings rather
# than suppressing them.
#
# If specific warnings need to be ignored in the future (e.g., for third-party
# library incompatibilities or false positives), they can be added to this list
# using the appropriate pattern format.
#
# For more information on Dialyzer and its warning patterns, see:
# https://hexdocs.pm/dialyxir/readme.html
# ===============================================================================

# This file contains an empty list, meaning no warnings will be ignored
[]
