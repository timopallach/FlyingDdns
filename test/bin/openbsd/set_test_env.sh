#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

export FLYINGDDNS_TMUX_NAME="flyingdns-server"
#POSTGRESQL_VERSION="$(brew list | grep postgresql | cut -d "@" -f 2)"
#export POSTGRESQL_VERSION
#PATH="$(brew --prefix)/opt/postgresql@${POSTGRESQL_VERSION}/bin:$PATH"
#export PATH
export MIX_ENV="prod"
export SECRET_KEY_BASE="12345678901234567890123456789012345678901234567890123456789012345"
export DATABASE_URL="ecto://postgres@[::1]/flying_ddns_${MIX_ENV}"
export PDNS_API_PASSWD="pdns_api_passwd"
export DOMAIN_NAME="example.org"
export FDDNS_PROTOCOL="http"
export FDDNS_HOSTNAME="[::1]"
export FDDNS_PORT="4000"
export FDDNS_IP_VERSION="ipv6"
export FDDNS_VERSION="0.0.0"
export FDDNS_COMMIT_SHA="0000000000000000000000000000000000000000"
export FDDNS_COMMIT_SHORT_SHA="00000000"
