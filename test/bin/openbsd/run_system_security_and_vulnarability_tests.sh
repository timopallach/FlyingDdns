#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

script_name="$(basename "${0}")"

printf "%b %b INFO:  Setting the required environment variables:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
# shellcheck source=/dev/null
if ! . test/bin/openbsd/set_test_env.sh; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

#printf "%b %b INFO:  Run feroxbuster:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#if ! feroxbuster --wordlist "${HOME}/Security/SecLists/Discovery/Web-Content/big.txt" --url "${FDDNS_PROTOCOL}://${FDDNS_HOSTNAME}:${FDDNS_PORT}"; then
#    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#    exit 2
#fi
#printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#
#printf "%b %b INFO:  Run ffuf:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#if ! ffuf -u "${FDDNS_PROTOCOL}://${FDDNS_HOSTNAME}:${FDDNS_PORT}"//FUZZ -w "${HOME}/Security/SecLists/Discovery/Web-Content/big.txt"; then
#    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#    exit 3
#fi
#printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#
#printf "%b %b INFO:  Update nuclei templates:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#if ! nuclei -update-templates; then
#    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#    exit 4
#fi
#printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#
#printf "%b %b INFO:  Run nuclei:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#if ! nuclei -ip-version 6 -target "${FDDNS_PROTOCOL}://${FDDNS_HOSTNAME}:${FDDNS_PORT}"; then
#    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#    exit 5
#fi
#printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#
#printf "%b %b INFO:  Run nikto:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
##if ! nikto -nossl -ipv6 -host "${FDDNS_PROTOCOL}://${FDDNS_HOSTNAME}:${FDDNS_PORT}"; then
#    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#    exit 6
#fi
#printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

#printf "%b %b INFO:  Run testssl.sh:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
##if ! testssl.sh "${FDDNS_PROTOCOL}://${FDDNS_HOSTNAME}:${FDDNS_PORT}"; then
#    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#    exit 7
#fi
#printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

#printf "%b %b INFO:  Run sslscan:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
##if ! sslscan "${FDDNS_PROTOCOL}://${FDDNS_HOSTNAME}:${FDDNS_PORT}"; then
#    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#    exit 8
#fi
#printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
