#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

script_name="$(basename "${0}")"

printf "%b %b INFO:  Terminating left over tmux session:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! test/bin/openbsd/terminate_tmux_sessions.sh; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Building and starting Flyingddns:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! test/bin/openbsd/build_and_run.sh; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Run Robotframework FlyingDdns tests:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! test/bin/openbsd/run_robotframework_tests.sh -k; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 5
fi
printf "%b %b INFO:  ==>> SUCCEDED\n\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Terminating the respective tmux session:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! test/bin/openbsd/terminate_tmux_sessions.sh; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 7
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  ==========================================================================\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
printf "%b %b INFO:  = FlyingDdns was build, run and tested. It is still working as EXPECTED. =\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
printf "%b %b INFO:  ==========================================================================\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
