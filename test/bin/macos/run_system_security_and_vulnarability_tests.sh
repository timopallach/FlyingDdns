#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns System Security and Vulnerability Test Runner
# ===============================================================================
#
# This script performs system-level security and vulnerability testing against
# a running instance of the FlyingDdns application on macOS. It uses various
# security tools to identify potential vulnerabilities in the deployed application.
#
# The script performs the following operations:
# 1. Sets up the required environment variables by sourcing set_test_env.sh
# 2. Runs feroxbuster to perform directory and file enumeration
# 3. Runs ffuf for fuzzing web endpoints
# 4. Updates nuclei templates to ensure the latest vulnerability checks
# 5. Runs nuclei to scan for known vulnerabilities
# 6. (Commented out) Additional security tools that can be enabled:
#    - nikto for web server vulnerability scanning
#    - testssl.sh for SSL/TLS configuration testing
#    - sslscan for SSL/TLS vulnerability scanning
#
# This script is typically run after the application has been built and started
# to validate the security of the deployed instance.
#
# Requirements:
# - Security tools must be installed (feroxbuster, ffuf, nuclei, etc.)
# - FlyingDdns application must be running (typically started by build_and_run.sh)
# ===============================================================================

script_name="$(basename "${0}")"

printf "%b %b INFO:  Setting the required environment variables:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
# shellcheck source=/dev/null
if ! . test/bin/macos/set_test_env.sh; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Run feroxbuster:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! feroxbuster --wordlist "${HOME}/Security/SecLists/Discovery/Web-Content/big.txt" --url "${FDDNS_PROTOCOL}://${FDDNS_HOSTNAME}:${FDDNS_PORT}"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Run ffuf:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! ffuf -u "${FDDNS_PROTOCOL}://${FDDNS_HOSTNAME}:${FDDNS_PORT}"//FUZZ -w "${HOME}/Security/SecLists/Discovery/Web-Content/big.txt"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Update nuclei templates:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! nuclei -update-templates; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Run nuclei:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! nuclei -ip-version 6 -target "${FDDNS_PROTOCOL}://${FDDNS_HOSTNAME}:${FDDNS_PORT}"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 5
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

#printf "%b %b INFO:  Run nikto:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
##if ! nikto -nossl -ipv6 -host "${FDDNS_PROTOCOL}://${FDDNS_HOSTNAME}:${FDDNS_PORT}"; then
#    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#    exit 6
#fi
#printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

#printf "%b %b INFO:  Run testssl.sh:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
##if ! testssl.sh "${FDDNS_PROTOCOL}://${FDDNS_HOSTNAME}:${FDDNS_PORT}"; then
#    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#    exit 7
#fi
#printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

#printf "%b %b INFO:  Run sslscan:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
##if ! sslscan "${FDDNS_PROTOCOL}://${FDDNS_HOSTNAME}:${FDDNS_PORT}"; then
#    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#    exit 8
#fi
#printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
