#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Build and Run Script
# ===============================================================================
#
# This script builds the FlyingDdns application and starts it in a tmux session
# for testing on macOS. It performs prerequisite checks, builds the application
# from source, and monitors its startup to ensure it's ready for testing.
#
# The script performs the following operations:
# 1. Sets up the required environment variables by sourcing set_test_env.sh
# 2. Terminates any existing tmux sessions to ensure a clean environment
# 3. Switches to the development branch in git
# 4. Verifies that PostgreSQL is running and accepting connections
# 5. Confirms that PowerDNS is running and accessible
# 6. Checks that the PowerDNS API is accessible
# 7. Builds the FlyingDdns application from source:
#    - Cleans previous build artifacts
#    - Gets dependencies
#    - Compiles the application
#    - Prepares assets
#    - Resets the database
#    - Creates a release
# 8. Starts the application in a tmux session
# 9. Waits for the application to be fully operational
#
# This script is used by other test scripts to prepare the application for testing.
#
# Requirements:
# - PostgreSQL must be installed and running
# - PowerDNS must be installed and running
# - Elixir/Mix must be installed
# - Git repository must be properly configured
# ===============================================================================

script_name="$(basename "${0}")"

printf "%b %b INFO:  Setting the required environment variables:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
# shellcheck source=/dev/null
if ! . test/bin/macos/set_test_env.sh; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Killing possibly running tmux sessions:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if tmux has-session -t "${FLYINGDDNS_TMUX_NAME}" 2>&1; then
    if ! tmux kill-session -t "${FLYINGDDNS_TMUX_NAME}"; then
        printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
        exit 2
    fi
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Make sure we are running the test on the development branch:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! git switch development; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Check if the postgresql service is up and running:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! pg_isready | grep "accepting connections"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Check if the pdns service is up and running:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! host localhost ::1 | grep "REFUSED"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 5
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Check if the pdns api is accessible:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! curl --ipv6 --silent --request GET http://\[::1\]:8081/api/v1/servers/localhost | grep Unauthorized; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 6
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Build FlyingDdns:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! time sh -c "(rm -rf _build deps ${HOME}/.mix mix.lock; mix local.hex --force; mix clean; mix deps.clean --all; mix deps.get --only \"${MIX_ENV}\" && mix compile && mix phx.digest && mix ecto.reset && mix release --overwrite)"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 7
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Start the FlyingDdns server in a tmux session:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! tmux new-session -d -s "${FLYINGDDNS_TMUX_NAME}" "_build/${MIX_ENV}/rel/flying_ddns/bin/flying_ddns start"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 8
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Waiting untill the FlyingDdns is up and running:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
max_curl_tries="30"
i="0"
curl_output="$(curl --ipv6 --silent "${FDDNS_PROTOCOL}://${FDDNS_HOSTNAME}:${FDDNS_PORT}"/status)"
printf "%b %b DEBUG: curl_output=<%b>, i=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${curl_output}" "${i}"
while [ "${curl_output}" != "OK" ]; do
    if [ "${i}" -lt "${max_curl_tries}" ]; then
        printf "%b %b DEBUG: Sleeping 1 seconds ...\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
        sleep 1
    else
        printf "%b %b ERROR: ==>> FAILED: max check retries reached\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
        exit 9
    fi
    i="$((i + 1))"
    curl_output="$(curl --ipv6 --silent "${FDDNS_PROTOCOL}://${FDDNS_HOSTNAME}:${FDDNS_PORT}/status")"
    printf "%b %b DEBUG: curl_output=<%b>, i=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${curl_output}" "${i}"
done
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
