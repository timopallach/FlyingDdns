#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Complete Test Suite Runner
# ===============================================================================
#
# This script orchestrates the complete testing process for the FlyingDdns
# application on macOS. It runs all security, vulnerability, functional, and
# system tests in sequence to ensure comprehensive validation of the application.
#
# The script performs the following operations in order:
# 1. Terminates any existing tmux sessions to ensure a clean environment
# 2. Runs software security and vulnerability scans on the codebase
# 3. Builds the FlyingDdns application and starts it in a tmux session
# 4. Executes the Robot Framework tests against the running application
# 5. Performs system security and vulnerability scans against the running instance
# 6. Terminates the tmux session to clean up resources
#
# This is the main entry point for comprehensive testing of the FlyingDdns
# application and should be used for validation before releases or major changes.
#
# Requirements:
# - All test dependencies must be installed (security tools, Robot Framework, etc.)
# - PostgreSQL and PowerDNS must be configured and running
# ===============================================================================

script_name="$(basename "${0}")"

printf "%b %b INFO:  Terminating left over tmux session:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! test/bin/macos/terminate_tmux_sessions.sh; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

#printf "%b %b INFO:  Stepping into the FlyingDdns repo directory:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#if ! cd "${HOME}"/Development/FlyingDdns; then
#    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#    exit 2
#fi
#printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Running software security and vulnerability scans:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! test/bin/macos/run_software_security_and_vulnarability_tests.sh; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Building and starting Flyingddns:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! test/bin/macos/build_and_run.sh; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Run Robotframework FlyingDdns tests:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! test/bin/macos/run_robotframework_tests.sh -k; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 5
fi
printf "%b %b INFO:  ==>> SUCCEDED\n\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Running system security and vulnerability scans:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! test/bin/macos/run_system_security_and_vulnarability_tests.sh; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 6
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Terminating the respective tmux session:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! test/bin/macos/terminate_tmux_sessions.sh; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 7
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  ==========================================================================\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
printf "%b %b INFO:  = FlyingDdns was build, run and tested. It is still working as EXPECTED. =\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
printf "%b %b INFO:  ==========================================================================\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
