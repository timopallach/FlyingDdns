#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Software Security and Vulnerability Test Runner
# ===============================================================================
#
# This script performs comprehensive software security and vulnerability testing
# on the FlyingDdns codebase. It runs a wide range of static analysis tools,
# linters, and security scanners to identify potential issues in the code.
#
# The script performs the following operations:
# 1. Sets up environment variables for testing
# 2. Code analysis:
#    - cloc: Counts lines of code and provides an overview of the codebase
#    - gitleaks: Scans for sensitive information in the git repository
#    - treefmt: Ensures consistent code formatting
#    - shfmt: Validates shell script formatting
#    - shellcheck: Performs static analysis on shell scripts
# 3. Elixir/Phoenix code quality:
#    - mix format: Checks Elixir code formatting
#    - packer fmt: Validates HCL file formatting
#    - robotidy: Checks Robot Framework file formatting
#    - robocop: Performs static analysis on Robot Framework files
#    - mix test: Runs unit tests with coverage reporting
#    - recode: Lints Elixir code
#    - sobelow: Scans for security vulnerabilities in Phoenix applications
#    - credo: Performs static analysis on Elixir code
#    - dialyzer: Performs type checking on Erlang/Elixir code
# 4. Dependency security:
#    - deps.audit: Checks for security vulnerabilities in dependencies
#    - hex.audit: Audits Hex packages for security issues
# 5. System security:
#    - trivy: Scans for vulnerabilities in the filesystem
#    - osv-scanner: Scans for open source vulnerabilities
#    - trufflehog: Detects secrets in the codebase
#    - semgrep: Performs pattern-based security scanning
# 6. SBOM (Software Bill of Materials) generation:
#    - syft: Creates an SBOM in CycloneDX format
#    - cyclonedx: Validates the SBOM
#    - grype: Scans the SBOM for vulnerabilities
#
# This script is typically run before building the application to ensure
# code quality and security standards are met.
#
# Requirements:
# - All security and code quality tools must be installed
# - Elixir/Mix must be installed
# ===============================================================================

script_name="$(basename "${0}")"

export MIX_ENV="dev"
export SECRET_KEY_BASE="12345678901234567890123456789012345678901234567890123456789012345"
export DATABASE_URL="ecto://postgres@localhost/flying_ddns_${MIX_ENV}"
export PDNS_API_PASSWD="pdns_api_passwd"

# Use cloc to get an overview of the used files in this project
printf "%b %b INFO:  Run cloc:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! cloc --unicode --exclude-dir=.git,_build,deps,log,tmp .; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# Check for leaks in the git repo
printf "%b %b INFO:  Run gitleaks:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
pwd
if ! gitleaks dir --no-banner --verbose --report-path gitleaks.log .; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# treefmt
printf "%b %b INFO:  Run treefmt:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! treefmt; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# Shell script linter
printf "%b %b INFO:  Run shfmt:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! shfmt --diff --posix --indent 4 bin/*.sh test/bin/*/*.sh packer/*.sh packer/bin/*.sh packer/bin/*/*.sh packer/plot/*.sh; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# Shell static code analyzer
printf "%b %b INFO:  Run shellcheck:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! shellcheck --norc --shell=sh bin/*.sh test/bin/*/*.sh packer/*.sh packer/bin/*.sh packer/bin/*/*.sh packer/plot/*.sh; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 5
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Prepare FlyingDdns for security and vulnerability scanning:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! time sh -c "(rm -rf _build deps ${HOME}/.mix && rm -f mix.lock && mix local.hex --force && mix clean && mix deps.clean --all && mix deps.get --only ${MIX_ENV})"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 6
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# mix format check
printf "%b %b INFO:  Run mix format check:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! mix format --check-formatted; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 7
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# HCL linter
printf "%b %b INFO:  Run HCL format check:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! packer fmt -check -recursive packer/hcl; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 8
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

## Gherkin files format check
# TODO: Figure out how to pass a specific gherkin style.
#printf "%b %b INFO:  Gherkin files format check:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#if ! npx @cucumber/gherkin-utils format packer/test/*.feature; then
#    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#    exit 9
#fi
#printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# Robotframework linter
printf "%b %b INFO:  Run robotidy:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! robotidy --check --verbose --target-version rf7 packer/test/; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 10
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# Robotframework static code analyzer
cd packer/test || exit 11
printf "%b %b INFO:  Run robocop:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! robocop --verbose .; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 11
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
cd - || exit 12

# Unit tests
printf "%b %b INFO:  Run mix test:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (MIX_ENV="test" mix deps.get && MIX_ENV="test" mix compile --warnings-as-errors && MIX_ENV="test" mix test --include skip --include pending); then
    printf "%b %b ERROR: ==>> FAILED: Tests failed\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 13
fi

# Run coverage check separately to handle potential issues
printf "%b %b INFO:  Run coverage check:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (MIX_ENV="test" mix coveralls.check --verbose && MIX_ENV="test" mix coveralls >code_coverage.txt); then
    # If coverage check fails but tests passed, we'll continue with a warning
    printf "%b %b WARNING: Coverage check failed but tests passed. Continuing...\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 14
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# Create the code coverate summary file
printf "%b %b INFO:  Create the code coverate summary file:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (./bin/coverage_report.sh >code_coverage.txt); then
    # If coverage check fails but tests passed, we'll continue with a warning
    printf "%b %b WARNING: Coverage check failed but tests passed. Continuing...\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 15
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# Elixir linter
printf "%b %b INFO:  Run recode:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (mix deps.get && mix recode --verbose); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 16
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Prepare FlyingDdns for security and vulnerability scanning:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! time sh -c "(rm -rf _build deps ${HOME}/.mix && rm -f mix.lock && mix local.hex --force && mix clean && mix deps.clean --all && mix deps.get --only ${MIX_ENV} && mix deps.compile && mix compile --warnings-as-errors)"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 17
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# Elixir & Phoenix framwork static code analyzer
printf "%b %b INFO:  Run sobelow:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! mix sobelow --ignore Config.HTTPS --verbose --exit low; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 18
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# Elixir static code analyzer
printf "%b %b INFO:  Run credo:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! mix credo --strict; then
    mix credo explain
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 19
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# Erlang static code analyzer
printf "%b %b INFO:  Run dialyzer:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! mix dialyzer --format dialyxir; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 20
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# Security scanner
printf "%b %b INFO:  Run deps.audit:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! mix deps.audit; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 21
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# Security scanner
printf "%b %b INFO:  Run hex.audit:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! mix hex.audit; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 22
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# Cleanup before running security scanners
printf "%b %b INFO:  Cleanup before running security scanners:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! time sh -c "(rm -rf _build deps ${HOME}/.mix mix.lock; MIX_ENV=prod mix local.hex --force; MIX_ENV=prod mix clean; MIX_ENV=prod mix deps.clean --all)"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 23
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# Security scanner
printf "%b %b INFO:  Run trivy:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! trivy filesystem --output trivy.log --scanners vuln,misconfig,secret,license --license-full .; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 24
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# Security scanner
printf "%b %b INFO:  Run osv-scanner:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! osv-scanner scan --recursive --verbosity verbose --output osv-scanner.log .; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 25
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# Security scanner
printf "%b %b INFO:  Run trufflehog:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! trufflehog --debug filesystem "file://."; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 26
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# Security scanner
printf "%b %b INFO:  Run semgrep:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! semgrep --dryrun --error --config auto .; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 27
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# SBOM handling
printf "%b %b INFO:  Build FlyingDdns for sbom creation:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! time sh -c "(rm -rf _build deps ${HOME}/.mix mix.lock; MIX_ENV=prod mix local.hex --force; MIX_ENV=prod mix clean; MIX_ENV=prod mix deps.clean --all; MIX_ENV=prod mix deps.get --only prod && MIX_ENV=prod mix compile --warnings-as-errors && MIX_ENV=prod mix phx.digest && MIX_ENV=prod mix ecto.reset && MIX_ENV=prod mix release --overwrite)"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 28
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Create SBOM using syft:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! syft scan --quiet --output cyclonedx-xml@1.6 dir:_build/prod/rel/flying_ddns/ >sbom.xml; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 29
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Make the sbom.xml more human readable using xmllint:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! (xmllint --format sbom.xml >sbom.xml.tmp && mv sbom.xml.tmp sbom.xml); then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 30
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Validate the syft SBOM:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! cyclonedx validate --input-version v1_6 --input-file sbom.xml; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 31
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

# Security scanner
printf "%b %b INFO:  Run grype:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! grype sbom:./sbom.xml; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 32
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
