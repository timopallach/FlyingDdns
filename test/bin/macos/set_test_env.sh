#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Test Environment Setup Script
# ===============================================================================
#
# This script sets up the environment variables required for testing the FlyingDdns
# application on macOS. It configures database connections, API credentials, and
# application settings needed for the test suite to run properly.
#
# The script performs the following operations:
# 1. Sets the tmux session name for the FlyingDdns server
# 2. Detects the installed PostgreSQL version from Homebrew
# 3. Updates the PATH to include PostgreSQL binaries
# 4. Sets environment variables for:
#    - Elixir/Phoenix application environment (MIX_ENV)
#    - Secret key for Phoenix
#    - Database connection URL
#    - PowerDNS API password
#    - Domain name for testing
#    - FlyingDdns server protocol, hostname, and port
#    - IP version for testing (IPv6)
#    - Version and commit information (placeholders for testing)
#
# This script is sourced by other test scripts to ensure a consistent
# environment across all test operations.
# ===============================================================================

export FLYINGDDNS_TMUX_NAME="flyingdns-server"
POSTGRESQL_VERSION="$(brew list | grep postgresql | cut -d "@" -f 2)"
export POSTGRESQL_VERSION
PATH="$(brew --prefix)/opt/postgresql@${POSTGRESQL_VERSION}/bin:$PATH"
export PATH
export MIX_ENV="prod"
export SECRET_KEY_BASE="12345678901234567890123456789012345678901234567890123456789012345"
export DATABASE_URL="ecto://postgres@[::1]/flying_ddns_${MIX_ENV}"
export PDNS_API_PASSWD="pdns_api_passwd"
export DOMAIN_NAME="example.org"
export FDDNS_PROTOCOL="http"
export FDDNS_HOSTNAME="[::1]"
export FDDNS_PORT="4000"
export FDDNS_IP_VERSION="ipv6"
export FDDNS_VERSION="0.0.0"
export FDDNS_COMMIT_SHA="0000000000000000000000000000000000000000"
export FDDNS_COMMIT_SHORT_SHA="00000000"
