#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Build and Test Runner
# ===============================================================================
#
# This script provides a streamlined workflow for building and testing the
# FlyingDdns application on macOS. It focuses on functional testing without
# the security and vulnerability scans included in the full test suite.
#
# The script performs the following operations in order:
# 1. Terminates any existing tmux sessions to ensure a clean environment
# 2. Builds the FlyingDdns application and starts it in a tmux session
# 3. Executes the Robot Framework tests against the running application
# 4. Terminates the tmux session to clean up resources
#
# This script is useful for developers who want to quickly validate functional
# changes without running the more time-consuming security and vulnerability scans.
#
# Requirements:
# - PostgreSQL and PowerDNS must be configured and running
# - Robot Framework and its dependencies must be installed
# ===============================================================================

script_name="$(basename "${0}")"

printf "%b %b INFO:  Terminating left over tmux session:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! test/bin/macos/terminate_tmux_sessions.sh; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Building and starting Flyingddns:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! test/bin/macos/build_and_run.sh; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Run Robotframework FlyingDdns tests:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! test/bin/macos/run_robotframework_tests.sh -k; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 5
fi
printf "%b %b INFO:  ==>> SUCCEDED\n\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Terminating the respective tmux session:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! test/bin/macos/terminate_tmux_sessions.sh; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 7
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  ==========================================================================\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
printf "%b %b INFO:  = FlyingDdns was build, run and tested. It is still working as EXPECTED. =\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
printf "%b %b INFO:  ==========================================================================\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
