#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Tmux Session Termination Script
# ===============================================================================
#
# This script terminates any running tmux sessions that were created for the
# FlyingDdns application. It's used to clean up resources before or after tests,
# ensuring that no leftover processes interfere with new test runs.
#
# The script performs the following operations:
# 1. Checks if a tmux session with the name specified in FLYINGDDNS_TMUX_NAME exists
# 2. If the session exists, it terminates the session using tmux kill-session
# 3. Reports success or failure of the operation
#
# This script is typically called at the beginning of test runs to ensure a clean
# environment and at the end to clean up resources.
#
# Requirements:
# - tmux must be installed
# - FLYINGDDNS_TMUX_NAME environment variable must be set (via set_test_env.sh)
# ===============================================================================

script_name="$(basename "${0}")"

printf "%b %b INFO:  Killing possibly running tmux sessions:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if tmux has-session -t "${FLYINGDDNS_TMUX_NAME}" 2>&1; then
    if ! tmux kill-session -t "${FLYINGDDNS_TMUX_NAME}"; then
        printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
        exit 1
    fi
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
