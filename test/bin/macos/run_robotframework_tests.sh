#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Robot Framework Test Runner
# ===============================================================================
#
# This script executes the Robot Framework test suite for the FlyingDdns
# application on macOS. It runs a series of tests to validate the functionality
# of PowerDNS utilities, PowerDNS API, and the FlyingDdns API.
#
# The script performs the following operations:
# 1. Navigates to the FlyingDdns repository directory
# 2. Sets up the required environment variables by sourcing set_test_env.sh
# 3. Navigates to the test directory
# 4. Runs the "Local pdnsutil tests" to validate PowerDNS utility functionality
# 5. Runs the "PDNS API tests" to validate PowerDNS API functionality
# 6. Runs the "FlyingDdns tests" to validate the FlyingDdns API functionality
# 7. Returns to the repository root directory
#
# Each test suite uses the Robot Framework with the Gherkin parser to execute
# feature files that define the expected behavior of the system. Test results
# are stored in log directories for later analysis.
#
# Requirements:
# - Robot Framework and the Gherkin parser must be installed
# - PowerDNS must be configured and running
# - FlyingDdns application must be running (typically started by build_and_run.sh)
# ===============================================================================

script_name="$(basename "${0}")"

printf "%b %b INFO:  Stepping into the FlyingDdns repo directory:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! cd "${HOME}"/Development/FlyingDdns; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Setting the required environment variables:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
# shellcheck source=/dev/null
if ! . test/bin/macos/set_test_env.sh; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 2
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

#printf "%b %b INFO:  Remove a possible left over zone from a failed previous test run:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
##if ! pdnsutil delete-zone "${DOMAIN_NAME}"; then
#    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
#    exit 3
#fi
#printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Stepping into the FlyingDdns test directory:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! cd "${HOME}"/Development/FlyingDdns/packer/test; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Run the \"Local pdnsutil tests\":\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! robot --exitonfailure \
    --exitonerror \
    --variable DEBUG:"${ROBOT_DEBUG}" \
    --variable DOMAIN_NAME:"${DOMAIN_NAME}" \
    --outputdir ../../log/robot_framework_pdnsutil/"${MIX_ENV}" \
    --loglevel TRACE \
    --xunit xunit.xml \
    --parser GherkinParser \
    pdnsutil.feature; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 5
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Run the \"PDNS API tests\":\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! robot --exitonfailure \
    --exitonerror \
    --variable DEBUG:"${ROBOT_DEBUG}" \
    --variable DOMAIN_NAME:"${DOMAIN_NAME}" \
    --variable PDNS_API_PASSWD:"${PDNS_API_PASSWD}" \
    --outputdir ../../log/robot_framework_pdns_api/"${MIX_ENV}" \
    --loglevel TRACE \
    --xunit xunit.xml \
    --parser GherkinParser \
    pdns_api.feature; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 6
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Run the \"FlyingDdns tests (%b://%b:%b %b)\":\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${FDDNS_PROTOCOL}" "${FDDNS_HOSTNAME}" "${FDDNS_PORT}" "${FDDNS_IP_VERSION}"
if ! robot --exitonfailure \
    --exitonerror \
    --variable DEBUG:"${ROBOT_DEBUG}" \
    --variable FLYINGDDNS_VERSION:"0.0.0" \
    --variable DOMAIN_NAME:"${DOMAIN_NAME}" \
    --variable FDDNS_PROTOCOL:"${FDDNS_PROTOCOL}" \
    --variable FDDNS_HOSTNAME:"${FDDNS_HOSTNAME}" \
    --variable FDDNS_PORT:"${FDDNS_PORT}" \
    --variable FDDNS_IP_VERSION:"${FDDNS_IP_VERSION}" \
    --variable FDDNS_VERSION:"${FDDNS_VERSION}" \
    --variable FDDNS_COMMIT_SHA:"${FDDNS_COMMIT_SHA}" \
    --variable FDDNS_COMMIT_SHORT_SHA:"${FDDNS_COMMIT_SHORT_SHA}" \
    --outputdir ../../log/robot_framework_fddns_api/"${MIX_ENV}" \
    --loglevel TRACE \
    --xunit xunit.xml \
    --parser GherkinParser \
    fddns_api.feature; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 7
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Stepping into the FlyingDdns repo directory:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! cd "${HOME}"/Development/FlyingDdns; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 8
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
