#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

script_name="$(basename "${0}")"

printf "%b %b INFO:  Setting the required environment variables:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
# shellcheck source=/dev/null
if ! . test/bin/freebsd/set_test_env.sh; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 1
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Killing possibly running tmux sessions:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if tmux has-session -t "${FLYINGDDNS_TMUX_NAME}" 2>&1; then
    if ! tmux kill-session -t "${FLYINGDDNS_TMUX_NAME}"; then
        printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
        exit 2
    fi
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Make sure we are running the test on the development branch:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! git switch development; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 3
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Check if the postgresql service is up and running:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! pg_isready | grep "accepting connections"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 4
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Check if the pdns service is up and running:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! host localhost ::1 | grep "REFUSED"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 5
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Check if the pdns api is accessible:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! curl --ipv6 --silent --request GET http://\[::1\]:8081/api/v1/servers/localhost | grep Unauthorized; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 6
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Build FlyingDdns:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! time sh -c "(rm -rf _build deps ${HOME}/.mix mix.lock; mix local.hex --force; mix clean; mix deps.clean --all; mix deps.get --only \"${MIX_ENV}\" && mix compile && mix phx.digest && mix ecto.reset && mix release --overwrite)"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 7
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Start the FlyingDdns server in a tmux session:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if ! tmux new-session -d -s "${FLYINGDDNS_TMUX_NAME}" "_build/${MIX_ENV}/rel/flying_ddns/bin/flying_ddns start"; then
    printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
    exit 8
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

printf "%b %b INFO:  Waiting untill the FlyingDdns is up and running:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
max_curl_tries="30"
i="0"
curl_output="$(curl --ipv6 --silent "${FDDNS_PROTOCOL}://${FDDNS_HOSTNAME}:${FDDNS_PORT}"/status)"
printf "%b %b DEBUG: curl_output=<%b>, i=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${curl_output}" "${i}"
while [ "${curl_output}" != "OK" ]; do
    if [ "${i}" -lt "${max_curl_tries}" ]; then
        printf "%b %b DEBUG: Sleeping 1 seconds ...\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
        sleep 1
    else
        printf "%b %b ERROR: ==>> FAILED: max check retries reached\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
        exit 9
    fi
    i="$((i + 1))"
    curl_output="$(curl --ipv6 --silent "${FDDNS_PROTOCOL}://${FDDNS_HOSTNAME}:${FDDNS_PORT}/status")"
    printf "%b %b DEBUG: curl_output=<%b>, i=<%b>\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}" "${curl_output}" "${i}"
done
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
