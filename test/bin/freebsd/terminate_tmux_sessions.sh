#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

script_name="$(basename "${0}")"

printf "%b %b INFO:  Killing possibly running tmux sessions:\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
if tmux has-session -t "${FLYINGDDNS_TMUX_NAME}" 2>&1; then
    if ! tmux kill-session -t "${FLYINGDDNS_TMUX_NAME}"; then
        printf "%b %b ERROR: ==>> FAILED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"
        exit 1
    fi
fi
printf "%b %b INFO:  ==>> SUCCEDED\n" "$(date "+%Y-%m-%d %H:%M:%S")" "${script_name}"

exit 0
