defmodule Mix.Tasks.Coveralls.CheckTest do
  @moduledoc """
  Tests for the Coveralls.Check mix task.

  This module tests the functionality of the Mix.Tasks.Coveralls.Check module,
  which checks if the code coverage is above a minimum threshold.
  """

  use ExUnit.Case, async: false
  import ExUnit.CaptureIO
  import Mock

  alias Mix.Tasks.Coveralls.Check

  # Use a temporary file for testing instead of the real coveralls.json
  @temp_config_file "coveralls_test.json"

  # Helper to create a temporary coveralls.json file with specified minimum coverage
  defp with_coveralls_config(min_coverage, fun) do
    # Create a temporary config file for testing
    config = %{
      "minimum_coverage" => min_coverage
    }

    File.write!(@temp_config_file, Jason.encode!(config))

    # Mock File.read to return our test config when coveralls.json is requested
    with_mock File, [:passthrough],
      read: fn
        "coveralls.json" -> File.read(@temp_config_file)
        path -> :meck.passthrough([path])
      end do
      try do
        fun.()
      after
        # Clean up the temporary file
        File.rm(@temp_config_file)
      end
    end
  end

  # Helper to mock System.cmd for testing
  defp with_mock_cmd(output, exit_code, fun) do
    with_mock System, [:passthrough],
      cmd: fn "mix", ["coveralls" | _], _opts -> {output, exit_code} end do
      fun.()
    end
  end

  # Helper to filter out coverage report lines
  defp filter_coverage_output(str) do
    if is_binary(str) and String.contains?(str, "[TOTAL]") do
      :ok
    else
      :meck.passthrough([str])
    end
  end

  # Helper to run the check and capture only the important output
  defp run_check_and_capture(mock_output, exit_code, expected_exit) do
    output =
      capture_io(fn ->
        with_mock IO, [:passthrough], puts: &filter_coverage_output/1 do
          with_mock_cmd(mock_output, exit_code, fn ->
            assert catch_exit(Check.run([])) == expected_exit
          end)
        end
      end)

    output
  end

  describe "run/1" do
    test "passes when coverage exceeds minimum threshold (80%)" do
      with_coveralls_config(80, fn ->
        output =
          run_check_and_capture(
            "[TOTAL] 85.0%\n----------------\n[TOTAL] 85.0%",
            0,
            {:shutdown, 0}
          )

        assert output =~ "Coverage: 85.0% (minimum: 80%)"
        assert output =~ "Coverage threshold check passed!"
        refute output =~ "[TOTAL] 85.0%"
      end)
    end

    test "passes when coverage equals minimum threshold (85%)" do
      with_coveralls_config(85, fn ->
        output =
          run_check_and_capture(
            "[TOTAL] 85.0%\n----------------\n[TOTAL] 85.0%",
            0,
            {:shutdown, 0}
          )

        assert output =~ "Coverage: 85.0% (minimum: 85%)"
        assert output =~ "Coverage threshold check passed!"
        refute output =~ "[TOTAL] 85.0%"
      end)
    end

    test "fails when coverage is below minimum threshold (75%)" do
      with_coveralls_config(80, fn ->
        output =
          run_check_and_capture(
            "[TOTAL] 75.0%\n----------------\n[TOTAL] 75.0%",
            0,
            {:shutdown, 1}
          )

        assert output =~ "Coverage: 75.0% (minimum: 80%)"
        assert output =~ "Coverage threshold check failed!"
        refute output =~ "[TOTAL] 75.0%"
      end)
    end

    test "exits with coveralls exit code when coveralls command fails" do
      with_coveralls_config(80, fn ->
        # For error cases, we want to see the output
        output =
          capture_io(fn ->
            with_mock_cmd("Error running coveralls", 2, fn ->
              assert catch_exit(Check.run(["--verbose"])) == {:shutdown, 2}
            end)
          end)

        assert output =~ "Error running coveralls"
        assert output =~ "Warning: Could not determine coverage percentage"
      end)
    end

    test "warns when coverage percentage cannot be determined" do
      with_coveralls_config(80, fn ->
        # For error cases, we want to see the output
        output =
          capture_io(fn ->
            with_mock_cmd("No coverage data", 0, fn ->
              assert catch_exit(Check.run(["--verbose"])) == {:shutdown, 0}
            end)
          end)

        assert output =~ "No coverage data"
        assert output =~ "Warning: Could not determine coverage percentage"
      end)
    end

    test "uses default minimum coverage when not specified in config" do
      # Create empty config without minimum_coverage
      File.write!(@temp_config_file, Jason.encode!(%{}))

      with_mock File, [:passthrough],
        read: fn
          "coveralls.json" -> File.read(@temp_config_file)
          path -> :meck.passthrough([path])
        end do
        try do
          output =
            run_check_and_capture(
              "[TOTAL] 75.0%\n----------------\n[TOTAL] 75.0%",
              0,
              {:shutdown, 1}
            )

          assert output =~ "Coverage: 75.0% (minimum: 80%)"
          assert output =~ "Coverage threshold check failed!"
          refute output =~ "[TOTAL] 75.0%"
        after
          File.rm(@temp_config_file)
        end
      end
    end

    test "passes arguments to coveralls command" do
      with_coveralls_config(80, fn ->
        with_mock System, [:passthrough],
          cmd: fn "mix", args, _opts ->
            assert "--umbrella" in args
            {"[TOTAL] 85.0%\n----------------\n[TOTAL] 85.0%", 0}
          end do
          with_mock IO, [:passthrough], puts: &filter_coverage_output/1 do
            assert catch_exit(Check.run(["--umbrella"])) == {:shutdown, 0}
          end
        end
      end)
    end

    test "handles decimal coverage values correctly" do
      with_coveralls_config(80.5, fn ->
        output =
          run_check_and_capture(
            "[TOTAL] 80.4%\n----------------\n[TOTAL] 80.4%",
            0,
            {:shutdown, 1}
          )

        assert output =~ "Coverage: 80.4% (minimum: 80.5%)"
        assert output =~ "Coverage threshold check failed!"
        refute output =~ "[TOTAL] 80.4%"
      end)

      with_coveralls_config(80.5, fn ->
        output =
          run_check_and_capture(
            "[TOTAL] 80.5%\n----------------\n[TOTAL] 80.5%",
            0,
            {:shutdown, 0}
          )

        assert output =~ "Coverage: 80.5% (minimum: 80.5%)"
        assert output =~ "Coverage threshold check passed!"
        refute output =~ "[TOTAL] 80.5%"
      end)
    end

    test "handles coverage with many decimal places" do
      with_coveralls_config(80, fn ->
        output =
          run_check_and_capture(
            "[TOTAL] 80.12345%\n----------------\n[TOTAL] 80.12345%",
            0,
            {:shutdown, 0}
          )

        assert output =~ "Coverage: 80.12345% (minimum: 80%)"
        assert output =~ "Coverage threshold check passed!"
        refute output =~ "[TOTAL] 80.12345%"
      end)
    end

    test "handles edge case with 100% coverage" do
      with_coveralls_config(80, fn ->
        output =
          run_check_and_capture(
            "[TOTAL] 100.0%\n----------------\n[TOTAL] 100.0%",
            0,
            {:shutdown, 0}
          )

        assert output =~ "Coverage: 100.0% (minimum: 80%)"
        assert output =~ "Coverage threshold check passed!"
        refute output =~ "[TOTAL] 100.0%"
      end)
    end

    test "handles edge case with 0% coverage" do
      with_coveralls_config(80, fn ->
        output =
          run_check_and_capture(
            "[TOTAL] 0.0%\n----------------\n[TOTAL] 0.0%",
            0,
            {:shutdown, 1}
          )

        assert output =~ "Coverage: 0.0% (minimum: 80%)"
        assert output =~ "Coverage threshold check failed!"
        refute output =~ "[TOTAL] 0.0%"
      end)
    end

    test "handles missing coveralls.json file" do
      # Mock File.read to simulate a missing coveralls.json file
      with_mock File, [:passthrough],
        read: fn
          "coveralls.json" -> {:error, :enoent}
          path -> :meck.passthrough([path])
        end do
        output =
          run_check_and_capture(
            "[TOTAL] 75.0%\n----------------\n[TOTAL] 75.0%",
            0,
            {:shutdown, 1}
          )

        assert output =~ "Warning: coveralls.json file not found"
        assert output =~ "Coverage: 75.0% (minimum: 80%)"
        assert output =~ "Coverage threshold check failed!"
        refute output =~ "[TOTAL] 75.0%"
      end
    end

    test "handles invalid JSON in coveralls.json" do
      # Create a file with invalid JSON
      File.write!(@temp_config_file, "invalid json")

      with_mock File, [:passthrough],
        read: fn
          "coveralls.json" -> File.read(@temp_config_file)
          path -> :meck.passthrough([path])
        end do
        try do
          output =
            run_check_and_capture(
              "[TOTAL] 85.0%\n----------------\n[TOTAL] 85.0%",
              0,
              {:shutdown, 0}
            )

          assert output =~ "Coverage: 85.0% (minimum: 80%)"
          assert output =~ "Coverage threshold check passed!"
          refute output =~ "[TOTAL] 85.0%"
        after
          File.rm(@temp_config_file)
        end
      end
    end

    test "handles empty coveralls.json file" do
      # Create an empty file
      File.write!(@temp_config_file, "")

      with_mock File, [:passthrough],
        read: fn
          "coveralls.json" -> File.read(@temp_config_file)
          path -> :meck.passthrough([path])
        end do
        try do
          output =
            run_check_and_capture(
              "[TOTAL] 85.0%\n----------------\n[TOTAL] 85.0%",
              0,
              {:shutdown, 0}
            )

          assert output =~ "Coverage: 85.0% (minimum: 80%)"
          assert output =~ "Coverage threshold check passed!"
          refute output =~ "[TOTAL] 85.0%"
        after
          File.rm(@temp_config_file)
        end
      end
    end

    test "handles unusual coverage output format" do
      with_coveralls_config(80, fn ->
        # For error cases, we want to see the output
        output =
          capture_io(fn ->
            with_mock_cmd("Coverage: 85.0%", 0, fn ->
              assert catch_exit(Check.run(["--verbose"])) == {:shutdown, 0}
            end)
          end)

        assert output =~ "Coverage: 85.0%"
        assert output =~ "Coverage: 85.0% (minimum: 80%)"
        assert output =~ "Coverage threshold check passed!"
      end)
    end

    test "handles very high minimum threshold" do
      with_coveralls_config(99.9, fn ->
        output =
          run_check_and_capture(
            "[TOTAL] 99.8%\n----------------\n[TOTAL] 99.8%",
            0,
            {:shutdown, 1}
          )

        assert output =~ "Coverage: 99.8% (minimum: 99.9%)"
        assert output =~ "Coverage threshold check failed!"
        refute output =~ "[TOTAL] 99.8%"
      end)
    end

    test "handles very low minimum threshold" do
      with_coveralls_config(0.1, fn ->
        output =
          run_check_and_capture(
            "[TOTAL] 0.2%\n----------------\n[TOTAL] 0.2%",
            0,
            {:shutdown, 0}
          )

        assert output =~ "Coverage: 0.2% (minimum: 0.1%)"
        assert output =~ "Coverage threshold check passed!"
        refute output =~ "[TOTAL] 0.2%"
      end)
    end

    test "handles negative minimum threshold" do
      with_coveralls_config(-10, fn ->
        output =
          run_check_and_capture(
            "[TOTAL] 0.0%\n----------------\n[TOTAL] 0.0%",
            0,
            {:shutdown, 0}
          )

        assert output =~ "Coverage: 0.0% (minimum: -10%)"
        assert output =~ "Coverage threshold check passed!"
        refute output =~ "[TOTAL] 0.0%"
      end)
    end

    test "handles minimum threshold above 100%" do
      with_coveralls_config(110, fn ->
        output =
          run_check_and_capture(
            "[TOTAL] 100.0%\n----------------\n[TOTAL] 100.0%",
            0,
            {:shutdown, 1}
          )

        assert output =~ "Coverage: 100.0% (minimum: 110%)"
        assert output =~ "Coverage threshold check failed!"
        refute output =~ "[TOTAL] 100.0%"
      end)
    end

    test "uses the last occurrence of coverage percentage when multiple are present" do
      with_coveralls_config(90, fn ->
        output =
          run_check_and_capture(
            "[TOTAL] 85.0%\n----------------\n[TOTAL] 97.6%",
            0,
            {:shutdown, 0}
          )

        assert output =~ "Coverage: 97.6% (minimum: 90%)"
        assert output =~ "Coverage threshold check passed!"
        refute output =~ "[TOTAL] 85.0%"
        refute output =~ "[TOTAL] 97.6%"
      end)
    end

    test "fails when the last occurrence is below threshold even if earlier ones pass" do
      with_coveralls_config(90, fn ->
        output =
          run_check_and_capture(
            "[TOTAL] 95.0%\n----------------\n[TOTAL] 85.0%",
            0,
            {:shutdown, 1}
          )

        assert output =~ "Coverage: 85.0% (minimum: 90%)"
        assert output =~ "Coverage threshold check failed!"
        refute output =~ "[TOTAL] 95.0%"
        refute output =~ "[TOTAL] 85.0%"
      end)
    end
  end
end
