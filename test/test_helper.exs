# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

ExUnit.start(timeout: 10_000)
Ecto.Adapters.SQL.Sandbox.mode(FlyingDdns.Repo, :manual)

ExUnit.configure(exclude: [:pending])

defmodule TestHelpers do
  @moduledoc """
  Helper functions and macros for test cases.

  This module provides utility functions to improve test organization and readability,
  including:
  - Support for marking tests as pending with custom messages
  - Helpers for skipping tests that are not yet implemented
  - Utilities for test organization and documentation
  """
  defmacro pending(message \\ "") do
    quote do
      @tag :pending
      test "PENDING: #{unquote(message)}" do
        IO.puts("Pending: #{unquote(message)}")
        assert true
      end
    end
  end
end
