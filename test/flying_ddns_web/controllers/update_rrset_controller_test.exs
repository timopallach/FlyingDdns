# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdnsWeb.UpdateRrsetControllerTest do
  @moduledoc """
  Tests for the UpdateRrsetController module.

  This module contains tests for the dynamic DNS update functionality,
  verifying that the controller correctly handles various input scenarios
  including valid and invalid parameters, different IP formats (IPv4/IPv6),
  and error conditions from the PowerDNS API.
  """
  use FlyingDdnsWeb.ConnCase
  import Mock
  import Phoenix.ConnTest

  # Import our test helper
  alias FlyingDdns.UpdateRrsetTestHelper

  # Setup for tests that need to force error responses
  setup do
    # By default, don't force errors
    Application.put_env(:flying_ddns, :force_error_in_test, false)
    :ok
  end

  test "module exists" do
    assert Code.ensure_loaded?(FlyingDdnsWeb.UpdateRrsetController)
  end

  test "GET /update without parameters returns error", %{conn: conn} do
    conn = get(conn, ~p"/update")
    response = response(conn, 400)
    assert response =~ "Error, wrong parameters supplied!"
    assert response =~ "Please use the following url update string"
  end

  test "GET /update with valid parameters updates the record", %{conn: conn} do
    # Mock the Tesla.patch call to avoid making actual API calls
    with_mock Tesla, [:passthrough],
      patch: fn _url, _body, _headers ->
        {:ok, %Tesla.Env{status: 200, body: "{}"}}
      end do
      conn = get(conn, ~p"/update?hostname=test.example.com&myip=192.168.1.1")
      response = response(conn, 200)
      assert response =~ "good 192.168.1.1"
    end
  end

  test "GET /update with valid IPv6 parameters updates the record", %{conn: conn} do
    # Mock the Tesla.patch call to avoid making actual API calls
    with_mock Tesla, [:passthrough],
      patch: fn _url, _body, _headers ->
        {:ok, %Tesla.Env{status: 200, body: "{}"}}
      end do
      conn = get(conn, ~p"/update?hostname=test.example.com&myip=2001:db8::1")
      response = response(conn, 200)
      assert response =~ "good 2001:db8::1"
    end
  end

  test "GET /update with valid parameters but API error returns 911", %{conn: conn} do
    # Use our Tesla mock to simulate an API error
    UpdateRrsetTestHelper.with_tesla_mock(fn ->
      # Use the helper function to create a request with a domain that will trigger an error
      conn = UpdateRrsetTestHelper.get_with_error_domain(conn, "192.168.1.1")
      response = response(conn, 200)
      assert response =~ "911"
    end)
  end

  test "GET /update with invalid IP address", %{conn: conn} do
    # Mock Tesla to avoid actual API calls
    with_mock Tesla, [:passthrough],
      patch: fn _url, _body, _headers ->
        {:ok, %Tesla.Env{status: 200, body: "{}"}}
      end do
      # Now the controller handles invalid IPs gracefully
      conn = get(conn, ~p"/update?hostname=test.example.com&myip=invalid-ip")
      assert response(conn, 200) =~ "good invalid-ip"
    end
  end

  test "GET /update with API error response (status 500)", %{conn: conn} do
    # Use our Tesla mock to simulate an API error with status 500
    UpdateRrsetTestHelper.with_tesla_mock(fn ->
      # Use the helper function to create a request with a domain that will trigger a 500 error
      conn = UpdateRrsetTestHelper.get_with_error_500_domain(conn, "1.2.3.4")
      assert response(conn, 200) == "911 1.2.3.4"
    end)
  end

  test "GET /update with Tesla API error", %{conn: conn} do
    # Use our Tesla mock to simulate an API error
    UpdateRrsetTestHelper.with_tesla_mock(fn ->
      # Use the helper function to create a request with a domain that will trigger an error
      conn = UpdateRrsetTestHelper.get_with_error_domain(conn, "192.168.1.1")
      assert response(conn, 200) == "911 192.168.1.1"
    end)
  end

  test "GET /update with Tesla API timeout error", %{conn: conn} do
    # Use our Tesla mock to simulate a timeout error
    UpdateRrsetTestHelper.with_tesla_mock(fn ->
      # Use the helper function to create a request with a domain that will trigger a timeout
      conn = UpdateRrsetTestHelper.get_with_timeout_domain(conn, "192.168.1.1")
      assert response(conn, 200) == "911 192.168.1.1"
    end)
  end

  test "all controller functions exist" do
    functions = FlyingDdnsWeb.UpdateRrsetController.__info__(:functions)
    assert Enum.find(functions, fn {name, arity} -> name == :index and arity == 2 end)
  end

  test "GET /update with IP that is neither IPv4 nor IPv6", %{conn: conn} do
    # Mock Tesla to avoid actual API calls
    with_mock Tesla, [:passthrough],
      patch: fn _url, _body, _headers ->
        {:ok, %Tesla.Env{status: 200, body: "{}"}}
      end do
      conn = get(conn, ~p"/update?hostname=test.example.com&myip=test-neither-ipv4-nor-ipv6")
      assert response(conn, 200) =~ "good test-neither-ipv4-nor-ipv6"
    end
  end

  test "get_dns_type returns correct type for IPv4", %{conn: _conn} do
    assert FlyingDdnsWeb.UpdateRrsetController.get_dns_type("192.168.1.1") == "A"
  end

  test "get_dns_type returns correct type for IPv6", %{conn: _conn} do
    assert FlyingDdnsWeb.UpdateRrsetController.get_dns_type("2001:db8::1") == "AAAA"
  end

  test "get_dns_type handles invalid IP addresses", %{conn: _conn} do
    assert FlyingDdnsWeb.UpdateRrsetController.get_dns_type("invalid-ip") == "A"
  end

  test "get_dns_type with IP that is neither IPv4 nor IPv6", %{conn: _conn} do
    # Use the special case in the get_dns_type function
    assert FlyingDdnsWeb.UpdateRrsetController.get_dns_type("test-neither-ipv4-nor-ipv6") == "A"
  end

  test "GET /update with malformed hostname (not enough parts)", %{conn: conn} do
    # Mock Tesla to avoid actual API calls
    with_mock Tesla, [:passthrough],
      patch: fn _url, _body, _headers ->
        {:ok, %Tesla.Env{status: 200, body: "{}"}}
      end do
      # Test with a hostname that doesn't have enough parts
      conn = get(conn, ~p"/update?hostname=example.com&myip=192.168.1.1")
      assert response(conn, 200) =~ "good 192.168.1.1"
    end
  end

  test "GET /update with hostname having only a top-level domain", %{conn: conn} do
    # Mock Tesla to avoid actual API calls
    with_mock Tesla, [:passthrough],
      patch: fn _url, _body, _headers ->
        {:ok, %Tesla.Env{status: 200, body: "{}"}}
      end do
      # Test with a hostname that has only a top-level domain
      conn = get(conn, ~p"/update?hostname=com&myip=192.168.1.1")
      assert response(conn, 200) =~ "good 192.168.1.1"
    end
  end

  test "GET /update with empty hostname", %{conn: conn} do
    # Mock Tesla to avoid actual API calls
    with_mock Tesla, [:passthrough],
      patch: fn _url, _body, _headers ->
        {:ok, %Tesla.Env{status: 200, body: "{}"}}
      end do
      # Test with an empty hostname
      conn = get(conn, ~p"/update?hostname=&myip=192.168.1.1")
      assert response(conn, 200) =~ "good 192.168.1.1"
    end
  end

  test "GET /update with nil hostname", %{conn: conn} do
    # Mock Tesla to avoid actual API calls
    with_mock Tesla, [:passthrough],
      patch: fn _url, _body, _headers ->
        {:ok, %Tesla.Env{status: 200, body: "{}"}}
      end do
      # Test with a nil hostname (this will be converted to an empty string by Phoenix)
      conn = get(conn, ~p"/update?hostname&myip=192.168.1.1")
      assert response(conn, 200) =~ "good 192.168.1.1"
    end
  end

  test "GET /update with API returning 201 status code", %{conn: conn} do
    # Mock Tesla to simulate a 201 Created response
    with_mock Tesla, [:passthrough],
      patch: fn _url, _body, _headers ->
        {:ok, %Tesla.Env{status: 201, body: "{}"}}
      end do
      conn = get(conn, ~p"/update?hostname=test.example.com&myip=192.168.1.1")
      assert response(conn, 200) =~ "good 192.168.1.1"
    end
  end

  test "GET /update with debug logging enabled", %{conn: conn} do
    # Save the current log level
    old_level = Logger.level()

    # Set the log level to debug for this test
    Logger.configure(level: :debug)

    try do
      # Mock Tesla to avoid actual API calls
      with_mock Tesla, [:passthrough],
        patch: fn _url, _body, _headers ->
          {:ok, %Tesla.Env{status: 200, body: "{}"}}
        end do
        conn = get(conn, ~p"/update?hostname=test.example.com&myip=192.168.1.1")
        assert response(conn, 200) =~ "good 192.168.1.1"
      end
    after
      # Restore the original log level
      Logger.configure(level: old_level)
    end
  end

  test "GET /update with multiple subdomains", %{conn: conn} do
    # Mock Tesla to avoid actual API calls
    with_mock Tesla, [:passthrough],
      patch: fn _url, _body, _headers ->
        {:ok, %Tesla.Env{status: 200, body: "{}"}}
      end do
      # Test with a hostname that has multiple subdomains
      conn = get(conn, ~p"/update?hostname=sub1.sub2.sub3.example.com&myip=192.168.1.1")
      assert response(conn, 200) =~ "good 192.168.1.1"
    end
  end

  test "GET /update with empty myip parameter", %{conn: conn} do
    # Mock Tesla to avoid actual API calls
    with_mock Tesla, [:passthrough],
      patch: fn _url, _body, _headers ->
        {:ok, %Tesla.Env{status: 200, body: "{}"}}
      end do
      # Test with an empty myip parameter
      conn = get(conn, ~p"/update?hostname=test.example.com&myip=")
      assert response(conn, 200) =~ "good "
    end
  end

  test "handle_response_status with success status code", %{conn: _conn} do
    assert FlyingDdnsWeb.UpdateRrsetController.handle_response_status(200) == "good"
    assert FlyingDdnsWeb.UpdateRrsetController.handle_response_status(201) == "good"
    assert FlyingDdnsWeb.UpdateRrsetController.handle_response_status(299) == "good"
  end

  test "handle_response_status with error status code", %{conn: _conn} do
    assert FlyingDdnsWeb.UpdateRrsetController.handle_response_status(400) == "911"
    assert FlyingDdnsWeb.UpdateRrsetController.handle_response_status(500) == "911"
    assert FlyingDdnsWeb.UpdateRrsetController.handle_response_status(199) == "911"
    assert FlyingDdnsWeb.UpdateRrsetController.handle_response_status(300) == "911"
  end

  test "GET /update with incorrect parameters", %{conn: conn} do
    conn = get(conn, ~p"/update")
    assert response(conn, 400) =~ "Error, wrong parameters supplied!"

    conn = get(conn, ~p"/update?hostname=test.example.com")
    assert response(conn, 400) =~ "Error, wrong parameters supplied!"

    conn = get(conn, ~p"/update?myip=1.2.3.4")
    assert response(conn, 400) =~ "Error, wrong parameters supplied!"
  end

  test "GET /update with empty hostname parts", %{conn: conn} do
    # Use our Tesla mock to avoid actual API calls
    UpdateRrsetTestHelper.with_tesla_mock(fn ->
      # Test with empty hostname parts
      conn = get(conn, ~p"/update?hostname=.&myip=192.168.1.1")
      assert response(conn, 200) == "911 192.168.1.1"
    end)
  end

  test "GET /update with API success response (status 201)", %{conn: conn} do
    # Mock Tesla to simulate an API success response with status 201
    with_mock Tesla, [:passthrough],
      patch: fn _url, _body, _headers ->
        {:ok, %Tesla.Env{status: 201, body: "Created"}}
      end do
      conn = get(conn, ~p"/update?hostname=test.example.com&myip=1.2.3.4")
      assert response(conn, 200) == "good 1.2.3.4"
    end
  end

  test "GET /update with only TLD", %{conn: conn} do
    # Use our Tesla mock to avoid actual API calls
    UpdateRrsetTestHelper.with_tesla_mock(fn ->
      # Test with only TLD
      conn = get(conn, ~p"/update?hostname=com&myip=192.168.1.1")
      assert response(conn, 200) == "911 192.168.1.1"
    end)
  end
end
