# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdnsWeb.ErrorHTMLTest do
  @moduledoc """
  Tests for the ErrorHTML view module.

  This module verifies that error templates render the expected HTML content
  for different HTTP error codes, ensuring that appropriate error messages
  are displayed to users when errors occur.
  """
  use FlyingDdnsWeb.ConnCase, async: true

  # Bring render_to_string/4 for testing custom views
  import Phoenix.Template

  test "renders 404.html" do
    assert render_to_string(FlyingDdnsWeb.ErrorHTML, "404", "html", []) == "Not Found"
  end

  test "renders 500.html" do
    assert render_to_string(FlyingDdnsWeb.ErrorHTML, "500", "html", []) == "Internal Server Error"
  end
end
