# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdnsWeb.ShowProjectInformationControllerTest do
  @moduledoc """
  Tests for the ShowProjectInformationController module.

  This module contains tests for the project information endpoints,
  verifying that the controller correctly returns application status,
  version information, and Git commit details. It ensures that all
  informational endpoints respond with the expected data.
  """
  use FlyingDdnsWeb.ConnCase

  test "module exists" do
    assert Code.ensure_loaded?(FlyingDdnsWeb.ShowProjectInformationController)
  end

  test "GET / renders the welcome page", %{conn: conn} do
    conn = get(conn, ~p"/")
    assert response(conn, 200) =~ "Welcome to FlyingDdns"
  end

  test "GET /status returns OK status", %{conn: conn} do
    conn = get(conn, ~p"/status")
    assert response(conn, 200) =~ "OK"
  end

  test "GET /version returns the application version", %{conn: conn} do
    conn = get(conn, ~p"/version")
    version = Application.spec(:flying_ddns, :vsn)
    assert response(conn, 200) =~ "#{version}"
  end

  test "GET /commit_sha returns the commit SHA", %{conn: conn} do
    conn = get(conn, ~p"/commit_sha")
    commit_sha = Application.get_env(:flying_ddns, :commit_sha)
    assert response(conn, 200) =~ "#{commit_sha}"
  end

  test "GET /commit_short_sha returns the short commit SHA", %{conn: conn} do
    conn = get(conn, ~p"/commit_short_sha")
    commit_short_sha = Application.get_env(:flying_ddns, :commit_short_sha)
    assert response(conn, 200) =~ "#{commit_short_sha}"
  end

  test "all controller functions exist" do
    functions = FlyingDdnsWeb.ShowProjectInformationController.__info__(:functions)
    assert Enum.find(functions, fn {name, arity} -> name == :index and arity == 2 end)
    assert Enum.find(functions, fn {name, arity} -> name == :status and arity == 2 end)
    assert Enum.find(functions, fn {name, arity} -> name == :version and arity == 2 end)
    assert Enum.find(functions, fn {name, arity} -> name == :commit_sha and arity == 2 end)
    assert Enum.find(functions, fn {name, arity} -> name == :commit_short_sha and arity == 2 end)
  end
end
