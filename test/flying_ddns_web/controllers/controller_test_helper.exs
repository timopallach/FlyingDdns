defmodule FlyingDdnsWeb.ControllerTestHelper do
  @moduledoc """
  Helper functions for controller tests.

  This module provides utility functions to simplify controller testing, including:
  - Checking if modules exist before testing them
  - Verifying that routes exist in the router
  - Handling test skipping when prerequisites are not met
  """
  import ExUnit.Assertions

  def check_module_exists(module_name) do
    if Code.ensure_loaded?(module_name) do
      true
    else
      IO.puts("Module #{inspect(module_name)} does not exist, skipping tests")
      ExUnit.Callbacks.skip("Module does not exist")
      false
    end
  end

  def check_route_exists(router, method, path) do
    routes = router.__routes__()

    route =
      Enum.find(routes, fn route ->
        route.verb == method && String.contains?(route.path, path)
      end)

    if route do
      true
    else
      IO.puts("Route #{method} #{path} does not exist, skipping tests")
      ExUnit.Callbacks.skip("Route does not exist")
      false
    end
  end
end
