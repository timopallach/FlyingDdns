# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdnsWeb.Auth.UserSessionControllerTest do
  @moduledoc """
  Tests for the UserSessionController.

  This module verifies the functionality of the user session management, including:
  - User login with various parameters (remember me, return to)
  - User logout process
  - Handling of invalid credentials
  - Session persistence and redirection behavior
  """
  use FlyingDdnsWeb.ConnCase, async: true

  @moduletag timeout: 10_000

  import FlyingDdns.AccountsFixtures

  setup do
    %{user: user_fixture()}
  end

  test "POST /auth/users/log_in logs the user in", %{conn: conn, user: user} do
    conn =
      post(conn, ~p"/auth/users/log_in", %{
        "user" => %{"email" => user.email, "password" => valid_user_password()}
      })

    assert redirected_to(conn) == ~p"/"
    assert Phoenix.Flash.get(conn.assigns.flash, :info) == "Welcome back!"
  end

  test "POST /auth/users/log_in logs the user in with remember me", %{conn: conn, user: user} do
    conn =
      post(conn, ~p"/auth/users/log_in", %{
        "user" => %{
          "email" => user.email,
          "password" => valid_user_password(),
          "remember_me" => "true"
        }
      })

    assert redirected_to(conn) == ~p"/"
    assert conn.resp_cookies["_flying_ddns_web_user_remember_me"]
    assert Phoenix.Flash.get(conn.assigns.flash, :info) == "Welcome back!"
  end

  test "POST /auth/users/log_in logs the user in with return to", %{conn: conn, user: user} do
    conn =
      conn
      |> init_test_session(user_return_to: "/foo/bar")
      |> post(~p"/auth/users/log_in", %{
        "user" => %{
          "email" => user.email,
          "password" => valid_user_password()
        }
      })

    assert redirected_to(conn) == "/foo/bar"
    assert Phoenix.Flash.get(conn.assigns.flash, :info) == "Welcome back!"
  end

  test "POST /auth/users/log_in login following registration", %{conn: conn, user: user} do
    conn =
      post(conn, ~p"/auth/users/log_in", %{
        "_action" => "registered",
        "user" => %{"email" => user.email, "password" => valid_user_password()}
      })

    assert redirected_to(conn) == ~p"/"
    assert Phoenix.Flash.get(conn.assigns.flash, :info) == "Welcome back!"
  end

  test "POST /auth/users/log_in login following password update", %{conn: conn, user: user} do
    conn =
      post(conn, ~p"/auth/users/log_in", %{
        "_action" => "password_updated",
        "user" => %{"email" => user.email, "password" => valid_user_password()}
      })

    assert redirected_to(conn) == ~p"/"
    assert Phoenix.Flash.get(conn.assigns.flash, :info) == "Welcome back!"
  end

  test "POST /auth/users/log_in redirects to login page with invalid credentials", %{conn: conn} do
    conn =
      post(conn, ~p"/auth/users/log_in", %{
        "user" => %{"email" => "invalid@email.com", "password" => "invalid_password"}
      })

    assert redirected_to(conn) == ~p"/auth/users/log_in"
    assert Phoenix.Flash.get(conn.assigns.flash, :error) == "Invalid email or password"
  end

  describe "DELETE /auth/users/log_out" do
    test "logs the user out", %{conn: conn, user: user} do
      conn = conn |> log_in_user(user) |> delete(~p"/auth/users/log_out")
      assert redirected_to(conn) == ~p"/"
      refute get_session(conn, :user_token)
      assert Phoenix.Flash.get(conn.assigns.flash, :info) =~ "Logged out successfully"
    end

    test "succeeds even if the user is not logged in", %{conn: conn} do
      conn = delete(conn, ~p"/auth/users/log_out")
      assert redirected_to(conn) == ~p"/"
      refute get_session(conn, :user_token)
      assert Phoenix.Flash.get(conn.assigns.flash, :info) =~ "Logged out successfully"
    end
  end
end
