defmodule FlyingDdnsWeb.Domainname.IndexNewTest do
  @moduledoc """
  Tests for the domain name index and new form functionality.

  This module verifies:
  - The index page correctly lists all domain names
  - The new domain name form renders properly
  - Authentication and access control for these pages
  """
  use FlyingDdnsWeb.ConnCase

  import FlyingDdns.DomainsFixtures
  import FlyingDdns.AccountsFixtures

  setup %{conn: conn} do
    user = user_fixture()
    conn = log_in_user(conn, user)
    domainname = domainname_fixture(%{user_id: user.id})

    %{conn: conn, user: user, domainname: domainname}
  end

  test "index lists all domainnames", %{conn: conn} do
    conn = get(conn, ~p"/domainnames")
    assert html_response(conn, 200) =~ "Listing Domainnames"
  end

  test "new domainname renders form", %{conn: conn} do
    conn = get(conn, ~p"/domainnames/new")
    assert html_response(conn, 200) =~ "New Domainname"
  end
end
