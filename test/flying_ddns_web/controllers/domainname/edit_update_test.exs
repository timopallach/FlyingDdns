defmodule FlyingDdnsWeb.Domainname.EditUpdateTest do
  @moduledoc """
  Tests for the domain name editing and updating functionality.

  This module verifies the domain name edit and update processes, including:
  - Rendering of the edit form for existing domain names
  - Successful updates with valid data
  - Error handling for invalid update data
  - Proper redirection after updates
  """
  use FlyingDdnsWeb.ConnCase

  import FlyingDdns.DomainsFixtures
  import FlyingDdns.AccountsFixtures

  setup %{conn: conn} do
    user = user_fixture()
    conn = log_in_user(conn, user)
    domainname = domainname_fixture(%{user_id: user.id})

    %{conn: conn, user: user, domainname: domainname}
  end

  test "edit domainname renders form for editing chosen domainname", %{
    conn: conn,
    domainname: domainname
  } do
    conn = get(conn, ~p"/domainnames/#{domainname}/edit")
    assert html_response(conn, 200) =~ "Edit Domainname"
  end

  test "update domainname redirects when data is valid", %{conn: conn, domainname: domainname} do
    conn = put(conn, ~p"/domainnames/#{domainname}", domainname: %{name: "updated name"})
    assert redirected_to(conn) == ~p"/domainnames/#{domainname}"

    conn = get(conn, ~p"/domainnames/#{domainname}")
    assert html_response(conn, 200) =~ "updated name"
  end

  test "update domainname renders errors when data is invalid", %{
    conn: conn,
    domainname: domainname
  } do
    conn = put(conn, ~p"/domainnames/#{domainname}", domainname: %{name: nil})
    assert html_response(conn, 200) =~ "Edit Domainname"
  end
end
