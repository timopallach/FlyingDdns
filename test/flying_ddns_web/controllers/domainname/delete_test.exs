defmodule FlyingDdnsWeb.Domainname.DeleteTest do
  @moduledoc """
  Tests for the domain name deletion functionality.

  This module verifies the domain name deletion process, including:
  - Successful deletion of domain names
  - Proper redirection after deletion
  - Verification that deleted domain names are no longer accessible
  """
  use FlyingDdnsWeb.ConnCase

  import FlyingDdns.DomainsFixtures
  import FlyingDdns.AccountsFixtures

  setup %{conn: conn} do
    user = user_fixture()
    conn = log_in_user(conn, user)
    domainname = domainname_fixture(%{user_id: user.id})

    %{conn: conn, user: user, domainname: domainname}
  end

  test "delete domainname deletes chosen domainname", %{conn: conn, domainname: domainname} do
    conn = delete(conn, ~p"/domainnames/#{domainname}")
    assert redirected_to(conn) == ~p"/domainnames"

    assert_error_sent 404, fn ->
      get(conn, ~p"/domainnames/#{domainname}")
    end
  end
end
