defmodule FlyingDdnsWeb.Domainname.CreateTest do
  @moduledoc """
  Tests for the domain name creation functionality.

  This module verifies the domain name creation process, including:
  - Successful creation with valid data
  - Error handling for invalid data
  - Proper redirection after creation
  - User association with created domain names
  """
  use FlyingDdnsWeb.ConnCase

  import FlyingDdns.DomainsFixtures
  import FlyingDdns.AccountsFixtures

  setup %{conn: conn} do
    user = user_fixture()
    conn = log_in_user(conn, user)
    domainname = domainname_fixture(%{user_id: user.id})

    %{conn: conn, user: user, domainname: domainname}
  end

  test "create domainname redirects to show when data is valid", %{conn: conn, user: user} do
    conn = post(conn, ~p"/domainnames", domainname: %{name: "some name", user_id: user.id})

    assert %{id: id} = redirected_params(conn)
    assert redirected_to(conn) == ~p"/domainnames/#{id}"

    conn = get(conn, ~p"/domainnames/#{id}")
    assert html_response(conn, 200) =~ "some name"
  end

  test "create domainname renders errors when data is invalid", %{conn: conn} do
    conn = post(conn, ~p"/domainnames", domainname: %{name: nil})
    assert html_response(conn, 200) =~ "New Domainname"
  end
end
