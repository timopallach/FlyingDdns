# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdnsWeb.ErrorJSONTest do
  @moduledoc """
  Tests for the ErrorJSON view module.

  This module verifies that error templates render the expected JSON responses
  for different HTTP error codes, ensuring that appropriate error structures
  are returned when API errors occur.
  """
  use FlyingDdnsWeb.ConnCase, async: true

  test "renders 404" do
    assert FlyingDdnsWeb.ErrorJSON.render("404.json", %{}) == %{errors: %{detail: "Not Found"}}
  end

  test "renders 500" do
    assert FlyingDdnsWeb.ErrorJSON.render("500.json", %{}) ==
             %{errors: %{detail: "Internal Server Error"}}
  end
end
