# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdnsWeb.PageControllerTest do
  @moduledoc """
  Tests for the PageController module.

  This module contains tests for the main page functionality of the application,
  verifying that the home page renders correctly with appropriate content for
  both authenticated and unauthenticated users. It ensures that the page
  structure, content type, and response format meet the expected requirements.
  """
  use FlyingDdnsWeb.ConnCase

  import FlyingDdns.AccountsFixtures

  test "module exists" do
    assert Code.ensure_loaded?(FlyingDdnsWeb.PageController)
  end

  test "GET /phoenix renders the home page", %{conn: conn} do
    conn = get(conn, ~p"/phoenix")
    response = html_response(conn, 200)

    # Adjust based on your actual implementation
    assert response =~ "Welcome" || response =~ "Flying DDNS" || response =~ "DDNS" ||
             response =~ "Flying"
  end

  test "GET /phoenix sets content type to text/html", %{conn: conn} do
    conn = get(conn, ~p"/phoenix")

    content_type =
      Enum.find_value(conn.resp_headers, fn {header, value} ->
        if header == "content-type", do: value
      end)

    assert content_type =~ "text/html"
  end

  test "home/2 function exists in PageController" do
    functions = FlyingDdnsWeb.PageController.__info__(:functions)
    assert Enum.find(functions, fn {name, arity} -> name == :home and arity == 2 end)
  end

  test "GET /phoenix with authenticated user shows user-specific content", %{conn: conn} do
    user = user_fixture()
    conn = log_in_user(conn, user)

    conn = get(conn, ~p"/phoenix")
    response = html_response(conn, 200)

    # Check for user-specific content if applicable
    assert response =~ "Welcome" || response =~ "Flying DDNS" || response =~ "Dashboard" ||
             response =~ "DDNS" || response =~ "Flying"
  end

  test "GET /phoenix with different accept header still returns HTML", %{conn: conn} do
    conn =
      conn
      |> put_req_header("accept", "text/html, application/json")
      |> get(~p"/phoenix")

    assert html_response(conn, 200)
  end

  test "GET /phoenix response includes page content", %{conn: conn} do
    conn = get(conn, ~p"/phoenix")
    response = html_response(conn, 200)

    # Check for content rather than specific HTML tags
    assert is_binary(response) and String.length(response) > 0
  end

  test "GET /phoenix includes some common page elements", %{conn: conn} do
    conn = get(conn, ~p"/phoenix")
    response = html_response(conn, 200)

    # Just check that we have some content
    assert String.length(response) > 0
  end

  test "GET /phoenix includes some page structure or content", %{conn: conn} do
    conn = get(conn, ~p"/phoenix")
    response = html_response(conn, 200)

    # Just check that we have some content
    assert String.length(response) > 0
  end

  test "GET /phoenix with custom params still renders the home page", %{conn: conn} do
    conn = get(conn, ~p"/phoenix", %{"custom" => "param"})
    response = html_response(conn, 200)

    # Check that the page still renders with custom params
    assert is_binary(response) and String.length(response) > 0
  end

  # Add a more specific test to ensure the home/2 function is being called
  test "PageController.home/2 function is called when accessing /phoenix path", %{conn: conn} do
    # Make the request
    conn = get(conn, ~p"/phoenix")

    # Verify the response
    assert html_response(conn, 200)
    assert conn.private[:phoenix_controller] == FlyingDdnsWeb.PageController
  end
end
