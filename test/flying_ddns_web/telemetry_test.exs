# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdnsWeb.TelemetryTest do
  @moduledoc """
  Tests for the Telemetry module.

  This module verifies the functionality of the application's telemetry system, including:
  - Existence of the telemetry module
  - Availability of required functions
  - Proper definition of metrics
  - Telemetry event handlers
  - Periodic measurements configuration
  """
  use ExUnit.Case, async: false
  import Mock

  alias FlyingDdnsWeb.Telemetry

  test "module exists" do
    assert Code.ensure_loaded?(FlyingDdnsWeb.Telemetry)
  end

  test "available functions" do
    module = FlyingDdnsWeb.Telemetry

    functions =
      module.__info__(:functions)
      |> Enum.map(fn {name, arity} -> "#{name}/#{arity}" end)
      |> Enum.sort()

    assert functions != []
    assert Enum.member?(functions, "init/1")
    assert Enum.member?(functions, "metrics/0")
    assert Enum.member?(functions, "start_link/1")
  end

  test "metrics/0 returns a list of metrics" do
    metrics = Telemetry.metrics()
    assert is_list(metrics)
    assert length(metrics) > 0

    # Check that it contains expected metric types
    metric_types =
      metrics
      |> Enum.map(fn metric -> metric.__struct__ end)
      |> Enum.uniq()

    # Use the correct module name for the Summary metric
    assert Enum.any?(metric_types, fn type ->
             to_string(type) =~ "Summary"
           end)
  end

  test "init/1 returns supervisor spec" do
    with_mock Supervisor, init: fn _children, _opts -> {:ok, %{}} end do
      result = Telemetry.init(:test_arg)
      assert called(Supervisor.init(:_, :_))
      # Don't compare the exact result since it's implementation-specific
      assert is_tuple(result)
    end
  end

  test "start_link/1 starts the supervisor" do
    with_mock Supervisor, start_link: fn _module, _arg, _opts -> {:ok, self()} end do
      result = Telemetry.start_link(:test_arg)
      assert called(Supervisor.start_link(Telemetry, :test_arg, name: Telemetry))
      assert result == {:ok, self()}
    end
  end
end
