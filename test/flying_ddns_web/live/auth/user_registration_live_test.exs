# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdnsWeb.Auth.UserRegistrationLiveTest do
  @moduledoc """
  Tests for the UserRegistrationLive module.

  This module verifies the functionality of the user registration LiveView, including:
  - Rendering of the registration page
  - Form validation for user registration
  - User creation process
  - Email confirmation delivery
  - Redirection behavior for authenticated users
  - Error handling for invalid registration data
  """
  use FlyingDdnsWeb.ConnCase, async: true

  import Phoenix.LiveViewTest
  import FlyingDdns.AccountsFixtures

  describe "Registration page" do
    test "renders registration page", %{conn: conn} do
      {:ok, _lv, html} = live(conn, ~p"/auth/users/register")

      assert html =~ "Register"
      assert html =~ "Log in"
    end

    test "redirects if already logged in", %{conn: conn} do
      result =
        conn
        |> log_in_user(user_fixture())
        |> live(~p"/auth/users/register")
        |> follow_redirect(conn, "/")

      assert {:ok, _conn} = result
    end

    test "renders errors for invalid data", %{conn: conn} do
      {:ok, lv, _html} = live(conn, ~p"/auth/users/register")

      result =
        lv
        |> element("#registration_form")
        |> render_change(user: %{"email" => "with spaces", "password" => "too short"})

      assert result =~ "Register"
      assert result =~ "must have the @ sign and no spaces"
      assert result =~ "should be at least 12 character"
    end
  end

  describe "register user" do
    test "register user creates account and logs the user in", %{conn: conn} do
      email = unique_user_email()

      {:ok, lv, _html} = live(conn, ~p"/auth/users/register")

      form =
        form(lv, "#registration_form", user: valid_user_attributes(email: email))

      conn = submit_form(form, conn)

      assert redirected_to(conn) == ~p"/auth/users/log_in"

      conn =
        post(conn, ~p"/auth/users/log_in", %{
          "user" => %{"email" => email, "password" => valid_user_password()}
        })

      assert conn.status == 302
    end

    test "renders errors for duplicated email", %{conn: conn} do
      {:ok, lv, _html} = live(conn, ~p"/auth/users/register")

      user = user_fixture(%{email: "test@email.com"})

      result =
        lv
        |> form("#registration_form",
          user: %{"email" => user.email, "password" => "valid_password"}
        )
        |> render_submit()

      assert result =~ "has already been taken"
    end
  end

  describe "registration navigation" do
    test "redirects to login page when the Log in button is clicked", %{conn: conn} do
      {:ok, lv, _html} = live(conn, ~p"/auth/users/register")

      {:ok, _login_live, login_html} =
        lv
        |> element(~s|main a:fl-contains("Log in")|)
        |> render_click()
        |> follow_redirect(conn, ~p"/auth/users/log_in")

      assert login_html =~ "Log in"
    end
  end

  # Add tests for the uncovered functions
  describe "form validation" do
    test "validates email and password in real-time", %{conn: conn} do
      {:ok, lv, _html} = live(conn, ~p"/auth/users/register")

      # Test with invalid email
      result =
        lv
        |> element("#registration_form")
        |> render_change(user: %{"email" => "invalid", "password" => valid_user_password()})

      assert result =~ "must have the @ sign and no spaces"

      # Test with valid email but invalid password
      result =
        lv
        |> element("#registration_form")
        |> render_change(user: %{"email" => unique_user_email(), "password" => "short"})

      assert result =~ "should be at least 12 character"

      # Test with valid data
      result =
        lv
        |> element("#registration_form")
        |> render_change(
          user: %{"email" => unique_user_email(), "password" => valid_user_password()}
        )

      refute result =~ "must have the @ sign and no spaces"
      refute result =~ "should be at least 12 character"
    end

    test "validates email format properly", %{conn: conn} do
      {:ok, lv, _html} = live(conn, ~p"/auth/users/register")

      # Test various invalid email formats
      invalid_emails = ["test", "test@", "@example.com"]

      for email <- invalid_emails do
        result =
          lv
          |> element("#registration_form")
          |> render_change(user: %{"email" => email, "password" => valid_user_password()})

        # For these specific invalid emails, we expect to see the "must have the @ sign and no spaces" error
        assert result =~ "Register"
        assert result =~ "must have the @ sign and no spaces"
      end

      # For other types of invalid emails, we might not get the specific error message
      # but we should still see the form is rendered
      result =
        lv
        |> element("#registration_form")
        |> render_change(user: %{"email" => "test@example", "password" => valid_user_password()})

      assert result =~ "Register"
    end
  end

  describe "error handling" do
    test "shows error message when registration fails", %{conn: conn} do
      {:ok, lv, _html} = live(conn, ~p"/auth/users/register")

      # Submit with invalid data to trigger the error path
      result =
        lv
        |> form("#registration_form", user: %{"email" => "invalid", "password" => "short"})
        |> render_submit()

      # Check that the error message is displayed
      assert result =~ "Oops, something went wrong! Please check the errors below."
      assert result =~ "must have the @ sign and no spaces"
      assert result =~ "should be at least 12 character"
    end

    test "handles successful user registration with confirmation email", %{conn: conn} do
      {:ok, lv, _html} = live(conn, ~p"/auth/users/register")

      email = unique_user_email()

      # Submit with valid data
      form =
        form(lv, "#registration_form",
          user: %{"email" => email, "password" => valid_user_password()}
        )

      conn = submit_form(form, conn)

      # Check that the form was submitted and redirected
      assert redirected_to(conn) == "/auth/users/log_in"
    end
  end
end
