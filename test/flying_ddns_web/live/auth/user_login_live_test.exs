# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdnsWeb.Auth.UserLoginLiveTest do
  @moduledoc """
  Tests for the UserLoginLive module.

  This module verifies the functionality of the user login LiveView, including:
  - Rendering of the login page
  - User authentication process
  - Form validation for login credentials
  - Redirection behavior for authenticated users
  - Error handling for invalid login attempts
  - Remember me functionality
  """
  use FlyingDdnsWeb.ConnCase, async: true

  import Phoenix.LiveViewTest
  import FlyingDdns.AccountsFixtures

  describe "Log in page" do
    test "renders log in page", %{conn: conn} do
      {:ok, _lv, html} = live(conn, ~p"/auth/users/log_in")

      assert html =~ "Log in"
      assert html =~ "Register"
      assert html =~ "Forgot your password?"
    end

    test "redirects if already logged in", %{conn: conn} do
      result =
        conn
        |> log_in_user(user_fixture())
        |> live(~p"/auth/users/log_in")
        |> follow_redirect(conn, "/")

      assert {:ok, _conn} = result
    end
  end

  describe "user login" do
    @tag timeout: 10_000
    test "redirects if user login with valid credentials", %{conn: conn} do
      # gitleaks:allow
      password = "123456789abcdef"
      user = user_fixture(%{password: password})

      {:ok, lv, _html} = live(conn, ~p"/auth/users/log_in")

      form =
        form(lv, "#login_form", user: %{email: user.email, password: password, remember_me: true})

      conn = submit_form(form, conn)

      assert redirected_to(conn) == ~p"/"
    end

    test "user login redirects to login page with a flash error if there are no valid credentials",
         %{conn: conn} do
      {:ok, lv, _html} = live(conn, ~p"/auth/users/log_in")

      form =
        form(lv, "#login_form", user: %{email: "invalid@email.com", password: "invalid_password"})

      conn = submit_form(form, conn)

      assert redirected_to(conn) == ~p"/auth/users/log_in"
      assert Phoenix.Flash.get(conn.assigns.flash, :error) == "Invalid email or password"
    end
  end

  describe "login navigation" do
    test "redirects to registration page when the Register button is clicked", %{conn: conn} do
      {:ok, lv, _html} = live(conn, ~p"/auth/users/log_in")

      {:ok, _login_live, login_html} =
        lv
        |> element(~s|main a:fl-contains("Sign up")|)
        |> render_click()
        |> follow_redirect(conn, ~p"/auth/users/register")

      assert login_html =~ "Register"
    end

    test "redirects to forgot password page when the Forgot Password button is clicked", %{
      conn: conn
    } do
      {:ok, lv, _html} = live(conn, ~p"/auth/users/log_in")

      {:ok, conn} =
        lv
        |> element(~s|main a:fl-contains("Forgot your password?")|)
        |> render_click()
        |> follow_redirect(conn, ~p"/auth/users/reset_password")

      assert conn.resp_body =~ "Forgot your password?"
    end
  end
end
