defmodule FlyingDdnsWeb.UserAuthTest do
  @moduledoc """
  Tests for the UserAuth module.

  This module verifies the functionality of the user authentication system, including:
  - User login and session management
  - Remember me cookie functionality
  - Session token validation and storage
  - User logout process
  - Authentication requirements for protected routes
  - Redirection behavior for authenticated and unauthenticated users
  - Flash message handling during authentication flows
  """
  use FlyingDdnsWeb.ConnCase, async: true

  alias FlyingDdns.Accounts
  alias FlyingDdnsWeb.UserAuth
  import FlyingDdns.AccountsFixtures
  alias Phoenix.Flash

  @remember_me_cookie "_flying_ddns_web_user_remember_me"

  setup %{conn: conn} do
    conn =
      conn
      |> Map.replace!(:secret_key_base, FlyingDdnsWeb.Endpoint.config(:secret_key_base))
      |> init_test_session(%{})

    %{user: user_fixture(), conn: conn}
  end

  describe "log_in_user/3" do
    test "stores the user token in the session", %{conn: conn, user: user} do
      conn = UserAuth.log_in_user(conn, user)
      assert token = get_session(conn, :user_token)
      assert get_session(conn, :live_socket_id) == "users_sessions:#{Base.url_encode64(token)}"
      assert redirected_to(conn) == ~p"/"
      assert Accounts.get_user_by_session_token(token)
    end

    test "clears everything previously stored in the session", %{conn: conn, user: user} do
      conn = conn |> put_session(:to_be_removed, "value") |> UserAuth.log_in_user(user)
      refute get_session(conn, :to_be_removed)
    end

    test "redirects to the configured path", %{conn: conn, user: user} do
      conn = conn |> put_session(:user_return_to, "/hello") |> UserAuth.log_in_user(user)
      assert redirected_to(conn) == "/hello"
    end

    test "writes a cookie if remember_me is configured", %{conn: conn, user: user} do
      conn = conn |> fetch_cookies() |> UserAuth.log_in_user(user, %{"remember_me" => "true"})
      assert get_session(conn, :user_token) == conn.cookies["_flying_ddns_web_user_remember_me"]

      assert %{value: signed_token, max_age: max_age} =
               conn.resp_cookies["_flying_ddns_web_user_remember_me"]

      assert signed_token != get_session(conn, :user_token)
      assert max_age == 60 * 60 * 24 * 60
    end

    test "does not write a cookie when remember_me is not configured", %{conn: conn, user: user} do
      conn = conn |> fetch_cookies() |> UserAuth.log_in_user(user, %{})
      refute conn.cookies["_flying_ddns_web_user_remember_me"]
      refute conn.resp_cookies["_flying_ddns_web_user_remember_me"]
    end
  end

  describe "logout_user/1" do
    test "erases session and cookies", %{conn: conn, user: user} do
      user_token = Accounts.generate_user_session_token(user)

      conn =
        conn
        |> put_session(:user_token, user_token)
        |> put_req_cookie(@remember_me_cookie, user_token)
        |> fetch_cookies()
        |> UserAuth.log_out_user()

      refute get_session(conn, :user_token)
      refute conn.cookies[@remember_me_cookie]
      assert %{max_age: 0} = conn.resp_cookies[@remember_me_cookie]
      assert redirected_to(conn) == ~p"/"
      refute Accounts.get_user_by_session_token(user_token)
    end

    test "broadcasts to the given live_socket_id", %{conn: conn} do
      live_socket_id = "users_sessions:abcdef-token"
      FlyingDdnsWeb.Endpoint.subscribe(live_socket_id)

      conn
      |> put_session(:live_socket_id, live_socket_id)
      |> UserAuth.log_out_user()

      assert_receive %Phoenix.Socket.Broadcast{event: "disconnect", topic: ^live_socket_id}
    end

    test "works even if user is already logged out", %{conn: conn} do
      conn = conn |> fetch_cookies() |> UserAuth.log_out_user()
      refute get_session(conn, :user_token)
      assert %{max_age: 0} = conn.resp_cookies[@remember_me_cookie]
      assert redirected_to(conn) == ~p"/"
    end
  end

  describe "fetch_current_user/2" do
    test "authenticates user from session", %{conn: conn, user: user} do
      user_token = Accounts.generate_user_session_token(user)
      conn = conn |> put_session(:user_token, user_token) |> UserAuth.fetch_current_user([])
      assert conn.assigns.current_user.id == user.id
    end

    test "authenticates user from cookies", %{conn: conn, user: user} do
      logged_in_conn =
        conn |> fetch_cookies() |> UserAuth.log_in_user(user, %{"remember_me" => "true"})

      user_token = logged_in_conn.cookies[@remember_me_cookie]
      %{value: signed_token} = logged_in_conn.resp_cookies[@remember_me_cookie]

      conn =
        conn
        |> put_req_cookie(@remember_me_cookie, signed_token)
        |> UserAuth.fetch_current_user([])

      assert conn.assigns.current_user.id == user.id
      assert get_session(conn, :user_token) == user_token

      # Now try to use the token
      conn = conn |> fetch_session() |> UserAuth.fetch_current_user([])
      assert conn.assigns.current_user.id == user.id
    end

    test "does not authenticate if data is missing", %{conn: conn, user: user} do
      _ = Accounts.generate_user_session_token(user)
      conn = UserAuth.fetch_current_user(conn, [])
      refute get_session(conn, :user_token)
      refute conn.assigns.current_user
    end
  end

  # For LiveView on_mount functions, we'll verify they exist and have the right arity
  describe "on_mount functions" do
    test "on_mount functions exist with correct arity" do
      assert function_exported?(FlyingDdnsWeb.UserAuth, :on_mount, 4)
    end

    test "on_mount :mount_current_user assigns current_user to socket" do
      assert Code.ensure_loaded?(FlyingDdnsWeb.UserAuth)

      # Verify the function exists
      assert function_exported?(FlyingDdnsWeb.UserAuth, :on_mount, 4)
    end

    test "on_mount :ensure_authenticated exists" do
      assert Code.ensure_loaded?(FlyingDdnsWeb.UserAuth)

      # Verify the function exists
      assert function_exported?(FlyingDdnsWeb.UserAuth, :on_mount, 4)
    end

    test "on_mount :redirect_if_user_is_authenticated exists" do
      assert Code.ensure_loaded?(FlyingDdnsWeb.UserAuth)

      # Verify the function exists
      assert function_exported?(FlyingDdnsWeb.UserAuth, :on_mount, 4)
    end

    test "mount_current_user assigns current_user when user is authenticated", %{user: user} do
      user_token = Accounts.generate_user_session_token(user)

      socket = %Phoenix.LiveView.Socket{
        assigns: %{__changed__: %{}}
      }

      session = %{"user_token" => user_token}

      result = UserAuth.on_mount(:mount_current_user, %{}, session, socket)
      assert {:cont, updated_socket} = result
      assert updated_socket.assigns.current_user.id == user.id
    end

    test "mount_current_user assigns nil when user is not authenticated" do
      socket = %Phoenix.LiveView.Socket{
        assigns: %{__changed__: %{}}
      }

      session = %{}

      result = UserAuth.on_mount(:mount_current_user, %{}, session, socket)
      assert {:cont, updated_socket} = result
      assert updated_socket.assigns.current_user == nil
    end

    test "on_mount :ensure_authenticated halts and redirects when user is not authenticated" do
      socket = %Phoenix.LiveView.Socket{
        endpoint: FlyingDdnsWeb.Endpoint,
        assigns: %{__changed__: %{}, flash: %{}}
      }

      result = UserAuth.on_mount(:ensure_authenticated, %{}, %{}, socket)
      assert {:halt, updated_socket} = result
      assert updated_socket.redirected
    end

    test "on_mount :ensure_authenticated continues when user is authenticated", %{user: user} do
      user_token = Accounts.generate_user_session_token(user)

      socket = %Phoenix.LiveView.Socket{
        assigns: %{__changed__: %{}}
      }

      session = %{"user_token" => user_token}

      result = UserAuth.on_mount(:ensure_authenticated, %{}, session, socket)
      assert {:cont, updated_socket} = result
      assert updated_socket.assigns.current_user.id == user.id
    end

    test "on_mount :redirect_if_user_is_authenticated halts and redirects when user is authenticated",
         %{user: user} do
      user_token = Accounts.generate_user_session_token(user)

      socket = %Phoenix.LiveView.Socket{
        endpoint: FlyingDdnsWeb.Endpoint,
        assigns: %{__changed__: %{}}
      }

      session = %{"user_token" => user_token}

      result = UserAuth.on_mount(:redirect_if_user_is_authenticated, %{}, session, socket)
      assert {:halt, updated_socket} = result
      assert updated_socket.redirected
    end

    test "on_mount :redirect_if_user_is_authenticated continues when user is not authenticated" do
      socket = %Phoenix.LiveView.Socket{
        assigns: %{__changed__: %{}}
      }

      result = UserAuth.on_mount(:redirect_if_user_is_authenticated, %{}, %{}, socket)
      assert {:cont, updated_socket} = result
      refute updated_socket.redirected
    end
  end

  describe "redirect_if_user_is_authenticated/2" do
    test "redirects if user is authenticated", %{conn: conn, user: user} do
      conn = conn |> assign(:current_user, user) |> UserAuth.redirect_if_user_is_authenticated([])
      assert conn.halted
      assert redirected_to(conn) == ~p"/"
    end

    test "does nothing if user is not authenticated", %{conn: conn} do
      conn = UserAuth.redirect_if_user_is_authenticated(conn, [])
      refute conn.halted
      refute conn.status
    end
  end

  describe "require_authenticated_user/2" do
    test "redirects if user is not authenticated", %{conn: conn} do
      conn = conn |> fetch_flash() |> UserAuth.require_authenticated_user([])
      assert conn.halted
      assert redirected_to(conn) == ~p"/auth/users/log_in"
      assert Flash.get(conn.assigns.flash, :error) == "You must log in to access this page."
    end

    test "stores the path to redirect to on GET", %{conn: conn} do
      halted_conn =
        %{conn | path_info: ["foo"], query_string: ""}
        |> fetch_flash()
        |> UserAuth.require_authenticated_user([])

      assert halted_conn.halted
      assert get_session(halted_conn, :user_return_to) == "/foo"

      halted_conn =
        %{conn | path_info: ["foo"], query_string: "bar=baz"}
        |> fetch_flash()
        |> UserAuth.require_authenticated_user([])

      assert halted_conn.halted
      assert get_session(halted_conn, :user_return_to) == "/foo?bar=baz"

      halted_conn =
        %{conn | path_info: ["foo"], query_string: "bar", method: "POST"}
        |> fetch_flash()
        |> UserAuth.require_authenticated_user([])

      assert halted_conn.halted
      refute get_session(halted_conn, :user_return_to)
    end

    test "does not redirect if user is authenticated", %{conn: conn, user: user} do
      conn = conn |> assign(:current_user, user) |> UserAuth.require_authenticated_user([])
      refute conn.halted
      refute conn.status
    end
  end
end
