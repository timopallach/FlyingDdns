defmodule FlyingDdnsWeb.RouterTest do
  @moduledoc """
  Tests for the Router module.

  This module verifies the functionality of the application's router, including:
  - Existence of the router module
  - Availability of defined routes
  - Route path structure and organization
  - HTTP verb assignments for routes
  """
  use FlyingDdnsWeb.ConnCase

  test "router exists" do
    assert Code.ensure_loaded?(FlyingDdnsWeb.Router)
  end

  test "check available routes" do
    routes = FlyingDdnsWeb.Router.__routes__()
    _route_paths = Enum.map(routes, & &1.path)

    # Print available routes for debugging
    IO.puts("Available routes:")

    for route <- routes do
      IO.puts("#{route.verb} #{route.path}")
    end

    assert length(routes) > 0
  end

  test "auth routes are defined" do
    routes = FlyingDdnsWeb.Router.__routes__()

    # Check for auth routes
    auth_routes =
      Enum.filter(routes, fn route ->
        String.contains?(route.path, "auth/users")
      end)

    assert length(auth_routes) > 0
  end

  test "domainname routes are defined" do
    routes = FlyingDdnsWeb.Router.__routes__()

    # Check for domainname routes
    domainname_routes =
      Enum.filter(routes, fn route ->
        String.contains?(route.path, "domainnames")
      end)

    assert length(domainname_routes) > 0
  end
end
