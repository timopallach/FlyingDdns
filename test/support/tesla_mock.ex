defmodule FlyingDdns.TeslaMock do
  @moduledoc """
  Mock module for Tesla HTTP client.

  This module provides mock implementations for Tesla functions used in tests,
  allowing tests to run without making actual HTTP requests.
  """

  require Logger

  @doc """
  Mock implementation of Tesla.patch that can be used to simulate different responses.

  This implementation will:
  - Return error responses for non-example.com domains
  - Return success responses for example.com domains
  - Handle different error scenarios based on the domain and IP
  """
  def patch(url, _body, _opts) do
    # Extract the domain from the URL to determine the response
    domain = extract_domain_from_url(url)

    # Log the mock request for debugging
    Logger.debug("TeslaMock.patch called with URL: #{url}, domain: #{domain}")

    cond do
      # For non-example.com domains in error tests, return a connection refused error
      String.contains?(domain, "mydomain.net") ->
        Logger.debug("Returning connection refused error for #{domain}")
        {:error, %Tesla.Error{reason: :econnrefused}}

      # For timeout test domains
      String.contains?(domain, "timeout.test") ->
        Logger.debug("Returning timeout error for #{domain}")
        {:error, %Tesla.Error{reason: :timeout}}

      # For API error response test (status 500)
      String.contains?(domain, "error500.test") ->
        Logger.debug("Returning 500 error for #{domain}")
        {:ok, %Tesla.Env{status: 500, body: "{\"error\": \"Internal Server Error\"}"}}

      # For all other domains, return a success response
      true ->
        Logger.debug("Returning success response for #{domain}")
        {:ok, %Tesla.Env{status: 200, body: "{}"}}
    end
  end

  # Helper function to extract the domain from a URL
  defp extract_domain_from_url(url) do
    url
    |> String.split("/")
    |> Enum.at(2, "")
  end
end
