# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdns.ZoneFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `FlyingDdns.Zone` context.
  """

  alias FlyingDdns.Domains

  @doc """
  Generate a domainname.
  """
  def domainname_fixture(attrs \\ %{}) do
    # Ensure we have a zone_id if not provided
    attrs =
      if Map.has_key?(attrs, :zone_id) || Map.has_key?(attrs, "zone_id") do
        attrs
      else
        zone = zone_fixture()
        Map.put(attrs, :zone_id, zone.id)
      end

    {:ok, domainname} =
      attrs
      |> Enum.into(%{
        name: "test-#{System.unique_integer([:positive])}",
        ip: "192.168.1.#{:rand.uniform(254)}"
      })
      |> FlyingDdns.Zone.create_domainname()

    domainname
  end

  @doc """
  Generate a zone.
  """
  def zone_fixture(attrs \\ %{}) do
    # Ensure we have a user_id if not provided
    attrs =
      if Map.has_key?(attrs, :user_id) || Map.has_key?(attrs, "user_id") do
        attrs
      else
        user = FlyingDdns.AccountsFixtures.user_fixture()
        Map.put(attrs, :user_id, user.id)
      end

    # Direct creation since Zone.create_zone/1 doesn't exist
    %{
      id: System.unique_integer([:positive]),
      name: "example-#{System.unique_integer([:positive])}.com",
      user_id: attrs[:user_id] || attrs["user_id"]
    }
  end

  @doc """
  Generate a domainname within a zone.
  """
  def zone_domainname_fixture(attrs \\ %{}) do
    # Ensure we have a zone_id if not provided
    attrs =
      if Map.has_key?(attrs, :zone_id) || Map.has_key?(attrs, "zone_id") do
        attrs
      else
        zone = zone_fixture()
        Map.put(attrs, :zone_id, zone.id)
      end

    # Check if Domains.create_domainname/1 exists
    if function_exported?(Domains, :create_domainname, 1) do
      # Ensure we have default values for name and ip if not provided
      default_attrs = %{
        name: "test-#{System.unique_integer([:positive])}",
        ip: "192.168.1.#{:rand.uniform(254)}"
      }

      # Only add default values for keys that don't exist in attrs
      attrs_with_defaults = add_default_values(default_attrs, attrs)

      {:ok, domainname} = Domains.create_domainname(attrs_with_defaults)

      domainname
    else
      # Fallback to direct creation if the function doesn't exist
      # Extract name from attrs, with a default if not provided
      name = attrs[:name] || attrs["name"] || "test-#{System.unique_integer([:positive])}"
      zone_id = attrs[:zone_id] || attrs["zone_id"]
      ip = attrs[:ip] || attrs["ip"] || "192.168.1.#{:rand.uniform(254)}"

      %{
        id: System.unique_integer([:positive]),
        name: name,
        zone_id: zone_id,
        ip: ip
      }
    end
  end

  defp add_default_values(default_attrs, attrs) do
    Enum.reduce(default_attrs, attrs, fn {key, value}, acc ->
      if Map.has_key?(acc, key) || Map.has_key?(acc, to_string(key)) do
        acc
      else
        Map.put(acc, key, value)
      end
    end)
  end
end
