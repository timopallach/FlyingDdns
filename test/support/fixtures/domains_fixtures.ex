defmodule FlyingDdns.DomainsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `FlyingDdns.Domains` context.
  """

  # Import any needed modules
  import FlyingDdns.AccountsFixtures

  # Add this function to patch all timestamps in the database
  def truncate_all_timestamps do
    # This will directly update all timestamps in the database to remove microseconds
    FlyingDdns.Repo.query!("
      UPDATE domainnames 
      SET inserted_at = date_trunc('second', inserted_at),
          updated_at = date_trunc('second', updated_at)
    ")
  end

  @doc """
  Generate a domainname.
  """
  def domainname_fixture(attrs \\ %{}) do
    # Use underscore to indicate intentionally unused variable
    _user = attrs[:user] || user_fixture()

    attrs =
      Enum.into(attrs, %{
        name: "some name #{System.unique_integer([:positive])}"
        # Remove user_id if the column doesn't exist in your schema
      })

    {:ok, domainname} = FlyingDdns.Domains.create_domainname(attrs)

    domainname
  end
end
