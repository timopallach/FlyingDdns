defmodule FlyingDdns.TeslaMockTest do
  @moduledoc """
  Tests for the TeslaMock module which provides mock HTTP responses for Tesla client.
  """
  use ExUnit.Case, async: true

  alias FlyingDdns.TeslaMock

  describe "patch/3" do
    test "returns connection refused error for mydomain.net" do
      url = "https://mydomain.net/api/update"
      body = %{ip: "192.168.1.1"}
      opts = []

      assert {:error, %Tesla.Error{reason: :econnrefused}} = TeslaMock.patch(url, body, opts)
    end

    test "returns timeout error for timeout.test domain" do
      url = "https://timeout.test/api/update"
      body = %{ip: "192.168.1.1"}
      opts = []

      assert {:error, %Tesla.Error{reason: :timeout}} = TeslaMock.patch(url, body, opts)
    end

    test "returns 500 error for error500.test domain" do
      url = "https://error500.test/api/update"
      body = %{ip: "192.168.1.1"}
      opts = []

      assert {:ok, %Tesla.Env{status: 500}} = TeslaMock.patch(url, body, opts)
    end

    test "returns success response for other domains" do
      url = "https://example.com/api/update"
      body = %{ip: "192.168.1.1"}
      opts = []

      assert {:ok, %Tesla.Env{status: 200}} = TeslaMock.patch(url, body, opts)
    end

    # Test with an invalid URL to indirectly test the extract_domain_from_url function
    test "handles invalid URLs" do
      url = "invalid-url"
      body = %{ip: "192.168.1.1"}
      opts = []

      # Should default to the success case
      assert {:ok, %Tesla.Env{status: 200}} = TeslaMock.patch(url, body, opts)
    end
  end
end
