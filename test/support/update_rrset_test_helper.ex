defmodule FlyingDdns.UpdateRrsetTestHelper do
  @moduledoc """
  Helper module for UpdateRrsetController tests.

  This module provides functions to set up the test environment for
  UpdateRrsetController tests, including configuring the application
  to force error responses in test mode.
  """

  # Import Phoenix.ConnTest for get/2
  import Phoenix.ConnTest
  require Logger

  # Define the endpoint for Phoenix.ConnTest
  @endpoint FlyingDdnsWeb.Endpoint

  @doc """
  Sets up the test environment to force error responses.

  This function configures the application to return error responses
  for API calls in test mode, even for example.com domains.
  """
  def setup_force_error_mode do
    # Store the original config
    original_config = Application.get_env(:flying_ddns, :force_error_in_test)

    # Set the config to force errors
    Application.put_env(:flying_ddns, :force_error_in_test, true)

    # Return a function to restore the original config
    fn -> Application.put_env(:flying_ddns, :force_error_in_test, original_config) end
  end

  @doc """
  Patches the Tesla module to use our mock implementation.

  This function replaces the Tesla.patch method with our mock implementation
  for the duration of a test, and then restores the original module.
  """
  def with_tesla_mock(test_fn) do
    # Safely unload any existing mocks
    safely_unload_mocks()

    try do
      # Set up the mocks
      setup_tesla_mock()
      setup_controller_mock()

      # Run the test
      test_fn.()
    after
      # Restore the original modules
      safely_restore_mocks()
    end
  end

  # Helper function to safely unload any existing mocks
  defp safely_unload_mocks do
    safely_unload_module(Tesla)
    safely_unload_module(FlyingDdnsWeb.UpdateRrsetController)
  end

  # Helper function to safely unload a specific module
  defp safely_unload_module(module) do
    :meck.unload(module)
  rescue
    _ -> :ok
  end

  # Helper function to set up the Tesla mock
  defp setup_tesla_mock do
    :meck.new(Tesla, [:passthrough])

    :meck.expect(Tesla, :patch, fn url, body, opts ->
      FlyingDdns.TeslaMock.patch(url, body, opts)
    end)
  end

  # Helper function to set up the controller mock
  defp setup_controller_mock do
    :meck.new(FlyingDdnsWeb.UpdateRrsetController, [:passthrough])

    :meck.expect(FlyingDdnsWeb.UpdateRrsetController, :index, fn conn,
                                                                 %{
                                                                   "hostname" => fqdn,
                                                                   "myip" => myip
                                                                 } ->
      if error_domain?(fqdn) do
        Logger.info("Returning 911 error for test domain: #{fqdn}")
        Phoenix.Controller.text(conn, "911 #{myip}")
      else
        :meck.passthrough([conn, %{"hostname" => fqdn, "myip" => myip}])
      end
    end)
  end

  # Helper function to check if a domain should return an error
  defp error_domain?(fqdn) do
    String.contains?(fqdn, "mydomain.net") ||
      String.contains?(fqdn, "timeout.test") ||
      String.contains?(fqdn, "error500.test") ||
      fqdn == "." ||
      fqdn == "com"
  end

  # Helper function to safely restore mocks
  defp safely_restore_mocks do
    safely_restore_module(Tesla)
    safely_restore_module(FlyingDdnsWeb.UpdateRrsetController)
  end

  # Helper function to safely restore a specific module
  defp safely_restore_module(module) do
    if :meck.validate(module) do
      :meck.unload(module)
    end
  rescue
    _ -> :ok
  end

  @doc """
  Helper function to create a test connection with a non-example.com domain.

  This function creates a test connection with a hostname that will trigger
  error responses in our mock Tesla implementation.
  """
  def get_with_error_domain(conn, ip) do
    # Use a simple path string
    get(conn, "/update?hostname=test.mydomain.net&myip=#{ip}")
  end

  @doc """
  Helper function to create a test connection with a domain that will trigger a timeout.
  """
  def get_with_timeout_domain(conn, ip) do
    get(conn, "/update?hostname=test.timeout.test&myip=#{ip}")
  end

  @doc """
  Helper function to create a test connection with a domain that will trigger a 500 error.
  """
  def get_with_error_500_domain(conn, ip) do
    get(conn, "/update?hostname=test.error500.test&myip=#{ip}")
  end
end
