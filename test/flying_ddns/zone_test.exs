# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdns.ZoneTest do
  @moduledoc """
  Tests for the Zone context.

  This module verifies the functionality of the Zone context, which manages DNS zones,
  including:
  - Zone creation, retrieval, update, and deletion
  - Domain name management within zones
  - Validation of zone attributes
  - Proper handling of zone-related operations
  """
  use FlyingDdns.DataCase

  alias FlyingDdns.Zone

  describe "domainnames" do
    alias FlyingDdns.Zone.Domainname

    import FlyingDdns.ZoneFixtures

    @invalid_attrs %{name: nil}

    test "list_domainnames/0 returns all domainnames" do
      domainname = domainname_fixture()
      assert Zone.list_domainnames() == [domainname]
    end

    test "get_domainname!/1 returns the domainname with given id" do
      domainname = domainname_fixture()
      assert Zone.get_domainname!(domainname.id) == domainname
    end

    test "create_domainname/1 with valid data creates a domainname" do
      valid_attrs = %{name: "some name"}

      assert {:ok, %Domainname{} = domainname} = Zone.create_domainname(valid_attrs)
      assert domainname.name == "some name"
    end

    test "create_domainname/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Zone.create_domainname(@invalid_attrs)
    end

    test "update_domainname/2 with valid data updates the domainname" do
      domainname = domainname_fixture()
      update_attrs = %{name: "some updated name"}

      assert {:ok, %Domainname{} = domainname} = Zone.update_domainname(domainname, update_attrs)
      assert domainname.name == "some updated name"
    end

    test "update_domainname/2 with invalid data returns error changeset" do
      domainname = domainname_fixture()
      assert {:error, %Ecto.Changeset{}} = Zone.update_domainname(domainname, @invalid_attrs)
      assert domainname == Zone.get_domainname!(domainname.id)
    end

    test "delete_domainname/1 deletes the domainname" do
      domainname = domainname_fixture()
      assert {:ok, %Domainname{}} = Zone.delete_domainname(domainname)
      assert_raise Ecto.NoResultsError, fn -> Zone.get_domainname!(domainname.id) end
    end

    test "change_domainname/1 returns a domainname changeset" do
      domainname = domainname_fixture()
      assert %Ecto.Changeset{} = Zone.change_domainname(domainname)
    end
  end
end
