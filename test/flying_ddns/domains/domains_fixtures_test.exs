# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdns.DomainsFixturesTest do
  @moduledoc """
  Tests for the DomainsFixtures module.

  This module verifies the functionality of the test fixtures used for domain testing,
  including:
  - Creation of domain name fixtures with default and custom values
  - Association of domain names with users
  - Timestamp truncation functionality for consistent testing
  """
  use FlyingDdns.DataCase, async: true

  alias FlyingDdns.AccountsFixtures
  alias FlyingDdns.DomainsFixtures

  test "module exists" do
    assert Code.ensure_loaded?(FlyingDdns.DomainsFixtures)
  end

  describe "domainname_fixture/1" do
    test "creates a domainname with default values" do
      domainname = DomainsFixtures.domainname_fixture()
      assert domainname.name =~ "some name"
    end

    test "creates a domainname with provided values" do
      custom_name = "custom-name-#{System.unique_integer([:positive])}"
      domainname = DomainsFixtures.domainname_fixture(%{name: custom_name})
      assert domainname.name == custom_name
    end

    test "creates a domainname with associated user" do
      user = AccountsFixtures.user_fixture()
      domainname = DomainsFixtures.domainname_fixture(%{user: user})
      assert domainname.name =~ "some name"
    end
  end

  describe "truncate_all_timestamps/0" do
    test "truncates timestamps in the database" do
      # Create a domainname with microsecond precision
      domainname = DomainsFixtures.domainname_fixture()

      # Verify the domainname was created
      assert domainname.id != nil

      # Call the function to truncate timestamps
      DomainsFixtures.truncate_all_timestamps()

      # We can't easily verify the actual truncation in a test
      # since the database operation happens outside our test transaction
      # But we can at least ensure the function runs without errors
      assert true
    end
  end
end
