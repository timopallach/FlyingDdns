# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdns.ApplicationTest do
  @moduledoc """
  Tests for the Application module.

  This module verifies the functionality of the FlyingDdns.Application module,
  including:
  - Existence of required functions
  - Proper handling of configuration changes
  - Application startup behavior
  """
  use ExUnit.Case, async: false
  import Mock

  alias FlyingDdns.Application

  test "module exists" do
    assert Code.ensure_loaded?(FlyingDdns.Application)
  end

  test "available functions" do
    module = FlyingDdns.Application

    functions =
      module.__info__(:functions)
      |> Enum.map(fn {name, arity} -> "#{name}/#{arity}" end)
      |> Enum.sort()

    assert functions != []
    assert Enum.member?(functions, "config_change/3")
    assert Enum.member?(functions, "start/2")
  end

  test "config_change/3 calls endpoint config_change" do
    with_mock FlyingDdnsWeb.Endpoint, config_change: fn _changed, _removed -> :ok end do
      changed = [key: "value"]
      removed = ["old_key"]

      result = Application.config_change(changed, %{}, removed)

      assert result == :ok
      assert called(FlyingDdnsWeb.Endpoint.config_change(changed, removed))
    end
  end
end
