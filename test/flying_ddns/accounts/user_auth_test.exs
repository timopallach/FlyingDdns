defmodule FlyingDdns.Accounts.UserAuthTest do
  @moduledoc """
  Tests for the UserAuth module in the Accounts context.

  This module verifies the authentication functionality, including:
  - User login process and token storage
  - Session management
  - Token creation and validation
  """
  use FlyingDdnsWeb.ConnCase, async: true

  alias FlyingDdns.Accounts
  alias FlyingDdns.Accounts.UserAuth

  import FlyingDdns.AccountsFixtures

  setup %{conn: conn} do
    conn =
      conn
      |> Map.replace!(:secret_key_base, FlyingDdnsWeb.Endpoint.config(:secret_key_base))
      |> init_test_session(%{})

    %{user: user_fixture(), conn: conn}
  end

  describe "log_in_user/2" do
    test "stores the user token in the session", %{conn: conn, user: user} do
      conn = UserAuth.log_in_user(conn, user)
      assert token = get_session(conn, :user_token)
      assert get_session(conn, :live_socket_id) == "users_sessions:#{Base.url_encode64(token)}"
    end

    test "creates a token in the database", %{conn: conn, user: user} do
      conn = UserAuth.log_in_user(conn, user)
      assert token = get_session(conn, :user_token)

      # Verify the token exists in the database
      assert Accounts.get_user_by_session_token(token)
    end

    test "returns the conn with the user token", %{conn: conn, user: user} do
      result_conn = UserAuth.log_in_user(conn, user)
      assert is_map(result_conn)
      assert result_conn.private[:plug_session]
    end
  end
end
