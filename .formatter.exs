# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Elixir Code Formatter Configuration
# ===============================================================================
#
# This file configures the Elixir code formatter for the FlyingDdns project.
# The formatter ensures consistent code style across the codebase when running
# `mix format`.
#
# Configuration includes:
# - Importing formatting rules from dependencies (Ecto, Phoenix)
# - Defining which subdirectories should be formatted
# - Specifying which file types and patterns to include
# - Configuring which function calls can omit parentheses
#
# The formatter helps maintain code quality and readability by enforcing
# consistent style conventions across the project.
#
# For more information on the Elixir formatter, see:
# https://hexdocs.pm/mix/master/Mix.Tasks.Format.html
# ===============================================================================

[
  import_deps: [:ecto, :ecto_sql, :phoenix],
  subdirectories: ["priv/*/migrations"],
  plugins: [Phoenix.LiveView.HTMLFormatter],
  inputs: ["*.{heex,ex,exs}", "{config,lib,test}/**/*.{heex,ex,exs}", "priv/*/seeds.exs"],
  locals_without_parens: [
    # Add Phoenix controller functions
    render: 2,
    render: 3,
    redirect: 2,
    text: 2
  ]
]
