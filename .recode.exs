# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# FlyingDdns Recode Configuration
# ===============================================================================
#
# This file configures Recode, a code quality tool for Elixir that helps enforce
# coding standards and best practices. Recode analyzes the codebase and can
# automatically fix certain issues.
#
# The configuration includes:
# - Version specification for compatibility
# - Autocorrect settings (whether to automatically fix issues)
# - Dry run mode (preview changes without applying them)
# - Verbosity and output formatting options
# - Input file patterns to analyze
# - Task configurations for specific code quality checks
#
# Recode complements other code quality tools like Credo and the Elixir formatter
# by focusing on structural code improvements and enforcing project-specific
# conventions.
#
# For more information on Recode configuration, see:
# https://hexdocs.pm/recode/readme.html
# ===============================================================================

[
  version: "0.7.3",
  # Can also be set/reset with `--autocorrect`/`--no-autocorrect`.
  autocorrect: false,
  # With "--dry" no changes will be written to the files.
  # Can also be set/reset with `--dry`/`--no-dry`.
  # If dry is true then verbose is also active.
  dry: true,
  # Enables or disables color in the output.
  color: true,
  # Can also be set/reset with `--verbose`/`--no-verbose`.
  verbose: true,
  # Can be overwritten by calling `mix recode "lib/**/*.ex"`.
  inputs: ["{mix,.formatter}.exs", "{apps,config,lib,test}/**/*.{ex,exs}"],
  formatters: [Recode.CLIFormatter],
  tasks: [
    # Tasks could be added by a tuple of the tasks module name and an options
    # keyword list. A task can be deactivated by `active: false`. The execution of
    # a deactivated task can be forced by calling `mix recode --task ModuleName`.
    {Recode.Task.AliasExpansion, []},
    {Recode.Task.AliasOrder, []},
    {Recode.Task.Dbg, [autocorrect: false]},
    {Recode.Task.EnforceLineLength, [active: false]},
    {Recode.Task.FilterCount, []},
    {Recode.Task.IOInspect, [autocorrect: false]},
    {Recode.Task.LocalsWithoutParens,
     [
       active: false,
       config: [
         locals_without_parens: [
           # Add Phoenix controller functions
           render: 2,
           render: 3,
           redirect: 2,
           text: 2
         ]
       ]
     ]},
    {Recode.Task.Moduledoc, []},
    {Recode.Task.Nesting, []},
    {Recode.Task.PipeFunOne, []},
    {Recode.Task.SinglePipe, []},
    {Recode.Task.Specs, [exclude: ["test/**/*.{ex,exs}", "mix.exs"], config: [only: :visible]]},
    {Recode.Task.TagFIXME, [exit_code: 2]},
    {Recode.Task.TagTODO, [exit_code: 4]},
    {Recode.Task.TestFileExt, []},
    {Recode.Task.UnnecessaryIfUnless, []},
    {Recode.Task.UnusedVariable, [active: false]}
  ]
]
