# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdns.MixProject do
  @moduledoc """
  Mix project configuration for FlyingDdns.

  This module defines the project configuration, dependencies, and build settings
  for the FlyingDdns application. It includes configuration for compilation,
  testing, and deployment.

  ## Key Configuration Areas

  * **Project Settings**: Basic project metadata including name, version, and Elixir version requirements
  * **Build Configuration**: Compilation paths, environment-specific settings, and start behavior
  * **Dependencies**: External libraries and tools required by the application
  * **Testing Tools**: ExUnit configuration, ExCoveralls for code coverage, and Dialyzer for static analysis
  * **Aliases**: Custom mix task combinations for streamlined development workflows
  * **Release Configuration**: Settings for creating deployable releases

  ## Development Workflow

  The project supports various development workflows through mix tasks:
  * `mix test` - Run the test suite
  * `mix coveralls` - Generate code coverage reports
  * `mix dialyzer` - Perform static code analysis
  * `mix format` - Format code according to style guidelines
  * `mix docs` - Generate documentation
  * `mix release` - Create a deployable release

  ## Deployment

  The project is configured for deployment using Elixir releases, which package
  the application with its dependencies and the Erlang VM for standalone execution.
  """
  use Mix.Project

  def project do
    [
      app: :flying_ddns,
      version: "0.0.0",
      elixir: "~> 1.16",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test,
        "coveralls.check": :test
      ],
      test: [timeout: 10000],
      dialyzer: [
        plt_file: {:no_warn, "priv/plts/dialyzer.plt"},
        ignore_warnings: ".dialyzer_ignore.exs"
      ]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {FlyingDdns.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:bcrypt_elixir, "== 3.2.1"},
      {:castore, "== 1.0.12"},
      {:certifi, "== 2.14.0"},
      {:combine, "== 0.10.0"},
      {:comeonin, "== 5.5.1"},
      {:cowboy_telemetry, "== 0.4.0"},
      {:cowboy, "== 2.13.0"},
      {:cowlib, "== 2.14.0"},
      {:credo, "== 1.7.11", only: [:dev, :test], runtime: false},
      {:db_connection, "== 2.7.0"},
      {:decimal, "== 2.3.0"},
      {:dialyxir, "== 1.4.5", only: [:dev, :test], runtime: false},
      {:ecto_sql, "== 3.12.1"},
      {:ecto, "== 3.12.5"},
      {:elixir_make, "== 0.9.0"},
      {:esbuild, "== 0.9.0", runtime: Mix.env() == :dev},
      {:excoveralls, "== 0.18.5", only: :test},
      {:expo, "== 1.1.0"},
      {:finch, "== 0.19.0"},
      {:floki, "== 0.37.0", only: :test},
      {:gen_smtp, "== 1.2.0"},
      {:gettext, "== 0.26.2"},
      {:hackney, "== 1.23.0"},
      {:hpax, "== 1.0.2"},
      {:idna, "== 6.1.1"},
      {:jason, "== 1.4.4"},
      {:metrics, "== 1.0.1"},
      {:mime, "== 2.0.6"},
      {:mimerl, "== 1.3.0"},
      {:mint, "== 1.7.1"},
      {:mix_audit, "== 2.1.4", only: [:dev, :test], runtime: false},
      {:mock, "== 0.3.9", only: :test},
      # {:net_address, "== 0.3.1"},
      {:net_address, git: "https://github.com/xoro/net_address.git"},
      {:nimble_options, "== 1.1.1"},
      {:nimble_pool, "== 1.1.0"},
      {:parse_trans, "== 3.4.1"},
      {:phoenix_ecto, "== 4.6.3"},
      {:phoenix_html, "== 4.2.1"},
      {:phoenix_live_dashboard, "== 0.8.6"},
      {:phoenix_live_reload, "== 1.5.3", only: :dev},
      {:phoenix_live_view, "== 1.0.5"},
      {:phoenix_pubsub, "== 2.1.3"},
      {:phoenix_template, "== 1.0.4"},
      {:phoenix, "== 1.7.20"},
      {:plug_cowboy, "== 2.7.3"},
      {:plug, "== 1.16.1"},
      {:postgrex, "== 0.20.0"},
      {:ranch, "== 2.2.0"},
      {:recode, "== 0.7.3", only: :dev},
      {:remote_ip, "== 1.2.0"},
      {:sobelow, "== 0.13.0", only: [:dev, :test], runtime: false},
      {:ssl_verify_fun, "== 1.1.7"},
      {:swoosh, "== 1.18.1"},
      {:tailwind, "== 0.3.1", runtime: Mix.env() == :dev},
      {:telemetry_metrics, "== 1.1.0"},
      {:telemetry_poller, "== 1.1.0"},
      {:telemetry, "== 1.3.0"},
      {:tesla, "== 1.14.1"},
      {:unicode_util_compat, "== 0.7.0"},
      {:websock, "== 0.5.3"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to install project dependencies and perform other setup tasks, run:
  #
  #     $ mix setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      setup: ["deps.get", "ecto.setup", "assets.setup", "assets.build"],
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop --force", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate --quiet", "test"],
      "assets.setup": ["tailwind.install --if-missing", "esbuild.install --if-missing"],
      "assets.build": ["tailwind default", "esbuild default"],
      "assets.deploy": ["tailwind default --minify", "esbuild default --minify", "phx.digest"]
    ]
  end
end
