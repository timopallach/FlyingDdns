defmodule Mix.Tasks.Coveralls.Check do
  @moduledoc """
  Checks if the code coverage is above the minimum threshold.

  This task reads the minimum coverage threshold from the `coveralls.json` file
  and compares it with the actual coverage. If the coverage is below the threshold,
  the task exits with a non-zero status code.

  ## Usage

      mix coveralls.check

  You can also pass arguments to the underlying coveralls task:

      mix coveralls.check --umbrella

  To see the full coverage report, use the --verbose flag:

      mix coveralls.check --verbose
  """

  use Mix.Task

  @shortdoc "Checks if the code coverage is above the minimum threshold"
  @default_min_coverage 80

  @impl Mix.Task
  @spec run(list()) :: no_return()
  def run(args) do
    min_coverage = get_minimum_coverage()
    {output, exit_code} = run_coveralls(args)

    maybe_print_output(output, exit_code, args)
    coverage_percentage = extract_coverage_percentage(output)

    check_coverage(coverage_percentage, min_coverage, exit_code)
  end

  # Read the minimum coverage threshold from the coveralls.json file
  defp get_minimum_coverage do
    case File.read("coveralls.json") do
      {:ok, content} ->
        parse_config_content(content)

      _ ->
        safe_puts(
          "Warning: coveralls.json file not found, using default minimum coverage of #{@default_min_coverage}%"
        )

        @default_min_coverage
    end
  end

  defp parse_config_content(content) do
    case Jason.decode(content) do
      {:ok, config} -> Map.get(config, "minimum_coverage", @default_min_coverage)
      _ -> @default_min_coverage
    end
  end

  # Run the coveralls task to generate the coverage report
  defp run_coveralls(args) do
    System.cmd("mix", ["coveralls" | args], stderr_to_stdout: true)
  end

  # Determine if we should print the output
  defp maybe_print_output(output, exit_code, args) do
    show_report =
      "--umbrella" in args &&
        System.get_env("MIX_ENV") == "test" &&
        !is_nil(System.get_env("TERM"))

    if exit_code != 0 || Enum.member?(args, "--verbose") || show_report do
      safe_puts(output)
    end
  end

  # Extract the coverage percentage from the output
  defp extract_coverage_percentage(output) do
    # Try multiple regex patterns to match different output formats
    total_matches = Regex.scan(~r/\[TOTAL\]\s+(\d+\.\d+)%/, output)
    coverage_matches = Regex.scan(~r/Coverage:\s+(\d+\.\d+)%/, output)
    total_word_matches = Regex.scan(~r/Total:\s+(\d+\.\d+)%/, output)
    any_percentage_matches = Regex.scan(~r/(\d+\.\d+)%/, output)

    cond do
      # Standard format: [TOTAL] 85.0% - use the last occurrence
      total_matches != [] ->
        [_, percentage] = List.last(total_matches)
        String.to_float(percentage)

      # Alternative format: Coverage: 85.0% - use the last occurrence
      coverage_matches != [] ->
        [_, percentage] = List.last(coverage_matches)
        String.to_float(percentage)

      # Another possible format: Total: 85.0% - use the last occurrence
      total_word_matches != [] ->
        [_, percentage] = List.last(total_word_matches)
        String.to_float(percentage)

      # If we can find any percentage in the output - use the last occurrence
      any_percentage_matches != [] ->
        [_, percentage] = List.last(any_percentage_matches)
        String.to_float(percentage)

      true ->
        nil
    end
  end

  # Check if the coverage is above the minimum threshold
  @spec check_coverage(nil | float(), number(), integer()) :: no_return()
  defp check_coverage(nil, _min_coverage, exit_code) do
    safe_puts("Warning: Could not determine coverage percentage")
    exit({:shutdown, exit_code})
  end

  defp check_coverage(percentage, min_coverage, _exit_code) when percentage >= min_coverage do
    print_coverage_result(percentage, min_coverage, true)
    exit({:shutdown, 0})
  end

  defp check_coverage(percentage, min_coverage, _exit_code) do
    print_coverage_result(percentage, min_coverage, false)
    exit({:shutdown, 1})
  end

  defp print_coverage_result(percentage, min_coverage, passed) do
    safe_puts("Coverage: #{percentage}% (minimum: #{min_coverage}%)")

    if passed do
      safe_puts("Coverage threshold check passed!")
    else
      safe_puts("Coverage threshold check failed!")
    end
  end

  # Safe version of IO.puts that handles errors
  defp safe_puts(message) do
    IO.write(:stdio, "#{message}\n")
    :ok
  rescue
    _ -> :ok
  catch
    _, _ -> :ok
  end
end
