# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdns.Mailer do
  @moduledoc """
  Provides email delivery functionality using Swoosh.
  This module is used for sending emails throughout the application.
  """
  use Swoosh.Mailer, otp_app: :flying_ddns
end
