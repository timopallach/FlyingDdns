defmodule FlyingDdns.Accounts.UserAuth do
  @moduledoc """
  Authentication functions for user accounts.
  """
  import Plug.Conn
  # import Phoenix.Controller

  alias FlyingDdns.Accounts

  # Log in a user using the session
  @spec log_in_user(Plug.Conn.t(), FlyingDdns.Accounts.User.t()) :: Plug.Conn.t()
  def log_in_user(conn, user) do
    token = Accounts.generate_user_session_token(user)

    conn
    |> put_session(:user_token, token)
    |> put_session(:live_socket_id, "users_sessions:#{Base.url_encode64(token)}")
  end

  # Other auth-related functions can go here
end
