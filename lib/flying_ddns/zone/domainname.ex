# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdns.Zone.Domainname do
  use Ecto.Schema
  import Ecto.Changeset

  @moduledoc false

  @type t :: %__MODULE__{
          id: integer() | nil,
          name: String.t() | nil,
          inserted_at: NaiveDateTime.t() | nil,
          updated_at: NaiveDateTime.t() | nil
        }

  schema "domainnames" do
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(domainname, attrs) do
    domainname
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
