# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdns.Zone do
  @moduledoc """
  The Zone context.
  """

  import Ecto.Query, warn: false
  alias FlyingDdns.Repo

  alias FlyingDdns.Zone.Domainname

  @doc """
  Returns the list of domainnames.

  ## Examples

      iex> list_domainnames()
      [%Domainname{}, ...]

  """
  @spec list_domainnames() :: [Domainname.t()]
  def list_domainnames do
    Repo.all(Domainname)
  end

  @doc """
  Gets a single domainname.

  Raises `Ecto.NoResultsError` if the Domainname does not exist.

  ## Examples

      iex> get_domainname!(123)
      %Domainname{}

      iex> get_domainname!(456)
      ** (Ecto.NoResultsError)

  """
  @spec get_domainname!(integer()) :: Domainname.t()
  def get_domainname!(id), do: Repo.get!(Domainname, id)

  @doc """
  Creates a domainname.

  ## Examples

      iex> create_domainname(%{field: value})
      {:ok, %Domainname{}}

      iex> create_domainname(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_domainname(map()) :: {:ok, Domainname.t()} | {:error, Ecto.Changeset.t()}
  def create_domainname(attrs \\ %{}) do
    %Domainname{}
    |> Domainname.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a domainname.

  ## Examples

      iex> update_domainname(domainname, %{field: new_value})
      {:ok, %Domainname{}}

      iex> update_domainname(domainname, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_domainname(Domainname.t(), map()) ::
          {:ok, Domainname.t()} | {:error, Ecto.Changeset.t()}
  def update_domainname(%Domainname{} = domainname, attrs) do
    domainname
    |> Domainname.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a domainname.

  ## Examples

      iex> delete_domainname(domainname)
      {:ok, %Domainname{}}

      iex> delete_domainname(domainname)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_domainname(Domainname.t()) :: {:ok, Domainname.t()} | {:error, Ecto.Changeset.t()}
  def delete_domainname(%Domainname{} = domainname) do
    Repo.delete(domainname)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking domainname changes.

  ## Examples

      iex> change_domainname(domainname)
      %Ecto.Changeset{data: %Domainname{}}

  """
  @spec change_domainname(Domainname.t(), map()) :: Ecto.Changeset.t()
  def change_domainname(%Domainname{} = domainname, attrs \\ %{}) do
    Domainname.changeset(domainname, attrs)
  end
end
