# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdns.Repo do
  @moduledoc """
  Ecto repository for the FlyingDdns application.
  Handles database interactions and provides access to the PostgreSQL database.
  """
  use Ecto.Repo,
    otp_app: :flying_ddns,
    adapter: Ecto.Adapters.Postgres
end
