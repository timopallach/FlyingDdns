defmodule FlyingDdns.Domains do
  @moduledoc """
  The Domains context.
  """

  import Ecto.Query, warn: false
  alias FlyingDdns.Domains.Domainname
  alias FlyingDdns.Repo

  @doc """
  Creates a domainname.
  """
  @spec create_domainname(map()) :: {:ok, Domainname.t()} | {:error, Ecto.Changeset.t()}
  def create_domainname(attrs \\ %{}) do
    # Ensure any datetime values in attrs are truncated to seconds
    attrs = maybe_truncate_datetimes(attrs)

    %Domainname{}
    |> Domainname.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a domainname.
  """
  @spec update_domainname(Domainname.t(), map()) ::
          {:ok, Domainname.t()} | {:error, Ecto.Changeset.t()}
  def update_domainname(%Domainname{} = domainname, attrs) do
    # Ensure any datetime values in attrs are truncated to seconds
    attrs = maybe_truncate_datetimes(attrs)

    domainname
    |> Domainname.changeset(attrs)
    |> Repo.update()
  end

  # Helper function to truncate datetime values
  defp maybe_truncate_datetimes(attrs) do
    attrs
    |> maybe_truncate_datetime(:inserted_at)
    |> maybe_truncate_datetime(:updated_at)

    # Add other datetime fields as needed
  end

  defp maybe_truncate_datetime(attrs, key) do
    case Map.get(attrs, key) do
      %NaiveDateTime{} = dt -> Map.put(attrs, key, NaiveDateTime.truncate(dt, :second))
      _ -> attrs
    end
  end

  # Add other domain-related functions here
end
