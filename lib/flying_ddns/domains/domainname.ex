defmodule FlyingDdns.Domains.Domainname do
  @moduledoc """
  Schema for domain names in the application.
  """
  use Ecto.Schema
  import Ecto.Changeset

  @type t :: %__MODULE__{
          id: integer() | nil,
          name: String.t() | nil,
          user_id: integer() | nil,
          inserted_at: NaiveDateTime.t() | nil,
          updated_at: NaiveDateTime.t() | nil
        }

  schema "domainnames" do
    field :name, :string
    # If description doesn't exist, we need to update our fixtures and tests
    # field :description, :string
    field :user_id, :id

    timestamps()
  end

  @doc false
  def changeset(domainname, attrs) do
    domainname
    |> cast(attrs, [:name])
    |> validate_required([:name])

    # Add other validations here
  end
end
