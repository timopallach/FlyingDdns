# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdnsWeb.PageController do
  @moduledoc """
  Controller for rendering the Phoenix framework's default home page.

  This controller handles requests to the Phoenix default route and
  renders the home page template.
  """
  use FlyingDdnsWeb, :controller

  @spec home(Plug.Conn.t(), map()) :: Plug.Conn.t()
  def home(conn, _params) do
    # The home page is often custom made,
    # so skip the default app layout.
    conn
    |> put_resp_content_type("text/html")
    |> render(:home)
  end
end
