defmodule FlyingDdnsWeb.Auth.UserSessionHTML do
  @moduledoc """
  HTML rendering for user session pages.

  This module embeds templates from the user_session_html directory and
  provides HTML rendering for the UserSessionController.
  """
  use FlyingDdnsWeb, :html

  embed_templates "user_session_html/*"
end
