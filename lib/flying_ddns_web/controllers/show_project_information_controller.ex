# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdnsWeb.ShowProjectInformationController do
  @moduledoc """
  Controller for displaying project information and metadata.

  This module provides endpoints that return various pieces of information
  about the FlyingDdns project, including its status, version, and Git commit
  information. These endpoints are useful for monitoring and debugging.
  """
  use FlyingDdnsWeb, :controller
  require Logger

  @doc """
  Displays the main project information page.
  """
  @spec index(Plug.Conn.t(), map()) :: Plug.Conn.t()
  def index(conn, _params) do
    text(
      conn,
      "Welcome to FlyingDdns"
    )
  end

  @doc """
  Returns the project status.
  """
  @spec status(Plug.Conn.t(), map()) :: Plug.Conn.t()
  def status(conn, _params) do
    text(conn, "OK")
  end

  @doc """
  Returns the project version.
  """
  @spec version(Plug.Conn.t(), map()) :: Plug.Conn.t()
  def version(conn, _params) do
    version = Application.spec(:flying_ddns, :vsn)
    text(conn, "#{version}")
  end

  @doc """
  Returns the project's commit SHA.
  """
  @spec commit_sha(Plug.Conn.t(), map()) :: Plug.Conn.t()
  def commit_sha(conn, _params) do
    commit_sha = Application.get_env(:flying_ddns, :commit_sha)

    text(
      conn,
      "#{commit_sha}"
    )
  end

  @doc """
  Returns the project's short commit SHA.
  """
  @spec commit_short_sha(Plug.Conn.t(), map()) :: Plug.Conn.t()
  def commit_short_sha(conn, _params) do
    commit_short_sha = Application.get_env(:flying_ddns, :commit_short_sha)

    text(
      conn,
      "#{commit_short_sha}"
    )
  end
end
