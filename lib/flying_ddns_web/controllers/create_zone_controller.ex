# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdnsWeb.CreateZoneController do
  @moduledoc """
  Controller for creating DNS zones in PowerDNS.

  This module provides an interface to create new DNS zones through the PowerDNS API.
  It accepts zone name and nameserver parameters and communicates with the PowerDNS
  server to create the requested zone.
  """
  use FlyingDdnsWeb, :controller
  use Tesla
  require Logger

  # This is an example call using curl to create a new zone:
  # curl --request POST --data '{"name":"example.org.", "kind": "Native", "masters": [], "nameservers": ["ns.example.org."]}' -v -H 'X-API-Key: changeme' http://[::1]:8081/api/v1/servers/localhost/zones

  @doc """
  Creates a new DNS zone with the provided parameters.

  Accepts zonename and nameserver parameters and communicates with the PowerDNS API
  to create the requested zone.

  When called without the required parameters, returns an error message with
  instructions on how to use the API.
  """
  @spec index(Plug.Conn.t(), %{required(String.t()) => String.t()}) :: Plug.Conn.t()
  def index(conn, %{"zonename" => zonename, "nameserver1" => nameserver1} = params) do
    # Check if we have a second nameserver in the URI.
    nameserver2 = Map.get(params, "nameserver2", "")

    # Log the functions input parameter.
    Logger.debug(
      "zonename=<#{zonename}>, nameserver1=<#{nameserver1}>, nameserver2=<#{nameserver2}>"
    )

    # Defining function global variables.
    pdns_api_passwd = Application.get_env(:flying_ddns, :config)[:pdns_api_passwd]

    # Creating the nameserver list for the REST call.
    nameservers =
      if "#{nameserver2}" == "" do
        "#{nameserver1}\."
      else
        "#{nameserver1}\., #{nameserver2}\."
      end

    Logger.debug("nameservers=<#{nameservers}>")

    # The following method uses the PowerDNS API using elixir's tesla module to change the recordset.
    {:ok, response} =
      Tesla.post(
        "http://[::1]:8081/api/v1/servers/localhost/zones",
        "{\"name\": \"#{zonename}.\", \"kind\": \"Native\", \"masters\": [], \"nameservers\": [\"#{nameservers}\"]}",
        headers: [{"X-Api-Key", "#{pdns_api_passwd}"}]
      )

    Logger.debug("response.status=<#{response.status}>")
    Logger.debug("response.body=<#{response.body}>")

    ddns_return_string =
      if response.status >= 200 and response.status <= 299 do
        :OK
      else
        :NOK
      end

    # Send the http response back to the client.
    Logger.debug("ddns_return_string=<#{ddns_return_string}>")

    text(
      conn,
      "#{ddns_return_string}"
    )
  end

  @spec index(Plug.Conn.t(), map()) :: Plug.Conn.t()
  def index(conn, _params) do
    conn
    |> put_status(400)
    |> text(
      "Error, wrong parameters supplied!<br>Please see the following example url to create a new zone: <br>https://flyingddns.example.com/createZone?zonename=domain.local&nameserver1=ns.domain.local"
    )
  end
end
