# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdnsWeb.DomainnameHTML do
  @moduledoc """
  HTML rendering for domain name management pages.

  This module embeds templates from the domainname_html directory and
  provides HTML rendering for the DomainnameController. It includes
  functions for rendering domain name forms and other UI components.
  """
  use FlyingDdnsWeb, :html

  embed_templates "domainname_html/*"

  @doc """
  Renders a domainname form.
  """
  attr :changeset, Ecto.Changeset, required: true
  attr :action, :string, required: true

  def domainname_form(assigns)
end
