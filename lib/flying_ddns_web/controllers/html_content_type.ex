defmodule FlyingDdnsWeb.HtmlContentType do
  @moduledoc """
  Plug to ensure HTML content type is set for all responses.
  """
  import Plug.Conn

  @spec init(Plug.opts()) :: Plug.opts()
  def init(opts), do: opts

  @spec call(Plug.Conn.t(), Plug.opts()) :: Plug.Conn.t()
  def call(conn, _opts) do
    put_resp_content_type(conn, "text/html")
  end
end
