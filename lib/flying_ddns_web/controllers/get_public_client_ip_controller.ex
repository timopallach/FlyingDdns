# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdnsWeb.GetPublicClientIpController do
  @moduledoc """
  Controller for retrieving the client's public IP address.

  This controller provides an endpoint that returns the client's public IP address
  as plain text, which can be used for dynamic DNS updates and IP tracking.
  """
  use FlyingDdnsWeb, :controller
  require Logger
  plug RemoteIp

  # We are going to get the current public IP that we use for our connection to the internet.
  @spec index(Plug.Conn.t(), map()) :: Plug.Conn.t()
  def index(conn, _params) do
    ip = conn.remote_ip |> :inet.ntoa() |> to_string()
    Logger.debug("ip=<#{ip}>")

    text conn, "#{ip}"
  end
end
