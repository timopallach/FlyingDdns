# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdnsWeb.PageHTML do
  @moduledoc """
  HTML rendering for the Phoenix framework's default home page.

  This module embeds templates from the page_html directory and
  provides HTML rendering for the PageController.
  """
  use FlyingDdnsWeb, :html

  embed_templates "page_html/*"
end
