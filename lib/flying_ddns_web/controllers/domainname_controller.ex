# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdnsWeb.DomainnameController do
  @moduledoc """
  Controller for managing domain names in the application.

  This module provides CRUD operations for domain names, allowing users to
  create, read, update, and delete domain name records. It interacts with
  the Zone context to perform these operations.
  """
  use FlyingDdnsWeb, :controller

  alias FlyingDdns.Zone
  alias FlyingDdns.Zone.Domainname

  @spec index(Plug.Conn.t(), map()) :: Plug.Conn.t()
  def index(conn, _params) do
    domainnames = Zone.list_domainnames()
    render(conn, :index, domainnames: domainnames)
  end

  @spec new(Plug.Conn.t(), map()) :: Plug.Conn.t()
  def new(conn, _params) do
    changeset = Zone.change_domainname(%Domainname{})
    render(conn, :new, changeset: changeset)
  end

  @spec create(Plug.Conn.t(), map()) :: Plug.Conn.t()
  def create(conn, %{"domainname" => domainname_params}) do
    case Zone.create_domainname(domainname_params) do
      {:ok, domainname} ->
        conn
        |> put_flash(:info, "Domainname created successfully.")
        |> redirect(to: ~p"/domainnames/#{domainname}")

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, :new, changeset: changeset)
    end
  end

  @spec show(Plug.Conn.t(), map()) :: Plug.Conn.t()
  def show(conn, %{"id" => id}) do
    domainname = Zone.get_domainname!(id)
    render(conn, :show, domainname: domainname)
  end

  @spec edit(Plug.Conn.t(), map()) :: Plug.Conn.t()
  def edit(conn, %{"id" => id}) do
    domainname = Zone.get_domainname!(id)
    changeset = Zone.change_domainname(domainname)
    render(conn, :edit, domainname: domainname, changeset: changeset)
  end

  @spec update(Plug.Conn.t(), map()) :: Plug.Conn.t()
  def update(conn, %{"id" => id, "domainname" => domainname_params}) do
    domainname = Zone.get_domainname!(id)

    case Zone.update_domainname(domainname, domainname_params) do
      {:ok, domainname} ->
        conn
        |> put_flash(:info, "Domainname updated successfully.")
        |> redirect(to: ~p"/domainnames/#{domainname}")

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, :edit, domainname: domainname, changeset: changeset)
    end
  end

  @spec delete(Plug.Conn.t(), map()) :: Plug.Conn.t()
  def delete(conn, %{"id" => id}) do
    domainname = Zone.get_domainname!(id)
    {:ok, _domainname} = Zone.delete_domainname(domainname)

    conn
    |> put_flash(:info, "Domainname deleted successfully.")
    |> redirect(to: ~p"/domainnames")
  end
end
