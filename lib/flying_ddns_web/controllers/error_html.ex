# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdnsWeb.ErrorHTML do
  @moduledoc """
  HTML rendering for error pages in the application.

  This module provides templates and rendering logic for HTTP error responses,
  converting status codes to appropriate error messages.
  """
  use FlyingDdnsWeb, :html

  # If you want to customize your error pages,
  # uncomment the embed_templates/1 call below
  # and add pages to the error directory:
  #
  #   * lib/flying_ddns_web/controllers/error_html/404.html.heex
  #   * lib/flying_ddns_web/controllers/error_html/500.html.heex
  #
  # embed_templates "error_html/*"

  # The default is to render a plain text page based on
  # the template name. For example, "404.html" becomes
  # "Not Found".
  @spec render(String.t(), map()) :: String.t()
  def render(template, _assigns) do
    Phoenix.Controller.status_message_from_template(template)
  end
end
