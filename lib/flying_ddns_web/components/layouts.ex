# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdnsWeb.Layouts do
  @moduledoc """
  Provides layout templates for the application.
  Embeds all templates from the layouts directory.
  """
  use FlyingDdnsWeb, :html

  embed_templates "layouts/*"
end
