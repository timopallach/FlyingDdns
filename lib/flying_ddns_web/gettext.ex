# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdnsWeb.Gettext do
  @moduledoc """
  A module providing Internationalization with a gettext-based API.

  By using [Gettext](https://hexdocs.pm/gettext),
  your module gains a set of macros for translations, for example:

      import FlyingDdnsWeb.Gettext

      # Simple translation
      gettext("Here is the string to translate")

      # Plural translation
      ngettext("Here is the string to translate",
               "Here are the strings to translate",
               3)

      # Domain-based translation
      dgettext("errors", "Here is the error message to translate")

  See the [Gettext Docs](https://hexdocs.pm/gettext) for detailed usage.
  """

  use Gettext.Backend, otp_app: :flying_ddns

  # Add explicit implementations for the functions that are being called
  # but not properly exposed by the Gettext.Backend

  @doc """
  Translates a string with interpolations in the given domain.

  This is the function being called from core_components.ex's translate_error.
  """
  @spec dgettext(binary(), binary(), Keyword.t() | map()) :: binary()
  def dgettext(domain, msgid, bindings) do
    Gettext.dgettext(__MODULE__, domain, msgid, bindings)
  end

  @doc """
  Translates a plural string with interpolations in the given domain.

  This is the function being called from core_components.ex's translate_error.
  """
  @spec dngettext(binary(), binary(), binary(), non_neg_integer(), Keyword.t() | map()) ::
          binary()
  def dngettext(domain, msgid, msgid_plural, count, bindings) do
    Gettext.dngettext(__MODULE__, domain, msgid, msgid_plural, count, bindings)
  end
end
