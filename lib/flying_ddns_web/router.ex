# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdnsWeb.Router do
  @moduledoc """
  Defines the routing structure for the FlyingDdns web application.

  This module maps HTTP requests to controllers and live views, and organizes
  routes into different pipelines and scopes based on authentication requirements
  and functionality.
  """
  use FlyingDdnsWeb, :router

  import FlyingDdnsWeb.UserAuth

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, html: {FlyingDdnsWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers, %{"content-security-policy" => "default-src 'self'"}
    plug :fetch_current_user
    plug FlyingDdnsWeb.HtmlContentType
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", FlyingDdnsWeb do
    pipe_through :browser

    get "/phoenix", PageController, :home
    get "/", ShowProjectInformationController, :index
    get "/status", ShowProjectInformationController, :status
    get "/version", ShowProjectInformationController, :version
    get "/commit_sha", ShowProjectInformationController, :commit_sha
    get "/commit_short_sha", ShowProjectInformationController, :commit_short_sha
    get "/create_zone", CreateZoneController, :index
    get "/update", UpdateRrsetController, :index
    get "/ip", GetPublicClientIpController, :index
  end

  scope "/", FlyingDdnsWeb do
    pipe_through [:browser, :require_authenticated_user]

    resources "/domainnames", DomainnameController
  end

  # Other scopes may use custom stacks.
  # scope "/api", FlyingDdnsWeb do
  #   pipe_through :api
  # end

  # Enable LiveDashboard and Swoosh mailbox preview in development
  if Application.compile_env(:flying_ddns, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: FlyingDdnsWeb.Telemetry
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end

  ## Authentication routes

  scope "/auth", FlyingDdnsWeb.Auth, as: :auth do
    pipe_through [:browser, :redirect_if_user_is_authenticated]

    live_session :redirect_if_user_is_authenticated,
      on_mount: [{FlyingDdnsWeb.Auth.UserAuth, :redirect_if_user_is_authenticated}] do
      live "/users/register", UserRegistrationLive, :new
      live "/users/log_in", UserLoginLive, :new
      live "/users/reset_password", UserForgotPasswordLive, :new
      live "/users/reset_password/:token", UserResetPasswordLive, :edit
    end

    post "/users/log_in", UserSessionController, :create
  end

  scope "/auth", FlyingDdnsWeb.Auth, as: :auth do
    pipe_through [:browser, :require_authenticated_user]

    live_session :require_authenticated_user,
      on_mount: [{FlyingDdnsWeb.Auth.UserAuth, :ensure_authenticated}] do
      live "/users/settings", UserSettingsLive, :edit
      live "/users/settings/confirm_email/:token", UserSettingsLive, :confirm_email
    end
  end

  scope "/auth", FlyingDdnsWeb.Auth, as: :auth do
    pipe_through [:browser]

    delete "/users/log_out", UserSessionController, :delete

    live_session :current_user,
      on_mount: [{FlyingDdnsWeb.Auth.UserAuth, :mount_current_user}] do
      live "/users/confirm/:token", UserConfirmationLive, :edit
      live "/users/confirm", UserConfirmationInstructionsLive, :new
    end
  end
end
