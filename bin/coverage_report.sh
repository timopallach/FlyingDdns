#!/bin/sh

# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# ===============================================================================
# Coverage Report Script
# ===============================================================================
# This script runs the ExCoveralls coverage check and displays only the coverage
# report table, without all the test output.
#
# Usage:
#   ./bin/coverage_report.sh [options]
#
# Options:
#   --env=<env>      Specify the Mix environment (default: test)
#   --help           Show this help message
# ===============================================================================

# Function to display help
show_help() {
    printf "Usage: %b [options]\n" "$0"
    printf "\n"
    printf "Options:\n"
    printf "  --env=<env>      Specify the Mix environment (default: test)\n"
    printf "  --help           Show this help message\n"
    printf "\n"
    exit 0
}

# Default values
env="test"

# Parse command line arguments
while [ $# -gt 0 ]; do
    case $1 in
    --env=*)
        env="${1#*=}"
        shift
        ;;
    --help)
        show_help
        ;;
    *)
        printf "Unknown option: %b\n" "$1"
        printf "Use --help for usage information\n"
        exit 1
        ;;
    esac
done

# Check if dependencies are installed
if ! mix deps.get --only "$env" >/dev/null 2>&1; then
    printf "Installing dependencies for environment %b...\n" "$env"
    mix deps.get --only "$env" || {
        printf "Error: Failed to install dependencies.\n"
        exit 1
    }
fi

# Create a temporary file
temp_file=$(mktemp)
trap 'rm -f "$temp_file"' EXIT

# Run the coverage check and save to temporary file
if ! MIX_ENV="$env" mix coveralls >"$temp_file" 2>&1; then
    printf "Error: Failed to run coverage check.\n"
    exit 1
fi

# Extract and display the coverage report table
sed -n '/^COV    FILE/,/^\[TOTAL\]/p' "$temp_file"
