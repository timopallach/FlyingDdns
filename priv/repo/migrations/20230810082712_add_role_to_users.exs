# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdns.Repo.Migrations.AddRoleToUsers do
  use Ecto.Migration

  def change do
    alter table("users") do
      add :admin, :boolean, default: false
    end
  end
end
