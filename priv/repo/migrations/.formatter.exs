# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

[
  import_deps: [:ecto_sql],
  inputs: ["*.exs"]
]
