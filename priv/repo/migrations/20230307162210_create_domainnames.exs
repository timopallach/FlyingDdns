# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

defmodule FlyingDdns.Repo.Migrations.CreateDomainnames do
  use Ecto.Migration

  def change do
    create table(:domainnames) do
      add :name, :string

      timestamps()
    end
  end
end
