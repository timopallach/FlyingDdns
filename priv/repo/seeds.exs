# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2018 - 2025, Timo Pallach (timo@pallach.de).

# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     FlyingDdns.Repo.insert!(%FlyingDdns.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

FlyingDdns.Accounts.register_user(%{
  email: "admin@example.org",
  # gitleaks:allow
  password: "1234567890as",
  # gitleaks:allow
  password_confirmation: "1234567890as",
  admin: true
})
